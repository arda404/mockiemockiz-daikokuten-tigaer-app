package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.mockie.daikokuten.database.repositories.PrdTypes
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.listeners.TypeOnSelectedListener


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [StockTakingScopeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [StockTakingScopeFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class StockTakingScopeFragment : Fragment() {

    lateinit var viewD:View

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewD = inflater.inflate(R.layout.fragment_stock_taking_scope, container, false)
        val save = viewD.findViewById<Button>(R.id.save)

        populateSpinnerType(viewD)

        AlertHelper(context).stockTakingWarning()

        save.setOnClickListener {
            onSave()
        }

        return viewD
    }


    private fun populateSpinnerType(v: View)
    {
        val types = PrdTypes(activity).getAllTypes()
        val listSpinner = ArrayList<StringWithTag>()

        (0 until types.count()).mapTo(listSpinner) { StringWithTag(types[it].readable, types[it].type) }

        val adapter = ArrayAdapter(activity,
                R.layout.spinner_item, listSpinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        val spin = v.findViewById<Spinner>(R.id.typeSpinner)
        try{
            spin.adapter = adapter
            spin.onItemSelectedListener = TypeOnSelectedListener(activity, R.layout.spinner_item, -1)
        }catch (e: Exception){}
    }

    private fun onSave()
    {
        val typeSpinner = viewD.findViewById<Spinner>(R.id.typeSpinner)
        val sizeSpinner = viewD.findViewById<Spinner>(R.id.sizeSpinner)
        val color = viewD.findViewById<EditText>(R.id.selectedColor)

        val type = typeSpinner.getItemAtPosition(typeSpinner.selectedItemPosition) as StringWithTag
        val size = sizeSpinner.getItemAtPosition(sizeSpinner.selectedItemPosition) as StringWithTag

        val someFragment = StockTakingFragment()

        val bdl = Bundle(3)
        bdl.putString("type", type.tag)
        bdl.putString("size", size.tag)
        bdl.putString("color", color.text.toString())
        someFragment.arguments = bdl

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.inventoryContainer, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment StockTakingScopeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                StockTakingScopeFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
