package com.mockie.daikokuten

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.mockie.daikokuten.helpers.AlertHelper
import kotlinx.android.synthetic.main.activity_inventory.*
import kotlinx.android.synthetic.main.activity_new_main.*
import kotlinx.android.synthetic.main.app_bar_new_main.*

class InventoryActivity :
        BaseNavigationDrawer(),
        PrintBarcodeFragment.OnFragmentInteractionListener,
        StockerFragment.OnFragmentInteractionListener,
        StockTakingFragment.OnFragmentInteractionListener,
        MovingStock.OnFragmentInteractionListener,
        BarcodeForm.OnFragmentInteractionListener,
        BarcodeToPrint.OnFragmentInteractionListener,
        StockTakingScopeFragment.OnFragmentInteractionListener
{
    override var contentV = R.layout.activity_new_inventory

    override fun onFragmentInteraction(uri: Uri) {

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {

            R.id.inventory_navigation_home -> {
                startActivity(Intent(this, NewMainActivity::class.java))
                finish()
                return@OnNavigationItemSelectedListener true
            }

            R.id.inventory_navigation_print_barcode -> {
                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment = BarcodeToPrint()
                fragmentTransaction.replace(R.id.inventoryContainer, fragment, "barcode_to_print")
                fragmentTransaction.commit()

                return@OnNavigationItemSelectedListener true
            }

            R.id.inventory_navigation_stocker -> {
                addStock()
                return@OnNavigationItemSelectedListener true
            }

            R.id.inventory_navigation_move_item -> {
                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment = MovingStock()
                fragmentTransaction.replace(R.id.inventoryContainer, fragment, "move_stocker_fragment")
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }

            R.id.inventory_navigation_stock_opname -> {
                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment = StockTakingScopeFragment()
                fragmentTransaction.replace(R.id.inventoryContainer, fragment, "stock_taking_fragment")
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }

    fun addStock()
    {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragment = StockerFragment()
        fragmentTransaction.replace(R.id.inventoryContainer, fragment, "stocker_fragment")
        fragmentTransaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_inventory)

        AlertHelper(this).alertLocation()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (intent.extras != null)
        {
            val intentFragment = intent.extras!!.getString("go_to_fragment")

            when (intentFragment) {

                "add_stock" -> {
                    navigation.selectedItemId = R.id.inventory_navigation_stocker
                }

                "stock_taking" -> {
                    navigation.selectedItemId = R.id.inventory_navigation_stock_opname
                }

                "move_stock" -> {
                    navigation.selectedItemId = R.id.inventory_navigation_move_item
                }

                "print_barcode" -> {
                    navigation.selectedItemId = R.id.inventory_navigation_print_barcode
                }

            }
        }

    }
}
