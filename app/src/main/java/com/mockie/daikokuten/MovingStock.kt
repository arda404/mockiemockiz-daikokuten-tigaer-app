package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.database.repositories.Inventory as InventoryRepository
import com.mockie.daikokuten.database.repositories.User
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.helpers.DraftHelper
import com.mockie.daikokuten.database.repositories.MovedInventory as MI
import com.mockie.daikokuten.listeners.RecyclerItemTouchHelper
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.sync.helpers.MovedInventorySynchronizer


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MovingStock.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MovingStock.newInstance] factory method to
 * create an instance of this fragment.
 */
class MovingStock : BarcodeRecyclerBaseFragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    val pageName: String = "move_stock"

    val keyName:String = "barcode"

    lateinit var adapter: RecyclerViewBarcodeAdapter
    lateinit var newStore: Spinner
    lateinit var oldStore: TextView

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is RecyclerViewBarcodeAdapter.ViewHolder) {

            adapter.removeItem(viewHolder.adapterPosition)
            DraftHelper(context).draftBarcode(pageName, barcodeList)

            if (barcodeList.size < 1)
            {
                BarcodeList.add("", 1, "", 0,null)
            }

        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val inflaterView = inflater!!.inflate(R.layout.fragment_moving_stock, container, false)
        oldStore = inflaterView.findViewById(R.id.oldStore)
        val recyclerView = inflaterView.findViewById<RecyclerView>(R.id.recyclerView)
        val saveButton = inflaterView.findViewById<Button>(R.id.move_stock)

        val fab = inflaterView.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            onClickAdd()
        }

        setPermissionsRoles("developer")
        setPermissionsRoles("productEditor")
        checkPermission()

        newStore = inflaterView.findViewById(R.id.newStore)
        oldStore.text = User(this.context).getLoggedInUserStore()

        barcodeList = DraftHelper(context).getDraftBarcode(pageName)
        barcodeList.add(BarcodeList("", 1, "", 0,"barcode"))

        adapter = RecyclerViewBarcodeAdapter(this.context, recyclerView, barcodeList, pageName, keyName)
        recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = false
        recyclerView.layoutManager = layoutManager

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        spinnerNewStore()

        saveButton.setOnClickListener {
            moveStock()
        }

        return inflaterView
    }

    private fun onClickAdd()
    {
        // save draft
        DraftHelper(context).draftBarcode(pageName, barcodeList)

        val someFragment = BarcodeForm()

        val bdl = Bundle(2)
        bdl.putString("pageName", pageName)
        bdl.putString("keyName", keyName)
        someFragment.arguments = bdl

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.inventoryContainer, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }

    fun spinnerNewStore()
    {
        val res = this.resources
        val storesAsArray = res.getStringArray(R.array.dodol)
        val stores = storesAsArray.toMutableList()
        for (j in stores.size - 1 downTo 0) {
            if (stores[j] == oldStore.text)
            {
                stores.removeAt(j)
            }
        }
        val arrAdapter = ArrayAdapter(context, R.layout.spinner_item, stores)
        arrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        newStore.adapter = arrAdapter
    }

    private fun moveStock()
    {
        val currentStore: String? = User(this.context).getLoggedInUserStore()

        var emptyList = true
        var qty = 0

        for (a in 0 until barcodeList.size)
        {
            if (barcodeList[a].barcode != "")
            {
                emptyList = false
            }
            qty += barcodeList[a].qty.toString().toInt()
        }

        if (!emptyList && qty > 0)
        {
            val dialog = AlertHelper(context).barcodeListConfirmation(adapter, barcodeList, pageName)
            val yes = dialog.findViewById<Button>(R.id.editButton)

            yes.setOnClickListener {

                dialog.hide()
                dialog.dismiss()

                for (barcode in barcodeList) {
                    MI(context).save(
                            barcode.barcode,
                            currentStore!!,
                            newStore.selectedItem.toString(),
                            barcode.qty.toString().toInt())
                }

                for(a in 0 until barcodeList.size) {
                    adapter.removeItem(0)
                }

                DraftHelper(context).removeDraftBarcode(pageName)
                BarcodeList.add("", 10, "", 0,null)

                MovedInventorySynchronizer(this.context).syncNow()
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment StockerFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): MovingStock {
            val fragment = MovingStock()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
