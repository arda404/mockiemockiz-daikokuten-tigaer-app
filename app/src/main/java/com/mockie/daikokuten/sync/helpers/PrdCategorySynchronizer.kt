package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.PrdCategoryModel
import com.mockie.daikokuten.sync.workers.PrdCategorySyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class PrdCategorySynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        Log.d(":sync_status", "PrdCategorySynchronizer syncNow called ")

        WorkManager.getInstance().cancelAllWorkByTag(PrdCategoryModel.ONE_TIME_TAG)

        val categorySyncWork = OneTimeWorkRequest.Builder(PrdCategorySyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(PrdCategoryModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(categorySyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {

        Log.d(":sync_status", "PrdCategorySynchronizer setPeriodicSync called ")

        WorkManager.getInstance().cancelAllWorkByTag(PrdCategoryModel.PERIODIC_TAG)

        val categorySync = PeriodicWorkRequest.Builder(
                PrdCategorySyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .addTag(PrdCategoryModel.PERIODIC_TAG)
                .setConstraints(myConstraints)

        val categorySyncWork = categorySync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("prd_category_worker", ExistingPeriodicWorkPolicy.KEEP, categorySyncWork)
    }
}