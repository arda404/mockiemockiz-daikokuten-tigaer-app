package com.mockie.daikokuten.sync.models

open class StockTakingModel {

    companion object {

        val ONE_TIME_TAG = "stock_taking_once_tag"

        val PERIODIC_TAG = "stock_taking_periodic_tag"

    }

}

