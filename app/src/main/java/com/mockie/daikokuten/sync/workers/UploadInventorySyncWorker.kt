package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.database.repositories.Inventory
import com.mockie.daikokuten.sync.helpers.InventorySynchronizer
import com.mockie.daikokuten.sync.theSync.UploadInventory


class UploadInventorySyncWorker(appContext: Context, workerParams:WorkerParameters): Worker(appContext, workerParams) {

    override fun doWork(): Result {

        Log.d(":sync_status", "UploadInventorySyncWorker doWork() called")

        var ret = Result.FAILURE

        val dataInventory = Inventory(applicationContext).getDataToSync()

        if (dataInventory != null) {

            val response = UploadInventory(applicationContext).doHttp(
                    dataInventory.localCreatedAt,
                    dataInventory.id,
                    dataInventory.code,
                    dataInventory.quantity,
                    dataInventory.store
            )

            ret = UploadInventory(applicationContext).processResponse(dataInventory.id, response)

            val check = Inventory(applicationContext).getTotalRow()

            if (check > 0) {

                InventorySynchronizer(applicationContext).syncNow()

            }
        }

        return ret
    }

}