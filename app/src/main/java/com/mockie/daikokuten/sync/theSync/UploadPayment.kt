package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import com.mockie.daikokuten.api.AddPaymentDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.PaymentToSync
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class UploadPayment(val context: Context) {

    fun doHttp(
            paymentId: ArrayList<Int>,
            paymentUserId: ArrayList<Int>,
            paymentStore: ArrayList<String>,
            paymentCustomerId: ArrayList<Int>,
            paymentCustomerPhone: ArrayList<String>,
            paymentAmount: ArrayList<Int>,
            paymentMethod: ArrayList<String>
    ): Response<AddPaymentDataResponse>?
    {
        try{
            val typeAPI = RestAPI(context)

            val callResponse = typeAPI.addPayment(
                    paymentId,
                    paymentUserId,
                    paymentStore,
                    paymentCustomerId,
                    paymentCustomerPhone,
                    paymentAmount,
                    paymentMethod)

            return callResponse.execute()
        } catch (e:Exception) { return null }
    }

    fun processResponse(response: Response<AddPaymentDataResponse>?) {

        Log.d(":sync_status", "upload payment response " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: AddPaymentDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    val paymentIds = myResponse.payment_ids

                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {

                        dbHelper.use {
                            transaction {
                                for (i in 0 until paymentIds.size)
                                {
                                    PaymentToSync(context).removePaymentToSync(paymentIds[i])
                                }

                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.PaymentToSync.TABLE_NAME, myResponse.time)
                            }
                        }

                    } catch (e:Exception) {}
                }

            } else {

                Log.d(":dump", "uploadPaymentasynctask status failed" )

            }
        }

    }

}