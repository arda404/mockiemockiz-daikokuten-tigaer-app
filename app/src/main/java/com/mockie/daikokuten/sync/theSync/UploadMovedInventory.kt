package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.MovedInventoryDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.MovedInventory
import com.mockie.daikokuten.database.repositories.SyncLog
import com.mockie.daikokuten.sync.helpers.MovedInventorySynchronizer
import org.jetbrains.anko.db.transaction
import retrofit2.Response


class UploadMovedInventory (val context: Context) {

    fun doHttp(localCreatedAt:String, inventoryId:Int, code:String, quantity: Int, oldStore: String, newStore:String): Response<MovedInventoryDataResponse>?
    {
        try {

            val typeAPI = RestAPI(context)

            val callResponse = typeAPI.moveInventory(localCreatedAt, inventoryId, code, quantity, oldStore, newStore)
            return callResponse.execute()

        } catch (e:Exception) { return null }

    }


    fun processResponse(id:Int, response: Response<MovedInventoryDataResponse>?):ListenableWorker.Result
    {
        Log.d(":sync_status", "upload move inventory response " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {
                val myResponse: MovedInventoryDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {
                        dbHelper.use {
                            transaction {
                                MovedInventory(context).removeMovedInventoryById(id)
                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.MovedInventory.TABLE_NAME, myResponse.time)
                            }
                        }
                    } catch (e:Exception){}
                }

                return ListenableWorker.Result.SUCCESS
            }
        }

        return ListenableWorker.Result.FAILURE
    }

}