package com.mockie.daikokuten.sync.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.helpers.CustomerAddressSynchronizer


class CustomerAddressSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters)
{
    override fun doWork(): Result {

        val response = com.mockie.daikokuten.sync.theSync.CustomerAddress(applicationContext).doHttp()

        val returnValue = com.mockie.daikokuten.sync.theSync.CustomerAddress(applicationContext).processResponse(response)

        if (returnValue.second)
        {
            CustomerAddressSynchronizer(applicationContext).syncNow()
        }

        return returnValue.first
    }
}