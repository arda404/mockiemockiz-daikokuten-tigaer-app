package com.mockie.daikokuten.sync.asyncTask

import android.content.Context
import android.os.AsyncTask
import androidx.work.ListenableWorker
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.api.UserDataResponse
import com.mockie.daikokuten.helpers.AlertHelper
import retrofit2.Response


class CheckUserTokenAsyncTask(val context: Context): AsyncTask<Void, Void, Response<UserDataResponse>>()
{
    override fun doInBackground(vararg params: Void): Response<UserDataResponse>? {

        return try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getUser()

            callResponse.execute()
        } catch (e:Exception) {
            null
        }

    }

    override fun onPostExecute(response: Response<UserDataResponse>?) {

        val resp = com.mockie.daikokuten.sync.theSync.CheckUserToken(context).processResponse(response)

        if (resp == ListenableWorker.Result.FAILURE)
        {
            AlertHelper(context).wrongToken()
        }
    }
}