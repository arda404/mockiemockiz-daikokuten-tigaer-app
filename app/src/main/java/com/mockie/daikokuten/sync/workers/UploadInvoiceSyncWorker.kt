package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.database.repositories.InvoiceItemToSync
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.database.repositories.PaymentToSync
import com.mockie.daikokuten.database.repositories.ReturnToSync
import com.mockie.daikokuten.sync.helpers.UploadInvoiceSynchronizer
import com.mockie.daikokuten.sync.theSync.UploadInvoice


class UploadInvoiceSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters)
{

    fun doSync():ListenableWorker.Result {

        Log.d(":sync_status", "UploadInvoiceSyncWorker doWork")

        val dataInvoice = InvoiceToSync(applicationContext).getInvoiceToSync()

        if (dataInvoice != null)
        {
            val itemToSync = InvoiceItemToSync(applicationContext).getInvoiceItemToSyncByTempInvoiceId(dataInvoice.invoice_id)
            val itemCode = ArrayList<String>()
            val itemQuantity = ArrayList<String>()
            val itemPrice = ArrayList<Int>()

            for (a in 0 until itemToSync.size) {
                itemCode.add(itemToSync[a].code)
                itemQuantity.add(itemToSync[a].quantity.toString())
                itemPrice.add(itemToSync[a].price)
            }

            val returnToSync = ReturnToSync(applicationContext).getReturnByTempInvoiceId(dataInvoice.invoice_id)
            val returnLocalCreatedAt = ArrayList<String>()
            val returnId = ArrayList<Int>()
            val returnInvoiceId = ArrayList<Int>()
            val returnUserId = ArrayList<Int>()
            val returnStore = ArrayList<String>()
            val returnCustomerId = ArrayList<Int>()
            val returnCustomerPhone = ArrayList<String>()
            val returnBarcode = ArrayList<String>()
            val returnQty = ArrayList<Int>()
            val returnAmount = ArrayList<Int>()

            for (a in 0 until returnToSync.size) {
                returnLocalCreatedAt.add(returnToSync[a].localCreatedAt)
                returnId.add(returnToSync[a]._id)
                returnInvoiceId.add(returnToSync[a].realInvoiceId)
                returnUserId.add(returnToSync[a].userId)
                returnStore.add(returnToSync[a].store)
                returnCustomerId.add(returnToSync[a].customerId)
                returnCustomerPhone.add(returnToSync[a].customerPhone)
                returnBarcode.add(returnToSync[a].barcode)
                returnQty.add(returnToSync[a].quantity)
                returnAmount.add(returnToSync[a].calculatedPrice)
            }

            Log.d("shit", "id: " + returnId.toString())
            Log.d("shit", "inv id: " + returnInvoiceId.toString())
            Log.d("shit", "user id: " + returnUserId.toString())
            Log.d("shit", "store: " + returnStore.toString())
            Log.d("shit", "cus id: " + returnCustomerId.toString())
            Log.d("shit", "cus phone: " + returnCustomerPhone.toString())
            Log.d("shit", "code: " + returnBarcode.toString())
            Log.d("shit", "qty: " + returnQty.toString())
            Log.d("shit", "amount: " + returnAmount.toString())

            val paymentToSync = PaymentToSync(applicationContext).getAllPaymentNoIdByPhone(dataInvoice.customer_phone)
            val paymentId = ArrayList<Int>()
            val paymentUserId = ArrayList<Int>()
            val paymentStore = ArrayList<String>()
            val paymentCustomerId = ArrayList<Int>()
            val paymentCustomerPhone = ArrayList<String>()
            val paymentAmount = ArrayList<Int>()
            val paymentMethod = ArrayList<String>()

            for (a in 0 until paymentToSync.size)
            {
                paymentId.add(paymentToSync[a]._id)
                paymentUserId.add(paymentToSync[a].userId)
                paymentStore.add(paymentToSync[a].store)
                paymentCustomerId.add(paymentToSync[a].customerId)
                paymentCustomerPhone.add(paymentToSync[a].customerPhone)
                paymentAmount.add(paymentToSync[a].amount)
                paymentMethod.add(paymentToSync[a].method)
            }

            val response = UploadInvoice(applicationContext).doHttp(
                    dataInvoice.localCreatedAt,
                    dataInvoice.invoice_id,
                    dataInvoice.real_invoice_id,
                    dataInvoice.customer_id,
                    dataInvoice.shipping_fee,
                    dataInvoice.total_billing,
                    dataInvoice.status,
                    dataInvoice.store,
                    dataInvoice.address_id,
                    dataInvoice.note,
                    dataInvoice.user_id,
                    dataInvoice.customer_name,
                    dataInvoice.customer_phone,
                    dataInvoice.address_name,
                    dataInvoice.address_phone,
                    dataInvoice.address,
                    dataInvoice.additionalPricePerItem,
                    itemCode,
                    itemQuantity,
                    itemPrice,
                    paymentId,
                    paymentUserId,
                    paymentStore,
                    paymentCustomerId,
                    paymentCustomerPhone,
                    paymentAmount,
                    paymentMethod,
                    returnId,
                    returnLocalCreatedAt,
                    returnInvoiceId,
                    returnStore,
                    returnUserId,
                    returnCustomerId,
                    returnCustomerPhone,
                    returnBarcode,
                    returnQty,
                    returnAmount)

            UploadInvoice(applicationContext).processResponse(dataInvoice.invoice_id, response)
        }

        val totalDataToSync = InvoiceToSync(applicationContext).getTotalRow()

        if (totalDataToSync > 0)
        {
            UploadInvoiceSynchronizer(applicationContext).syncNow()
        }

        return Result.SUCCESS

    }

    override fun doWork(): Result {

        return doSync()

    }

}

