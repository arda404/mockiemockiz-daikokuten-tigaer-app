package com.mockie.daikokuten.sync.helpers

import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.messaging.FirebaseMessagingService
import android.util.Log
import org.json.JSONObject
import org.json.JSONException
import android.content.Intent
import com.mockie.daikokuten.LoginActivity
import com.mockie.daikokuten.NewMainActivity
import com.mockie.daikokuten.manager.TigaerNotificationManager


/**
 * Created by mockie on 21/12/17.
 */

class TIgaerFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        val dataSize = if (remoteMessage?.data?.size == null) 0 else remoteMessage.data.size

        if (dataSize > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage?.data.toString())
            try {
                val json = JSONObject(remoteMessage?.data.toString())
                sendPushNotification(json)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }

        }
    }

    private fun sync(type: String)
    {
        if (type == "invoice")
        {
            GetInvoiceSynchronizer(applicationContext).syncNow()
        }
        else if (type == "customer")
        {
            CustomerSynchronizer(applicationContext).syncNow()
        }
        else if (type == "price_rules")
        {
            PriceRuleSynchronizer(applicationContext).syncNow()
        }
        else if (type == "prd_type")
        {
            PrdTypeSynchronizer(applicationContext).syncNow()
        }
        else if (type == "prd_size")
        {
            PrdSizeSynchronizer(applicationContext).syncNow()
        }
        else if (type == "prd_price")
        {
            PrdPriceSynchronizer(applicationContext).syncNow()
        }
        else if (type == "inventory")
        {
            InventorySynchronizer(applicationContext).syncNow()
        }
        else if (type == "move_inventory")
        {
            MovedInventorySynchronizer(applicationContext).syncNow()
        }
        else if (type == "stock_taking")
        {
            StockTakingSynchronizer(applicationContext).syncNow()
        }
        else if (type == "customer_address")
        {
            CustomerAddressSynchronizer(applicationContext).syncNow()
        }
    }

    //this method will display the notification
    //We are passing the JSONObject that is received from
    //firebase cloud messaging
    protected fun sendPushNotification(json: JSONObject) {
        //optionally we can display the json into log

        try {

            //getting the json data
            val data = json.getJSONObject("data")

            //parsing json data
            val type = data.getString("type")
            val _data = data.getString("data")
            val title = data.getString("title")
            val message = data.getString("message")
            val imageUrl = data.getString("image")

            val mNotificationManager = TigaerNotificationManager(applicationContext)

            if (type=="sync") {
                sync(_data)
                mNotificationManager.showSyncNotification(title)
            } else {

                var intent = Intent(applicationContext, NewMainActivity::class.java)

                when(type) {
                    "relogin" -> intent = Intent(applicationContext, LoginActivity::class.java)
                }

                //if there is no image
                if (imageUrl.trim().isEmpty()) {
                    mNotificationManager.showSmallNotification(title, message, intent)
                } else {
                    mNotificationManager.showBigNotification(title, message, imageUrl, intent)
                }
            }
        } catch (e: JSONException) {
            Log.e(TAG, "Json Exception: " + e.message)
        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }

    }


    companion object {
        private val TAG = "MyFirebaseMsgService"
    }
}