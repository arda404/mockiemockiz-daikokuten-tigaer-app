package com.mockie.daikokuten.sync.models

class PrdCategoryModel {

    companion object {

        val ONE_TIME_TAG = "prdcategory_once_tag"

        val PERIODIC_TAG = "prdcategory_periodic_tag"

    }

}