package com.mockie.daikokuten.sync.workers


import android.content.Context
import androidx.work.Data
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.helpers.GetInvoiceSynchronizer
import com.mockie.daikokuten.sync.theSync.Invoice

class InvoiceSyncWorker (appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): ListenableWorker.Result {

        val response = Invoice(applicationContext).doHttp()
        val doWork = Invoice(applicationContext).processResponse(response)

        if (doWork.first.second)
        {
            GetInvoiceSynchronizer(applicationContext).syncNow()
        }

        val data = Data.Builder()
        data.putIntArray("invoice_ids", doWork.second.toIntArray())
        outputData = data.build()

        return doWork.first.first

    }
}

