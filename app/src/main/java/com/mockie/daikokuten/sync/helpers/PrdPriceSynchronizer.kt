package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.PrdPriceModel
import com.mockie.daikokuten.sync.workers.PrdPriceSyncWorker
import java.util.concurrent.TimeUnit


/**
 * Created by mockie on 17/01/18.
 */


class PrdPriceSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        WorkManager.getInstance().cancelAllWorkByTag(PrdPriceModel.ONE_TIME_TAG)
        val priceSyncWork = OneTimeWorkRequest.Builder(PrdPriceSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(PrdPriceModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(priceSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {

        WorkManager.getInstance().cancelAllWorkByTag(PrdPriceModel.PERIODIC_TAG)
        val priceSync = PeriodicWorkRequest.Builder(
                PrdPriceSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .setConstraints(myConstraints)
                .addTag(PrdPriceModel.PERIODIC_TAG)

        val priceSyncWork = priceSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("prd_price_worker", ExistingPeriodicWorkPolicy.KEEP, priceSyncWork)
    }
}