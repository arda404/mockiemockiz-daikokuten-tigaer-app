package com.mockie.daikokuten.sync.helpers

import android.content.Context
import androidx.work.*
import com.mockie.daikokuten.sync.models.PrdPriceRuleModel
import com.mockie.daikokuten.sync.workers.PrdPriceRuleSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class PriceRuleSynchronizer(val context: Context)  {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        WorkManager.getInstance().cancelAllWorkByTag(PrdPriceRuleModel.ONE_TIME_TAG)
        val priceRuleSyncWork = OneTimeWorkRequest.Builder(PrdPriceRuleSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(PrdPriceRuleModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(priceRuleSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {

        WorkManager.getInstance().cancelAllWorkByTag(PrdPriceRuleModel.PERIODIC_TAG)
        val priceRuleSync = PeriodicWorkRequest.Builder(
                PrdPriceRuleSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .setConstraints(myConstraints)
                .addTag(PrdPriceRuleModel.PERIODIC_TAG)

        val priceRuleSyncWork = priceRuleSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("prd_price_rule_worker", ExistingPeriodicWorkPolicy.KEEP, priceRuleSyncWork)
    }
}