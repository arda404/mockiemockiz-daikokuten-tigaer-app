package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.theSync.PaymentMethod
import com.mockie.daikokuten.sync.theSync.PrdPriceRule

class PaymentMethodSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters)
{

    override fun doWork(): Result {

        Log.d(":sync_status", "PaymentMethodSynchronizer doWork() called ")

        val response = PaymentMethod(applicationContext).doHttp()

        return PaymentMethod(applicationContext).processResponse(response)
    }
}

