package com.mockie.daikokuten.sync.accounts.adapters

import android.accounts.Account
import android.content.*
import android.os.Bundle
import com.mockie.daikokuten.sync.helpers.*


/**
 * Created by mockie on 19/12/17.
 */

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
class SyncInventoryAdapter : AbstractThreadedSyncAdapter {

    override fun onPerformSync(
            account: Account,
            extras: Bundle,
            authority: String,
            provider: ContentProviderClient,
            syncResult: SyncResult) {

//        InventorySynchronizer(context).syncNow()
//        MovedInventorySynchronizer(context).syncWithAsyncTask()
//        StockTakingSynchronizer(context).syncWithAsyncTask()
//        PrdTypeSynchronizer(context).syncWithAsyncTask()
//        PrdSizeSynchronizer(context).syncWithAsyncTask()
//        PrdCategorySynchronizer(context).syncWithAsyncTask()
//        PrdPriceSynchronizer(context).syncWithAsyncTask()
    }

    // Global variables
    // Define a variable to contain a content resolver instance
    internal var mContentResolver: ContentResolver

    /**
     * Set up the sync adapter
     */
    constructor(context: Context, autoInitialize: Boolean) : super(context, autoInitialize) {
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.contentResolver
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    constructor(
            context: Context,
            autoInitialize: Boolean,
            allowParallelSyncs: Boolean) : super(context, autoInitialize, allowParallelSyncs) {
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.contentResolver

    }
}