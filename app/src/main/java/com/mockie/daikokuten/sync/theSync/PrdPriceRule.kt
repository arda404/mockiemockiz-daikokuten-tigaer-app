package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.PriceRuleDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.PriceRules
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class PrdPriceRule(val context: Context) {

    fun doHttp():Response<PriceRuleDataResponse>?
    {
        try {

            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getPriceRule()

             return callResponse.execute()
        } catch (e:Exception) { return null }
    }
    
    fun processResponse(response: Response<PriceRuleDataResponse>?): ListenableWorker.Result
    {
        var returnValue = ListenableWorker.Result.FAILURE

        Log.d(":sync_status", "PrdPriceRule response " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: PriceRuleDataResponse = response.body()!!

                SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PriceRules.TABLE_NAME, myResponse.total_data)

                if (myResponse.status == "success")
                {
                    val priceRule = myResponse.data
                    val total = priceRule.size

                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {

                        dbHelper.use {
                            transaction {
                                PriceRules(context).removeAll()

                                for (i in 0 until total) {
                                    PriceRules(context).insert(
                                            priceRule[i].store,
                                            priceRule[i].start,
                                            priceRule[i].amount
                                    )
                                }

                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.PriceRules.TABLE_NAME, myResponse.time)
                                SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PriceRules.TABLE_NAME, 0)
                            }
                        }

                        returnValue = ListenableWorker.Result.SUCCESS

                    } catch (e: Exception) {

                        returnValue = ListenableWorker.Result.FAILURE

                    }
                }
            }
        }

        return returnValue
    }

}

