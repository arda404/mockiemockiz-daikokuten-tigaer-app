package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.UploadInventoryModel
import com.mockie.daikokuten.sync.workers.UploadInventorySyncWorker

/**
 * Created by mockie on 17/01/18.
 */


class  InventorySynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow():OneTimeWorkRequest
    {
        Log.d(":sync_status", "InventorySynchronizer syncNow called ")

        WorkManager.getInstance().cancelAllWorkByTag(UploadInventoryModel.ONE_TIME_TAG)
        val uploadInventorySyncWork = OneTimeWorkRequest.Builder(UploadInventorySyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(UploadInventoryModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(uploadInventorySyncWork)

        return uploadInventorySyncWork
    }
}