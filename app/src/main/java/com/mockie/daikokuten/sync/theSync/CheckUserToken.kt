package com.mockie.daikokuten.sync.theSync

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.api.UserDataResponse
import retrofit2.Response

class CheckUserToken(val context: Context)
{

    fun doHttp(): Response<UserDataResponse>?
    {
        try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getUser()

            return callResponse.execute()
        } catch (e:Exception) { return null }
    }

    fun processResponse(response: Response<UserDataResponse>?): ListenableWorker.Result
    {
        if (response != null)
        {
            if (response.isSuccessful) {
                val myResponse: UserDataResponse = response.body()!!

                if (myResponse.status != "success")
                {
                    return ListenableWorker.Result.FAILURE
                }
            }
        }

        return ListenableWorker.Result.SUCCESS
    }

}