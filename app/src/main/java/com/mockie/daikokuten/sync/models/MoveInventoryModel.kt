package com.mockie.daikokuten.sync.models

open class MoveInventoryModel {

    companion object {

        val ONE_TIME_TAG = "move_inventory_once_tag"

        val PERIODIC_TAG = "move_inventory_periodic_tag"

    }

}

