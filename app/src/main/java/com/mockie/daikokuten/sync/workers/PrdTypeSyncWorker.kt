package com.mockie.daikokuten.sync.workers


import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.helpers.PrdTypeSynchronizer
import com.mockie.daikokuten.sync.theSync.PrdType

class PrdTypeSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        val response = PrdType(applicationContext).doHttp()

        val returnValue = PrdType(applicationContext).processResponse(response)

        if(returnValue.second)
        {
            PrdTypeSynchronizer(applicationContext).syncNow()
        }

        return returnValue.first
    }

}

