package com.mockie.daikokuten.sync.models

class UploadInvoiceModel {

    companion object {

        val ONE_TIME_TAG = "upload_invoice_once_tag"

        val PERIODIC_TAG = "upload_invoice_periodic_tag"

        val PERIODIC_UNLOCK_TAG = "upload_invoice_periodic_unlock_tag"

    }

}