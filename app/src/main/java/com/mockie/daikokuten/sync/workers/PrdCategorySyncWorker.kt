package com.mockie.daikokuten.sync.workers


import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.helpers.PrdCategorySynchronizer
import com.mockie.daikokuten.sync.theSync.PrdCategory

class PrdCategorySyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        Log.d(":sync_status", "PrdCategorySynchronizer doWork called ")

        val response = PrdCategory(applicationContext).doHttp()

        val returnValue = PrdCategory(applicationContext).processResponse(response)

        if (returnValue.second)
        {
            PrdCategorySynchronizer(applicationContext).syncNow()
        }

        return returnValue.first

    }

}