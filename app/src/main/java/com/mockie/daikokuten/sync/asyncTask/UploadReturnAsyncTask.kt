package com.mockie.daikokuten.sync.asyncTask

import android.content.Context
import android.os.AsyncTask
import com.mockie.daikokuten.api.AddReturnDataResponse
import com.mockie.daikokuten.sync.theSync.UploadReturn
import retrofit2.Response

class UploadReturnAsyncTask(var context:Context,
                            val returnLocalCreatedAt: ArrayList<String>,
                            val returnId: ArrayList<Int>,
                            val returnInvoiceId: ArrayList<Int>,
                            val returnUserId: ArrayList<Int>,
                            val returnStore: ArrayList<String>,
                            val returnCustomerId: ArrayList<Int>,
                            val returnCustomerPhone: ArrayList<String>,
                            val returnBarcode: ArrayList<String>,
                            val returnQty: ArrayList<Int>,
                            val returnAmount: ArrayList<Int>
                        ): AsyncTask<Void, Void, Response<AddReturnDataResponse>>()  {


    override fun doInBackground(vararg params: Void): Response<AddReturnDataResponse>? {

        return UploadReturn(context).doHttp(
                returnLocalCreatedAt,
                returnId,
                returnInvoiceId,
                returnUserId,
                returnStore,
                returnCustomerId,
                returnCustomerPhone,
                returnBarcode,
                returnQty,
                returnAmount
            )
    }

    override fun onPostExecute(response: Response<AddReturnDataResponse>?) {
        super.onPostExecute(response)
        UploadReturn(context).processResponse(response)

    }

}