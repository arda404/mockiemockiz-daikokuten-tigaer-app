package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.theSync.CheckUserToken


class CheckUserToken(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        Log.d(":dump dowork", "CheckUserToken called")

        val response = CheckUserToken(applicationContext).doHttp()

        return CheckUserToken(applicationContext).processResponse(response)

    }

}