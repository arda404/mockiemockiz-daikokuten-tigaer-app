package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.CustomerDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.Customer
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class Customer(val context: Context) {

    fun doHttp(): Response<CustomerDataResponse>?
    {
        return try {

            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getCustomer()

            callResponse.execute()

        } catch (e:Exception)
        {
            null
        }
    }

    fun processResponse(response: Response<CustomerDataResponse>?): Pair<ListenableWorker.Result, Boolean>
    {
        var retry = false

        var returnValue = ListenableWorker.Result.FAILURE

        Log.d(":sync_status", "customer " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: CustomerDataResponse = response.body()!!

                if (myResponse.status == "success") {

                    val customers = myResponse.data
                    val total = customers.size

                    Log.d("size customer", total.toString())

                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {

                        dbHelper.use {

                            transaction {

                                if (myResponse.current_page == 1)
                                {
                                    SyncLog(context).updateTableLastSyncTemp(com.mockie.daikokuten.database.models.Customer.TABLE_NAME, myResponse.until_time)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.Customer.TABLE_NAME, myResponse.total)
                                }

                                for (i in 0 until total) {

                                    val checkCustomer = Customer(context).getCustomerById(customers[i].id)

                                    if (checkCustomer == null)
                                    {
                                        Customer(context).insert(
                                                customers[i].id,
                                                customers[i].name,
                                                customers[i].phone,
                                                customers[i].balance,
                                                customers[i].created_at,
                                                customers[i].updated_at
                                        )
                                    } else
                                    {
                                        Customer(context).update(
                                                customers[i].id,
                                                customers[i].name,
                                                customers[i].phone,
                                                customers[i].balance,
                                                customers[i].created_at,
                                                customers[i].updated_at
                                        )
                                    }
                                }

                                /**
                                 * UPDATE PROGRESS IN TABLE INFORMATION
                                 */
                                SyncLog(context).updateTableProgress(com.mockie.daikokuten.database.models.Customer.TABLE_NAME, myResponse.progress)

                                /**
                                 * UPDATE NEXT PAGE IN TABLE INFORMATION IF CURRENT PAGE IS NOT THE LAST PAGE AND DO SYNC AGAIN IN 5 SECONDS
                                 */
                                if (myResponse.current_page < myResponse.last_page)
                                {

                                    val nextPage = myResponse.current_page + 1
                                    SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.Customer.TABLE_NAME, nextPage)

                                    retry = true

                                } else {

                                    val syncLogInfo = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.Customer.TABLE_NAME)

                                    var lastSync:String? = ""
                                    if (syncLogInfo != null)
                                    {
                                        lastSync = syncLogInfo.last_sync_temp
                                    }

                                    SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.Customer.TABLE_NAME, lastSync)
                                    SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.Customer.TABLE_NAME, 1)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.Customer.TABLE_NAME, 0)
                                }

                            }

                        }

                        returnValue = ListenableWorker.Result.SUCCESS

                    } catch (e: Exception) {

                        returnValue = ListenableWorker.Result.FAILURE
//                    throw e

                    }
                }
            }
        }

        return Pair(returnValue, retry)
    }

}