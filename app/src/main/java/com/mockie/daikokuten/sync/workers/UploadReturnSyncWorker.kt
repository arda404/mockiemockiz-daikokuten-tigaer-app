package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.database.repositories.ReturnToSync
import com.mockie.daikokuten.sync.theSync.UploadReturn

class UploadReturnSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters)
{

    fun doSync():Result {

        Log.d(":dump dowork", "UploadReturnSyncWorker")

        val dataReturn = ReturnToSync(applicationContext).getAllReturnWithRealInvoice()

        val returnLocalCreatedAt = ArrayList<String>()
        val returnId = ArrayList<Int>()
        val returnInvoiceId = ArrayList<Int>()
        val returnUserId = ArrayList<Int>()
        val returnStore = ArrayList<String>()
        val returnCustomerId = ArrayList<Int>()
        val returnCustomerPhone = ArrayList<String>()
        val returnBarcode = ArrayList<String>()
        val returnQty = ArrayList<Int>()
        val returnAmount = ArrayList<Int>()

        for (a in 0 until dataReturn.size)
        {
            Log.d("popo", "id" + dataReturn[a]._id + ", store:" + dataReturn[a].store+ ", userid:" + dataReturn[a].userId)
            Log.d("popo", "customer id" + dataReturn[a].customerId + ", cus phone:" + dataReturn[a].customerPhone)
            Log.d("popo", "barcode" + dataReturn[a].barcode + ", qty:" + dataReturn[a].quantity + ", amount" + dataReturn[a].calculatedPrice)

            returnLocalCreatedAt.add(dataReturn[a].localCreatedAt)
            returnId.add(dataReturn[a]._id)
            returnInvoiceId.add(dataReturn[a].realInvoiceId)
            returnUserId.add(dataReturn[a].userId)
            returnStore.add(dataReturn[a].store)
            returnCustomerId.add(dataReturn[a].customerId)
            returnCustomerPhone.add(dataReturn[a].customerPhone)
            returnBarcode.add(dataReturn[a].barcode)
            returnQty.add(dataReturn[a].quantity)
            returnAmount.add(dataReturn[a].calculatedPrice)
        }

        val response = UploadReturn(applicationContext).doHttp(
                returnLocalCreatedAt,
                returnId,
                returnInvoiceId,
                returnUserId,
                returnStore,
                returnCustomerId,
                returnCustomerPhone,
                returnBarcode,
                returnQty,
                returnAmount)

        UploadReturn(applicationContext).processResponse(response)

        return Result.SUCCESS

    }

    override fun doWork(): Result {

        return doSync()

    }

}

