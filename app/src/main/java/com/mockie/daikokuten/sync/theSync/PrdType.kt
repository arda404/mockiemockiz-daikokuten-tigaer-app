package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.ProductTypeDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.PrdTypes
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response


class PrdType(val context: Context)
{

    fun doHttp():Response<ProductTypeDataResponse>?
    {
        try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getType()

            return  callResponse.execute()
        } catch (e:Exception) { return null }
    }

    fun processResponse(response: Response<ProductTypeDataResponse>?): Pair<ListenableWorker.Result, Boolean>
    {
        var retry = false
        var returnValue = ListenableWorker.Result.FAILURE

        Log.d(":sync_status", "PrdType response " + response.toString() )

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: ProductTypeDataResponse = response.body()!!
                Log.d("fuck prdtype", myResponse.current_page.toString() + " | " + myResponse.last_page)
                if (myResponse.status == "success")
                {
                    val types = myResponse.data
                    val total = types.size

                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {

                        dbHelper.use {
                            transaction {
                                /**
                                 * update table log information if the current page is the first page
                                 */
                                if (myResponse.current_page == 1)
                                {
                                    SyncLog(context).updateTableLastSyncTemp(com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME, myResponse.until_time)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME, myResponse.total)
                                }

                                /**
                                 * Do loop through the items. insert if not existed and update otherwise.
                                 */
                                for (i in 0 until total) {

                                    val type = PrdTypes(context).getTypeById(types[i].productType)

                                    if (type == null)
                                    {
                                        PrdTypes(context).insert(
                                                types[i].productType,
                                                types[i].readable,
                                                types[i].patterned,
                                                types[i].sorter,
                                                types[i].appactive,
                                                types[i].categories,
                                                types[i].updated_at
                                        )
                                    } else
                                    {
                                        PrdTypes(context).update(
                                                types[i].productType,
                                                types[i].readable,
                                                types[i].patterned,
                                                types[i].sorter,
                                                types[i].appactive,
                                                types[i].categories,
                                                types[i].updated_at
                                        )
                                    }
                                }

                                /**
                                 * UPDATE PROGRESS IN TABLE INFORMATION
                                 */
                                SyncLog(context).updateTableProgress(com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME, myResponse.progress)

                                /**
                                 * UPDATE NEXT PAGE IN TABLE INFORMATION IF CURRENT PAGE IS NOT THE LAST PAGE AND DO SYNC AGAIN IN 5 SECONDS
                                 */
                                if (myResponse.current_page < myResponse.last_page) {

                                    val nextPage = myResponse.current_page + 1
                                    SyncLog(context).updateTableNextPage(
                                            com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME, nextPage)

                                    retry = true

                                } else {

                                    val syncLogInfo = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME)

                                    var lastSync:String? = ""

                                    if (syncLogInfo != null)
                                    {
                                        lastSync = syncLogInfo.last_sync_temp
                                    }

                                    SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME, lastSync)
                                    SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME, 1)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PrdTypes.TABLE_NAME, 0)
                                }
                            }
                        }

                        returnValue = ListenableWorker.Result.SUCCESS

                    } catch (e: Exception) {

                        returnValue = ListenableWorker.Result.FAILURE

                    }
                }
            }
        }

        return Pair(returnValue, retry)
    }
}