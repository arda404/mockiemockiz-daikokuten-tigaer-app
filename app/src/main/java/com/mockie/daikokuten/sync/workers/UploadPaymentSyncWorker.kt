package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.database.repositories.InvoiceItemToSync
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.database.repositories.PaymentToSync
import com.mockie.daikokuten.sync.helpers.UploadInvoiceSynchronizer
import com.mockie.daikokuten.sync.theSync.UploadInvoice
import com.mockie.daikokuten.sync.theSync.UploadPayment


class UploadPaymentSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters)
{

    fun doSync():Result {

        Log.d(":dump dowork", "UploadInvoiceSyncWorker")

        val dataPayment = PaymentToSync(applicationContext).getAllPaymentWithCustomerId()

        val paymentId = ArrayList<Int>()
        val paymentUserId = ArrayList<Int>()
        val paymentStore = ArrayList<String>()
        val paymentCustomerId = ArrayList<Int>()
        val paymentCustomerPhone = ArrayList<String>()
        val paymentAmount = ArrayList<Int>()
        val paymentMethod = ArrayList<String>()

        for (a in 0 until dataPayment.size)
        {
            paymentId.add(dataPayment[a]._id)
            paymentUserId.add(dataPayment[a].userId)
            paymentStore.add(dataPayment[a].store)
            paymentCustomerId.add(dataPayment[a].customerId)
            paymentCustomerPhone.add(dataPayment[a].customerPhone)
            paymentAmount.add(dataPayment[a].amount)
            paymentMethod.add(dataPayment[a].method)
        }

        val response = UploadPayment(applicationContext).doHttp(
                paymentId,
                paymentUserId,
                paymentStore,
                paymentCustomerId,
                paymentCustomerPhone,
                paymentAmount,
                paymentMethod)

        UploadPayment(applicationContext).processResponse(response)

        return Result.SUCCESS

    }

    override fun doWork(): Result {

        return doSync()

    }

}

