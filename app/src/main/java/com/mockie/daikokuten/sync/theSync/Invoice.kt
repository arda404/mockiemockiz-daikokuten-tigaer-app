package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.InvoiceSyncDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.*
import com.mockie.daikokuten.database.repositories.Customer
import com.mockie.daikokuten.database.repositories.CustomerAddress
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class Invoice(val context: Context)
{
    fun doHttp(): Response<InvoiceSyncDataResponse>?
    {
        return try{
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getInvoices()

            callResponse.execute()

        } catch (e:Exception){
            null
        }
    }

    fun processResponse(response: Response<InvoiceSyncDataResponse>?): Pair<Pair<ListenableWorker.Result, Boolean>, ArrayList<Int>>
    {
        var retry = false
        var returnValue = ListenableWorker.Result.FAILURE
        val invoiceIdsDataOutPut = arrayListOf<Int>()

        Log.d(":sync_status", "GetInvoice response " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: InvoiceSyncDataResponse = response.body()!!
                if (myResponse.status == "success")
                {
                    val dataR = myResponse.data
                    val total = dataR.size

                    try {

                        val dbHelper = BaseRepository(context).getDbInstance()

                        dbHelper.use {
                            try {
                                transaction {

                                    if (myResponse.current_page == 1)
                                    {
                                        SyncLog(context).updateTableLastSyncTemp(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME, myResponse.until_time)
                                        SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME, myResponse.total)
                                    }

                                    for (i in 0 until total) {

                                        val theInvoice = dataR[i].invoice
                                        val theItem = dataR[i].item
                                        val theAddress = dataR[i].address
                                        val theCustomer = dataR[i].customer

                                        val invoiceId = theInvoice.id
                                        val checkInvoice = Invoices(context).getInvoiceById(invoiceId)

                                        invoiceIdsDataOutPut.add(theInvoice.id)

                                        if (checkInvoice == null)
                                        {
                                            Invoices(context).insert(
                                                    theInvoice.id,
                                                    theInvoice.customer_id,
                                                    theInvoice.address_id,
                                                    theInvoice.shipping_charge,
                                                    theInvoice.total_billing,
                                                    theInvoice.status,
                                                    theInvoice.store,
                                                    theInvoice.note,
                                                    theInvoice.user_id,
                                                    theInvoice.created_at,
                                                    theInvoice.updated_at,
                                                    theInvoice.additional_charge_per_item,
                                                    theInvoice.meta,
                                                    theItem
                                            )
                                        }
                                        else
                                        {
                                            Invoices(context).update(
                                                    theInvoice.id,
                                                    theInvoice.customer_id,
                                                    theInvoice.address_id,
                                                    theInvoice.shipping_charge,
                                                    theInvoice.total_billing,
                                                    theInvoice.status,
                                                    theInvoice.store,
                                                    theInvoice.note,
                                                    theInvoice.user_id,
                                                    theInvoice.created_at,
                                                    theInvoice.updated_at,
                                                    theInvoice.additional_charge_per_item,
                                                    theInvoice.meta,
                                                    theItem
                                            )
                                        }

                                        if(theAddress != null)
                                        {
                                            val checkCustomerAddress = CustomerAddress(context).getCustomerAddressById(theAddress.id)

                                            if (checkCustomerAddress == null)
                                            {
                                                CustomerAddress(context).insert(
                                                        theAddress.id,
                                                        theAddress.customer_id,
                                                        theAddress.name,
                                                        theAddress.phone,
                                                        theAddress.address,
                                                        theAddress.created_at,
                                                        theAddress.updated_at
                                                )
                                            } else
                                            {
                                                CustomerAddress(context).update(
                                                        theAddress.id,
                                                        theAddress.customer_id,
                                                        theAddress.name,
                                                        theAddress.phone,
                                                        theAddress.address,
                                                        theAddress.created_at,
                                                        theAddress.updated_at
                                                )
                                            }
                                        }

                                        val checkCustomer = Customer(context).getCustomerById(theCustomer.id)
                                        if (checkCustomer == null) {
                                            Customer(context).insert(
                                                    theCustomer.id,
                                                    theCustomer.name,
                                                    theCustomer.phone,
                                                    theCustomer.balance,
                                                    theCustomer.created_at,
                                                    theCustomer.updated_at
                                            )
                                        } else {
                                            Customer(context).update(
                                                    theCustomer.id,
                                                    theCustomer.name,
                                                    theCustomer.phone,
                                                    theCustomer.balance,
                                                    theCustomer.created_at,
                                                    theCustomer.updated_at
                                            )
                                        }

                                        if (theInvoice.status == 1000)
                                        {
                                            Invoices(context).removeInvoiceByInvoiceId(theInvoice.id)
                                        }

                                    }

                                    /**
                                     * UPDATE PROGRESS IN TABLE INFORMATION
                                     */
                                    SyncLog(context).updateTableProgress(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME, myResponse.progress)

                                    if (myResponse.current_page < myResponse.last_page)
                                    {
                                        val nextPage = myResponse.current_page + 1
                                        SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME, nextPage)

                                        retry = true

                                    } else {

                                        val syncLogInfo = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME)

                                        var lastSync:String? = ""
                                        if (syncLogInfo != null)
                                        {
                                            lastSync = syncLogInfo.last_sync_temp
                                        }

                                        SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME, lastSync)
                                        SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME, 1)
                                        SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME, 0)
                                    }

                                }
                            } catch (e:Exception)
                            {
                                Log.d("dodol err", e.toString())
                            }
                        }

                        returnValue = ListenableWorker.Result.SUCCESS

                    } catch (e: Exception) {

                        returnValue = ListenableWorker.Result.FAILURE

                    }
                }
            }
        }

        return Pair(Pair(returnValue, retry), invoiceIdsDataOutPut)
    }
}