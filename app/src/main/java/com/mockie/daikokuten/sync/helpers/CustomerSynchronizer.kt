package com.mockie.daikokuten.sync.helpers


import android.content.Context
import androidx.work.*
import com.mockie.daikokuten.sync.models.CustomerModel
import com.mockie.daikokuten.sync.workers.CustomerSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class CustomerSynchronizer(val context: Context) {

    var myConstraints: Constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        WorkManager.getInstance().cancelAllWorkByTag(CustomerModel.ONE_TIME_TAG)
        val customerSyncWork = OneTimeWorkRequest.Builder(CustomerSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(CustomerModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(customerSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {

        WorkManager.getInstance().cancelAllWorkByTag(CustomerModel.PERIODIC_TAG)
        val customerSync = PeriodicWorkRequest.Builder(
                CustomerSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .addTag(CustomerModel.PERIODIC_TAG)
                .setConstraints(myConstraints)

        val customerSyncWork = customerSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("customer_worker", ExistingPeriodicWorkPolicy.KEEP, customerSyncWork)
    }
}