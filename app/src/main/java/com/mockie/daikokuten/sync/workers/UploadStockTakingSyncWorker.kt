package com.mockie.daikokuten.sync.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.database.repositories.StockTaking
import com.mockie.daikokuten.sync.helpers.StockTakingSynchronizer
import com.mockie.daikokuten.sync.theSync.UploadStockTaking

class UploadStockTakingSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        val dataStockTaking = StockTaking(applicationContext).getDataToSync()

        if (dataStockTaking != null)
        {
            val response = UploadStockTaking(applicationContext).doHttp(
                    dataStockTaking.localCreatedAt,
                    dataStockTaking.id,
                    dataStockTaking.code,
                    dataStockTaking.stock_deviation,
                    dataStockTaking.store)

            if(StockTaking(applicationContext).getCount() > 0)
            {
                StockTakingSynchronizer(applicationContext).syncNow()
            }

            return UploadStockTaking(applicationContext).processResponse(dataStockTaking.id, response)
        }

        return Result.SUCCESS
    }
}