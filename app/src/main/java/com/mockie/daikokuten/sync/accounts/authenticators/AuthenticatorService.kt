package com.mockie.daikokuten.sync.accounts.authenticators

import android.app.Service
import android.content.Intent
import android.os.IBinder


/**
 * Created by mockie on 19/12/17.
 */


/**
 * A bound Service that instantiates the authenticator
 * when started.
 */
class AuthenticatorService : Service() {

    // Instance field that stores the authenticator object
    private var mAuthenticator: Authenticator? = null

    override fun onCreate() {
        // Create a new authenticator object
        mAuthenticator = Authenticator(this)
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    override fun onBind(intent: Intent): IBinder {
        return mAuthenticator!!.iBinder
    }
}
