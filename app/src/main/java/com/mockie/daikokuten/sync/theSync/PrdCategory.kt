package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.ProductCategoryDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.PrdCategories
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class PrdCategory(val context: Context)
{

    fun doHttp(): Response<ProductCategoryDataResponse>?
    {
        try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getCategory()

            return callResponse.execute()
        } catch (e:Exception) { return null }
    }

    fun processResponse(response: Response<ProductCategoryDataResponse>?): Pair<ListenableWorker.Result, Boolean>
    {
        var retry = false

        var returnValue = ListenableWorker.Result.FAILURE

        Log.d(":sync_status", "PrdCategorySynchronizer processResponse called " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: ProductCategoryDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    val categories = myResponse.data
                    val total = categories.size

                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {

                        dbHelper.use {
                            transaction {
                                if (myResponse.current_page == 1)
                                {
                                    SyncLog(context).updateTableLastSyncTemp(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME, myResponse.until_time)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME, myResponse.total)
                                }

                                for (i in 0 until total) {

                                    val cat = PrdCategories(context).getCategoryById(categories[i].category)

                                    if (cat==null)
                                    {

                                        PrdCategories(context).insert(
                                                categories[i].category,
                                                categories[i].readable,
                                                categories[i].sort,
                                                categories[i].updated_at
                                        )

                                    } else {

                                        PrdCategories(context).update(
                                                categories[i].category,
                                                categories[i].readable,
                                                categories[i].sort,
                                                categories[i].updated_at
                                        )

                                    }

                                }

                                /**
                                 * UPDATE PROGRESS IN TABLE INFORMATION
                                 */
                                SyncLog(context).updateTableProgress(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME, myResponse.progress)

                                /**
                                 * UPDATE NEXT PAGE IN TABLE INFORMATION IF CURRENT PAGE IS NOT THE LAST PAGE AND DO SYNC AGAIN IN 5 SECONDS
                                 */
                                if (myResponse.current_page < myResponse.last_page)
                                {

                                    val nextPage = myResponse.current_page + 1
                                    SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME, nextPage)

                                    retry = true

                                } else {

                                    val syncLogInfo = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME)
                                    var lastSync:String? = ""
                                    if (syncLogInfo != null)
                                    {
                                        lastSync = syncLogInfo.last_sync_temp
                                    }

                                    SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME, lastSync)
                                    SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME, 1)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PrdCategories.TABLE_NAME, 0)
                                }
                            }
                        }

                        returnValue = ListenableWorker.Result.SUCCESS

                    } catch (e: Exception) {

                        returnValue = ListenableWorker.Result.FAILURE

                    }
                }
            }
        }

        return Pair(returnValue, retry)
    }

}