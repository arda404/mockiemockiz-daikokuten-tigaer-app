package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.PaymentModel
import com.mockie.daikokuten.sync.workers.UploadPaymentSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class UploadPaymentSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        Log.d(":dump", "UploadPaymentSyncHelper onetime called")
        WorkManager.getInstance().cancelAllWorkByTag(PaymentModel.ONE_TIME_TAG)
        val uploadPaymentSyncWork = OneTimeWorkRequest.Builder(UploadPaymentSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(PaymentModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(uploadPaymentSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {
        Log.d(":dump", "UploadPaymentSyncHelper called")

        WorkManager.getInstance().cancelAllWorkByTag(PaymentModel.PERIODIC_TAG)
        val uploadPaymentSync = PeriodicWorkRequest.Builder(
                UploadPaymentSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .addTag(PaymentModel.PERIODIC_TAG)
                .setConstraints(myConstraints)

        val uploadPaymentSyncWork = uploadPaymentSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("upload_payment_worker", ExistingPeriodicWorkPolicy.KEEP, uploadPaymentSyncWork)
    }
}