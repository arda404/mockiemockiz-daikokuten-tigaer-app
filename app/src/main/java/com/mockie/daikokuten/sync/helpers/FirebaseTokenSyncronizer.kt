package com.mockie.daikokuten.sync.helpers

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.work.*
import com.google.firebase.iid.FirebaseInstanceId
import com.mockie.daikokuten.sync.models.FireBaseTokenModel
import com.mockie.daikokuten.sync.workers.FirebaseTokenWorker
import java.util.concurrent.TimeUnit


class FirebaseTokenSyncronizer (val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun setPeriodicSync(activity: Activity, interval: Long = 15) {

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(activity) { instanceIdResult ->

            val newToken = instanceIdResult.token

            Log.d(":sync_status", "FirebaseTokenSyncronizer periodic sync called")

            WorkManager.getInstance().cancelAllWorkByTag(FireBaseTokenModel.PERIODIC_TAG)
            val myData = Data.Builder()
                    .putString("FIREBASE_TOKEN", newToken)
                    .build()

            val firebaseTokenWorker = PeriodicWorkRequest.Builder(
                    FirebaseTokenWorker::class.java,
                    interval,
                    TimeUnit.MINUTES)
                    .addTag(FireBaseTokenModel.PERIODIC_TAG)
                    .setInputData(myData)
                    .setConstraints(myConstraints)

            val firebaseToken = firebaseTokenWorker.build()
            WorkManager.getInstance().enqueue(firebaseToken)
        }
    }

}