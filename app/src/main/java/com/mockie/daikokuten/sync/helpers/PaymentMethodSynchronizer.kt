package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.PaymentMethodModel
import com.mockie.daikokuten.sync.workers.PaymentMethodSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class PaymentMethodSynchronizer(val context: Context)  {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun setPeriodicSync(interval: Long = 15) {

        Log.d(":sync_status", "PaymentMethodSynchronizer setPeriodicSync called ")

        WorkManager.getInstance().cancelAllWorkByTag(PaymentMethodModel.PERIODIC_TAG)

        val paymentMethodSync = PeriodicWorkRequest.Builder(
                PaymentMethodSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .addTag(PaymentMethodModel.PERIODIC_TAG)
                .setConstraints(myConstraints)

        val paymentMethodSyncWork = paymentMethodSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("payment_method_worker", ExistingPeriodicWorkPolicy.KEEP, paymentMethodSyncWork)

    }

}