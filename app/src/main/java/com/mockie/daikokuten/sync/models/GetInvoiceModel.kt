package com.mockie.daikokuten.sync.models

open class GetInvoiceModel {

    companion object {

        val ONE_TIME_TAG = "get_invoice_worker_one_tag"

        val PERIODIC_TAG = "get_invoice_worker_tag"

    }

}

