package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.UploadInvoiceModel
import com.mockie.daikokuten.sync.workers.UnlockInvoiceToSync
import com.mockie.daikokuten.sync.workers.UploadInvoiceSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class UploadInvoiceSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        Log.d(":dump", "UploadInvoiceSyncHelper onetime called")

        WorkManager.getInstance().cancelAllWorkByTag(UploadInvoiceModel.ONE_TIME_TAG)
        val uploadInvoiceSyncWork = OneTimeWorkRequest.Builder(UploadInvoiceSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(UploadInvoiceModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(uploadInvoiceSyncWork)
    }

    fun setPeriodicSyncToUnlock() {

        WorkManager.getInstance().cancelAllWorkByTag(UploadInvoiceModel.PERIODIC_UNLOCK_TAG)
        val unlockInvoiceSync = PeriodicWorkRequest.Builder(
                UnlockInvoiceToSync::class.java,
                15,
                TimeUnit.MINUTES)
                .addTag(UploadInvoiceModel.PERIODIC_UNLOCK_TAG)
                .setConstraints(myConstraints)

        val unlockInvoiceSyncWork = unlockInvoiceSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("unlock_invoice_worker", ExistingPeriodicWorkPolicy.KEEP, unlockInvoiceSyncWork)
    }

}