package com.mockie.daikokuten.sync.models

open class CustomerAddressModel {

    companion object {

        val ONE_TIME_TAG = "customer_address_one_tag"

        val PERIODIC_TAG = "customer_address_periodic_tag"

    }

}

