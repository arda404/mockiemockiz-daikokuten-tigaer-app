package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.PrdSizeModel
import com.mockie.daikokuten.sync.workers.PrdSizeWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class PrdSizeSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow():OneTimeWorkRequest
    {
        Log.d(":sync_status", "PrdSizeWorker syncNow called ")

        WorkManager.getInstance().cancelAllWorkByTag(PrdSizeModel.ONE_TIME_TAG)
        val sizeSync = OneTimeWorkRequest.Builder(PrdSizeWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(PrdSizeModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(sizeSync)

        return sizeSync
    }

    fun setPeriodicSync(interval: Long = 15) {

        Log.d(":sync_status", "PrdSizeWorker setPeriodicSync called ")

        WorkManager.getInstance().cancelAllWorkByTag(PrdSizeModel.PERIODIC_TAG)
        val sizeSync = PeriodicWorkRequest.Builder(
                PrdSizeWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .addTag(PrdSizeModel.PERIODIC_TAG)
                .setConstraints(myConstraints)

        val sizeSyncWork = sizeSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("prd_size_worker", ExistingPeriodicWorkPolicy.KEEP, sizeSyncWork)
    }

}