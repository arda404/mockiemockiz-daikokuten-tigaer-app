package com.mockie.daikokuten.sync.accounts

import com.mockie.daikokuten.sync.accounts.models.AccountModel


class InventoryAccount: AccountModel {

    override val name: String = "daikokuten_inventory_synchronizer"

    override val type: String = "id.tigaer"

    override val authority: String = "com.mockie.daikokuten.sync.accounts.inventory.providers"

    override val isSyncable: Int = 1

    override val isSynAutomatically: Boolean = true

    override val periodicSync: Long = 60

}

