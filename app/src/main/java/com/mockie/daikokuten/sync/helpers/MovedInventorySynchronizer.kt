package com.mockie.daikokuten.sync.helpers

import android.content.Context
import androidx.work.*
import com.mockie.daikokuten.sync.models.MoveInventoryModel
import com.mockie.daikokuten.sync.workers.UploadMovedInventorySyncWorker

/**
 * Created by mockie on 17/01/18.
 */

class MovedInventorySynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        WorkManager.getInstance().cancelAllWorkByTag(MoveInventoryModel.ONE_TIME_TAG)
        val uploadMoveInventorySyncWork = OneTimeWorkRequest.Builder(UploadMovedInventorySyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(MoveInventoryModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(uploadMoveInventorySyncWork)
    }
}