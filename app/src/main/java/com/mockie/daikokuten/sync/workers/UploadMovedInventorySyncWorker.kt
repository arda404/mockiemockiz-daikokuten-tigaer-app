package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.database.repositories.MovedInventory
import com.mockie.daikokuten.sync.helpers.MovedInventorySynchronizer
import com.mockie.daikokuten.sync.theSync.UploadMovedInventory


class UploadMovedInventorySyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        Log.d(":dump sync", "uploadMovedInv called")

        val movedInventory = MovedInventory(applicationContext).getDataToSync()

        if (movedInventory != null)
        {
            val response = UploadMovedInventory(applicationContext).doHttp(
                    movedInventory.localCreatedAt,
                    movedInventory.id,
                    movedInventory.code,
                    movedInventory.quantity,
                    movedInventory.oldStore,
                    movedInventory.newStore
            )

            if (MovedInventory(applicationContext).getCount() > 0)
            {
                MovedInventorySynchronizer(applicationContext).syncNow()
            }

            return UploadMovedInventory(applicationContext).processResponse(movedInventory.id, response)

        }

        return Result.FAILURE

    }

}