package com.mockie.daikokuten.sync.helpers

import android.content.Context
import androidx.work.*
import com.mockie.daikokuten.sync.models.CustomerAddressModel
import com.mockie.daikokuten.sync.workers.CustomerAddressSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class CustomerAddressSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        WorkManager.getInstance().cancelAllWorkByTag(CustomerAddressModel.ONE_TIME_TAG)
        val customerAddressSyncWork = OneTimeWorkRequest.Builder(CustomerAddressSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(CustomerAddressModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(customerAddressSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {

        WorkManager.getInstance().cancelAllWorkByTag(CustomerAddressModel.PERIODIC_TAG)
        val customerAddressSync = PeriodicWorkRequest.Builder(
                CustomerAddressSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .addTag(CustomerAddressModel.PERIODIC_TAG)
                .setConstraints(myConstraints)

        val customerAddressSyncWork = customerAddressSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("customer_address_worker", ExistingPeriodicWorkPolicy.KEEP, customerAddressSyncWork)
    }
}