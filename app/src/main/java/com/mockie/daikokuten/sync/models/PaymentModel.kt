package com.mockie.daikokuten.sync.models

open class PaymentModel {

    companion object {

        val ONE_TIME_TAG = "payment_one_tag"

        val PERIODIC_TAG = "payment_periodic_tag"

    }

}

