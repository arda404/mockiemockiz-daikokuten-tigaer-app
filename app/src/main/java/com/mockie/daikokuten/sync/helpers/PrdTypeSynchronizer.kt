package com.mockie.daikokuten.sync.helpers

import android.content.Context
import androidx.work.*
import com.mockie.daikokuten.sync.models.PrdTypeModel
import com.mockie.daikokuten.sync.workers.PrdTypeSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class PrdTypeSynchronizer(val context: Context, var tryCounter: Int = 0)  {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        WorkManager.getInstance().cancelAllWorkByTag(PrdTypeModel.ONE_TIME_TAG)
        val typeSyncWork = OneTimeWorkRequest.Builder(PrdTypeSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(PrdTypeModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(typeSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {
        WorkManager.getInstance().cancelAllWorkByTag(PrdTypeModel.PERIODIC_TAG)
        val typeSync = PeriodicWorkRequest.Builder(
                PrdTypeSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .setConstraints(myConstraints)
                .addTag(PrdTypeModel.PERIODIC_TAG)

        val typeSyncWork = typeSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("prd_type_worker", ExistingPeriodicWorkPolicy.KEEP, typeSyncWork)
    }
}