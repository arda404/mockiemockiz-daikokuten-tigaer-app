package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.CustomerAddressDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.CustomerAddress
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response


class CustomerAddress(val context: Context) {

    fun doHttp(): Response<CustomerAddressDataResponse>?
    {
        return try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getCustomerAddress()

            callResponse.execute()
        } catch (e:Exception)
        {
            null
        }
    }
    
    fun processResponse(response: Response<CustomerAddressDataResponse>?): Pair<ListenableWorker.Result, Boolean>
    {
        var retry = false

        var returnValue = ListenableWorker.Result.FAILURE

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: CustomerAddressDataResponse = response.body()!!

                Log.d(":dump", "cust.add resp" + myResponse.status)

                if (myResponse.status == "success")
                {

                    val addresses = myResponse.data
                    val total = addresses.size
                    Log.d(":dump", "cust.add total" + total.toString())

                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {

                        dbHelper.use {

                            transaction {

                                if (myResponse.current_page == 1)
                                {
                                    SyncLog(context).updateTableLastSyncTemp(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME, myResponse.until_time)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME, myResponse.total)
                                }


                                for (i in 0 until total) {

                                    val checkCustomerAddress = CustomerAddress(context).getCustomerAddressById(addresses[i].id)

                                    if (checkCustomerAddress == null)
                                    {
                                        CustomerAddress(context).insert(
                                                addresses[i].id,
                                                addresses[i].customer_id,
                                                addresses[i].name,
                                                addresses[i].phone,
                                                addresses[i].address,
                                                addresses[i].created_at,
                                                addresses[i].updated_at
                                        )
                                    } else
                                    {
                                        CustomerAddress(context).update(
                                                addresses[i].id,
                                                addresses[i].customer_id,
                                                addresses[i].name,
                                                addresses[i].phone,
                                                addresses[i].address,
                                                addresses[i].created_at,
                                                addresses[i].updated_at
                                        )
                                    }
                                }

                                /**
                                 * UPDATE PROGRESS IN TABLE INFORMATION
                                 */
                                SyncLog(context).updateTableProgress(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME, myResponse.progress)


                                /**
                                 * UPDATE NEXT PAGE IN TABLE INFORMATION IF CURRENT PAGE IS NOT THE LAST PAGE AND DO SYNC AGAIN IN 5 SECONDS
                                 */
                                if (myResponse.current_page < myResponse.last_page)
                                {

                                    val nextPage = myResponse.current_page + 1
                                    SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME, nextPage)

                                    retry = true

                                } else {

                                    val syncLogInfo = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME)

                                    var lastSync:String? = ""
                                    if (syncLogInfo != null)
                                    {
                                        lastSync = syncLogInfo.last_sync_temp
                                    }

                                    SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME, lastSync)
                                    SyncLog(context).updateTableNextPage(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME, 1)
                                    SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.CustomerAddress.TABLE_NAME, 0)
                                }
                            }

                        }

                        returnValue = ListenableWorker.Result.SUCCESS

                    } catch (e: Exception) {

                        returnValue = ListenableWorker.Result.FAILURE

                    }
                }
            }
        }

        return Pair(returnValue, retry)
    }

}