package com.mockie.daikokuten.sync.models

class PrdPriceRuleModel {

    companion object {

        val ONE_TIME_TAG = "prd_price_rule_once_tag"

        val PERIODIC_TAG = "prd_price_rule_periodic_tag"

    }

}