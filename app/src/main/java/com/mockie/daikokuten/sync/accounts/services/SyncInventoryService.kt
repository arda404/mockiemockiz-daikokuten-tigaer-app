package com.mockie.daikokuten.sync.accounts.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.work.Configuration
import androidx.work.WorkManager
import com.mockie.daikokuten.sync.accounts.adapters.SyncInventoryAdapter


/**
 * Created by mockie on 19/12/17.
 */

/**
 * Define a Service that returns an IBinder for the
 * sync adapter class, allowing the sync adapter framework to call
 * onPerformSync().
 */
class SyncInventoryService : Service() {
    /*
     * Instantiate the sync adapter object.
     */
    override fun onCreate() {
        /*
         * Create the sync adapter as a singleton.
         * Set the sync adapter as syncable
         * Disallow parallel syncs
         */
        synchronized(sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                Log.d(":shit", "SyncInventoryService")
                WorkManager.initialize(applicationContext, Configuration.Builder().build())
                sSyncAdapter = SyncInventoryAdapter(applicationContext, true)
            }
        }
    }

    /**
     * Return an object that allows the system to invoke
     * the sync adapter.
     *
     */
    override fun onBind(intent: Intent): IBinder {
        /*
         * Get the object that allows external processes
         * to call onPerformSync(). The object is created
         * in the base class code when the SyncAdapter
         * constructors call super()
         */
        return sSyncAdapter!!.syncAdapterBinder
    }

    companion object {
        // Storage for an instance of the sync adapter
        private var sSyncAdapter: SyncInventoryAdapter? = null
        // Object to use as a thread-safe lock
        private val sSyncAdapterLock = Any()
    }
}