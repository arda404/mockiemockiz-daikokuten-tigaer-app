package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.InventoryDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.Inventory
import com.mockie.daikokuten.database.repositories.SyncLog
import com.mockie.daikokuten.sync.helpers.InventorySynchronizer
import retrofit2.Response


class UploadInventory(val context: Context)
{

    fun doHttp(localCreatedAt:String, inventoryId:Int, code:String, quantity:Int, store:String):Response<InventoryDataResponse>?
    {
        try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.addInventory(localCreatedAt, inventoryId, code, quantity, store)

            return callResponse.execute()

        } catch (e:Exception) {
            return null
        }
    }

    fun processResponse(inventoryId:Int, response: Response<InventoryDataResponse>?): ListenableWorker.Result
    {
        Log.d(":sync_status", "UploadInventorySyncWorker response " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: InventoryDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    Inventory(context).removeInventoryById(inventoryId)
                    SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.Inventory.TABLE_NAME, myResponse.time)

                    return ListenableWorker.Result.SUCCESS
                }

                return ListenableWorker.Result.FAILURE
            }
        }

        return ListenableWorker.Result.FAILURE
    }

}