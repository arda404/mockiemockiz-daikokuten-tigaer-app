package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.api.StockTakingDataResponse
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.StockTaking
import com.mockie.daikokuten.database.repositories.SyncLog
import com.mockie.daikokuten.sync.helpers.StockTakingSynchronizer
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class UploadStockTaking(val context: Context)
{

    fun doHttp(localCreatedAt:String, localId: Int, code:String, stockDeviation:Int, store:String):Response<StockTakingDataResponse>?
    {
        try{
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.stockTaking(localCreatedAt, localId, code, stockDeviation, store)
            val response = callResponse.execute()

            return response
        } catch (e:Exception){ return null }
    }


    fun processResponse(id:Int, response: Response<StockTakingDataResponse>?): ListenableWorker.Result
    {
        Log.d(":sync_status", " upload_stock_taking response: " + response.toString())

        if (response != null)
        {

            if (response.isSuccessful) {

                val myResponse: StockTakingDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {
                        dbHelper.use {
                            transaction {
                                StockTaking(context).removeStockTakingById(id)
                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.StockTaking.TABLE_NAME, myResponse.time)
                            }
                        }
                    } catch (e:Exception) {}

                }

                return ListenableWorker.Result.SUCCESS
            }
        }

        return ListenableWorker.Result.FAILURE
    }

}

