package com.mockie.daikokuten.sync.helpers

import android.content.Context
import androidx.work.*
import com.mockie.daikokuten.sync.models.GetInvoiceModel
import com.mockie.daikokuten.sync.workers.InvoiceSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class GetInvoiceSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow():OneTimeWorkRequest
    {
        WorkManager.getInstance().cancelAllWorkByTag(GetInvoiceModel.ONE_TIME_TAG)
        val invoiceSyncWork = OneTimeWorkRequest.Builder(InvoiceSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(GetInvoiceModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(invoiceSyncWork)

        return invoiceSyncWork
    }

    fun setPeriodicSync(interval: Long = 15):PeriodicWorkRequest {

        WorkManager.getInstance().cancelAllWorkByTag(GetInvoiceModel.PERIODIC_TAG)
        val invoiceSync = PeriodicWorkRequest.Builder(
                InvoiceSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .setConstraints(myConstraints)
                .addTag(GetInvoiceModel.PERIODIC_TAG)
                .build()

        WorkManager.getInstance().enqueueUniquePeriodicWork("get_invoice_worker", ExistingPeriodicWorkPolicy.KEEP, invoiceSync)

        return invoiceSync
    }
}