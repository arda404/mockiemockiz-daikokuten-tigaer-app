package com.mockie.daikokuten.sync.models

open class PrdPriceModel {

    companion object {

        val ONE_TIME_TAG = "PrdPrice_once_tag"

        val PERIODIC_TAG = "PrdPrice_periodic_tag"

    }

}

