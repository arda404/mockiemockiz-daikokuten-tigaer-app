package com.mockie.daikokuten.sync.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.helpers.CustomerSynchronizer

class CustomerSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters)
{
    override fun doWork(): Result {

        val response = com.mockie.daikokuten.sync.theSync.Customer(applicationContext).doHttp()

        val returnValue = com.mockie.daikokuten.sync.theSync.Customer(applicationContext).processResponse(response)

        if (returnValue.second)
        {
            CustomerSynchronizer(applicationContext).syncNow()
        }

        return returnValue.first

    }

}

