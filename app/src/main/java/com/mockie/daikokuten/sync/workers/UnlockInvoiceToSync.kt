package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.theSync.UnlockInvoiceToSync


class UnlockInvoiceToSync(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        UnlockInvoiceToSync(applicationContext).process()

        return Result.SUCCESS
    }

}