package com.mockie.daikokuten.sync.models

class PrdSizeModel {

    companion object {

        val ONE_TIME_TAG = "prdsize_once_tag"

        val PERIODIC_TAG = "prdsize_periodic_tag"

    }

}