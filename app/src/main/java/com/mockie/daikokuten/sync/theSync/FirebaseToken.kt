package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.RegisterDeviceDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.FirebaseToken
import com.mockie.daikokuten.database.repositories.User
import retrofit2.Response

class FirebaseToken(val context: Context)
{

    fun doHttp(token: String): Response<RegisterDeviceDataResponse>?
    {
        return try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.registerDevice(token)

            callResponse.execute()
        } catch (e:Exception) {
            Log.e(":dump_error", "doHttp failed to sync token to server")
            null
        }
    }

    fun processResponse(token: String, response: Response<RegisterDeviceDataResponse>?): ListenableWorker.Result
    {
        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: RegisterDeviceDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    FirebaseToken(context).insert(token)

                    return ListenableWorker.Result.SUCCESS
                }
            }
        }

        return ListenableWorker.Result.FAILURE
    }

    fun registerToken(refreshedToken:String?, oldFirebaseToken:String?): ListenableWorker.Result
    {
        Log.d(":sync_status", "FirebaseTokenSyncronizer registerToken() called")

        var returnValue = ListenableWorker.Result.SUCCESS

        val userLogin: com.mockie.daikokuten.database.models.User? = User(context).isLogin()

        if (userLogin != null && refreshedToken != null)
        {
            if (oldFirebaseToken != refreshedToken)
            {
                FirebaseToken(context).deleteToken(oldFirebaseToken)

                val response = doHttp(refreshedToken)
                returnValue = processResponse(refreshedToken, response)
            }

        }

        return returnValue
    }

}