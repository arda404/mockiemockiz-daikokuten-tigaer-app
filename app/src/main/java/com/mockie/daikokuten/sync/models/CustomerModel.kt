package com.mockie.daikokuten.sync.models

open class CustomerModel {

    companion object {

        val ONE_TIME_TAG = "customer_one_tag"

        val PERIODIC_TAG = "customer_periodic_tag"

    }

}

