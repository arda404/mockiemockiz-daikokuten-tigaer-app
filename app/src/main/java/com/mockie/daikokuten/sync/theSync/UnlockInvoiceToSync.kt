package com.mockie.daikokuten.sync.theSync

import android.content.Context
import com.mockie.daikokuten.database.repositories.InvoiceToSync


class UnlockInvoiceToSync(val context: Context) {

    fun process()
    {
        InvoiceToSync(context).unlockPreviousData()
    }

}
