package com.mockie.daikokuten.sync.models

open class PrdTypeModel {

    companion object {

        val ONE_TIME_TAG = "PrdType_once_tag"

        val PERIODIC_TAG = "PrdType_periodic_tag"

    }

}

