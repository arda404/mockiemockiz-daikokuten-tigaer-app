package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import com.mockie.daikokuten.api.AddInvoiceDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.*
import com.mockie.daikokuten.sync.helpers.UploadInvoiceSynchronizer
import org.jetbrains.anko.db.transaction
import retrofit2.Response
import java.math.BigDecimal
import java.text.DecimalFormat

class UploadInvoice(val context: Context) {

    fun doHttp(
            localCreatedAt:String?,
            tempId:Int,
            realInvoiceId: Int,
            customerId:Int,
            shippingFree: Double,
            totalBilling:Double,
            status:Int,
            store:String,
            addressId:Int,
            note:String?,
            userId:Int,
            customerName:String,
            customerPhone:String,
            addressName:String?,
            addressPhone:String?,
            address:String?,
            additionalPricePerItem:Int,
            itemCode: ArrayList<String>,
            itemQuantity: ArrayList<String>,
            itemPrice: ArrayList<Int>,
            paymentId: ArrayList<Int>,
            paymentUserId: ArrayList<Int>,
            paymentStore: ArrayList<String>,
            paymentCustomerId: ArrayList<Int>,
            paymentCustomerPhone: ArrayList<String>,
            paymentAmount: ArrayList<Int>,
            paymentMethod: ArrayList<String>,
            returnId: ArrayList<Int>,
            returnLocalCreatedAt: ArrayList<String>,
            returnInvoiceId: ArrayList<Int>,
            returnStore: ArrayList<String>,
            returnUserId: ArrayList<Int>,
            returnCustomerId: ArrayList<Int>,
            returnCustomerPhone: ArrayList<String>,
            returnBarcode: ArrayList<String>,
            returnQty: ArrayList<Int>,
            returnAmount: ArrayList<Int>
    ): Response<AddInvoiceDataResponse>?
    {
        try{
            val typeAPI = RestAPI(context)

            val callResponse = typeAPI.addInvoice(
                    localCreatedAt,
                    tempId,
                    realInvoiceId,
                    customerId,
                    shippingFree,
                    totalBilling,
                    status,
                    store,
                    addressId,
                    note,
                    userId,
                    customerName,
                    customerPhone,
                    addressName,
                    addressPhone,
                    address,
                    additionalPricePerItem,
                    itemCode,
                    itemQuantity,
                    itemPrice,
                    paymentId,
                    paymentUserId,
                    paymentStore,
                    paymentCustomerId,
                    paymentCustomerPhone,
                    paymentAmount,
                    paymentMethod,
                    returnId,
                    returnLocalCreatedAt,
                    returnInvoiceId,
                    returnStore,
                    returnUserId,
                    returnCustomerId,
                    returnCustomerPhone,
                    returnBarcode,
                    returnQty,
                    returnAmount)

            return callResponse.execute()
        } catch (e:Exception) { return null }
    }

    fun processResponse(tempId:Int, response: Response<AddInvoiceDataResponse>?) {

        Log.d(":sync_status", "UploadInvoiceSyncWorker response" + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: AddInvoiceDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {
                        dbHelper.use {
                            transaction {
                                val paymentIds = myResponse.payment_ids
                                for (i in 0 until paymentIds.size)
                                {
                                    PaymentToSync(context).removePaymentToSync(paymentIds[i])
                                }

                                val returnIds = myResponse.return_ids
                                for (i in 0 until returnIds.size)
                                {
                                    ReturnToSync(context).removeReturnToSync(returnIds[i])
                                }

                                InvoiceToSync(context).removeInvoiceToSync(tempId)

                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.InvoiceToSync.TABLE_NAME, myResponse.time)
                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.PaymentToSync.TABLE_NAME, myResponse.time)
                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.ReturnToSync.TABLE_NAME, myResponse.time)
                            }
                        }
                    } catch (e:Exception) {}
                }

            } else {

                Log.d(":dump", "uploadInvoiceasynctask status failed" )

            }
        }

    }

}