package com.mockie.daikokuten.sync.accounts

import com.mockie.daikokuten.sync.accounts.models.AccountModel


class InvoiceAccount: AccountModel {

    override val name: String = "daikokuten_invoice_synchronizer"

    override val type: String = "id.tigaer"

    override val authority: String = "com.mockie.daikokuten.sync.accounts.invoice.providers"

    override val isSyncable: Int = 1

    override val isSynAutomatically: Boolean = true

    override val periodicSync: Long = 1

}

