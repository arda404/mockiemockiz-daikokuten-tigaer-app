package com.mockie.daikokuten.sync.accounts.models

import android.accounts.Account

/**
 * Created by mockie on 06/07/18.
 */

class RegisteredAccount(
        val account: Account,
        val name:String,
        val type:String,
        val authority:String,
        val isSyncable: Int,
        val isSyncAutomatically: Boolean,
        val periodicSync:Long
)