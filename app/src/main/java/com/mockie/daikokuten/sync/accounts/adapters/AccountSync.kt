package com.mockie.daikokuten.sync.accounts.adapters

import android.accounts.Account
import android.accounts.AccountManager
import android.content.ContentResolver
import android.content.Context
import android.os.Bundle
import android.util.Log
import com.mockie.daikokuten.sync.accounts.models.AccountModel
import com.mockie.daikokuten.sync.accounts.models.RegisteredAccount

/**
 * Created by mockie on 06/07/18.
 */

class AccountSync (val context: Context) {

    // The authority for the sync adapter's content provider
    var registeredAccount = ArrayList<RegisteredAccount>()

    val settingsBundle = Bundle()

    var accountManager:AccountManager

    init {

        accountManager = context.getSystemService(
                Context.ACCOUNT_SERVICE) as AccountManager

    }

    fun createSyncAccount(account: ArrayList<AccountModel>){

        // Create the account type and default account

        for (a in 0 until account.size)
        {
            val newAccount = Account(
                    account[a].name, account[a].type)
            // Get an instance of the Android account manager
            val accountManager = context.getSystemService(
                    Context.ACCOUNT_SERVICE) as AccountManager

            accountManager.addAccountExplicitly(newAccount, null, null)

            val theAccount = RegisteredAccount(
                    newAccount,
                    account[a].name,
                    account[a].type,
                    account[a].authority,
                    account[a].isSyncable,
                    account[a].isSynAutomatically,
                    account[a].periodicSync)

            registeredAccount.add(theAccount)
            setSyncAutomatically(theAccount)
        }
    }

    fun getAccount(theAccount:AccountModel): Account?
    {
        val accounts = accountManager.getAccountsByType(theAccount.type)

        for (account in accounts) {
            if (account.name == account.name)
            {
                return account
            }
        }

        return null
    }

    private fun setSyncAutomatically(account: RegisteredAccount)
    {
        Log.d(":shit", account.name + " | " + account.authority + " | " + account.isSyncable.toString() + " | " + account.isSyncAutomatically.toString())
        ContentResolver.setIsSyncable(account.account, account.authority, account.isSyncable)
        ContentResolver.setSyncAutomatically(account.account, account.authority, account.isSyncAutomatically)
        ContentResolver.addPeriodicSync(account.account, account.authority, Bundle(), account.periodicSync)
    }

    fun syncNow(account: Account, accountModel: AccountModel)
    {
        ContentResolver.requestSync(account, accountModel.authority, settingsBundle)
    }

}