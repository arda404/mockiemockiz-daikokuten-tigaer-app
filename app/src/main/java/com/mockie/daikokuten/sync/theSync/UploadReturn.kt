package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import com.mockie.daikokuten.api.AddReturnDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.ReturnToSync
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class UploadReturn(val context: Context) {

    fun doHttp(
            returnLocalCreatedAt: ArrayList<String>,
            returnId: ArrayList<Int>,
            returnInvoiceId: ArrayList<Int>,
            returnUserId: ArrayList<Int>,
            returnStore: ArrayList<String>,
            returnCustomerId: ArrayList<Int>,
            returnCustomerPhone: ArrayList<String>,
            returnBarcode: ArrayList<String>,
            returnQty: ArrayList<Int>,
            returnAmount: ArrayList<Int>
    ): Response<AddReturnDataResponse>?
    {
        try{
            val typeAPI = RestAPI(context)

            val callResponse = typeAPI.addReturn(
                    returnLocalCreatedAt,
                    returnId,
                    returnInvoiceId,
                    returnUserId,
                    returnStore,
                    returnCustomerId,
                    returnCustomerPhone,
                    returnBarcode,
                    returnQty,
                    returnAmount)

            return callResponse.execute()
        } catch (e:Exception) { return null }
    }

    fun processResponse(response: Response<AddReturnDataResponse>?) {

        Log.d(":sync_status", "upload return response " + response.toString())

        if (response != null)
        {

            if (response.isSuccessful) {

                val myResponse: AddReturnDataResponse = response.body()!!

                if (myResponse.status == "success")
                {
                    val returnIds = myResponse.return_ids

                    val dbHelper = BaseRepository(context).getDbInstance()

                    try {

                        dbHelper.use {
                            transaction {

                                for (i in 0 until returnIds.size)
                                {
                                    ReturnToSync(context).removeReturnToSync(returnIds[i])
                                }

                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.ReturnToSync.TABLE_NAME, myResponse.time)
                            }
                        }

                    } catch (e:Exception) {}

                }

            } else {

                Log.d(":dump", "uploadReturnasynctask status failed" )

            }
        }

    }

}