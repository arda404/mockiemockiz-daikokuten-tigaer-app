package com.mockie.daikokuten.sync.theSync

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import com.mockie.daikokuten.api.GetPaymentMethodDataResponse
import com.mockie.daikokuten.api.RestAPI
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.PaymentMethod
import com.mockie.daikokuten.database.repositories.SyncLog
import org.jetbrains.anko.db.transaction
import retrofit2.Response

class PaymentMethod(val context: Context) {

    fun doHttp():Response<GetPaymentMethodDataResponse>?
    {
        try {
            val typeAPI = RestAPI(context)
            val callResponse = typeAPI.getPaymentMethod()

            return callResponse.execute()
        } catch (e:Exception) { return null }
    }
    
    fun processResponse(response: Response<GetPaymentMethodDataResponse>?): ListenableWorker.Result
    {
        var returnValue = ListenableWorker.Result.FAILURE

        Log.d(":sync_status", "PaymentMethodSynchronizer processResponse " + response.toString())

        if (response != null)
        {
            if (response.isSuccessful) {

                val myResponse: GetPaymentMethodDataResponse = response.body()!!

                SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PaymentMethod.TABLE_NAME, myResponse.total_data)

                if (myResponse.status == "success")
                {
                    val paymentMethod = myResponse.data
                    val total = paymentMethod.size

                    try {

                        val dbHelper = BaseRepository(context).getDbInstance()

                        dbHelper.use {
                            transaction {

                                PaymentMethod(context).removeAll()

                                for (i in 0 until total) {
                                    com.mockie.daikokuten.database.repositories.PaymentMethod(context).insert(
                                            paymentMethod[i].id,
                                            paymentMethod[i].name
                                    )

                                }

                                SyncLog(context).updateTableLastSync(com.mockie.daikokuten.database.models.PaymentMethod.TABLE_NAME, myResponse.time)
                                SyncLog(context).updateTableToDowload(com.mockie.daikokuten.database.models.PaymentMethod.TABLE_NAME, 0)

                            }
                        }

                        returnValue = ListenableWorker.Result.SUCCESS

                    } catch (e: Exception) {

                        returnValue = ListenableWorker.Result.FAILURE

                    }
                }
            }
        }

        return returnValue
    }
}

