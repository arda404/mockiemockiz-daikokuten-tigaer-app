package com.mockie.daikokuten.sync.asyncTask

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.mockie.daikokuten.api.AddInvoiceDataResponse
import com.mockie.daikokuten.api.AddPaymentDataResponse
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.sync.helpers.UploadInvoiceSynchronizer
import com.mockie.daikokuten.sync.theSync.UploadInvoice
import com.mockie.daikokuten.sync.theSync.UploadPayment
import retrofit2.Response
import java.math.BigDecimal
import java.text.DecimalFormat

class UploadPaymentAsyncTask(var context:Context,
                             val paymentId: ArrayList<Int>,
                             val paymentUserId: ArrayList<Int>,
                             val paymentStore: ArrayList<String>,
                             val paymentCustomerId: ArrayList<Int>,
                             val paymentCustomerPhone: ArrayList<String>,
                             val paymentAmount: ArrayList<Int>,
                             val paymentMethod: ArrayList<String>
                        ): AsyncTask<Void, Void, Response<AddPaymentDataResponse>>()  {


    override fun doInBackground(vararg params: Void): Response<AddPaymentDataResponse>? {

        return UploadPayment(context).doHttp(
                paymentId,
                paymentUserId,
                paymentStore,
                paymentCustomerId,
                paymentCustomerPhone,
                paymentAmount,
                paymentMethod
            )
    }

    override fun onPostExecute(response: Response<AddPaymentDataResponse>?) {
        super.onPostExecute(response)
        UploadPayment(context).processResponse(response)

    }

}