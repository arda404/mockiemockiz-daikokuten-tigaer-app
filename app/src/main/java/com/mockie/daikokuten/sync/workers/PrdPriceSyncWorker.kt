package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.os.Handler
import androidx.work.Worker
import com.mockie.daikokuten.sync.helpers.PrdPriceSynchronizer
import com.mockie.daikokuten.sync.theSync.PrdPrice
import android.os.Looper
import androidx.work.WorkerParameters


class PrdPriceSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        val response = PrdPrice(applicationContext).doHttp()

        val returnValue = PrdPrice(applicationContext).processResponse(response)

        if (returnValue.second)
        {
            PrdPriceSynchronizer(applicationContext).syncNow()
        }

        return returnValue.first
    }

}
