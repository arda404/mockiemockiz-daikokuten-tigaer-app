package com.mockie.daikokuten.sync.helpers

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.work.*
import com.mockie.daikokuten.NewMainActivity
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.manager.TigaerNotificationManager
import com.mockie.daikokuten.sync.asyncTask.CheckUserTokenAsyncTask
import com.mockie.daikokuten.sync.workers.CheckUserToken
import java.util.concurrent.TimeUnit

class CheckUserTokenSyncronizer(val context: Context) {

    var myConstraints:Constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        Log.d(":workmanager syncNow", "CheckUserTokenSyncronizer called")
        val checkUserTokenSyncWork = OneTimeWorkRequest.Builder(CheckUserToken::class.java)
                .setConstraints(myConstraints)
                .build()

        WorkManager.getInstance()?.enqueue(checkUserTokenSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {

        Log.d(":workmanager periodic", "CheckUserTokenSyncronizer called")

        val checkUserTokenSync = PeriodicWorkRequest.Builder(
                CheckUserToken::class.java,
                interval,
                TimeUnit.MINUTES)
                .setConstraints(myConstraints)

        val checkUserTokenSyncWork = checkUserTokenSync.build()
        WorkManager.getInstance()?.enqueue(checkUserTokenSyncWork)
    }

    fun syncWithAsyncTask()
    {
        CheckUserTokenAsyncTask(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

}