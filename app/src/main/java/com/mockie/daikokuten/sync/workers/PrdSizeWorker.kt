package com.mockie.daikokuten.sync.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.helpers.PrdSizeSynchronizer
import com.mockie.daikokuten.sync.theSync.PrdSize

class PrdSizeWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        Log.d(":sync_status", "PrdSizeWorker doWork called ")

        val response = PrdSize(applicationContext).doHttp()

        val returnValue = PrdSize(applicationContext).processResponse(response)

        if (returnValue.second)
        {
            PrdSizeSynchronizer(applicationContext).syncNow()
        }

        return returnValue.first
    }

}

