package com.mockie.daikokuten.sync.models

class UploadReturnModel {

    companion object {

        val ONE_TIME_TAG = "upload_return_one_tag"

        val PERIODIC_TAG = "upload_return_periodic_tag"

    }

}