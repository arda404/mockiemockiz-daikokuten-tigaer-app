package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.sync.models.UploadReturnModel
import com.mockie.daikokuten.sync.workers.UploadReturnSyncWorker
import java.util.concurrent.TimeUnit

/**
 * Created by mockie on 17/01/18.
 */


class UploadReturnSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow()
    {
        Log.d(":dump", "UploadReturnSyncHelper onetime called")

        WorkManager.getInstance().cancelAllWorkByTag(UploadReturnModel.ONE_TIME_TAG)
        val uploadReturnSyncWork = OneTimeWorkRequest.Builder(UploadReturnSyncWorker::class.java)
                .setConstraints(myConstraints)
                .addTag(UploadReturnModel.ONE_TIME_TAG)
                .build()

        WorkManager.getInstance().enqueue(uploadReturnSyncWork)
    }

    fun setPeriodicSync(interval: Long = 15) {
        Log.d(":dump", "UploadReturnSyncHelper called")

        WorkManager.getInstance().cancelAllWorkByTag(UploadReturnModel.PERIODIC_TAG)
        val uploadReturnSync = PeriodicWorkRequest.Builder(
                UploadReturnSyncWorker::class.java,
                interval,
                TimeUnit.MINUTES)
                .addTag(UploadReturnModel.PERIODIC_TAG)
                .setConstraints(myConstraints)

        val uploadReturnSyncWork = uploadReturnSync.build()
        WorkManager.getInstance().enqueueUniquePeriodicWork("upload_return_worker", ExistingPeriodicWorkPolicy.KEEP, uploadReturnSyncWork)
    }
}