package com.mockie.daikokuten.sync.helpers

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.work.*
import com.mockie.daikokuten.database.repositories.Inventory
import com.mockie.daikokuten.database.repositories.MovedInventory
import com.mockie.daikokuten.sync.models.StockTakingModel
import com.mockie.daikokuten.sync.workers.UploadStockTakingSyncWorker

/**
 * Created by mockie on 17/01/18.
 */


class StockTakingSynchronizer(val context: Context) {

    var myConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

    fun syncNow(attempt:Int = 1)
    {
        val handler = Handler(Looper.getMainLooper())
        Log.d(":sync_status", " upload_stock_taking: " + attempt.toString())
        handler.postDelayed({

            if (attempt < 100)
            {
                val newStock =  Inventory(context).getTotalRow()
                val moveStock = MovedInventory(context).getCount()

                if (newStock == 0 && moveStock == 0)
                {
                    WorkManager.getInstance().cancelAllWorkByTag(StockTakingModel.ONE_TIME_TAG)
                    val theWorker = OneTimeWorkRequest.Builder(UploadStockTakingSyncWorker::class.java)
                            .setConstraints(myConstraints)
                            .addTag(StockTakingModel.ONE_TIME_TAG)
                            .build()

                    WorkManager.getInstance().enqueue(theWorker)
                }
                else
                {
                    this.syncNow(attempt+1)
                }
            }

        }, 30000)
    }

//    fun setPeriodicSync(interval: Long = 15)
//    {
//        val newStock =  Inventory(context).getTotalRow()
//        val moveStock = MovedInventory(context).getCount()
//
//        if (newStock == 0 && moveStock == 0)
//        {
//            WorkManager.getInstance().cancelAllWorkByTag(StockTakingModel.PERIODIC_TAG)
//            val stockTakingSync = PeriodicWorkRequest.Builder(
//                    UploadStockTakingSyncWorker::class.java,
//                    interval,
//                    TimeUnit.MINUTES)
//                    .setConstraints(myConstraints)
//                    .addTag(StockTakingModel.PERIODIC_TAG)
//
//            val stockTakingSyncWork = stockTakingSync.build()
//            WorkManager.getInstance().enqueueUniquePeriodicWork("stock_taking_worker", ExistingPeriodicWorkPolicy.KEEP,stockTakingSyncWork)
//        }
//    }
}