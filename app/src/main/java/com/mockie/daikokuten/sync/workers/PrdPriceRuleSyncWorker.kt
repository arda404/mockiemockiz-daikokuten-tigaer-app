package com.mockie.daikokuten.sync.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.sync.theSync.PrdPriceRule

class PrdPriceRuleSyncWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters)
{

    override fun doWork(): Result {

        val response = PrdPriceRule(applicationContext).doHttp()

        return PrdPriceRule(applicationContext).processResponse(response)
    }
}

