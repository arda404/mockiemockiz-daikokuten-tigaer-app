package com.mockie.daikokuten.sync.accounts.models

/**
 * Created by mockie on 06/07/18.
 */

interface AccountModel {

    val name:String // unique

    val type:String // non unique id.tigaer

    val authority:String // unique

    val isSyncable:Int

    val isSynAutomatically:Boolean

    val periodicSync:Long // in seconds

}