package com.mockie.daikokuten.sync.workers


import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.mockie.daikokuten.database.repositories.FirebaseToken

class FirebaseTokenWorker(appContext: Context, workerParameters: WorkerParameters): Worker(appContext, workerParameters) {

    override fun doWork(): Result {

        Log.d(":sync_status", "FirebaseTokenSyncronizer doWork() called")

        val oldFirebaseToken = FirebaseToken(applicationContext).getToken()?.token
        val refreshedToken = inputData.getString("FIREBASE_TOKEN")

        return com.mockie.daikokuten.sync.theSync.FirebaseToken(applicationContext).registerToken(refreshedToken, oldFirebaseToken)

    }

}