package com.mockie.daikokuten.Printers

import android.content.Context
import android.widget.Toast
import com.example.tscdll.TSCUSBActivity
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.permissions.UsbPermission

/**
 * Created by mockie on 11/05/18.
 */

class TscTtp244Printer(val context: Context) {

    var mUsbPermission: UsbPermission = UsbPermission(context)
    var totalDevices: Int = 0
    val TscUSB = TSCUSBActivity()

    init {
        totalDevices = mUsbPermission.getTotalDevice()

        if (totalDevices < 1) {
            AlertHelper(this.context).printerNotFound()
        } else {
            mUsbPermission.showPermissionDialog()
        }
    }

    fun print(barcode: String, title: String?, size: String?, page:Int)
    {
        totalDevices = mUsbPermission.getTotalDevice()
        if (totalDevices > 0 && mUsbPermission.hasPermission()) {
            TscUSB.openport(mUsbPermission.getUsbManager(), mUsbPermission.getDevice())

            TscUSB.sendcommand("SIZE 103 mm,15 mm\r\n")
            TscUSB.sendcommand("GAP 3 mm,0 mm\r\n")
            TscUSB.sendcommand("DENSITY 10\r\n")
            TscUSB.sendcommand("CLS\r\n")

            val left = 100
            val center = 387
            val right = 669

            Toast.makeText(context, page.toString(), Toast.LENGTH_LONG).show()

            for (i in 0 until page) {

                TscUSB.sendcommand("BARCODE $left,32,\"128\",45,2,0,1,1,2,\"$barcode\" \r\n")
                TscUSB.sendcommand("BARCODE $center,32,\"128\",45,2,0,1,1,2,\"$barcode\" \r\n")
                TscUSB.sendcommand("BARCODE $right,32,\"128\",45,2,0,1,1,2,\"$barcode\" \r\n")

                TscUSB.sendcommand("TEXT $left,2,\"0\",0,6,9,2,\"$title\" \r\n")
                TscUSB.sendcommand("TEXT $center,2,\"0\",0,6,9,2,\"$title\" \r\n")
                TscUSB.sendcommand("TEXT $right,2,\"0\",0,6,9,2,\"$title\" \r\n")

                TscUSB.sendcommand("TEXT " + (left + 135) + ",50,\"0\",90,13,9,2,\"" + size + "\" \r\n")
                TscUSB.sendcommand("TEXT " + (center + 135) + ",50,\"0\",90,13,9,2,\"" + size + "\" \r\n")
                TscUSB.sendcommand("TEXT " + (right + 135) + ",50,\"0\",90,13,9,2,\"" + size + "\" \r\n")

                TscUSB.sendcommand("BAR " + (left + 107) + ",5,2,90 \r\n")
                TscUSB.sendcommand("BAR " + (center + 107) + ",5,2,90 \r\n")
                TscUSB.sendcommand("BAR " + (right + 107) + ",5,2,90 \r\n")

                TscUSB.sendcommand("PRINT 1\r\n")
            }
        } else {
            Toast.makeText(context, "Printer tidak dapat izin atau tidak di temukan", Toast.LENGTH_LONG).show()
        }
    }

}