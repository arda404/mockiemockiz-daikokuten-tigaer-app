package com.mockie.daikokuten.Printers

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.util.Log
import HPRTAndroidSDK.HPRTPrinterHelper
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.models.PairedDevices
import HPRTAndroidSDK.PublicFunction
import com.mockie.daikokuten.database.models.TheInvoice
import com.mockie.daikokuten.database.repositories.User
import com.mockie.daikokuten.helpers.BarcodeListHelper
import com.mockie.daikokuten.helpers.PaymentHelper
import com.mockie.daikokuten.models.PaymentMethod
import java.text.SimpleDateFormat
import java.util.*


class MPT2(val context: Context)
{
    val mBtAdapter = BluetoothAdapter.getDefaultAdapter()

    val pairedDeviceList = ArrayList<PairedDevices>()

    val PFun = PublicFunction(context)

    private fun InitSetting() {
        var settingValue: String = PFun.ReadSharedPreferencesData("Codepage")
        if (settingValue == "")
            PFun.WriteSharedPreferencesData("Codepage", "0,PC437(USA:Standard Europe)")

        settingValue = PFun.ReadSharedPreferencesData("Cut")
        if (settingValue == "")
            PFun.WriteSharedPreferencesData("Cut", "0")    //

        settingValue = PFun.ReadSharedPreferencesData("Cashdrawer")
        if (settingValue == "")
            PFun.WriteSharedPreferencesData("Cashdrawer", "0")

        settingValue = PFun.ReadSharedPreferencesData("Buzzer")
        if (settingValue == "")
            PFun.WriteSharedPreferencesData("Buzzer", "0")

        settingValue = PFun.ReadSharedPreferencesData("Feeds")
        if (settingValue == "")
            PFun.WriteSharedPreferencesData("Feeds", "0")
    }


    fun getPairedBluetoothDevices(): ArrayList<PairedDevices>
    {
        val pairedDevices = mBtAdapter.bondedDevices

        if (pairedDevices.size > 0) {
            for (device in pairedDevices)
            {
                val getUuidsMethod = device.bluetoothClass.deviceClass
                Log.d("gigibiru", device.uuids.toString())
                pairedDeviceList.add(PairedDevices(device.name, device.address))
            }
        }

        return pairedDeviceList
    }

    private fun alignToCenter(text:String): String
    {
        val width = 31
        val totalChar = text.length
        val mid = if (totalChar < 31) (width - totalChar) / 2 else 0

        return " ".repeat(mid) + text
    }

    private fun connectBluetooth(toothAddress:String): Boolean
    {
        InitSetting()

        val portOpen = if (!HPRTPrinterHelper.IsOpened())
        {
            HPRTPrinterHelper.PortOpen("Bluetooth,$toothAddress")
        }
        else
        {
            0
        }

        if(portOpen != 0)
        {
            AlertHelper(context).printerFailedToConnect()
            return false
        }

        return true
    }

    private fun toLeftAndRight(text: String?, text2: String?): String
    {
        val width = 31
        val firstLength = text?.length ?: 0
        val secondLength = text2?.length ?: 0
        val totalChar = firstLength + secondLength

        val fillTheSpace = width - totalChar

        return text + " ".repeat(fillTheSpace) + text2
    }

    fun printInvoice(toothAddress:String, theInvoice: TheInvoice)
    {
        if (connectBluetooth(toothAddress))
        {
            val Pact = PublicAction(context)

            for (a in 0 until 1)
            {
                Pact.BeforePrintAction()

                HPRTPrinterHelper.PrintText(alignToCenter("Celana 3R (" + User(context).getLoggedInUserStore() +")"), 1, 1, 0)
                HPRTPrinterHelper.PrintText(alignToCenter("celana3r.com | 0811-856-090"), 1, 1, 0)
                HPRTPrinterHelper.PrintText(alignToCenter(theInvoice.createdAt) + "\n", 1, 1, 0)

                HPRTPrinterHelper.PrintText(toLeftAndRight("Nota :", theInvoice.invoiceId.toString() + theInvoice.unsync), 0, 1, 0)
                HPRTPrinterHelper.PrintText(toLeftAndRight("Nama :", theInvoice.customerName), 0, 1, 0)
                HPRTPrinterHelper.PrintText(toLeftAndRight("Telp :", theInvoice.customerPhone), 0, 1, 0)

                HPRTPrinterHelper.PrintText("\n", 1, 1, 0)

                var no = 1
                val theItems = theInvoice._getItems()

                for (b in 0 until theItems.size)
                {
                    HPRTPrinterHelper.PrintText(alignToCenter("---- ($no) ----"), 1, 1, 0)
                    HPRTPrinterHelper.PrintText(BarcodeListHelper(context).getTypeReadable(theItems[b].code), 0, 1, 0)
                    HPRTPrinterHelper.PrintText(toLeftAndRight("Qty    :", theItems[b].quantity.toString()), 0, 1, 0)
                    HPRTPrinterHelper.PrintText(toLeftAndRight("Ukuran :", BarcodeListHelper(context).getSizeReadable(theItems[b].code)), 0, 1, 0)
                    HPRTPrinterHelper.PrintText(toLeftAndRight("Harga  :", PaymentHelper().amountToReadable(theItems[b].totalPricePerItem)), 0, 1, 0)
                    no++
                }

                HPRTPrinterHelper.PrintText(alignToCenter("~ *** ~"), 1, 1, 0)
                HPRTPrinterHelper.PrintText("\n", 1, 1, 0)

                HPRTPrinterHelper.PrintText(toLeftAndRight("Ongkos kirim :", PaymentHelper().amountToReadable(theInvoice.shippingFee)), 0, 1, 0)
                HPRTPrinterHelper.PrintText(toLeftAndRight("Total        :", PaymentHelper().amountToReadable(theInvoice.totalBilling)), 0, 1, 0)

                HPRTPrinterHelper.PrintText("\n", 1, 1, 0)

                HPRTPrinterHelper.PrintText(alignToCenter("-- Terima Kasih :) --") + "\n", 1, 1, 0)

                Pact.AfterPrintAction()
            }

            Pact.BeforePrintAction()

            if (theInvoice.addressName != null)
            {
                HPRTPrinterHelper.PrintText(toLeftAndRight("Nama   :", theInvoice.addressName), 0, 1, 0)
                HPRTPrinterHelper.PrintText(toLeftAndRight("Telp   :", theInvoice.addressPhone), 0, 1, 0)
                HPRTPrinterHelper.PrintText("Alamat :", 0, 1, 0)
                HPRTPrinterHelper.PrintText(theInvoice.address + "\n", 0, 1, 0)
            }

            Pact.AfterPrintAction()
        }
    }


    fun printPayment(toothAddress:String, payment: ArrayList<PaymentMethod>, customerName:String, customerPhone:String)
    {
        if (connectBluetooth(toothAddress))
        {
            val Pact = PublicAction(context)

            Pact.BeforePrintAction()

            val cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok"))
            val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val localInsertedAt = format1.format(cal.timeInMillis)

            HPRTPrinterHelper.PrintText(alignToCenter("Celana 3R (" + User(context).getLoggedInUserStore() +")"), 1, 1, 0)
            HPRTPrinterHelper.PrintText(alignToCenter("celana3r.com | 0811-856-090"), 1, 1, 0)
            HPRTPrinterHelper.PrintText(alignToCenter(localInsertedAt) + "\n", 1, 1, 0)

            HPRTPrinterHelper.PrintText(toLeftAndRight("Nama :", customerName), 0, 1, 0)
            HPRTPrinterHelper.PrintText(toLeftAndRight("Telp :", customerPhone), 0, 1, 0)

            HPRTPrinterHelper.PrintText(alignToCenter("~ *** ~"), 1, 1, 0)

            var changeAmount= 0
            var k = 1
            var total = 0
            for (pay in payment)
            {
                if (pay.method.toString() != "CHANGE")
                {
                    HPRTPrinterHelper.PrintText(toLeftAndRight(pay.method.toString(), PaymentHelper().amountToReadable(pay.amount.toInt())), 0, 1, 0)
                    total += pay.amount.toInt()
                    k++
                }
                else
                {
                    total -= pay.amount.toInt()
                    changeAmount = pay.amount.toInt()
                }

            }

            HPRTPrinterHelper.PrintText(alignToCenter("~ *** ~"), 1, 1, 0)

            HPRTPrinterHelper.PrintText(toLeftAndRight("Total   :", PaymentHelper().amountToReadable(total)), 0, 1, 0)
            HPRTPrinterHelper.PrintText(toLeftAndRight("Kembali :", PaymentHelper().amountToReadable(changeAmount)), 0, 1, 0)

            HPRTPrinterHelper.PrintText("\n", 1, 1, 0)
            HPRTPrinterHelper.PrintText(alignToCenter("----- Terima Kasih :) -----") + "\n", 1, 1, 0)

            Pact.AfterPrintAction()
        }
    }

}