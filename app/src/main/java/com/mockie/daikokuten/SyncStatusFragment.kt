package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mockie.daikokuten.database.repositories.SyncLog
import android.graphics.Color
import android.widget.Button
import com.mockie.daikokuten.sync.accounts.InventoryAccount
import com.mockie.daikokuten.sync.accounts.adapters.AccountSync
import com.mockie.daikokuten.database.models.*
import com.mockie.daikokuten.helpers.InternetCheck


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SyncStatusFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SyncStatusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SyncStatusFragment : Fragment() {


    lateinit var view_:View

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        view_ = inflater!!.inflate(R.layout.fragment_sync_status, container, false)
        val syncInventory = view_.findViewById<Button>(R.id.sync_inventory)

        syncInventory.setOnClickListener {

            syncInventory.isEnabled = false

            val syncInventoryService = AccountSync(context)
            val invAcc = syncInventoryService.getAccount(InventoryAccount())

            if (invAcc != null)
            {
                Log.d("button inventory sync", "account sync found")
                syncInventoryService.syncNow(invAcc, InventoryAccount())
            }

            val handler = Handler()

            handler.postDelayed({
                syncInventory.isEnabled = true
            }, 5000)

        }

        val thread = object : Thread() {

            override fun run() {
                    while (!this.isInterrupted) {
                        Thread.sleep(1000)
                        try {

                            activity.runOnUiThread {
                                try {
                                    prdType()
                                    prdSize()
                                    prdPrice()
                                    prdCategory()
                                    priceRule()
                                    invoiceToSync()
                                    customer()
                                    customerAddress()
                                    invoice()
                                    inventoryToSync()
                                    moveInventory()
                                    stockTakingToSync()
                                    checkConnectivity()
                                    paymentMethod()
                                    paymentToSync()
                                    returnToSync()
                                    ledgerToSync()
                                } catch (e:Exception) {}
                            }

                        } catch (e:Exception) {}
                    }
            }
        }

        try{
            thread.start()
        } catch (e:Exception) {}

        return view_
    }

    fun checkConnectivity() {
        val internetStatus = view_.findViewById<TextView>(R.id.InternetStatus)

        InternetCheck(object : InternetCheck.Consumer {
            override fun accept(internet: Boolean?) {
                if (internet == true) {
                    internetStatus.text = "ONLINE"
                    internetStatus.setTextColor(Color.GREEN)
                } else {
                    internetStatus.text = "OFFLINE"
                    internetStatus.setTextColor(Color.RED)
                }
            }
        })
    }

    fun stockTakingToSync()
    {
        val stockTakingToUpload = view_.findViewById<TextView>(R.id.stockTakingToUpload)
        val stockTakingToUploadProgress = view_.findViewById<TextView>(R.id.stockTakingToUploadProgress)
        val stockTakingToUploadLastSync = view_.findViewById<TextView>(R.id.stockTakingToUploadLastSync)
        val stockTakingToSyncLog = SyncLog(context).getTableInformation(StockTaking.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.StockTaking(context).getCount()

        stockTakingToUpload.text = rowCount.toString()

        if (rowCount == 0)
        {
            stockTakingToUploadProgress.text = "100%"
        } else {
            stockTakingToUploadProgress.text = "0%"
        }

        if (stockTakingToSyncLog != null)
        {
            stockTakingToUploadLastSync.text = stockTakingToSyncLog.last_sync
        }
    }

    fun invoiceToSync()
    {
        val invoiceToUpload = view_.findViewById<TextView>(R.id.invoiceToUpload)
        val invoiceToUploadProgress = view_.findViewById<TextView>(R.id.invoiceToUploadProgress)
        val invoiceToUploadLastSync = view_.findViewById<TextView>(R.id.invoiceToUploadLastSync)
        val invoiceToSyncLog = SyncLog(context).getTableInformation(InvoiceToSync.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.InvoiceToSync(context).getTotalRow()

        invoiceToUpload.text = rowCount.toString()

        if (rowCount == 0)
        {
            invoiceToUploadProgress.text = "100%"
        } else {
            invoiceToUploadProgress.text = "0%"
        }

        if (invoiceToSyncLog != null)
        {
            invoiceToUploadLastSync.text = invoiceToSyncLog.last_sync
        }
    }

    fun returnToSync()
    {
        val returnToUpload = view_.findViewById<TextView>(R.id.returnToUpload)
        val returnToUploadProgress = view_.findViewById<TextView>(R.id.returnToUploadProgress)
        val returnToUploadLastSync = view_.findViewById<TextView>(R.id.returnToUploadLastSync)
        val returnToSyncLog = SyncLog(context).getTableInformation(ReturnToSync.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.ReturnToSync(context).getTotalRow()

        returnToUpload.text = rowCount.toString()

        if (rowCount == 0)
        {
            returnToUploadProgress.text = "100%"
        } else {
            returnToUploadProgress.text = "0%"
        }

        if (returnToSyncLog != null)
        {
            returnToUploadLastSync.text = returnToSyncLog.last_sync
        }
    }

    fun ledgerToSync()
    {
        val ledgerToSyncToUpload = view_.findViewById<TextView>(R.id.ledgerToSyncToUpload)
        val ledgerToSyncToUploadProgress = view_.findViewById<TextView>(R.id.ledgerToSyncToUploadProgress)
        val ledgerToSyncToUploadLastSync = view_.findViewById<TextView>(R.id.ledgerToSyncToUploadLastSync)
        val ledgerToSyncToSyncLog = SyncLog(context).getTableInformation(LedgerToSync.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.LedgerToSync(context).getTotalRow()

        ledgerToSyncToUpload.text = rowCount.toString()

        if (rowCount == 0)
        {
            ledgerToSyncToUploadProgress.text = "100%"
        } else {
            ledgerToSyncToUploadProgress.text = "0%"
        }

        if (ledgerToSyncToSyncLog != null)
        {
            ledgerToSyncToUploadLastSync.text = ledgerToSyncToSyncLog.last_sync
        }
    }

    fun paymentToSync()
    {
        val paymentToUpload = view_.findViewById<TextView>(R.id.paymentToUpload)
        val paymentToUploadProgress = view_.findViewById<TextView>(R.id.paymentToUploadProgress)
        val paymentToUploadLastSync = view_.findViewById<TextView>(R.id.paymentToUploadLastSync)
        val paymentToSyncLog = SyncLog(context).getTableInformation(PaymentToSync.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.PaymentToSync(context).getTotalRow()

        paymentToUpload.text = rowCount.toString()

        if (rowCount == 0)
        {
            paymentToUploadProgress.text = "100%"
        } else {
            paymentToUploadProgress.text = "0%"
        }

        if (paymentToSyncLog != null)
        {
            paymentToUploadLastSync.text = paymentToSyncLog.last_sync
        }
    }

    fun inventoryToSync()
    {
        val inventoryToUpload = view_.findViewById<TextView>(R.id.inventoryToUpload)
        val inventoryToUploadProgress = view_.findViewById<TextView>(R.id.inventoryToUploadProgress)
        val inventoryToUploadLastSync = view_.findViewById<TextView>(R.id.inventoryToUploadLastSync)
        val inventoryToSyncLog = SyncLog(context).getTableInformation(Inventory.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.Inventory(context).getTotalRow()

        inventoryToUpload.text = rowCount.toString()

        if (rowCount == 0)
        {
            inventoryToUploadProgress.text = "100%"
        } else {
            inventoryToUploadProgress.text = "0%"
        }

        if (inventoryToSyncLog != null)
        {
            inventoryToUploadLastSync.text = inventoryToSyncLog.last_sync
        }
    }

    fun moveInventory()
    {
        val moveInventoryToUpload = view_.findViewById<TextView>(R.id.moveInventoryToUpload)
        val moveInventoryToUploadProgress = view_.findViewById<TextView>(R.id.moveInventoryToUploadProgress)
        val moveInventoryToUploadLastSync = view_.findViewById<TextView>(R.id.moveInventoryToUploadLastSync)
        val moveInventoryToSyncLog = SyncLog(context).getTableInformation(MovedInventory.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.MovedInventory(context).getCount()

        moveInventoryToUpload.text = rowCount.toString()

        if (rowCount == 0)
        {
            moveInventoryToUploadProgress.text = "100%"
        } else {
            moveInventoryToUploadProgress.text = "0%"
        }

        if (moveInventoryToSyncLog != null)
        {
            moveInventoryToUploadLastSync.text = moveInventoryToSyncLog.last_sync
        }
    }

    fun invoice()
    {
        val invoiceDownload = view_.findViewById<TextView>(R.id.invoiceDownloaded)
        val invoiceToDownload = view_.findViewById<TextView>(R.id.invoiceToDownload)
        val invoiceProgress = view_.findViewById<TextView>(R.id.invoiceProgress)
        val invoiceLastSync = view_.findViewById<TextView>(R.id.invoiceLastSync)
        val invoiceSyncLog = SyncLog(context).getTableInformation(Invoices.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.Invoices(context).getTotalRow()

        invoiceDownload.text = rowCount.toString()
        invoiceToDownload.text = invoiceSyncLog!!.to_download.toString()
        invoiceProgress.text = invoiceSyncLog.progress
        invoiceLastSync.text = invoiceSyncLog.last_sync
    }

    fun customer()
    {
        val customerDownload = view_.findViewById<TextView>(R.id.customerDownloaded)
        val customerToDownload = view_.findViewById<TextView>(R.id.customerToDownload)
        val customerProgress = view_.findViewById<TextView>(R.id.customerProgress)
        val customerLastSync = view_.findViewById<TextView>(R.id.customerLastSync)

        val customerSyncLog = SyncLog(context).getTableInformation(Customer.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.Customer(context).getTotalRow()

        customerDownload.text = rowCount.toString()
        customerToDownload.text = customerSyncLog!!.to_download.toString()
        customerProgress.text = customerSyncLog.progress
        customerLastSync.text = customerSyncLog.last_sync
    }

    fun customerAddress()
    {
        val customerAddressDownload = view_.findViewById<TextView>(R.id.customerAddressDownloaded)
        val customerAddressToDownload = view_.findViewById<TextView>(R.id.customerAddressToDownload)
        val customerAddressProgress = view_.findViewById<TextView>(R.id.customerAddressProgress)
        val customerAddressLastSync = view_.findViewById<TextView>(R.id.customerAddressLastSync)
        val customerAddressSyncLog = SyncLog(context).getTableInformation(CustomerAddress.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.CustomerAddress(context).getTotalRow()

        customerAddressDownload.text = rowCount.toString()
        customerAddressToDownload.text = customerAddressSyncLog!!.to_download.toString()
        customerAddressProgress.text = customerAddressSyncLog.progress
        customerAddressLastSync.text = customerAddressSyncLog.last_sync
    }

    fun prdType()
    {
        val prdTypeDownload = view_.findViewById<TextView>(R.id.PrdTypeDownloaded)
        val prdTypeToDownload = view_.findViewById<TextView>(R.id.PrdTypeToDownload)
        val prdTypeProgress = view_.findViewById<TextView>(R.id.PrdTypeProgress)
        val prdTypeLastSync = view_.findViewById<TextView>(R.id.PrdTypeLastSync)
        val prdTypeSyncLog = SyncLog(context).getTableInformation(PrdTypes.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.PrdTypes(context).getTotalRow()

        prdTypeDownload.text = rowCount.toString()
        prdTypeToDownload.text = prdTypeSyncLog!!.to_download.toString()
        prdTypeProgress.text = prdTypeSyncLog.progress
        prdTypeLastSync.text = prdTypeSyncLog.last_sync
    }

    fun prdSize()
    {
        val prdSizeDownload = view_.findViewById<TextView>(R.id.PrdSizeDownloaded)
        val prdSizeToDownload = view_.findViewById<TextView>(R.id.PrdSizeToDownload)
        val prdSizeProgress = view_.findViewById<TextView>(R.id.PrdSizeProgress)
        val prdSizeLastSync = view_.findViewById<TextView>(R.id.PrdSizeLastSync)
        val prdSizeSyncLog = SyncLog(context).getTableInformation(PrdSizes.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.PrdSizes(context).getTotalRow()

        prdSizeDownload.text = rowCount.toString()
        prdSizeToDownload.text = prdSizeSyncLog!!.to_download.toString()
        prdSizeProgress.text = prdSizeSyncLog.progress
        prdSizeLastSync.text = prdSizeSyncLog.last_sync
    }

    fun prdPrice()
    {
        val prdPriceDownload = view_.findViewById<TextView>(R.id.PrdPriceDownloaded)
        val prdPriceToDownload = view_.findViewById<TextView>(R.id.PrdPriceToDownload)
        val prdPriceProgress = view_.findViewById<TextView>(R.id.PrdPriceProgress)
        val prdPriceLastSync = view_.findViewById<TextView>(R.id.PrdPriceLastSync)
        val prdPriceSyncLog = SyncLog(context).getTableInformation(PrdPrices.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.PrdPrices(context).getTotalRow()

        prdPriceDownload.text = rowCount.toString()
        prdPriceToDownload.text = prdPriceSyncLog!!.to_download.toString()
        prdPriceProgress.text = prdPriceSyncLog.progress
        prdPriceLastSync.text = prdPriceSyncLog.last_sync
    }

    fun prdCategory()
    {
        val prdCategoryDownload = view_.findViewById<TextView>(R.id.PrdCategoryDownloaded)
        val prdCategoryToDownload = view_.findViewById<TextView>(R.id.PrdCategoryToDownload)
        val prdCategoryProgress = view_.findViewById<TextView>(R.id.PrdCategoryProgress)
        val prdCategoryLastSync = view_.findViewById<TextView>(R.id.PrdCategoryLastSync)
        val prdCategorySyncLog = SyncLog(context).getTableInformation(PrdCategories.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.PrdCategories(context).getTotalRow()

        prdCategoryDownload.text = rowCount.toString()
        prdCategoryToDownload.text = prdCategorySyncLog!!.to_download.toString()
        prdCategoryProgress.text = prdCategorySyncLog.progress
        prdCategoryLastSync.text = prdCategorySyncLog.last_sync
    }

    fun priceRule()
    {
        val priceRuleDownload = view_.findViewById<TextView>(R.id.priceRuleDownloaded)
        val priceRuleToDownload = view_.findViewById<TextView>(R.id.priceRuleToDownload)
        val priceRuleProgress = view_.findViewById<TextView>(R.id.priceRuleProgress)
        val priceRuleLastSync = view_.findViewById<TextView>(R.id.priceRuleLastSync)
        val priceRuleSyncLog = SyncLog(context).getTableInformation(PriceRules.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.PriceRules(context).getTotalRow()

        priceRuleDownload.text = rowCount.toString()
        priceRuleToDownload.text = priceRuleSyncLog!!.to_download.toString()
        priceRuleProgress.text = priceRuleSyncLog.progress
        priceRuleLastSync.text = priceRuleSyncLog.last_sync
    }

    fun paymentMethod()
    {
        val paymentMethodDownload = view_.findViewById<TextView>(R.id.paymentMethodDownloaded)
        val paymentMethodToDownload = view_.findViewById<TextView>(R.id.paymentMethodToDownload)
        val paymentMethodProgress = view_.findViewById<TextView>(R.id.paymentMethodProgress)
        val paymentMethodLastSync = view_.findViewById<TextView>(R.id.paymentMethodLastSync)
        val paymentMethodSyncLog = SyncLog(context).getTableInformation(PaymentMethod.TABLE_NAME)

        val rowCount = com.mockie.daikokuten.database.repositories.PaymentMethod(context).getTotalRow()

        paymentMethodDownload.text = rowCount.toString()
        paymentMethodToDownload.text = paymentMethodSyncLog!!.to_download.toString()
        paymentMethodProgress.text = paymentMethodSyncLog.progress
        paymentMethodLastSync.text = paymentMethodSyncLog.last_sync
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SyncStatusFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): SyncStatusFragment {
            val fragment = SyncStatusFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
