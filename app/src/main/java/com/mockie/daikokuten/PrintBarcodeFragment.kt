package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mockie.daikokuten.database.repositories.BarcodeToPrintRepository
import com.mockie.daikokuten.database.repositories.PrdCategories
import com.mockie.daikokuten.helpers.BarcodeGenerator
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.listeners.CategoryOnSelectedListener
import com.mockie.daikokuten.listeners.TypeOnSelectedListener


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PrintBarcodeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PrintBarcodeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PrintBarcodeFragment : BarcodeRecyclerBaseFragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    lateinit var categorySpinner: Spinner
    lateinit var typeSpinner: Spinner
    lateinit var sizeSpinner: Spinner
    lateinit var codeType: EditText
    lateinit var versionType: EditText
    lateinit var qtyText: EditText
    lateinit var typeRaw: StringWithTag
    lateinit var sizeRaw: StringWithTag
    lateinit var type: Any
    lateinit var size: Any
    lateinit var code: String
    lateinit var version: String
    lateinit var save:Button

    var qty: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view:View = inflater!!.inflate(R.layout.fragment_print_barcode, container, false)
        save = view.findViewById(R.id.save)
        categorySpinner = view.findViewById(R.id.categorySpinner)
        sizeSpinner = view.findViewById(R.id.sizeSpinner)
        typeSpinner = view.findViewById(R.id.typeSpinner)
        qtyText = view.findViewById(R.id.quantityText)
        codeType = view.findViewById(R.id.codeText)
        versionType = view.findViewById(R.id.versiText)


        typeSpinner.onItemSelectedListener = TypeOnSelectedListener(activity, R.layout.spinner_item, -1)

        setPermissionsRoles("stocker")
        setPermissionsRoles("developer")
        setPermissionsRoles("productEditor")
        checkPermission()

        populateSpinnerCategory()

        save.setOnClickListener {
            onCLickSave()
            save.isEnabled = false
        }

        val fab = view.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            onClickList()
        }

        return view
    }

    private fun onClickList()
    {
        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragment2 = BarcodeToPrint()
        fragmentTransaction.replace(R.id.inventoryContainer, fragment2, "print_barcode_fragment")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    private fun onCLickSave()
    {
        typeRaw = typeSpinner.getItemAtPosition(typeSpinner.selectedItemPosition) as StringWithTag
        sizeRaw = sizeSpinner.getItemAtPosition(sizeSpinner.selectedItemPosition) as StringWithTag
        code = codeType.text.toString().trim()
        version = versionType.text.toString().trim()
        qty = qtyText.text.toString().toInt()
        type = typeRaw.tag
        size = sizeRaw.tag

        if(code == "" || version == "" || qty < 1) {
            Toast.makeText(context, "Kode, Versi, qty wajib di isi ", Toast.LENGTH_LONG).show()
        } else {
            val barcodeGenerator = BarcodeGenerator(
                    context,
                    type.toString(),
                    size.toString(), code, version)

            BarcodeToPrintRepository(context).insert(
                    barcodeGenerator.getBarcode(),
                    barcodeGenerator.getBarcodeTitle(),
                    sizeRaw.toString(),
                    qty
            )
        }

        val handler = Handler()
        handler.postDelayed({
            save.isEnabled = true
        }, 500)

        onClickList()

    }

    override fun onStop() {
//        context.unregisterReceiver(mUsbPermission.getUsb())
        super.onStop()
    }

    private fun populateSpinnerCategory()
    {
        val types = PrdCategories(activity).getAllCategories()
        val listSpinner = ArrayList<StringWithTag>()

        (0 until types.count()).mapTo(listSpinner) {
            StringWithTag(types[it].readable, types[it].category)
        }

        val adapter = ArrayAdapter(activity,
                R.layout.spinner_item, listSpinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        categorySpinner.adapter = adapter
        categorySpinner.onItemSelectedListener = CategoryOnSelectedListener(activity, R.layout.spinner_item)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PrintBarcodeFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): PrintBarcodeFragment {
            val fragment = PrintBarcodeFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
