package com.mockie.daikokuten.services

/**
 * Created by mockie on 20/02/18.
 */

import android.util.Log
import com.mockie.daikokuten.manager.SharedPrefManager
import com.google.firebase.messaging.FirebaseMessagingService




/**
 * Created by mockie on 21/12/17.
 */

class FirebaseService: FirebaseMessagingService() {

    override fun onNewToken(refreshedToken: String?) {
        Log.d(":workmanager FS", refreshedToken)
        super.onNewToken(refreshedToken)

        if (refreshedToken != null)
        {
            storeToken(refreshedToken)
        }
    }

    private fun storeToken(token: String) {
        //we will save the token in sharedpreferences later
        SharedPrefManager(applicationContext).saveDeviceToken(token)
    }

}