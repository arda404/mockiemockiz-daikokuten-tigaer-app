package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class InvoiceItemToSync(val _id: Int, val invoice_id:Int, val realInvoiceId:Int, val code: String, val quantity:Int, val price:Int, val retur:Int) {
    companion object {
        const val TABLE_NAME = "invoice_item_to_sync"
        const val COLUMN_ID = "_id"
        const val COLUMN_TEMP_INVOICE_ID = "temp_invoice_id"
        const val COLUMN_REAL_INVOICE_ID = "real_invoice_id"
        const val COLUMN_ITEM_CODE = "code"
        const val COLUMN_ITEM_QUANTITY = "quantity"
        const val COLUMN_ITEM_PRICE = "price"
        const val COLUMN_ITEM_RETUR = "retur"
    }
}