package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.mockie.daikokuten.api.ItemTheData
import com.mockie.daikokuten.database.models.*
import com.mockie.daikokuten.database.models.Customer
import com.mockie.daikokuten.database.models.CustomerAddress
import com.mockie.daikokuten.database.models.Invoices
import com.mockie.daikokuten.models.SearchKeyword
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 18/12/17.
 */

class Invoices(override val context: Context):BaseRepository(context) {

    fun insert(
            id:Int,
            customerId:Int,
            addressId:Int,
            shippingCharge:Int?,
            totalBilling:Int,
            status:Int,
            store:String?,
            note:String?,
            userId:Int,
            createdAt:String,
            updatedAt:String,
            additionalPricePerItem: Int,
            meta:String,
            itemList: List<ItemTheData>
    )
    {

        getDbInstance().use {

            insert(Invoices.TABLE_NAME,
                    Invoices.COLUMN_ID to id,
                    Invoices.COLUMN_CUSTOMER_ID to customerId,
                    Invoices.COLUMN_ADDRESS_ID to addressId,
                    Invoices.COLUMN_SHIPPING_CHARGE to shippingCharge,
                    Invoices.COLUMN_TOTAL_BILLING to totalBilling,
                    Invoices.COLUMN_STATUS to status,
                    Invoices.COLUMN_STORE to store,
                    Invoices.COLUMN_NOTE to note,
                    Invoices.COLUMN_USER_ID to userId,
                    Invoices.COLUMN_CREATED_AT to createdAt,
                    Invoices.COLUMN_UPDATED_AT to updatedAt,
                    Invoices.COLUMN_ADDITIONAL_PRICE_PER_ITEM to additionalPricePerItem,
                    Invoices.COLUMN_META to meta
            ).toInt()

            InvoiceItem(context).removeInvoiceItemByInvoiceId(id)

            for (a in 0 until itemList.size)
            {
                InvoiceItem(context).insert(itemList[a].id, itemList[a].invoice_id, itemList[a].code, itemList[a].quantity, itemList[a].price, itemList[a].retur)
            }
        }
    }

    fun update(
            id:Int,
            customerId:Int,
            addressId:Int,
            shippingCharge:Int,
            totalBilling:Int,
            status:Int,
            store:String?,
            note:String?,
            userId:Int,
            createdAt:String,
            updatedAt:String,
            additionalPricePerItem:Int,
            meta:String,
            itemList: List<ItemTheData>
    )
    {
        getDbInstance().use {

            try {
                transaction {

                    val args = ContentValues()
                    args.put(Invoices.COLUMN_CUSTOMER_ID, customerId)
                    args.put(Invoices.COLUMN_ADDRESS_ID, addressId)
                    args.put(Invoices.COLUMN_SHIPPING_CHARGE, shippingCharge)
                    args.put(Invoices.COLUMN_TOTAL_BILLING, totalBilling)
                    args.put(Invoices.COLUMN_STATUS, status)
                    args.put(Invoices.COLUMN_STORE, store)
                    args.put(Invoices.COLUMN_NOTE, note)
                    args.put(Invoices.COLUMN_USER_ID, userId)
                    args.put(Invoices.COLUMN_CREATED_AT, createdAt)
                    args.put(Invoices.COLUMN_UPDATED_AT, updatedAt)
                    args.put(Invoices.COLUMN_UPDATED_AT, updatedAt)
                    args.put(Invoices.COLUMN_ADDITIONAL_PRICE_PER_ITEM, additionalPricePerItem)
                    args.put(Invoices.COLUMN_META, meta)

                    update(Invoices.TABLE_NAME, args, Invoices.COLUMN_ID + "=" + id, null)

                    InvoiceItem(context).removeInvoiceItemByInvoiceId(id)

                    for (a in 0 until itemList.size)
                    {
                        InvoiceItem(context).insert(itemList[a].id, itemList[a].invoice_id, itemList[a].code, itemList[a].quantity, itemList[a].price, itemList[a].retur)
                    }

                }
            } catch (e:Exception) {}

        }
    }


    fun removeInvoiceByInvoiceId(invoiceId:Int)
    {
        getDbInstance().use {

            try {
                transaction {

                    delete(Invoices.TABLE_NAME,
                            Invoices.COLUMN_ID + "=" + invoiceId)

                    InvoiceItem(context).removeInvoiceItemByInvoiceId(invoiceId)

                }
            } catch (e:Exception) {}

        }
    }

    fun getInvoiceListData(offset:Int, limit:Int, searchKeyword: SearchKeyword): ArrayList<InvoicesCustomerAddressRelationship>
    {
        val db = getDbInstance().readableDatabase
        val data: ArrayList<InvoicesCustomerAddressRelationship> = ArrayList()

        var columns = Invoices.TABLE_NAME+"."+Invoices.COLUMN_ID + " AS invoice_id,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_USER_ID + " AS user_id,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_CUSTOMER_ID + " AS cus_customer_id,"
        columns += Customer.TABLE_NAME+"."+Customer.COLUMN_NAME + " AS customer_name,"
        columns += Customer.TABLE_NAME+"."+Customer.COLUMN_PHONE + " AS customer_phone,"
        columns += Invoices.COLUMN_ADDRESS_ID + " AS address_id,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_NAME + " AS address_name,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_PHONE + " AS address_phone,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_ADDRESS + ","
        columns += Invoices.COLUMN_SHIPPING_CHARGE + ","
        columns += Invoices.COLUMN_TOTAL_BILLING + ","
        columns += Invoices.COLUMN_STATUS + ","
        columns += Invoices.COLUMN_NOTE + ","
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_CREATED_AT + " as inv_created_at,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_UPDATED_AT + " as inv_updated_at,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_ADDITIONAL_PRICE_PER_ITEM + " as inv_additional_price_per_item,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_META + " as inv_meta,"
        columns += Customer.TABLE_NAME+"."+Customer.COLUMN_ID + " AS rel_customer_id,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_ID + " AS rel_address_id"

        var query = "SELECT " + columns + " FROM " + Invoices.TABLE_NAME
        query += " LEFT JOIN " + Customer.TABLE_NAME + " ON " + "cus_customer_id = rel_customer_id"
        query += " LEFT JOIN " + CustomerAddress.TABLE_NAME + " ON " + "address_id = rel_address_id"

        query += " WHERE " + Invoices.COLUMN_STATUS + "!=1000 "

        if (searchKeyword.customerName != "") {
            query += " AND " + Customer.COLUMN_NAME + " LIKE '%" + searchKeyword.customerName + "%'"
        }

        if (searchKeyword.customerPhone != "") {
            query += " AND " + Customer.COLUMN_NAME + " = '" + searchKeyword.customerPhone + "'"
        }

        if (searchKeyword.status != 2000)
        {
            query += " AND " + Invoices.COLUMN_STATUS + " = " + searchKeyword.status
        }

        query += " ORDER BY invoice_id DESC LIMIT " + offset.toString() + "," + limit.toString()

        val sql:Cursor = db.rawQuery(query, null)

        if(sql.moveToFirst()) {
            do {

                data.add(InvoicesCustomerAddressRelationship(
                        sql.getInt(sql.getColumnIndex("invoice_id")),
                        sql.getInt(sql.getColumnIndex("user_id")),
                        sql.getInt(sql.getColumnIndex("cus_customer_id")),
                        sql.getString(sql.getColumnIndex("customer_name")),
                        sql.getString(sql.getColumnIndex("customer_phone")),
                        sql.getInt(sql.getColumnIndex("address_id")),
                        sql.getString(sql.getColumnIndex("address_name")),
                        sql.getString(sql.getColumnIndex("address_phone")),
                        sql.getString(sql.getColumnIndex(CustomerAddress.COLUMN_ADDRESS)),
                        sql.getString(sql.getColumnIndex(Invoices.COLUMN_SHIPPING_CHARGE)).toDouble(),
                        sql.getString(sql.getColumnIndex(Invoices.COLUMN_TOTAL_BILLING)).toDouble(),
                        sql.getInt(sql.getColumnIndex(Invoices.COLUMN_STATUS)),
                        sql.getString(sql.getColumnIndex(Invoices.COLUMN_NOTE)),
                        sql.getString(sql.getColumnIndex("inv_created_at")),
                        sql.getString(sql.getColumnIndex("inv_updated_at")),
                        sql.getInt(sql.getColumnIndex("inv_additional_price_per_item")),
                        sql.getString(sql.getColumnIndex("inv_meta"))
                ))

            }while(sql.moveToNext())
        }

        sql.close()

        return data
    }

    fun getInvoiceById(id:Int): Invoices?
    {
        return getDbInstance().use {

            var columns = Invoices.COLUMN_ID + "," + Invoices.COLUMN_CUSTOMER_ID + ","
            columns += Invoices.COLUMN_ADDRESS_ID + "," + Invoices.COLUMN_SHIPPING_CHARGE + ","
            columns += Invoices.COLUMN_TOTAL_BILLING + "," + Invoices.COLUMN_STATUS + ","
            columns += Invoices.COLUMN_STORE + "," + Invoices.COLUMN_NOTE + ","
            columns += Invoices.COLUMN_USER_ID + ","
            columns += Invoices.COLUMN_CREATED_AT + "," + Invoices.COLUMN_UPDATED_AT + ","
            columns += Invoices.COLUMN_ADDITIONAL_PRICE_PER_ITEM + ","
            columns += Invoices.COLUMN_META

            val query = select(Invoices.TABLE_NAME, columns)
                    .whereArgs("(" + Invoices.COLUMN_ID  + "={id})",
                            "id" to id)

            query.exec {
                val rowParser = classParser<Invoices>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun getInvoiceWithRelationsById(id:Int):InvoicesCustomerAddressRelationship?
    {
        val db = getDbInstance().readableDatabase
        var data:InvoicesCustomerAddressRelationship? = null

        var columns = Invoices.TABLE_NAME+"."+Invoices.COLUMN_ID + " AS invoice_id,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_USER_ID + " AS user_id,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_CUSTOMER_ID + " AS cus_customer_id,"
        columns += Customer.TABLE_NAME+"."+Customer.COLUMN_NAME + " AS customer_name,"
        columns += Customer.TABLE_NAME+"."+Customer.COLUMN_PHONE + " AS customer_phone,"
        columns += Invoices.COLUMN_ADDRESS_ID + " AS address_id,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_NAME + " AS address_name,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_PHONE + " AS address_phone,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_ADDRESS + ","
        columns += Invoices.COLUMN_SHIPPING_CHARGE + ","
        columns += Invoices.COLUMN_TOTAL_BILLING + ","
        columns += Invoices.COLUMN_STATUS + ","
        columns += Invoices.COLUMN_NOTE + ","
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_CREATED_AT + " as inv_created_at,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_UPDATED_AT + " as inv_updated_at,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_ADDITIONAL_PRICE_PER_ITEM + " as inv_additional_price_per_item,"
        columns += Invoices.TABLE_NAME+"."+Invoices.COLUMN_META + " as inv_meta,"
        columns += Customer.TABLE_NAME+"."+Customer.COLUMN_ID + " AS rel_customer_id,"
        columns += CustomerAddress.TABLE_NAME+"."+CustomerAddress.COLUMN_ID + " AS rel_address_id"

        var query = "SELECT " + columns + " FROM " + Invoices.TABLE_NAME
        query += " LEFT JOIN " + Customer.TABLE_NAME + " ON " + "cus_customer_id = rel_customer_id"
        query += " LEFT JOIN " + CustomerAddress.TABLE_NAME + " ON " + "address_id = rel_address_id  WHERE invoice_id =" + id

        val sql:Cursor = db.rawQuery(query, null)

        if(sql.moveToFirst()) {
            data = InvoicesCustomerAddressRelationship(
                    sql.getInt(sql.getColumnIndex("invoice_id")),
                    sql.getInt(sql.getColumnIndex("user_id")),
                    sql.getInt(sql.getColumnIndex("cus_customer_id")),
                    sql.getString(sql.getColumnIndex("customer_name")),
                    sql.getString(sql.getColumnIndex("customer_phone")),
                    sql.getInt(sql.getColumnIndex("address_id")),
                    sql.getString(sql.getColumnIndex("address_name")),
                    sql.getString(sql.getColumnIndex("address_phone")),
                    sql.getString(sql.getColumnIndex(CustomerAddress.COLUMN_ADDRESS)),
                    sql.getString(sql.getColumnIndex(Invoices.COLUMN_SHIPPING_CHARGE)).toDouble(),
                    sql.getString(sql.getColumnIndex(Invoices.COLUMN_TOTAL_BILLING)).toDouble(),
                    sql.getInt(sql.getColumnIndex(Invoices.COLUMN_STATUS)),
                    sql.getString(sql.getColumnIndex(Invoices.COLUMN_NOTE)),
                    sql.getString(sql.getColumnIndex("inv_created_at")),
                    sql.getString(sql.getColumnIndex("inv_updated_at")),
                    sql.getInt(sql.getColumnIndex("inv_additional_price_per_item")),
                    sql.getString(sql.getColumnIndex("inv_meta"))
            )
        }

        sql.close()

        return data
    }

    fun truncateInvoice()
    {
        getDbInstance().use {

            try {
                transaction {
                    delete(Invoices.TABLE_NAME)

                    SyncLog(context).resetTableLastSync(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME)
                }
            } catch (e:Exception) {}

        }
    }

    fun getTotalRow(): Int
    {

        return getDbInstance().use {

            val query = select(Invoices.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount

        }

    }
}