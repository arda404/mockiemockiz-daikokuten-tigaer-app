package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import com.mockie.daikokuten.database.models.PrdCategories
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 18/12/17.
 */

class PrdCategories(override val context: Context):BaseRepository(context) {

    fun insert( category: String,
                               readable: String,
                               sort: Int,
                               updated_at: String): Int
    {
        return getDbInstance().use {
            insert(PrdCategories.TABLE_NAME,
                    PrdCategories.COLUMN_CATEGORY to category,
                    PrdCategories.COLUMN_READABLE to readable,
                    PrdCategories.COLUMN_SORT to sort,
                    PrdCategories.COLUMN_UPDATED_AT to updated_at
            ).toInt()
        }
    }

    fun update(category: String, readable: String, sort: Int, updated_at: String)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(PrdCategories.COLUMN_READABLE, readable)
            args.put(PrdCategories.COLUMN_SORT, sort)
            args.put(PrdCategories.COLUMN_UPDATED_AT, updated_at)

            update(PrdCategories.TABLE_NAME, args,PrdCategories.COLUMN_CATEGORY +"=\"" + category + "\"", null)
        }
    }

    fun getAllCategories(): List<PrdCategories>
    {
        return getDbInstance().use {
            var columns = PrdCategories.COLUMN_CATEGORY + ","
            columns = columns + PrdCategories.COLUMN_READABLE + ","
            columns = columns + PrdCategories.COLUMN_SORT + ","
            columns += PrdCategories.COLUMN_UPDATED_AT

            val query = select(PrdCategories.TABLE_NAME, columns)
                    .orderBy(PrdCategories.COLUMN_READABLE + " ASC ")

            query.exec {
                val rowParser = classParser<PrdCategories>()
                parseList(rowParser)
            }
        }
    }

    fun getCategoryById(id: String): PrdCategories?
    {
        return getDbInstance().use {
            var columns = PrdCategories.COLUMN_CATEGORY + ","
            columns = columns + PrdCategories.COLUMN_READABLE + ","
            columns = columns + PrdCategories.COLUMN_SORT + ","
            columns += PrdCategories.COLUMN_UPDATED_AT

            val query = select(PrdCategories.TABLE_NAME, columns)
                    .whereArgs( "(" + PrdCategories.COLUMN_CATEGORY +" = {category})",
                            "category" to id)

            query.exec {
                val rowParser = classParser<PrdCategories>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(PrdCategories.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

}