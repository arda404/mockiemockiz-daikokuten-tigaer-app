package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PrdTypes(
        val type: String,
        val readable: String,
        val patterned: Int,
        val sorter: Int,
        val appactive: Int,
        val categories: String,
        val updated_at: String
) {
    companion object {
        const val TABLE_NAME = "prdtypes"
        const val COLUMN_TYPE = "type"
        const val COLUMN_READABLE = "readable"
        const val COLUMN_PATTERNED = "patterned"
        const val COLUMN_SORTER = "sorter"
        const val COLUMN_APPACTIVE = "appactive"
        const val COLUMN_CATEGORIES = "categories"
        const val COLUMN_UPDATED_AT = "updated_at"
    }
}