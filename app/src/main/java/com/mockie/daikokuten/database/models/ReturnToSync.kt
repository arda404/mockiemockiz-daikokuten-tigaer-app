package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class ReturnToSync(
        val localCreatedAt:String,
        val _id:Int,
        val userId:Int,
        val store:String,
        val customerId:Int,
        val customerPhone:String,
        val tempInvoiceId:Int,
        val realInvoiceId:Int,
        val barcode:String,
        val quantity:Int,
        val calculatedPrice:Int
) {
    companion object {
        const val TABLE_NAME = "return_to_sync"
        const val COLUMN_LOCAL_CREATED_AT = "local_created_at"
        const val COLUMN_ID = "id"
        const val COLUMN_USER_ID = "user_id"
        const val COLUMN_STORE = "store"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_CUSTOMER_PHONE = "customer_phone"
        const val COLUMN_TEMP_INVOICE_ID = "temp_invoice_id"
        const val COLUMN_REAL_INVOICE_ID = "real_invoice_id"
        const val COLUMN_BARCODE = "barcode"
        const val COLUMN_QUANTITY = "quantity"
        const val COLUMN_CALCULATED_PRICE = "calculated_price"
    }
}