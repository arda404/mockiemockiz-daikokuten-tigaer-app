package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class User(

        val uid: Int,
        val name: String,
        val balance: Long,
        val api_key: String,
        val adminCaps: String,
        val store: String,
        val api_base_url: String,
        val email:String) {

    companion object {
        const val TABLE_NAME = "users"
        const val COLUMN_ID = "uid"
        const val COLUMN_NAME = "name"
        const val COLUMN_BALANCE = "balance"
        const val COLUMN_API_KEY = "api_key"
        const val COLUMN_API_BASE_URL = "api_base_url"
        const val COLUMN_ADMINCAPS = "admincaps"
        const val COLUMN_STORE = "store"
        const val COLUMN_EMAIL = "email"
    }
}