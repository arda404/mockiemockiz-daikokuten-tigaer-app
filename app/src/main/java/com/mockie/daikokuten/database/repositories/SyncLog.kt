package com.mockie.daikokuten.database.repositories

import android.content.Context
import org.jetbrains.anko.db.*
import com.mockie.daikokuten.database.models.SyncLog as SyncLogModel

/**
 * Created by mockie on 18/12/17.
 */

class SyncLog(override val context: Context):BaseRepository(context) {

    fun add(last_sync: Int): Int
    {
        return getDbInstance().use {
            insert(SyncLogModel.TABLE_NAME,
                    SyncLogModel.COLUMN_LAST_SYNC to last_sync
            ).toInt()
        }
    }

    fun getTableInformation(tableName: String): SyncLogModel?
    {
        return getDbInstance().use {
            var columns = SyncLogModel.COLUMN_ID + "," + SyncLogModel.COLUMN_TABLE_NAME + "," + SyncLogModel.COLUMN_LAST_SYNC
            columns += "," + SyncLogModel.COLUMN_LAST_SYNC_TEMP + "," + SyncLogModel.COLUMN_PROGRESS + ","
            columns += SyncLogModel.COLUMN_TO_DOWNLOAD + "," + SyncLogModel.COLUMN_NEXT_PAGE + ","
            columns += SyncLogModel.COLUMN_UNTIL_TIME

            val query = select(SyncLogModel.TABLE_NAME, columns)
                    .whereArgs("(table_name = '$tableName')")

            query.exec {
                val rowParser = classParser<SyncLogModel>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun resetTableLastSync(tableName: String)
    {
        getDbInstance().use {
            update(SyncLogModel.TABLE_NAME, SyncLogModel.COLUMN_LAST_SYNC to "0",
                    SyncLogModel.COLUMN_LAST_SYNC_TEMP to "0",
                    SyncLogModel.COLUMN_NEXT_PAGE to 0,
                    SyncLogModel.COLUMN_PROGRESS to "100%",
                    SyncLogModel.COLUMN_TO_DOWNLOAD to 0,
                    SyncLogModel.COLUMN_NEXT_PAGE to 0,
                    SyncLogModel.COLUMN_UNTIL_TIME to ""
            )
                    .whereArgs(SyncLogModel.COLUMN_TABLE_NAME + "='" + tableName +"'").exec()
        }
    }

    fun updateTableLastSync(tableName: String, lastSync: String?)
    {
        getDbInstance().use {
            update(SyncLogModel.TABLE_NAME, SyncLogModel.COLUMN_LAST_SYNC to lastSync)
                    .whereArgs(SyncLogModel.COLUMN_TABLE_NAME + "='" + tableName +"'").exec()
        }
    }

    fun updateTableLastSyncTemp(tableName: String, lastSyncTemp: String?)
    {
        getDbInstance().use {
            update(SyncLogModel.TABLE_NAME, SyncLogModel.COLUMN_LAST_SYNC_TEMP to lastSyncTemp)
                    .whereArgs(SyncLogModel.COLUMN_TABLE_NAME + "='" + tableName +"'").exec()
        }
    }

    fun updateTableNextPage(tableName: String, nextPage: Int)
    {
        getDbInstance().use {
            update(SyncLogModel.TABLE_NAME, SyncLogModel.COLUMN_NEXT_PAGE to nextPage)
                    .whereArgs(SyncLogModel.COLUMN_TABLE_NAME + "='" + tableName +"'").exec()
        }
    }

    fun updateTableProgress(tableName: String, progress: String)
    {
        getDbInstance().use {
            update(SyncLogModel.TABLE_NAME, SyncLogModel.COLUMN_PROGRESS to progress)
                    .whereArgs(SyncLogModel.COLUMN_TABLE_NAME + "='" + tableName +"'").exec()
        }
    }

    fun updateTableToDowload(tableName: String, toDownload: Int)
    {
        getDbInstance().use {
            update(SyncLogModel.TABLE_NAME, SyncLogModel.COLUMN_TO_DOWNLOAD to toDownload)
                    .whereArgs(SyncLogModel.COLUMN_TABLE_NAME + "='" + tableName +"'").exec()
        }
    }

    fun update(tableName: String, lastSync: String, lastSyncTemp: String, nextPage:Int)
    {
        getDbInstance().use {
            update(
                    SyncLogModel.TABLE_NAME, SyncLogModel.COLUMN_LAST_SYNC to lastSync,
                    SyncLogModel.COLUMN_LAST_SYNC_TEMP to lastSyncTemp,
                    SyncLogModel.COLUMN_NEXT_PAGE to nextPage
            )
                    .whereArgs(SyncLogModel.COLUMN_TABLE_NAME + "='" + tableName +"'").exec()
        }
    }
}