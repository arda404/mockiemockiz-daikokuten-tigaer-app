package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PrintBarcode(val _id: Int, val table_name:String, val last_sync: String) {
    companion object {
        const val TABLE_NAME = "sync_log"
        const val COLUMN_ID = "_id"
        const val COLUMN_TABLE_NAME = "table_name"
        const val COLUMN_LAST_SYNC = "last_sync"
    }    
}