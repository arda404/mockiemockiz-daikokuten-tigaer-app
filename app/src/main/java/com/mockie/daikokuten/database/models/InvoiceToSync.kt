package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class InvoiceToSync(
        val invoice_id: Int,
        val real_invoice_id: Int,
        val user_id:Int,
        val store:String,
        val customer_id:Int,
        val customer_name: String,
        val customer_phone:String,
        val address_id:Int,
        val address_name:String,
        val address_phone:String,
        val address:String,
        val note:String,
        val status: Int,
        val shipping_fee: Double,
        val total_billing: Double,
        val additionalPricePerItem: Int,
        val lockDate:String?,
        val localCreatedAt:String?
) {
    companion object {
        const val TABLE_NAME = "invoice_to_sync"
        const val COLUMN_INVOICE_ID = "invoice_id"
        const val COLUMN_REAL_INVOICE_ID = "real_invoice_id"
        const val COLUMN_USER_ID = "user_id"
        const val COLUMN_STORE = "store"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_CUSTOMER_NAME = "customer_name"
        const val COLUMN_CUSTOMER_PHONE = "customer_phone"
        const val COLUMN_ADDRESS_ID = "address_id"
        const val COLUMN_ADDRESS_NAME = "address_name"
        const val COLUMN_ADDRESS_PHONE = "address_phone"
        const val COLUMN_ADDRESS = "address"
        const val COLUMN_NOTE = "note"
        const val COLUMN_STATUS = "status"
        const val COLUMN_SHIPPING_FEE = "shipping_fee"
        const val COLUMN_TOTAL_BILLING = "total_billing"
        const val COLUMN_ADDITIONAL_PRICE_PER_ITEM = "additional_price_per_item"
        const val COLUMN_LOCK_DATE = "lock_date"
        const val COLUMN_LOCAL_CREATED_AT = "local_created_at"
    }
}