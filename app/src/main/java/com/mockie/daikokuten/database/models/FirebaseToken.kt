package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class FirebaseToken(
        val token: String) {
    companion object {
        const val TABLE_NAME = "firebase_token"
        const val COLUMN_TOKEN = "token"
    }
}