package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class DraftBarcode(
        val pageName: String,
        val barcode: String,
        val qty: Int,
        val meta: String,
        val price:Int) {

    companion object {
        const val TABLE_NAME = "draft_barcode"
        const val COLUMN_ID = "id"
        const val COLUMN_PAGE_NAME = "page_name"
        const val COLUMN_BARCODE = "barcode"
        const val COLUMN_QTY = "qty"
        const val COLUMN_META = "meta"
        const val COLUMN_PRICE = "price"
    }
}