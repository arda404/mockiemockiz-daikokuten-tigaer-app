package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class MovedInventory(
        val localCreatedAt:String,
        val id:Int,
        val code: String,
        val quantity: Int,
        val oldStore: String,
        val newStore: String
) {

    companion object {
        const val TABLE_NAME = "moved_inventory"
        const val COLUMN_ID = "id"
        const val COLUMN_CODE = "code"
        const val COLUMN_QUANTITY = "quantity"
        const val COLUMN_OLD_STORE = "old_store"
        const val COLUMN_NEW_STORE = "new_store"
        const val COLUMN_LOCAL_CREATED_AT = "local_created_at"
    }
}