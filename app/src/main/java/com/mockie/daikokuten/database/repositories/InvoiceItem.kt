package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.InvoiceItems
import org.jetbrains.anko.db.*

class InvoiceItem (override val context: Context): BaseRepository(context) {

    fun insert(id: Int, invoiceId:Int, code:String, qty:Int, price:Int, retur:Int):Int
    {
        return getDbInstance().use {

            insert(InvoiceItems.TABLE_NAME,
                    InvoiceItems.COLUMN_ID to id,
                    InvoiceItems.COLUMN_INVOICE_ID to invoiceId,
                    InvoiceItems.COLUMN_CODE to code,
                    InvoiceItems.COLUMN_QUANTITY to qty,
                    InvoiceItems.COLUMN_PRICE to price,
                    InvoiceItems.COLUMN_RETUR to retur).toInt()

        }
    }

    fun removeInvoiceItemByInvoiceId(invoiceId:Int)
    {
        getDbInstance().use {

            delete(InvoiceItems.TABLE_NAME,
                    InvoiceItems.COLUMN_INVOICE_ID + "=" + invoiceId)

        }
    }

    fun getInvoiceItemsByInvoiceId(invoiceId: Int): List<InvoiceItems>
    {

        return getDbInstance().use {

            var columns = InvoiceItems.COLUMN_ID + ", "
            columns += InvoiceItems.COLUMN_INVOICE_ID + ", "
            columns += InvoiceItems.COLUMN_CODE + ", "
            columns += InvoiceItems.COLUMN_QUANTITY + ","
            columns += InvoiceItems.COLUMN_PRICE + ","
            columns += InvoiceItems.COLUMN_RETUR

            val query = select(InvoiceItems.TABLE_NAME, columns)

            query.whereArgs(InvoiceItems.COLUMN_INVOICE_ID + "={invoiceId}", "invoiceId" to invoiceId)
            query.exec {
                val rowParser = classParser<InvoiceItems>()
                parseList(rowParser)
            }

        }

    }

}

