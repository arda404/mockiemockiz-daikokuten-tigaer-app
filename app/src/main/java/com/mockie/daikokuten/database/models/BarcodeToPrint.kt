package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class BarcodeToPrint(
        val id: Int,
        val barcode: String,
        val meta: String,
        val size: String,
        val qty: Int
) {
    companion object {
        const val TABLE_NAME = "barcode_to_print"
        const val COLUMN_ID = "id"
        const val COLUMN_BARCODE = "barcode"
        const val COLUMN_META = "meta"
        const val COLUMN_SIZE = "size"
        const val COLUMN_QTY = "qty"
    }
}