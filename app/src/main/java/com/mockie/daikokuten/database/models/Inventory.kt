package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class Inventory(
        val id: Int,
        val code: String,
        val quantity: Int,
        val store: String,
        val localCreatedAt:String
) {

    companion object {
        const val TABLE_NAME = "inventory"
        const val COLUMN_ID = "id"
        const val COLUMN_CODE = "code"
        const val COLUMN_QUANTITY = "quantity"
        const val COLUMN_STORE = "store"
        const val COLUMN_LOCAL_CREATED_AT = "local_created_at"
    }
}