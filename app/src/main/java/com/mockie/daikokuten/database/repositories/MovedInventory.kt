package com.mockie.daikokuten.database.repositories

import android.content.Context
import android.util.Log
import com.mockie.daikokuten.database.models.Inventory
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*
import java.text.SimpleDateFormat
import java.util.*
import com.mockie.daikokuten.database.models.SyncLog as SyncLogModel
import com.mockie.daikokuten.database.models.MovedInventory as MI


/**
 * Created by mockie on 18/12/17.
 */

class MovedInventory(override val context: Context):BaseRepository(context) {

    fun insert(barcode:String, currentStore:String, newStore:String, qty:Int): Int
    {
        val cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok"))
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val localInsertedAt = format1.format(cal.timeInMillis)

        return getDbInstance().use {

            insert(MI.TABLE_NAME,
                    MI.COLUMN_CODE to barcode,
                    MI.COLUMN_QUANTITY to qty,
                    MI.COLUMN_OLD_STORE to currentStore,
                    MI.COLUMN_NEW_STORE to newStore,
                    MI.COLUMN_LOCAL_CREATED_AT to localInsertedAt
            ).toInt()

        }
    }

    fun save(barcode:String, currentStore:String, newStore:String, qty:Int)
    {
        Log.d("lolo", barcode + " | " + currentStore + " | " + newStore + " | " + qty.toString())
        insert(barcode, currentStore, newStore, qty)
    }

    fun getCount(): Int
    {
        return getDbInstance().use {

            val query = select(MI.TABLE_NAME, "count(*) AS rowCount")
                    .limit(10)

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount

        }
    }

    private fun getItemByCodeAndStore(code: String, oldStore: String, newStore: String): MI?
    {
        return getDbInstance().use {

            val columns = MI.COLUMN_LOCAL_CREATED_AT + "," +  MI.COLUMN_ID + "," + MI.COLUMN_CODE + "," + MI.COLUMN_QUANTITY + "," +  MI.COLUMN_OLD_STORE + "," + MI.COLUMN_NEW_STORE
            val query = select(MI.TABLE_NAME, columns)
                    .whereArgs("(" + MI.COLUMN_CODE +" = {code}) AND ("+ MI.COLUMN_OLD_STORE + " = {old_store}) AND (" + MI.COLUMN_NEW_STORE + "={new_store})",
                            "code" to code,
                            "old_store" to oldStore,
                            "new_store" to newStore)

            query.exec {
                val rowParser = classParser<MI>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    private fun increaseQuantity(code: String, oldStore:String, newStore:String, qty: Int)
    {
        val data = this.getItemByCodeAndStore(code, oldStore, newStore)

        getDbInstance().use {
            val quantity = data!!.quantity + qty

            val query = update(MI.TABLE_NAME, MI.COLUMN_QUANTITY to quantity)
                    .whereArgs(MI.COLUMN_CODE + " = '{code}' AND " + MI.COLUMN_OLD_STORE + "={old_store} AND " + MI.COLUMN_NEW_STORE + "={new_store}",
                            "code" to code,
                            "old_store" to oldStore,
                            "new_store" to newStore)

            query.exec()
        }

    }

    fun getDataToSync(): MI?
    {
        return getDbInstance().use {
            var columns = MI.COLUMN_LOCAL_CREATED_AT + "," + MI.COLUMN_ID + "," + MI.COLUMN_CODE + "," + MI.COLUMN_QUANTITY + ","
            columns += MI.COLUMN_OLD_STORE + "," + MI.COLUMN_NEW_STORE

            val query = select(MI.TABLE_NAME, columns)
                    .limit(1).orderBy(MI.COLUMN_ID, SqlOrderDirection.ASC)

            query.exec {
                val rowParser = classParser<MI>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun removeMovedInventoryById(id:Int)
    {
        getDbInstance().use {
            delete(MI.TABLE_NAME, MI.COLUMN_ID + "=" + id)
        }
    }
}