package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import com.mockie.daikokuten.database.models.CustomerAddress
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.select


class CustomerAddress(override val context: Context):BaseRepository(context){

    fun insert(id:Int, customerId:Int, name: String, phone: String, address:String, createdAt: String, updatedAt: String): Int
    {
        return getDbInstance().use {

            insert(CustomerAddress.TABLE_NAME,
                    CustomerAddress.COLUMN_ID to id,
                    CustomerAddress.COLUMN_CUSTOMER_ID to customerId,
                    CustomerAddress.COLUMN_NAME to name,
                    CustomerAddress.COLUMN_PHONE to phone,
                    CustomerAddress.COLUMN_ADDRESS to address,
                    CustomerAddress.COLUMN_CREATED_AT to createdAt,
                    CustomerAddress.COLUMN_UPDATED_AT to updatedAt
            ).toInt()

        }
    }

    fun findCustomerAddressByNameAndCustomerId(name:String, customerId:Int): List<CustomerAddress>
    {
        return getDbInstance().use {

            var columns = CustomerAddress.COLUMN_ID + "," + CustomerAddress.COLUMN_CUSTOMER_ID + "," + CustomerAddress.COLUMN_NAME + ","
            columns += CustomerAddress.COLUMN_PHONE + "," + CustomerAddress.COLUMN_ADDRESS + "," + CustomerAddress.COLUMN_CREATED_AT + "," + CustomerAddress.COLUMN_UPDATED_AT

            val query = select(CustomerAddress.TABLE_NAME, columns)
                    .whereArgs( "(" + CustomerAddress.COLUMN_NAME +" LIKE {name}) AND ("  + CustomerAddress.COLUMN_CUSTOMER_ID +" = {customerId})",
                            "name" to "%$name%",
                            "customerId" to customerId)

                query.exec {
                val rowParser = classParser<CustomerAddress>()
                parseList(rowParser)
            }

        }
    }

    fun getCustomerAddressById(id:Int): CustomerAddress?
    {
        return getDbInstance().use {

            var columns = CustomerAddress.COLUMN_ID + "," + CustomerAddress.COLUMN_CUSTOMER_ID + "," + CustomerAddress.COLUMN_NAME + ","
            columns += CustomerAddress.COLUMN_PHONE + "," + CustomerAddress.COLUMN_ADDRESS + "," + CustomerAddress.COLUMN_CREATED_AT + "," + CustomerAddress.COLUMN_UPDATED_AT

            val query = select(CustomerAddress.TABLE_NAME, columns)
                    .whereArgs("(" + CustomerAddress.COLUMN_ID  + "={id})",
                            "id" to id)

            query.exec {
                val rowParser = classParser<CustomerAddress>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun update(_id: Int, customerId:Int, name:String, phone: String, address: String, created_at:String, updated_at:String)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(CustomerAddress.COLUMN_CUSTOMER_ID, customerId)
            args.put(CustomerAddress.COLUMN_NAME, name)
            args.put(CustomerAddress.COLUMN_PHONE, phone)
            args.put(CustomerAddress.COLUMN_ADDRESS, address)
            args.put(CustomerAddress.COLUMN_CREATED_AT, created_at)
            args.put(CustomerAddress.COLUMN_UPDATED_AT, updated_at)

            update(CustomerAddress.TABLE_NAME, args, CustomerAddress.COLUMN_ID + "=" + _id, null)
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {

            val query = select(CustomerAddress.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount

        }
    }

}