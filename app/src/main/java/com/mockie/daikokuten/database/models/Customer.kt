package com.mockie.daikokuten.database.models

import java.math.BigDecimal

/**
 * Created by mockie on 18/12/17.
 */

data class Customer(val _id: Int, val name:String, val phone: String, val balance: Double, val created_at:String, val updated_at:String) {
    companion object {
        const val TABLE_NAME = "customers"
        const val COLUMN_ID = "_id"
        const val COLUMN_NAME = "name"
        const val COLUMN_PHONE = "phone"
        const val COLUMN_BALANCE = "balance"
        const val COLUMN_CREATED_AT = "created_at"
        const val COLUMN_UPDATED_AT = "updated_at"
    }
}