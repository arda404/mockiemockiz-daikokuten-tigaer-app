package com.mockie.daikokuten.database.repositories

import android.content.Context
import android.widget.Toast
import com.mockie.daikokuten.database.models.Inventory
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import com.mockie.daikokuten.database.models.SyncLog as SyncLogModel

/**
 * Created by mockie on 18/12/17.
 */

class Inventory(override val context: Context): BaseRepository(context) {

    fun insert(inventory: Inventory): Int {

        return getDbInstance().use {

            val cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok"))
            val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val localInsertedAt = format1.format(cal.timeInMillis)

                insert(Inventory.TABLE_NAME,
                    Inventory.COLUMN_CODE to inventory.code,
                    Inventory.COLUMN_QUANTITY to inventory.quantity,
                    Inventory.COLUMN_STORE to inventory.store,
                    Inventory.COLUMN_LOCAL_CREATED_AT to localInsertedAt
            ).toInt()

        }

    }

    fun getItemByCodeAndStore(code: String, store:String): Inventory?
    {
        return getDbInstance().use {

            var columns = Inventory.COLUMN_ID + "," + Inventory.COLUMN_CODE + "," + Inventory.COLUMN_QUANTITY + "," + Inventory.COLUMN_STORE
            columns += "," + Inventory.COLUMN_LOCAL_CREATED_AT

            val query = select(Inventory.TABLE_NAME, columns)
                    .whereArgs("(" + Inventory.COLUMN_CODE +" = {code}) AND (" + Inventory.COLUMN_STORE + " = {store})",
                            "code" to code,
                            "store" to store)

            query.exec {
                val rowParser = classParser<Inventory>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun increaseQuantity(code: String, store:String, qty: Int)
    {
        val data = this.getItemByCodeAndStore(code, store)
        getDbInstance().use {

            val quantity = if (data == null) qty else (data.quantity + qty)

            Toast.makeText(context, quantity.toString(), Toast.LENGTH_SHORT).show()

            val query = update(Inventory.TABLE_NAME, Inventory.COLUMN_QUANTITY to quantity)
                    .whereArgs(Inventory.COLUMN_CODE + " = {code} AND " + Inventory.COLUMN_STORE + "={store}",
                            "code" to code,
                            "store" to store)

            query.exec()

        }
    }

    fun getDataToSync(): Inventory?
    {
        return getDbInstance().use {

            var columns = Inventory.COLUMN_ID + "," + Inventory.COLUMN_CODE + "," + Inventory.COLUMN_QUANTITY + ","
            columns += Inventory.COLUMN_STORE + "," + Inventory.COLUMN_LOCAL_CREATED_AT

            val query = select(Inventory.TABLE_NAME, columns)
                    .limit(1)

            query.exec {
                val rowParser = classParser<Inventory>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }


    fun dodol(): List<Inventory>
    {
        return getDbInstance().use {

            var columns = Inventory.COLUMN_ID + "," + Inventory.COLUMN_CODE + "," + Inventory.COLUMN_QUANTITY + ","
            columns += Inventory.COLUMN_STORE + "," + Inventory.COLUMN_LOCAL_CREATED_AT

            val query = select(Inventory.TABLE_NAME, columns)

            query.exec {
                val rowParser = classParser<Inventory>()
                parseList(rowParser)
            }
        }
    }

    fun removeInventoryById(id:Int)
    {
        getDbInstance().use {
            delete(Inventory.TABLE_NAME, Inventory.COLUMN_ID + "=" + id)
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {

            val query = select(Inventory.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount

        }
    }
}