package com.mockie.daikokuten.database.models

import java.math.BigDecimal

/**
 * Created by mockie on 18/12/17.
 */

data class CustomerInvoice(val tempInvoiceId: Int, val customerId:Int,  val name:String, val phone: String) {
    companion object {
        const val TABLE_NAME = "customers"
        const val COLUMN_TEMP_INVOICE_ID = "_id"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_NAME = "name"
        const val COLUMN_PHONE = "phone"
    }
}