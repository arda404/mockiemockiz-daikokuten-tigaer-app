package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PaymentMethod(
        val _id:Int,
        val name: String
) {
    companion object {
        const val TABLE_NAME = "payment_method"
        const val COLUMN_ID = "id"
        const val COLUMN_READABLE = "readable"
    }
}