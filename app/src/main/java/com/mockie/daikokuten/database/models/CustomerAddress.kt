package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class CustomerAddress(val _id: Int, val customer_id:Int, val name: String, val phone:String, val address:String, val created_at:String, val updated_at:String) {
    companion object {
        const val TABLE_NAME = "customer_addresses"
        const val COLUMN_ID = "_id"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_NAME = "name"
        const val COLUMN_PHONE = "phone"
        const val COLUMN_ADDRESS = "address"
        const val COLUMN_CREATED_AT = "created_at"
        const val COLUMN_UPDATED_AT = "updated_at"
    }
}