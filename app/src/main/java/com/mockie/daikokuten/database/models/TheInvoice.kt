package com.mockie.daikokuten.database.models

import android.content.Context
import com.google.gson.Gson
import com.mockie.daikokuten.database.repositories.InvoiceItem
import com.mockie.daikokuten.database.repositories.InvoiceItemToSync
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.database.repositories.Invoices
import com.mockie.daikokuten.helpers.BarcodeListHelper
import com.mockie.daikokuten.helpers.PurchaseDetailHelper
import com.mockie.daikokuten.models.BarcodeList
import org.json.JSONException
import android.provider.Contacts.People
import org.json.JSONObject
import com.google.gson.GsonBuilder





class TheInvoice(context:Context, tempInvoiceId:Int, realInvoiceId:Int)
{
    var isInvoiceExisted = false
    var invoiceId = 0
    var unsync = ""
    var userId = 0
    var customerId = 0
    var customerName:String? = ""
    var customerPhone:String? = ""
    var addressId = 0
    var addressName:String? = ""
    var addressPhone:String? = ""
    var address:String? = ""
    var shippingFee = 0
    var totalBilling = 0
    var status = 0
    var note:String? = ""
    var createdAt = ""
    var additionalPrice = 0
    var meta = ""

    val items = ArrayList<TheInvoiceItems>()

    init {
        if (tempInvoiceId > 0)
        {
            val invoice = InvoiceToSync(context).getInvoiceToSyncById(tempInvoiceId)
            if (invoice != null)
            {
                unsync = " (Lokal) "
                isInvoiceExisted = true
                invoiceId = invoice.invoice_id // temp id
                userId = invoice.user_id
                customerId = invoice.customer_id
                customerName = invoice.customer_name
                customerPhone = invoice.customer_phone
                addressId = invoice.address_id
                addressName = invoice.address_name
                addressPhone = invoice.address_phone
                address = invoice.address
                shippingFee = invoice.shipping_fee.toInt()
                totalBilling = invoice.total_billing.toInt()
                status = invoice.status
                note = invoice.note
                createdAt = invoice.localCreatedAt!!
                additionalPrice = invoice.additionalPricePerItem

                val theInvoiceItems = InvoiceItemToSync(context).getInvoiceItemToSyncByTempInvoiceId(invoice.invoice_id)
                val barcodeList = ArrayList<BarcodeList>()
                for (a in 0 until theInvoiceItems.size)
                {
                    barcodeList.add(BarcodeList(theInvoiceItems[a].code, theInvoiceItems[a].quantity, "", 0, ""))
                }

                val additionalPrice = PurchaseDetailHelper(context).getAdditonalFee(ArrayList(barcodeList))

                for (invoiceItem in theInvoiceItems) {

                    val type = BarcodeListHelper(context).getTypeReadable(invoiceItem.code)
                    val size = BarcodeListHelper(context).getTypeReadable(invoiceItem.code)
                    val price = BarcodeListHelper(context).getPrice(invoiceItem.code)
                    val desc = "$type, $size"

                    val totalPricePerItem = (price + additionalPrice) * invoiceItem.quantity

                    items.add(TheInvoiceItems(invoiceItem._id, invoiceItem.realInvoiceId, invoiceItem.code, invoiceItem.quantity, totalPricePerItem, desc))

                }
            }
        }
        else
        {
            val invoice = Invoices(context).getInvoiceWithRelationsById(realInvoiceId)
            if (invoice != null)
            {
                isInvoiceExisted = true
                invoiceId = invoice._id
                userId = invoice.user_id
                customerId = invoice.customer_id
                customerName = invoice.customer_name
                customerPhone = invoice.customer_phone
                addressId = invoice.address_id
                addressName = invoice.address_name
                addressPhone = invoice.address_phone
                address = invoice.address
                shippingFee = invoice.shipping_charge.toInt()
                totalBilling = invoice.total_billing.toInt()
                status = invoice.status
                note = invoice.note
                createdAt = invoice.created_at
                additionalPrice = invoice.additionalPricePerItem

                try {
                    val mainObject = JSONObject(invoice.meta)
                    val username = mainObject.getString("username")
                    meta = username
                } catch (e:Exception) {}

                val theItem = InvoiceItem(context).getInvoiceItemsByInvoiceId(invoice._id)

                val barcodeList = ArrayList<BarcodeList>()
                for (a in 0 until theItem.size)
                {
                    barcodeList.add(BarcodeList(theItem[a].code, theItem[a].quantity, "", 0, ""))
                }

                val additionalPrice = PurchaseDetailHelper(context).getAdditonalFee(ArrayList(barcodeList))

                for (a in 0 until theItem.size)
                {
                    val type = BarcodeListHelper(context).getTypeReadable(theItem[a].code)
                    val size = BarcodeListHelper(context).getTypeReadable(theItem[a].code)
                    val price = BarcodeListHelper(context).getPrice(theItem[a].code)
                    val desc = "$type, $size"

                    val totalPricePerItem = (price + additionalPrice) * theItem[a].quantity

                    items.add(TheInvoiceItems(theItem[a]._id, theItem[a].invoice_id, theItem[a].code, theItem[a].quantity, totalPricePerItem, desc))
                }
            }
        }
    }

    fun _getItems(): ArrayList<TheInvoiceItems>
    {
        return items
    }
}