package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.InvoiceItemToSync
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 18/12/17.
 */

class InvoiceItemToSync(override val context: Context):BaseRepository(context) {

    fun insert(
            tempInvoiceId: Int,
            realInvoiceId: Int,
            code:String,
            qty:Int,
            price: Int,
            retur:Int
    ): Int
    {
        return getDbInstance().use {

            insert(InvoiceItemToSync.TABLE_NAME,
                    InvoiceItemToSync.COLUMN_TEMP_INVOICE_ID to tempInvoiceId,
                    InvoiceItemToSync.COLUMN_REAL_INVOICE_ID to realInvoiceId,
                    InvoiceItemToSync.COLUMN_ITEM_CODE to code,
                    InvoiceItemToSync.COLUMN_ITEM_QUANTITY to qty,
                    InvoiceItemToSync.COLUMN_ITEM_PRICE to price,
                    InvoiceItemToSync.COLUMN_ITEM_RETUR to retur
            ).toInt()

        }
    }

    fun removeInvoiceItemToSync(invoiceId:Int)
    {
        getDbInstance().use {

            delete(InvoiceItemToSync.TABLE_NAME,
                    InvoiceItemToSync.COLUMN_TEMP_INVOICE_ID + "=" + invoiceId)

        }
    }

    fun getInvoiceItemToSyncByTempInvoiceId(tempInvoiceId:Int): List<InvoiceItemToSync>
    {

        return getDbInstance().use {

            var columns = InvoiceItemToSync.COLUMN_ID + "," + InvoiceItemToSync.COLUMN_TEMP_INVOICE_ID +"," + InvoiceItemToSync.COLUMN_REAL_INVOICE_ID + "," + InvoiceItemToSync.COLUMN_ITEM_CODE + ","
            columns += InvoiceItemToSync.COLUMN_ITEM_QUANTITY + "," + InvoiceItemToSync.COLUMN_ITEM_PRICE + "," + InvoiceItemToSync.COLUMN_ITEM_RETUR

            val query = select(InvoiceItemToSync.TABLE_NAME, columns)
                    .whereArgs(InvoiceItemToSync.COLUMN_TEMP_INVOICE_ID + "={tempInvoiceId}", "tempInvoiceId" to tempInvoiceId)

            query.exec {
                val rowParser = classParser<InvoiceItemToSync>()
                parseList(rowParser)
            }

        }

    }
}