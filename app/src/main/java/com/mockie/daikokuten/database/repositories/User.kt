package com.mockie.daikokuten.database.repositories

import android.content.Context
import org.jetbrains.anko.db.*
import com.mockie.daikokuten.database.models.User as UserModel

/**
 * Created by mockie on 18/12/17.
 */

class User (override val context: Context):BaseRepository(context) {

    fun isLogin(): UserModel?
    {
        return getDbInstance().use {
            var columns = UserModel.COLUMN_ID + "," + UserModel.COLUMN_NAME + "," + UserModel.COLUMN_BALANCE + ","
            columns = columns + UserModel.COLUMN_API_KEY + "," + UserModel.COLUMN_ADMINCAPS + "," + UserModel.COLUMN_STORE + "," + UserModel.COLUMN_API_BASE_URL
            columns += "," + UserModel.COLUMN_EMAIL

            val query = select(UserModel.TABLE_NAME, columns)

            query.exec {
                val rowParser = classParser<UserModel>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getLoggedInUserId(): Int
    {
        return isLogin()!!.uid
    }

    fun getLoggedInUserStore(): String?
    {
        return isLogin()?.store
    }

    /**
     *  User.COLUMN_ID to INTEGER + PRIMARY_KEY,
    User.COLUMN_NAME to TEXT,
    User.COLUMN_BALANCE to INTEGER,
    User.COLUMN_ADMINCAPS to TEXT,
    User.COLUMN_API_KEY to TEXT,
    User.COLUMN_STORE to TEXT,
    User.COLUMN_API_KEY to TEXT)
     */

    fun add(id: Int, name: String, balance: Int, adminCap: String, store:String, apiKey: String, apiBaseUrl: String, email:String): Int
    {
        return getDbInstance().use {
            insert(UserModel.TABLE_NAME,
                    UserModel.COLUMN_ID to id,
                    UserModel.COLUMN_NAME to name,
                    UserModel.COLUMN_BALANCE to balance,
                    UserModel.COLUMN_ADMINCAPS to adminCap,
                    UserModel.COLUMN_STORE to store,
                    UserModel.COLUMN_API_KEY to apiKey,
                    UserModel.COLUMN_API_BASE_URL to apiBaseUrl,
                    UserModel.COLUMN_EMAIL to email
            ).toInt()
        }
    }

    fun logout()
    {
        getDbInstance().use {
            delete(UserModel.TABLE_NAME)
        }
    }

}