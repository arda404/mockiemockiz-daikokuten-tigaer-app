package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.PriceRules
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.select
import java.lang.reflect.Array


/**
 * Created by mockie on 18/12/17.
 */

class PriceRules(override val context: Context):BaseRepository(context) {

    fun removeAll()
    {
        val db = getDbInstance().writableDatabase
        db.execSQL("DELETE FROM " + PriceRules.TABLE_NAME)
    }

    fun insert(
             store: String,
             start: Int,
             amount: Int
    ): Int
    {
        return getDbInstance().use {
            insert(PriceRules.TABLE_NAME,
                    PriceRules.COLUMN_STORE to store,
                    PriceRules.COLUMN_START to start,
                    PriceRules.COLUMN_AMOUNT to amount
            ).toInt()
        }
    }

    fun getPriceRules(totalItem:Int, store:String): PriceRules?
    {
        return getDbInstance().use {
            var columns = PriceRules.COLUMN_ID + "," + PriceRules.COLUMN_STORE + ","
            columns = columns + PriceRules.COLUMN_START + "," + PriceRules.COLUMN_AMOUNT

            val query = select(PriceRules.TABLE_NAME, columns)
                    .whereArgs( "(" + PriceRules.COLUMN_START +" <= {totalItem}) AND ("+ PriceRules.COLUMN_STORE +" = {store})",
                            "totalItem" to totalItem,
                            "store" to store)
                    .orderBy(PriceRules.COLUMN_START + " DESC ")
                    .limit(0, 1)

            query.exec {
                val rowParser = classParser<PriceRules>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getAllPriceRules():List<PriceRules>
    {
        return getDbInstance().use {
            var columns = PriceRules.COLUMN_ID + "," + PriceRules.COLUMN_STORE + ","
            columns = columns + PriceRules.COLUMN_START + "," + PriceRules.COLUMN_AMOUNT

            val query = select(PriceRules.TABLE_NAME, columns)

            query.exec {
                val rowParser = classParser<PriceRules>()
                parseList(rowParser)
            }
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(PriceRules.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }
}