package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import com.mockie.daikokuten.database.models.PrdPrices
import com.mockie.daikokuten.database.models.PrdSizes
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*
import com.mockie.daikokuten.database.models.SyncLog as SyncLogModel

/**
 * Created by mockie on 18/12/17.
 */

class PrdSizes(override val context: Context):BaseRepository(context) {

    fun insert(size: String, readable: String, sorter: Int, updated_at: String): Int
    {
        return getDbInstance().use {
            insert(PrdSizes.TABLE_NAME,
                    PrdSizes.COLUMN_SIZE to size,
                    PrdSizes.COLUMN_READABLE to readable,
                    PrdSizes.COLUMN_SORTER to sorter,
                    PrdSizes.COLUMN_UPDATED_AT to updated_at
            ).toInt()
        }
    }

    fun update(size: String, readable: String, sorter: Int, updated_at: String)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(PrdSizes.COLUMN_READABLE, readable)
            args.put(PrdSizes.COLUMN_SORTER, sorter)
            args.put(PrdSizes.COLUMN_UPDATED_AT, updated_at)

            update(PrdSizes.TABLE_NAME, args, PrdSizes.COLUMN_SIZE + "=\"" + size + "\"", null)
        }
    }

    private fun getSizeIds(data: List<PrdPrices>): Array<String?>
    {
        val result  = arrayOfNulls<String>(data.count())

        for (i in 0 until data.count()) {
            result[i] = "'" + data[i].typesize + "'"
        }

        return result
    }

    fun getSizeByType(type: String, appActive: Int): List<PrdSizes>
    {
        val prices = com.mockie.daikokuten.database.repositories.PrdPrices(context).getSizeIdsByType(type, appActive)
        val sizeIds = getSizeIds(prices)


        return getReadableByIds(sizeIds)
    }

    private fun getReadableByIds(ids: Array<String?>): List<PrdSizes>
    {
        return getDbInstance().use {
            var columns = PrdSizes.COLUMN_SIZE + "," + PrdSizes.COLUMN_READABLE + ","
            columns = columns + PrdSizes.COLUMN_SORTER + "," + PrdSizes.COLUMN_UPDATED_AT

            val query = select(PrdSizes.TABLE_NAME, columns)
                    .whereArgs( PrdSizes.COLUMN_SIZE +" IN (" + ids.joinToString(",") + ")")
                    .orderBy(PrdSizes.COLUMN_SORTER + " ASC")

            query.exec {
                val rowParser = classParser<PrdSizes>()
                parseList(rowParser)
            }
        }
    }

    fun getReadableById(id: String): PrdSizes?
    {
        return getDbInstance().use {
            var columns = PrdSizes.COLUMN_SIZE + "," + PrdSizes.COLUMN_READABLE + ","
            columns = columns + PrdSizes.COLUMN_SORTER + "," + PrdSizes.COLUMN_UPDATED_AT

            val query = select(PrdSizes.TABLE_NAME, columns)
                    .whereArgs( PrdSizes.COLUMN_SIZE +" = {size}", "size" to id)

            query.exec {
                val rowParser = classParser<PrdSizes>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(PrdSizes.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

}