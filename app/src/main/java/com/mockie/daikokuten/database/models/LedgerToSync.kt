package com.mockie.daikokuten.database.models

import java.math.BigDecimal

/**
 * Created by mockie on 18/12/17.
 */

data class LedgerToSync(
        val _id: Int,
        val userId:Int,
        val customerId:Int,
        val customerPhone:String,
        val invoiceId: Int,
        val tempInvoiceId: Int,
        val store: String,
        val value: Int,
        val type:String
) {
    companion object {
        const val TABLE_NAME = "ledger_to_sync"
        const val COLUMN_ID = "_id"
        const val COLUMN_USER_ID = "user_id"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_CUSTOMER_PHONE = "customer_phone"
        const val COLUMN_INVOICE_ID = "invoice_id"
        const val COLUMN_TEMP_INVOICE_ID = "temp_invoice_id"
        const val COLUMN_STORE = "store"
        const val COLUMN_VALUE = "value"
        const val COLUMN_TYPE = "type"
    }
}