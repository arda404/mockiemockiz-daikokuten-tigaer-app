package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.PaymentMethod as PaymentMethodModel
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*


/**
 * Created by mockie on 18/12/17.
 */

class PaymentMethod(override val context: Context):BaseRepository(context) {

    fun removeAll()
    {
        getDbInstance().use {
            delete(PaymentMethodModel.TABLE_NAME)
        }
    }

    fun insert(
             id: Int,
             readable: String
    ): Int
    {
        return getDbInstance().use {
            insert(PaymentMethodModel.TABLE_NAME,
                    PaymentMethodModel.COLUMN_ID to id,
                    PaymentMethodModel.COLUMN_READABLE to readable
            ).toInt()
        }
    }

    fun getAllPaymentMethod(): List<PaymentMethodModel>
    {
        return getDbInstance().use {
            val columns = PaymentMethodModel.COLUMN_ID + "," + PaymentMethodModel.COLUMN_READABLE

            val query = select(PaymentMethodModel.TABLE_NAME, columns)

            query.exec {
                val rowParser = classParser<PaymentMethodModel>()
                parseList(rowParser)
            }
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(PaymentMethodModel.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }
}