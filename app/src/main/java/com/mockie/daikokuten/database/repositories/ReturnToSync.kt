package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.ReturnToSync
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*
import java.text.SimpleDateFormat
import java.util.*


class ReturnToSync(override val context: Context):BaseRepository(context) {

    fun insert(
            userId: Int,
            store: String,
            customerId: Int,
            customerPhone:String,
            tempInvoiceId: Int,
            realInvoiceId: Int,
            barcode: String,
            quantity: Int,
            calculatedPrice: Int
    ): Int
    {
        val cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok"))
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val localInsertedAt = format1.format(cal.timeInMillis)

        return getDbInstance().use {
            insert(ReturnToSync.TABLE_NAME,
                    ReturnToSync.COLUMN_USER_ID to userId,
                    ReturnToSync.COLUMN_STORE to store,
                    ReturnToSync.COLUMN_CUSTOMER_ID to customerId,
                    ReturnToSync.COLUMN_CUSTOMER_PHONE to customerPhone,
                    ReturnToSync.COLUMN_TEMP_INVOICE_ID to tempInvoiceId,
                    ReturnToSync.COLUMN_REAL_INVOICE_ID to realInvoiceId,
                    ReturnToSync.COLUMN_BARCODE to barcode,
                    ReturnToSync.COLUMN_QUANTITY to quantity,
                    ReturnToSync.COLUMN_CALCULATED_PRICE to calculatedPrice,
                    ReturnToSync.COLUMN_LOCAL_CREATED_AT to localInsertedAt
            ).toInt()
        }
    }

    fun getReturnByTempInvoiceId(tempInvoiceId: Int):List<ReturnToSync>
    {
        return getDbInstance().use {
            var columns = ReturnToSync.COLUMN_LOCAL_CREATED_AT + "," + ReturnToSync.COLUMN_ID + "," + ReturnToSync.COLUMN_USER_ID + "," + ReturnToSync.COLUMN_STORE + "," + ReturnToSync.COLUMN_CUSTOMER_ID + "," + ReturnToSync.COLUMN_CUSTOMER_PHONE + ","
            columns += ReturnToSync.COLUMN_TEMP_INVOICE_ID + "," + ReturnToSync.COLUMN_REAL_INVOICE_ID + "," + ReturnToSync.COLUMN_BARCODE + "," + ReturnToSync.COLUMN_QUANTITY + "," + ReturnToSync.COLUMN_CALCULATED_PRICE

            val query = select(ReturnToSync.TABLE_NAME, columns)
                    .whereArgs(ReturnToSync.COLUMN_TEMP_INVOICE_ID + "=" + tempInvoiceId)

             query.exec {
                val rowParser = classParser<ReturnToSync>()
                parseList(rowParser)
            }
        }
    }

    fun getReturnBy(tempInvoiceId: Int, realInvoiceId: Int, barcode:String):ReturnToSync?
    {
        return getDbInstance().use {
            var columns = ReturnToSync.COLUMN_LOCAL_CREATED_AT + "," + ReturnToSync.COLUMN_ID + "," + ReturnToSync.COLUMN_USER_ID + "," + ReturnToSync.COLUMN_STORE + "," + ReturnToSync.COLUMN_CUSTOMER_ID + "," + ReturnToSync.COLUMN_CUSTOMER_PHONE + ","
            columns += ReturnToSync.COLUMN_TEMP_INVOICE_ID + "," + ReturnToSync.COLUMN_REAL_INVOICE_ID + "," + ReturnToSync.COLUMN_BARCODE + "," + ReturnToSync.COLUMN_QUANTITY + "," + ReturnToSync.COLUMN_CALCULATED_PRICE

            val query = select(ReturnToSync.TABLE_NAME, columns)
                    .whereArgs(
                            ReturnToSync.COLUMN_TEMP_INVOICE_ID + "=" + tempInvoiceId + " AND " +
                                    ReturnToSync.COLUMN_REAL_INVOICE_ID + "=" + realInvoiceId + " AND " +
                                    ReturnToSync.COLUMN_BARCODE + "='" + barcode + "'"
                    )

            query.exec {
                val rowParser = classParser<ReturnToSync>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getAllReturnWithRealInvoice():List<ReturnToSync>
    {
        return getDbInstance().use {
            var columns = ReturnToSync.COLUMN_LOCAL_CREATED_AT + "," + ReturnToSync.COLUMN_ID + "," + ReturnToSync.COLUMN_USER_ID + "," + ReturnToSync.COLUMN_STORE + "," + ReturnToSync.COLUMN_CUSTOMER_ID + "," + ReturnToSync.COLUMN_CUSTOMER_PHONE + ","
            columns += ReturnToSync.COLUMN_TEMP_INVOICE_ID + "," + ReturnToSync.COLUMN_REAL_INVOICE_ID + "," + ReturnToSync.COLUMN_BARCODE + "," + ReturnToSync.COLUMN_QUANTITY + "," + ReturnToSync.COLUMN_CALCULATED_PRICE

            val query = select(ReturnToSync.TABLE_NAME, columns)
                    .whereArgs(ReturnToSync.COLUMN_REAL_INVOICE_ID + "!=0")

            query.exec {
                val rowParser = classParser<ReturnToSync>()
                parseList(rowParser)
            }
        }
    }

    fun removeReturnToSync(returnId:Int)
    {
        getDbInstance().use {
            delete(ReturnToSync.TABLE_NAME,
                    ReturnToSync.COLUMN_ID + "=" + returnId)
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(ReturnToSync.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

}
