package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PrdSizes(
        val size: String,
        val readable: String,
        val sorter: Int,
        val updated_at: String
) {
    companion object {
        const val TABLE_NAME = "prdsizes"
        const val COLUMN_SIZE = "size"
        const val COLUMN_READABLE = "readable"
        const val COLUMN_SORTER = "sorter"
        const val COLUMN_UPDATED_AT = "updated_at"
    }
}