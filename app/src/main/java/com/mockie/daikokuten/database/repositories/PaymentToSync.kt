package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import com.mockie.daikokuten.database.models.PaymentToSync
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*


class PaymentToSync(override val context: Context):BaseRepository(context) {

    fun insert(
            userId: Int,
            store: String,
            customerId: Int,
            customerPhone:String,
            amount: Int,
            method: String
    ): Int
    {
        return getDbInstance().use {
            insert(PaymentToSync.TABLE_NAME,
                    PaymentToSync.COLUMN_USER_ID to userId,
                    PaymentToSync.COLUMN_STORE to store,
                    PaymentToSync.COLUMN_CUSTOMER_ID to customerId,
                    PaymentToSync.COLUMN_CUSTOMER_PHONE to customerPhone,
                    PaymentToSync.COLUMN_AMOUNT to amount,
                    PaymentToSync.COLUMN_METHOD to method
            ).toInt()
        }
    }

    fun getAllPaymentNoIdByPhone(phone:String):List<PaymentToSync>
    {
        return getDbInstance().use {
            var columns = PaymentToSync.COLUMN_ID + "," + PaymentToSync.COLUMN_USER_ID + "," + PaymentToSync.COLUMN_STORE + "," + PaymentToSync.COLUMN_CUSTOMER_ID + "," + PaymentToSync.COLUMN_CUSTOMER_PHONE + ","
            columns += PaymentToSync.COLUMN_AMOUNT + "," + PaymentToSync.COLUMN_METHOD

            val query = select(PaymentToSync.TABLE_NAME, columns)
                    .whereArgs(PaymentToSync.COLUMN_CUSTOMER_PHONE + "='" + phone + "' AND (" + PaymentToSync.COLUMN_CUSTOMER_ID+" = 0)")

            query.exec {
                val rowParser = classParser<PaymentToSync>()
                parseList(rowParser)
            }
        }
    }


    fun getAllPaymentWithCustomerId():List<PaymentToSync>
    {
        return getDbInstance().use {
            var columns = PaymentToSync.COLUMN_ID + "," + PaymentToSync.COLUMN_USER_ID + "," + PaymentToSync.COLUMN_STORE + "," + PaymentToSync.COLUMN_CUSTOMER_ID + "," + PaymentToSync.COLUMN_CUSTOMER_PHONE + ","
            columns += PaymentToSync.COLUMN_AMOUNT + "," + PaymentToSync.COLUMN_METHOD

            val query = select(PaymentToSync.TABLE_NAME, columns)
                    .whereArgs(PaymentToSync.COLUMN_CUSTOMER_ID+" != 0")

            query.exec {
                val rowParser = classParser<PaymentToSync>()
                parseList(rowParser)
            }
        }
    }

    fun getPaymentByPhone(phone:String):List<PaymentToSync>
    {
        return getDbInstance().use {
            var columns = PaymentToSync.COLUMN_ID  + "," + PaymentToSync.COLUMN_USER_ID + "," + PaymentToSync.COLUMN_STORE +  "," + PaymentToSync.COLUMN_CUSTOMER_ID + "," + PaymentToSync.COLUMN_CUSTOMER_PHONE + ","
            columns += PaymentToSync.COLUMN_AMOUNT + "," + PaymentToSync.COLUMN_METHOD

            val query = select(PaymentToSync.TABLE_NAME, columns)
                    .whereArgs(PaymentToSync.COLUMN_CUSTOMER_PHONE + "='" + phone + "' AND " + PaymentToSync.COLUMN_METHOD + "!='CHANGE'")

            query.exec {
                val rowParser = classParser<PaymentToSync>()
                parseList(rowParser)
            }
        }
    }

    fun getPaymentChangeByPhone(phone:String):List<PaymentToSync>
    {
        return getDbInstance().use {
            var columns = PaymentToSync.COLUMN_ID  + "," + PaymentToSync.COLUMN_USER_ID + "," + PaymentToSync.COLUMN_STORE +  "," + PaymentToSync.COLUMN_CUSTOMER_ID + "," + PaymentToSync.COLUMN_CUSTOMER_PHONE + ","
            columns += PaymentToSync.COLUMN_AMOUNT + "," + PaymentToSync.COLUMN_METHOD

            val query = select(PaymentToSync.TABLE_NAME, columns)
                    .whereArgs(PaymentToSync.COLUMN_CUSTOMER_PHONE + "='" + phone + "' AND " + PaymentToSync.COLUMN_METHOD + "='CHANGE'")

            query.exec {
                val rowParser = classParser<PaymentToSync>()
                parseList(rowParser)
            }
        }
    }

    fun updateCustomerByPhone(oldPhone:String, phone:String)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(PaymentToSync.COLUMN_CUSTOMER_PHONE, phone)

            update(PaymentToSync.TABLE_NAME, args, PaymentToSync.COLUMN_CUSTOMER_PHONE + "='" + oldPhone + "'", null)
        }
    }

    fun removePaymentToSync(paymentId:Int)
    {
        getDbInstance().use {
            transaction {
                delete(PaymentToSync.TABLE_NAME,
                        PaymentToSync.COLUMN_ID + "=" + paymentId)
            }
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(PaymentToSync.TABLE_NAME, "count(*) AS rowCount")

             query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

}
