package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PrdPrices(
        val typesize: String,
        val price: Int,
        val weight: Int,
        val sort: Int,
        val appactive: Int,
        val updated_at: String
) {
    companion object {
        const val TABLE_NAME = "prdprices"
        const val COLUMN_TYPE_SIZE = "typesize"
        const val COLUMN_PRICE = "price"
        const val COLUMN_WEIGHT = "weight"
        const val COLUMN_SORT = "sort"
        const val COLUMN_APPACTIVE = "appactive"
        const val COLUMN_UPDATED_AT = "updated_at"
    }
}