package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PrdCategories(
        val category: String,
        val readable: String,
        val sort: Int,
        val updated_at: String
) {
    companion object {
        const val TABLE_NAME = "prdcategories"
        const val COLUMN_CATEGORY = "category"
        const val COLUMN_READABLE = "readable"
        const val COLUMN_SORT = "sort"
        const val COLUMN_UPDATED_AT = "updated_at"
    }
}