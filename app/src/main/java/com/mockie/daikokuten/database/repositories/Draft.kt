package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.Draft
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 18/12/17.
 */

class Draft(override val context: Context):BaseRepository(context) {

    fun insert(pageName: String, keyName: String, content:String?): Int
    {
        return getDbInstance().use {

            insert(Draft.TABLE_NAME,
                    Draft.COLUMN_PAGE_NAME to pageName,
                    Draft.COLUMN_KEY_NAME to keyName,
                    Draft.COLUMN_CONTENT to content
            ).toInt()

        }
    }

    fun getDraftByPageNameAndKeyName(pageName: String, keyName: String): Draft?
    {
        return getDbInstance().use {

            var columns = Draft.COLUMN_PAGE_NAME + "," + Draft.COLUMN_PAGE_NAME + ","
            columns += Draft.COLUMN_CONTENT

            val query = select(Draft.TABLE_NAME, columns)
                    .whereArgs("(" + Draft.COLUMN_PAGE_NAME  + "={page_name}) AND (" + Draft.COLUMN_KEY_NAME + "={key_name})",
                            "page_name" to pageName, "key_name" to keyName)

             query.exec {
                val rowParser = classParser<Draft>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun removeAllDraft(pageName: String)
    {
        getDbInstance().use {

            delete(Draft.TABLE_NAME, Draft.COLUMN_PAGE_NAME + "='" + pageName + "'")

        }
    }

}