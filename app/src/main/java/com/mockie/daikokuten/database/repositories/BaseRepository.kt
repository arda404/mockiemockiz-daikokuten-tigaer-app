package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.DatabaseHelper

open class BaseRepository(open val context: Context) {

    fun getDbInstance(): DatabaseHelper
    {
        return DatabaseHelper.getInstance(context)
    }

}

