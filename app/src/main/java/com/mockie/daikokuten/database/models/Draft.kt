package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class Draft(
        val pageName: String,
        val key: String,
        val content: String?) {

    companion object {
        const val TABLE_NAME = "draft"
        const val COLUMN_ID = "id"
        const val COLUMN_PAGE_NAME = "page_name"
        const val COLUMN_KEY_NAME = "key_name"
        const val COLUMN_CONTENT = "content"
    }
}