package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PriceRules(
        val _id:Int,
        val store: String,
        val start: Int,
        val amount: Int
) {
    companion object {
        const val TABLE_NAME = "price_rules"
        const val COLUMN_ID = "id"
        const val COLUMN_STORE = "store"
        const val COLUMN_START = "start"
        const val COLUMN_AMOUNT = "amount"
    }
}