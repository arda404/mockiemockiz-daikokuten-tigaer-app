package com.mockie.daikokuten.database.upgrades

import android.database.sqlite.SQLiteDatabase
import com.mockie.daikokuten.database.models.*
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 15/05/18.
 */


class Version6(database: SQLiteDatabase) {

    fun createSyncLog(database: SQLiteDatabase)
    {
        database.dropTable(SyncLog.TABLE_NAME, true)
        database.createTable(
                SyncLog.TABLE_NAME,
                true,
                SyncLog.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                SyncLog.COLUMN_TABLE_NAME to TEXT,
                SyncLog.COLUMN_LAST_SYNC to TEXT)

        // insert
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PriceRules.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PrdTypes.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PrdSizes.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PrdPrices.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PrdCategories.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
    }

    private fun createPrdType(database: SQLiteDatabase)
    {
        database.dropTable(PrdTypes.TABLE_NAME, true)
        database.createTable(
                PrdTypes.TABLE_NAME,
                true,
                PrdTypes.COLUMN_TYPE to TEXT + PRIMARY_KEY,
                PrdTypes.COLUMN_READABLE to TEXT,
                PrdTypes.COLUMN_PATTERNED to INTEGER,
                PrdTypes.COLUMN_SORTER to INTEGER,
                PrdTypes.COLUMN_APPACTIVE to INTEGER,
                PrdTypes.COLUMN_CATEGORIES to TEXT,
                PrdTypes.COLUMN_UPDATED_AT to TEXT
        )
    }

    private fun createCategory(database: SQLiteDatabase)
    {
        database.dropTable(PrdCategories.TABLE_NAME, true)
        database.createTable(
                PrdCategories.TABLE_NAME,
                true,
                PrdCategories.COLUMN_CATEGORY to TEXT + PRIMARY_KEY,
                PrdCategories.COLUMN_READABLE to TEXT,
                PrdCategories.COLUMN_SORT to INTEGER,
                PrdCategories.COLUMN_UPDATED_AT to TEXT
        )
    }

    private fun createPrdPrice(database: SQLiteDatabase)
    {
        database.dropTable(PrdPrices.TABLE_NAME, true)
        database.createTable(
                PrdPrices.TABLE_NAME,
                true,
                PrdPrices.COLUMN_TYPE_SIZE to TEXT + PRIMARY_KEY,
                PrdPrices.COLUMN_PRICE to INTEGER,
                PrdPrices.COLUMN_WEIGHT to INTEGER,
                PrdPrices.COLUMN_SORT to INTEGER,
                PrdPrices.COLUMN_APPACTIVE to INTEGER,
                PrdPrices.COLUMN_UPDATED_AT to TEXT
        )
    }

    private fun createPrdSize(database: SQLiteDatabase)
    {
        database.dropTable(PrdSizes.TABLE_NAME, true)
        database.createTable(
                PrdSizes.TABLE_NAME,
                true,
                PrdSizes.COLUMN_SIZE to TEXT + PRIMARY_KEY,
                PrdSizes.COLUMN_READABLE to TEXT,
                PrdSizes.COLUMN_SORTER to INTEGER,
                PrdSizes.COLUMN_UPDATED_AT to TEXT
        )
    }

    private fun createInventory(database: SQLiteDatabase)
    {
        database.dropTable(Inventory.TABLE_NAME, true)
        database.createTable(
                Inventory.TABLE_NAME,
                true,
                Inventory.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                Inventory.COLUMN_CODE to TEXT,
                Inventory.COLUMN_QUANTITY to INTEGER,
                Inventory.COLUMN_STORE to TEXT,
                Inventory.COLUMN_LOCAL_CREATED_AT to TEXT
        )
    }

    private fun createStockTaking(database: SQLiteDatabase)
    {
        database.dropTable(StockTaking.TABLE_NAME, true)
        database.createTable(
                StockTaking.TABLE_NAME,
                true,
                StockTaking.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                StockTaking.COLUMN_CODE to TEXT,
                StockTaking.COLUMN_STOCK_DEVIATION to INTEGER,
                StockTaking.COLUMN_STORE to TEXT,
                StockTaking.COLUMN_LOCAL_CREATED_AT to TEXT
        )
    }

    private fun createMoveInventory(database: SQLiteDatabase)
    {
        database.dropTable(MovedInventory.TABLE_NAME, true)
        database.createTable(
                MovedInventory.TABLE_NAME,
                true,
                MovedInventory.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                Inventory.COLUMN_CODE to TEXT,
                MovedInventory.COLUMN_QUANTITY to INTEGER,
                MovedInventory.COLUMN_OLD_STORE to TEXT,
                MovedInventory.COLUMN_NEW_STORE to TEXT,
                MovedInventory.COLUMN_LOCAL_CREATED_AT to TEXT
        )
    }

    private fun createUser(database: SQLiteDatabase)
    {
        database.dropTable(User.TABLE_NAME, true)
        database.createTable(
                User.TABLE_NAME,
                true,
                User.COLUMN_ID to INTEGER + PRIMARY_KEY,
                User.COLUMN_NAME to TEXT,
                User.COLUMN_BALANCE to INTEGER,
                User.COLUMN_ADMINCAPS to TEXT,
                User.COLUMN_STORE to TEXT,
                User.COLUMN_API_KEY to TEXT,
                User.COLUMN_API_BASE_URL to TEXT,
                User.COLUMN_EMAIL to TEXT)
    }

    private fun createDraft(database: SQLiteDatabase)
    {
        database.dropTable(Draft.TABLE_NAME, true)
        database.createTable(
                Draft.TABLE_NAME,
                true,
                Draft.COLUMN_ID to INTEGER + PRIMARY_KEY,
                Draft.COLUMN_PAGE_NAME to TEXT,
                Draft.COLUMN_KEY_NAME to TEXT,
                Draft.COLUMN_CONTENT to TEXT)
    }

    private fun createPriceRule(database: SQLiteDatabase)
    {
        database.dropTable(PriceRules.TABLE_NAME, true)
        database.createTable(
                PriceRules.TABLE_NAME,
                true,
                PriceRules.COLUMN_ID to INTEGER + PRIMARY_KEY,
                PriceRules.COLUMN_START to INTEGER,
                PriceRules.COLUMN_AMOUNT to INTEGER,
                PriceRules.COLUMN_STORE to TEXT)
    }

    private fun createFireBase(database: SQLiteDatabase)
    {
        database.dropTable(FirebaseToken.TABLE_NAME, true)
        database.createTable(
                FirebaseToken.TABLE_NAME,
                true,
                FirebaseToken.COLUMN_TOKEN to TEXT)
    }

    private fun createBarcodeToPrint(database: SQLiteDatabase)
    {
        database.dropTable(BarcodeToPrint.TABLE_NAME, true)
        database.createTable(
                BarcodeToPrint.TABLE_NAME,
                true,
                BarcodeToPrint.COLUMN_ID to INTEGER + PRIMARY_KEY,
                BarcodeToPrint.COLUMN_BARCODE to TEXT,
                BarcodeToPrint.COLUMN_META to TEXT,
                BarcodeToPrint.COLUMN_SIZE to TEXT,
                BarcodeToPrint.COLUMN_QTY to INTEGER)
    }

    fun upgrade(database: SQLiteDatabase)
    {
        createSyncLog(database)
        createPrdType(database)
        createCategory(database)
        createPrdPrice(database)
        createPrdSize(database)
        createInventory(database)
        createStockTaking(database)
        createMoveInventory(database)
        createUser(database)
        createDraft(database)
        createPriceRule(database)
        createFireBase(database)
        createBarcodeToPrint(database)
    }

}