package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import com.mockie.daikokuten.database.models.PrdPrices
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*
import com.mockie.daikokuten.database.models.SyncLog as SyncLogModel

/**
 * Created by mockie on 18/12/17.
 */

class PrdPrices(override val context: Context):BaseRepository(context) {

    fun insert(
             typesize: String,
             price: Int,
             weight: Int,
             sort: Int,
             appactive: Int,
             updated_at: String
    ): Int
    {
        return getDbInstance().use {
            insert(PrdPrices.TABLE_NAME,
                    PrdPrices.COLUMN_TYPE_SIZE to typesize,
                    PrdPrices.COLUMN_PRICE to price,
                    PrdPrices.COLUMN_WEIGHT to weight,
                    PrdPrices.COLUMN_SORT to sort,
                    PrdPrices.COLUMN_APPACTIVE to appactive,
                    PrdPrices.COLUMN_UPDATED_AT to updated_at
            ).toInt()
        }
    }

    fun update(
            typesize: String,
            price: Int,
            weight: Int,
            sort: Int,
            appactive: Int,
            updated_at: String
    ){
        getDbInstance().use {
            val args = ContentValues()
            args.put(PrdPrices.COLUMN_PRICE, price)
            args.put(PrdPrices.COLUMN_WEIGHT, weight)
            args.put(PrdPrices.COLUMN_SORT, sort)
            args.put(PrdPrices.COLUMN_APPACTIVE, appactive)
            args.put(PrdPrices.COLUMN_UPDATED_AT, updated_at)

            update(PrdPrices.TABLE_NAME, args ,PrdPrices.COLUMN_TYPE_SIZE +"=\""+typesize+"\"", null)
        }
    }

    fun getPriceByTypeSize(typesize: String): PrdPrices?
    {
        return getDbInstance().use {
            var columns = PrdPrices.COLUMN_TYPE_SIZE + ","
            columns = columns + PrdPrices.COLUMN_PRICE + "," + PrdPrices.COLUMN_WEIGHT + ","
            columns = columns + PrdPrices.COLUMN_SORT  + "," + PrdPrices.COLUMN_APPACTIVE + ","
            columns += PrdPrices.COLUMN_UPDATED_AT

            val query = select(PrdPrices.TABLE_NAME, columns).whereArgs("(" + PrdPrices.COLUMN_TYPE_SIZE + "={typesize})", "typesize" to typesize)

            query.exec {
                val rowParser = classParser<PrdPrices>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getSizeIdsByType(type: String, appActive: Int): List<PrdPrices>
    {
        return getDbInstance().use {
            var columns = "SUBSTR(" + PrdPrices.COLUMN_TYPE_SIZE + ", 4, 5) AS typesize,"
            columns = columns + PrdPrices.COLUMN_PRICE + "," + PrdPrices.COLUMN_WEIGHT + ","
            columns = columns + PrdPrices.COLUMN_SORT  + "," + PrdPrices.COLUMN_APPACTIVE + ","
            columns += PrdPrices.COLUMN_UPDATED_AT

            val query = select(PrdPrices.TABLE_NAME, columns)

            if (appActive < 0)
            {
                query.whereArgs("SUBSTR(" + PrdPrices.COLUMN_TYPE_SIZE +", 1, 3)={type}", "type" to type)
            } else {
                query.whereArgs("SUBSTR(" + PrdPrices.COLUMN_TYPE_SIZE +", 1, 3)={type} AND ("+ PrdPrices.COLUMN_APPACTIVE+"={appactive})",
                        "type" to type, "appactive" to appActive)
            }

            query.orderBy("sort ASC")

            query.exec {
                val rowParser = classParser<PrdPrices>()
                parseList(rowParser)
            }
        }
    }

    fun getPriceByCode(code: String): PrdPrices?
    {
        return getDbInstance().use {
            var columns = PrdPrices.COLUMN_TYPE_SIZE + ","
            columns = columns + PrdPrices.COLUMN_PRICE + "," + PrdPrices.COLUMN_WEIGHT + ","
            columns = columns + PrdPrices.COLUMN_SORT + "," + PrdPrices.COLUMN_APPACTIVE + ","
            columns += PrdPrices.COLUMN_UPDATED_AT

            val query = select(PrdPrices.TABLE_NAME, columns)
                    .whereArgs( "(" + PrdPrices.COLUMN_TYPE_SIZE +" = {typeSize}) AND ("+ PrdPrices.COLUMN_APPACTIVE+"={appactive})",
                            "typeSize" to code, "appactive" to 1)

            query.exec {
                val rowParser = classParser<PrdPrices>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(PrdPrices.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

}