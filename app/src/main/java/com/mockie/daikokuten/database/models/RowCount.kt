package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 26/02/18.
 */

data class RowCount(val rowCount:Int)