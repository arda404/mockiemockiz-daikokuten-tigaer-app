package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.PrdTypes
import org.jetbrains.anko.db.*
import com.mockie.daikokuten.database.models.SyncLog as SyncLogModel
import android.content.ContentValues
import com.mockie.daikokuten.database.models.RowCount


/**
 * Created by mockie on 18/12/17.
 */

class PrdTypes(override val context: Context):BaseRepository(context) {

    fun insert( type: String,
                               readable: String,
                               patterned: Int,
                               sorter: Int,
                               appactive: Int,
                               categories: String,
                               updated_at: String): Int
    {
        return getDbInstance().use {
            insert(PrdTypes.TABLE_NAME,
                    PrdTypes.COLUMN_TYPE to type,
                    PrdTypes.COLUMN_READABLE to readable,
                    PrdTypes.COLUMN_SORTER to sorter,
                    PrdTypes.COLUMN_PATTERNED to patterned,
                    PrdTypes.COLUMN_APPACTIVE to appactive,
                    PrdTypes.COLUMN_CATEGORIES to categories,
                    PrdTypes.COLUMN_UPDATED_AT to updated_at
            ).toInt()
        }
    }

    fun update( type: String,
                readable: String,
                patterned: Int,
                sorter: Int,
                appactive: Int,
                categories: String,
                updated_at: String)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(PrdTypes.COLUMN_READABLE, readable)
            args.put(PrdTypes.COLUMN_SORTER, sorter)
            args.put(PrdTypes.COLUMN_PATTERNED, patterned)
            args.put(PrdTypes.COLUMN_APPACTIVE, appactive)
            args.put(PrdTypes.COLUMN_CATEGORIES, categories)
            args.put(PrdTypes.COLUMN_UPDATED_AT, updated_at)

            update(PrdTypes.TABLE_NAME, args ,PrdTypes.COLUMN_TYPE +"=\""+type+"\"", null)
        }
    }

    fun getTypeById(id: String, appactive: Int? = null): PrdTypes?
    {
        return getDbInstance().use {
            var columns = PrdTypes.COLUMN_TYPE + ","
            columns = columns + PrdTypes.COLUMN_READABLE + "," + PrdTypes.COLUMN_PATTERNED + ","
            columns = columns + PrdTypes.COLUMN_SORTER + "," + PrdTypes.COLUMN_APPACTIVE + ","
            columns += PrdTypes.COLUMN_CATEGORIES + ","
            columns += PrdTypes.COLUMN_UPDATED_AT

            var where = "(" + PrdTypes.COLUMN_TYPE +" = '" + id + "')"

            if (appactive != null)
            {
                where += " AND (" + PrdTypes.COLUMN_APPACTIVE + "=" + appactive.toInt() + ")"
            }

            val query = select(PrdTypes.TABLE_NAME, columns)
                    .whereSimple(where)

            query.exec {
                val rowParser = classParser<PrdTypes>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun getTypeByCategory(category: String, appactive: Int? = null): List<PrdTypes>
    {
        return getDbInstance().use {
            var columns = PrdTypes.COLUMN_TYPE + ","
            columns = columns + PrdTypes.COLUMN_READABLE + "," + PrdTypes.COLUMN_PATTERNED + ","
            columns = columns + PrdTypes.COLUMN_SORTER + "," + PrdTypes.COLUMN_APPACTIVE + ","
            columns += PrdTypes.COLUMN_CATEGORIES + ","
            columns += PrdTypes.COLUMN_UPDATED_AT

            var where = "("+ PrdTypes.COLUMN_CATEGORIES +" LIKE '%"+ category +"%')"

            if (appactive != null)
            {
                where += " AND (" + PrdTypes.COLUMN_APPACTIVE + "=" + appactive.toInt() + ")"
            }


            val query = select(PrdTypes.TABLE_NAME, columns)
                    .whereSimple(where)
                    .orderBy(PrdTypes.COLUMN_READABLE + " ASC ")

            query.exec {
                val rowParser = classParser<PrdTypes>()
                parseList(rowParser)
            }
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(PrdTypes.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

    fun getAllTypes(): List<PrdTypes>
    {
        return getDbInstance().use {
            var columns = PrdTypes.COLUMN_TYPE + ","
            columns = columns + PrdTypes.COLUMN_READABLE + "," + PrdTypes.COLUMN_PATTERNED + ","
            columns = columns + PrdTypes.COLUMN_SORTER + "," + PrdTypes.COLUMN_APPACTIVE + ","
            columns += PrdTypes.COLUMN_CATEGORIES + ","
            columns += PrdTypes.COLUMN_UPDATED_AT

            val query = select(PrdTypes.TABLE_NAME, columns)
                    .whereArgs( "(" + PrdTypes.COLUMN_APPACTIVE + "=1)")
                    .orderBy(PrdTypes.COLUMN_READABLE + " ASC ")

            query.exec {
                val rowParser = classParser<PrdTypes>()
                parseList(rowParser)
            }
        }
    }
}