package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.RowCount
import com.mockie.daikokuten.database.models.StockTaking as StockTakingModel
import org.jetbrains.anko.db.*
import java.text.SimpleDateFormat
import java.util.*
import com.mockie.daikokuten.database.models.SyncLog as SyncLogModel



/**
 * Created by mockie on 18/12/17.
 */

class StockTaking(override val context: Context):BaseRepository(context) {

    fun insert(code:String, store:String, stockDeviation:Int): Int
    {
        val cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok"))
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val localInsertedAt = format1.format(cal.timeInMillis)

        return getDbInstance().use {
            insert(StockTakingModel.TABLE_NAME,
                    StockTakingModel.COLUMN_CODE to code,
                    StockTakingModel.COLUMN_STOCK_DEVIATION to stockDeviation,
                    StockTakingModel.COLUMN_STORE to store,
                    StockTakingModel.COLUMN_LOCAL_CREATED_AT to localInsertedAt
            ).toInt()
        }
    }

    fun save(barcode:String, store:String, stockDeviation:Int)
    {
        if (barcode!= "" && store!= "")
        {
            getDbInstance().use {
                transaction {
                    removeStockTakingByCodeAndStore(barcode, store)
                    insert(barcode, store, stockDeviation)
                }
            }
        }
    }

    fun getCount(): Int
    {
        return getDbInstance().use {
            val query = select(StockTakingModel.TABLE_NAME, "count(*) AS rowCount")
                    .limit(10)

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

    fun getDataToSync(): StockTakingModel?
    {
        return getDbInstance().use {
            var columns = StockTakingModel.COLUMN_LOCAL_CREATED_AT + "," +  StockTakingModel.COLUMN_ID + "," + StockTakingModel.COLUMN_CODE + "," + StockTakingModel.COLUMN_STOCK_DEVIATION + ","
            columns += StockTakingModel.COLUMN_STORE

            val query = select(StockTakingModel.TABLE_NAME, columns)
                    .limit(1).orderBy(StockTakingModel.COLUMN_ID, SqlOrderDirection.ASC)

            query.exec {
                val rowParser = classParser<StockTakingModel>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun removeStockTakingByCodeAndStore(code:String, store:String)
    {
        getDbInstance().use {
            delete(StockTakingModel.TABLE_NAME,
                    StockTakingModel.COLUMN_CODE + "='" + code + "' AND store='" + store + "'")
        }
    }

    fun removeStockTakingById(id:Int)
    {
        getDbInstance().use {
            delete(StockTakingModel.TABLE_NAME,
                    StockTakingModel.COLUMN_ID + "=" + id)
        }
    }
}