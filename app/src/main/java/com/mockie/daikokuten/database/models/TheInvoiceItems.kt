package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class TheInvoiceItems(val _id: Int, val invoice_id:Int, val code: String, val quantity: Int, val totalPricePerItem:Int, val desc:String) {

}