package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import android.util.Log
import com.mockie.daikokuten.database.models.CustomerInvoice
import com.mockie.daikokuten.database.models.InvoiceToSync
import com.mockie.daikokuten.database.models.RowCount
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.models.SearchKeyword
import org.jetbrains.anko.db.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by mockie on 18/12/17.
 */

class InvoiceToSync(override val context: Context): BaseRepository(context) {

    fun insert(
            tempInvoiceId:Int,
            realInvoiceId: Int,
            userId:Int,
            store:String,
            customerId:Int,
            customerName: String,
            customerPhone:String,
            addressId:Int,
            addressName:String,
            addressPhone:String,
            address:String,
            note:String,
            status:Int,
            shippingFee: Double,
            totalBilling:Double,
            additionalPricePerItem:Int,
            barcodeList: ArrayList<BarcodeList>
    ): Boolean
    {
        val cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok"))
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val localInsertedAt = format1.format(cal.timeInMillis)

        return getDbInstance().use {

            try {

                transaction {
                    removeInvoiceToSync(tempInvoiceId)

                    val invoice = insert(InvoiceToSync.TABLE_NAME,
                            InvoiceToSync.COLUMN_REAL_INVOICE_ID to realInvoiceId,
                            InvoiceToSync.COLUMN_USER_ID to userId,
                            InvoiceToSync.COLUMN_STORE to store,
                            InvoiceToSync.COLUMN_CUSTOMER_ID to customerId,
                            InvoiceToSync.COLUMN_CUSTOMER_NAME to customerName,
                            InvoiceToSync.COLUMN_CUSTOMER_PHONE to customerPhone,
                            InvoiceToSync.COLUMN_ADDRESS_ID to addressId,
                            InvoiceToSync.COLUMN_ADDRESS_NAME to addressName,
                            InvoiceToSync.COLUMN_ADDRESS_PHONE to addressPhone,
                            InvoiceToSync.COLUMN_ADDRESS to address,
                            InvoiceToSync.COLUMN_NOTE to note,
                            InvoiceToSync.COLUMN_STATUS to status,
                            InvoiceToSync.COLUMN_SHIPPING_FEE to shippingFee,
                            InvoiceToSync.COLUMN_TOTAL_BILLING to totalBilling,
                            InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM to additionalPricePerItem,
                            InvoiceToSync.COLUMN_LOCAL_CREATED_AT to localInsertedAt
                    ).toInt()

                    for(a in 0 until barcodeList.size)
                    {
                        InvoiceItemToSync(context).insert(invoice,
                                realInvoiceId,
                                barcodeList[a].barcode,
                                barcodeList[a].qty.toString().toInt(),
                                barcodeList[a].price.toString().toInt(),
                                0
                        )
                    }
                }

                true
            } catch (e:Exception) {
                false
            }
        }

    }

    fun updateCustomerByPhone(oldPhone:String, name:String, phone:String)
    {
        getDbInstance().use {

            val args = ContentValues()
            args.put(InvoiceToSync.COLUMN_CUSTOMER_NAME, name)
            args.put(InvoiceToSync.COLUMN_CUSTOMER_PHONE, phone)

            update(InvoiceToSync.TABLE_NAME, args, InvoiceToSync.COLUMN_CUSTOMER_PHONE + "='" + oldPhone + "'", null)

        }
    }

    fun getInvoiceToSyncStatusReadyAndShippedByCustomerPhone(phone: String): List<InvoiceToSync>
    {
        return getDbInstance().use {

            var columns = InvoiceToSync.COLUMN_INVOICE_ID + "," + InvoiceToSync.COLUMN_REAL_INVOICE_ID + ","
            columns += InvoiceToSync.COLUMN_USER_ID + "," + InvoiceToSync.COLUMN_STORE + "," + InvoiceToSync.COLUMN_CUSTOMER_ID + ","
            columns += InvoiceToSync.COLUMN_CUSTOMER_NAME + "," + InvoiceToSync.COLUMN_CUSTOMER_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS_ID + ","
            columns += InvoiceToSync.COLUMN_ADDRESS_NAME + "," + InvoiceToSync.COLUMN_ADDRESS_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS + ","
            columns += InvoiceToSync.COLUMN_NOTE + "," + InvoiceToSync.COLUMN_STATUS + "," + InvoiceToSync.COLUMN_SHIPPING_FEE + ","
            columns += InvoiceToSync.COLUMN_TOTAL_BILLING + "," + InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM + "," + InvoiceToSync.COLUMN_LOCK_DATE + "," + InvoiceToSync.COLUMN_LOCAL_CREATED_AT

            var where = "(" + InvoiceToSync.COLUMN_CUSTOMER_PHONE +" = {phone}) AND "
            where += "(" + InvoiceToSync.COLUMN_STATUS + " ==1 OR "+ InvoiceToSync.COLUMN_STATUS + "==2)"

            val query = select(InvoiceToSync.TABLE_NAME, columns)
                    .whereArgs( where,
                            "phone" to phone)

                query.exec {
                val rowParser = classParser<InvoiceToSync>()
                parseList(rowParser)
            }

        }
    }

    fun updateCustomerAddressByPhone(oldPhone:String, name:String, phone:String, address:String)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(InvoiceToSync.COLUMN_ADDRESS_NAME, name)
            args.put(InvoiceToSync.COLUMN_ADDRESS_PHONE, phone)
            args.put(InvoiceToSync.COLUMN_ADDRESS, address)

            update(InvoiceToSync.TABLE_NAME, args, InvoiceToSync.COLUMN_ADDRESS_PHONE + "='" + oldPhone + "'", null)
        }
    }

    fun findCustomerByName(name:String): List<CustomerInvoice>
    {
        return getDbInstance().use {

            val columns = InvoiceToSync.COLUMN_INVOICE_ID + "," + InvoiceToSync.COLUMN_CUSTOMER_ID + "," + InvoiceToSync.COLUMN_CUSTOMER_NAME + "," + InvoiceToSync.COLUMN_CUSTOMER_PHONE

            val query = select(InvoiceToSync.TABLE_NAME, columns)
                    .whereArgs( "(" + InvoiceToSync.COLUMN_CUSTOMER_NAME +" LIKE {name})",
                            "name" to "%$name%")

            query.exec {
                val rowParser = classParser<CustomerInvoice>()
                parseList(rowParser)
            }

        }
    }

    fun removeInvoiceToSync(invoiceId:Int)
    {
        getDbInstance().use {
            try {
                transaction {
                    delete(InvoiceToSync.TABLE_NAME,
                            InvoiceToSync.COLUMN_INVOICE_ID + "=" + invoiceId)

                    InvoiceItemToSync(context).removeInvoiceItemToSync(invoiceId)
                }
            } catch (e:Exception) {}
        }
    }

    fun getInvoiceListData(searchKeyword:SearchKeyword): List<InvoiceToSync>
    {
        return getDbInstance().use {

            var columns = InvoiceToSync.COLUMN_INVOICE_ID + "," + InvoiceToSync.COLUMN_REAL_INVOICE_ID + ","
            columns += InvoiceToSync.COLUMN_USER_ID + "," + InvoiceToSync.COLUMN_STORE + "," + InvoiceToSync.COLUMN_CUSTOMER_ID + ","
            columns += InvoiceToSync.COLUMN_CUSTOMER_NAME + "," + InvoiceToSync.COLUMN_CUSTOMER_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS_ID + ","
            columns += InvoiceToSync.COLUMN_ADDRESS_NAME + "," + InvoiceToSync.COLUMN_ADDRESS_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS + ","
            columns += InvoiceToSync.COLUMN_NOTE + "," + InvoiceToSync.COLUMN_STATUS + "," + InvoiceToSync.COLUMN_SHIPPING_FEE + ","
            columns += InvoiceToSync.COLUMN_TOTAL_BILLING + "," + InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM + "," + InvoiceToSync.COLUMN_LOCK_DATE + "," + InvoiceToSync.COLUMN_LOCAL_CREATED_AT

            val query = select(InvoiceToSync.TABLE_NAME, columns)

            var where = InvoiceToSync.COLUMN_STATUS + "!=1000 "

            if (searchKeyword.invoiceId != 0) {
                where += " AND " + InvoiceToSync.COLUMN_REAL_INVOICE_ID + "=" + searchKeyword.invoiceId
            }

            if (searchKeyword.customerName != "") {
                where += " AND " + InvoiceToSync.COLUMN_CUSTOMER_NAME + " LIKE '%" + searchKeyword.customerName + "%'"
            }

            if (searchKeyword.customerPhone != "") {
                where += " AND " + InvoiceToSync.COLUMN_CUSTOMER_PHONE + " = '" + searchKeyword.customerPhone + "'"
            }

            if (searchKeyword.customerId != 0) {
                where += " AND " + InvoiceToSync.COLUMN_CUSTOMER_ID + " = " + searchKeyword.customerId
            }

            if (searchKeyword.status != 2000)
            {
                where += " AND " + InvoiceToSync.COLUMN_STATUS + " = " + searchKeyword.status
            }

            query.whereArgs(where)
            query.orderBy(InvoiceToSync.COLUMN_INVOICE_ID, SqlOrderDirection.DESC)

                query.exec {
                val rowParser = classParser<InvoiceToSync>()
                parseList(rowParser)
            }

        }

    }

    fun getInvoiceToSyncById(id:Int): InvoiceToSync?
    {
        return getDbInstance().use {

            var columns = InvoiceToSync.COLUMN_INVOICE_ID + "," + InvoiceToSync.COLUMN_REAL_INVOICE_ID + ","
            columns += InvoiceToSync.COLUMN_USER_ID + "," + InvoiceToSync.COLUMN_STORE + "," + InvoiceToSync.COLUMN_CUSTOMER_ID + ","
            columns += InvoiceToSync.COLUMN_CUSTOMER_NAME + "," + InvoiceToSync.COLUMN_CUSTOMER_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS_ID + ","
            columns += InvoiceToSync.COLUMN_ADDRESS_NAME + "," + InvoiceToSync.COLUMN_ADDRESS_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS + ","
            columns += InvoiceToSync.COLUMN_NOTE + "," + InvoiceToSync.COLUMN_STATUS + "," + InvoiceToSync.COLUMN_SHIPPING_FEE + ","
            columns += InvoiceToSync.COLUMN_TOTAL_BILLING + "," + InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM + "," + InvoiceToSync.COLUMN_LOCK_DATE + "," + InvoiceToSync.COLUMN_LOCAL_CREATED_AT

            val query = select(InvoiceToSync.TABLE_NAME, columns)
                    .whereArgs(InvoiceToSync.COLUMN_INVOICE_ID + "=" + id)

            query.exec {
                val rowParser = classParser<InvoiceToSync>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun checkIfUnSyncDataExisted(id:Int): InvoiceToSync?
    {
        return getDbInstance().use {

            var columns = InvoiceToSync.COLUMN_INVOICE_ID + "," + InvoiceToSync.COLUMN_REAL_INVOICE_ID + ","
            columns += InvoiceToSync.COLUMN_USER_ID + "," + InvoiceToSync.COLUMN_STORE + "," + InvoiceToSync.COLUMN_CUSTOMER_ID + ","
            columns += InvoiceToSync.COLUMN_CUSTOMER_NAME + "," + InvoiceToSync.COLUMN_CUSTOMER_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS_ID + ","
            columns += InvoiceToSync.COLUMN_ADDRESS_NAME + "," + InvoiceToSync.COLUMN_ADDRESS_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS + ","
            columns += InvoiceToSync.COLUMN_NOTE + "," + InvoiceToSync.COLUMN_STATUS + "," + InvoiceToSync.COLUMN_SHIPPING_FEE + ","
            columns += InvoiceToSync.COLUMN_TOTAL_BILLING + "," + InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM + "," + InvoiceToSync.COLUMN_LOCK_DATE + "," + InvoiceToSync.COLUMN_LOCAL_CREATED_AT

            val query = select(InvoiceToSync.TABLE_NAME, columns)
                    .whereArgs(InvoiceToSync.COLUMN_REAL_INVOICE_ID + "=" + id)
                    .orderBy(InvoiceToSync.COLUMN_INVOICE_ID, SqlOrderDirection.DESC)

            query.exec {
                val rowParser = classParser<InvoiceToSync>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun getInvoiceToSync(): InvoiceToSync?
    {
        return getDbInstance().use {
            var columns = InvoiceToSync.COLUMN_INVOICE_ID + "," + InvoiceToSync.COLUMN_REAL_INVOICE_ID + ","
            columns += InvoiceToSync.COLUMN_USER_ID + "," + InvoiceToSync.COLUMN_STORE + "," + InvoiceToSync.COLUMN_CUSTOMER_ID + ","
            columns += InvoiceToSync.COLUMN_CUSTOMER_NAME + "," + InvoiceToSync.COLUMN_CUSTOMER_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS_ID + ","
            columns += InvoiceToSync.COLUMN_ADDRESS_NAME + "," + InvoiceToSync.COLUMN_ADDRESS_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS + ","
            columns += InvoiceToSync.COLUMN_NOTE + "," + InvoiceToSync.COLUMN_STATUS + "," + InvoiceToSync.COLUMN_SHIPPING_FEE + ","
            columns += InvoiceToSync.COLUMN_TOTAL_BILLING + "," + InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM + "," + InvoiceToSync.COLUMN_LOCK_DATE + "," + InvoiceToSync.COLUMN_LOCAL_CREATED_AT

            val query = select(InvoiceToSync.TABLE_NAME, columns).limit(1)
                    .orderBy(InvoiceToSync.COLUMN_INVOICE_ID, SqlOrderDirection.ASC)
                    .whereArgs(InvoiceToSync.COLUMN_LOCK_DATE + "='' OR " + InvoiceToSync.COLUMN_LOCK_DATE + " IS NULL ")

            query.exec {
                val rowParser = classParser<InvoiceToSync>()
                parseList(rowParser)
            }.firstOrNull()
        }
    }

    fun test(): List<InvoiceToSync>
    {
        return getDbInstance().use {

            var columns = InvoiceToSync.COLUMN_INVOICE_ID + "," + InvoiceToSync.COLUMN_REAL_INVOICE_ID + ","
            columns += InvoiceToSync.COLUMN_USER_ID + "," + InvoiceToSync.COLUMN_STORE + "," + InvoiceToSync.COLUMN_CUSTOMER_ID + ","
            columns += InvoiceToSync.COLUMN_CUSTOMER_NAME + "," + InvoiceToSync.COLUMN_CUSTOMER_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS_ID + ","
            columns += InvoiceToSync.COLUMN_ADDRESS_NAME + "," + InvoiceToSync.COLUMN_ADDRESS_PHONE + "," + InvoiceToSync.COLUMN_ADDRESS + ","
            columns += InvoiceToSync.COLUMN_NOTE + "," + InvoiceToSync.COLUMN_STATUS + "," + InvoiceToSync.COLUMN_SHIPPING_FEE + ","
            columns += InvoiceToSync.COLUMN_TOTAL_BILLING  + "," + InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM + "," + InvoiceToSync.COLUMN_LOCK_DATE + "," + InvoiceToSync.COLUMN_LOCAL_CREATED_AT

            val query = select(InvoiceToSync.TABLE_NAME, columns)
                    .orderBy(InvoiceToSync.COLUMN_INVOICE_ID, SqlOrderDirection.ASC)
//                .whereArgs(InvoiceToSync.COLUMN_LOCK_DATE + "!=''")

            query.exec {
                val rowParser = classParser<InvoiceToSync>()
                parseList(rowParser)
            }

        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(InvoiceToSync.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

    fun lockData(phone:String)
    {
        val cal = Calendar.getInstance()
        cal.add(Calendar.MINUTE, 15)
        val _15minute = cal.timeInMillis

        getDbInstance().use {

            val args = ContentValues()
            args.put(InvoiceToSync.COLUMN_LOCK_DATE, _15minute)

            update(InvoiceToSync.TABLE_NAME, args, InvoiceToSync.COLUMN_CUSTOMER_PHONE + "='" + phone + "'", null)

        }
    }

    fun unlockAllData()
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(InvoiceToSync.COLUMN_LOCK_DATE, "")

            update(InvoiceToSync.TABLE_NAME, args, InvoiceToSync.COLUMN_LOCK_DATE + "!= ''", null)
        }
    }

    fun unlockPreviousData()
    {
        val cal = Calendar.getInstance()

        getDbInstance().use {
            val args = ContentValues()
            args.put(InvoiceToSync.COLUMN_LOCK_DATE, "")

            update(InvoiceToSync.TABLE_NAME, args, InvoiceToSync.COLUMN_LOCK_DATE + "<" + cal.timeInMillis, null)
        }
    }

    fun unlockData(phone:String)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(InvoiceToSync.COLUMN_LOCK_DATE, "")

            update(InvoiceToSync.TABLE_NAME, args, InvoiceToSync.COLUMN_CUSTOMER_PHONE + "='" + phone + "'", null)
        }
    }

}