package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import com.mockie.daikokuten.database.models.Customer
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.*


class Customer(override val context: Context): BaseRepository(context){

    fun insert(id:Int, name: String, phone: String, balance:Int, createdAt: String, updatedAt: String): Int
    {
        return getDbInstance().use {

            insert(Customer.TABLE_NAME,
                Customer.COLUMN_ID to id,
                Customer.COLUMN_NAME to name,
                Customer.COLUMN_PHONE to phone,
                Customer.COLUMN_BALANCE to balance,
                Customer.COLUMN_CREATED_AT to createdAt,
                Customer.COLUMN_UPDATED_AT to updatedAt
            ).toInt()

        }
    }

    fun findCustomerByName(name:String): List<Customer>
    {
        return getDbInstance().use {

            var columns = Customer.COLUMN_ID + "," + Customer.COLUMN_NAME + "," + Customer.COLUMN_PHONE + "," + Customer.COLUMN_BALANCE
            columns += "," + Customer.COLUMN_CREATED_AT + "," + Customer.COLUMN_UPDATED_AT

            val query = select(Customer.TABLE_NAME, columns)
                    .whereArgs( "(" + Customer.COLUMN_NAME +" LIKE {name})",
                            "name" to "%$name%")
                    .orderBy(Customer.COLUMN_ID, SqlOrderDirection.DESC)

            query.exec {
                val rowParser = classParser<Customer>()
                parseList(rowParser)
            }

        }
    }

    fun getCustomerById(id:Int): Customer?
    {
        return getDbInstance().use {

            var columns = Customer.COLUMN_ID + "," + Customer.COLUMN_NAME + "," + Customer.COLUMN_PHONE + "," + Customer.COLUMN_BALANCE
            columns += "," + Customer.COLUMN_CREATED_AT + "," + Customer.COLUMN_UPDATED_AT

            val query = select(Customer.TABLE_NAME, columns)
                    .whereArgs("(" + Customer.COLUMN_ID  + "={id})",
                            "id" to id)

                query.exec {
                val rowParser = classParser<Customer>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun update(_id: Int, name:String, phone: String, balance: Int, created_at:String, updated_at:String)
    {
        getDbInstance().use {

            val args = ContentValues()
            args.put(Customer.COLUMN_NAME, name)
            args.put(Customer.COLUMN_PHONE, phone)
            args.put(Customer.COLUMN_BALANCE, balance)
            args.put(Customer.COLUMN_CREATED_AT, created_at)
            args.put(Customer.COLUMN_UPDATED_AT, updated_at)

            update(Customer.TABLE_NAME, args, Customer.COLUMN_ID + "=" + _id, null)

        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {

            val query = select(Customer.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount

        }
    }

}