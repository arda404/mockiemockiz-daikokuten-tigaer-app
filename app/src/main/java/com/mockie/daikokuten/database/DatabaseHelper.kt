package com.mockie.daikokuten.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.mockie.daikokuten.database.upgrades.Version6
import com.mockie.daikokuten.database.upgrades.Version8
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 17/12/17.
 */


class DatabaseHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "daikokuten", null, 1) {
    companion object {

        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(context: Context): DatabaseHelper {
            if (instance == null) {
                instance = DatabaseHelper(context.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(database: SQLiteDatabase) {
        Version6(database).upgrade(database)
        Version8(database).upgrade(database)
    }

    override fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

        if (oldVersion < 6)
        {
            Version6(database).upgrade(database)
        }

        if (oldVersion < 8)
        {
            Version8(database).upgrade(database)
        }

    }
}