package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class InvoiceItems(val _id: Int, val invoice_id:Int, val code: String, val quantity: Int, val price:Int, val retur:Int) {
    companion object {
        const val TABLE_NAME = "invoice_items"
        const val COLUMN_ID = "_id"
        const val COLUMN_INVOICE_ID = "invoice_id"
        const val COLUMN_CODE = "code"
        const val COLUMN_QUANTITY = "quantity"
        const val COLUMN_PRICE = "price"
        const val COLUMN_RETUR = "retur"
    }
}