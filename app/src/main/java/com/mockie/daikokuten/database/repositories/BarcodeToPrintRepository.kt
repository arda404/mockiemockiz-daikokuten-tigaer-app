package com.mockie.daikokuten.database.repositories

import android.content.ContentValues
import android.content.Context
import com.mockie.daikokuten.database.models.BarcodeToPrint
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 18/12/17.
 */

class BarcodeToPrintRepository(override val context: Context): BaseRepository(context) {

    fun insert(barcode: String, meta: String, size:String, qty: Int): Int
    {
        return getDbInstance().use {

             insert(BarcodeToPrint.TABLE_NAME,
                    BarcodeToPrint.COLUMN_BARCODE to barcode,
                    BarcodeToPrint.COLUMN_META to meta,
                    BarcodeToPrint.COLUMN_SIZE to size,
                    BarcodeToPrint.COLUMN_QTY to qty).toInt()

        }
    }

    fun update(id: Int, barcode: String, meta: String, size:String, qty: Int)
    {
        getDbInstance().use {
            val args = ContentValues()
            args.put(BarcodeToPrint.COLUMN_BARCODE, barcode)
            args.put(BarcodeToPrint.COLUMN_META, meta)
            args.put(BarcodeToPrint.COLUMN_SIZE, size)
            args.put(BarcodeToPrint.COLUMN_QTY, qty)

            update(BarcodeToPrint.TABLE_NAME, args,BarcodeToPrint.COLUMN_ID +"=" + id + "", null)
        }
    }

    fun getAllBarcodes(): List<BarcodeToPrint>
    {
        return getDbInstance().use {
            var columns = BarcodeToPrint.COLUMN_ID + "," + BarcodeToPrint.COLUMN_BARCODE + "," + BarcodeToPrint.COLUMN_META + ","
            columns += BarcodeToPrint.COLUMN_SIZE + ","
            columns += BarcodeToPrint.COLUMN_QTY

            val query = select(BarcodeToPrint.TABLE_NAME, columns)
                    .orderBy(BarcodeToPrint.COLUMN_ID + " DESC ")

            query.exec {
                val rowParser = classParser<BarcodeToPrint>()
                parseList(rowParser)
            }
        }
    }

    fun removeBarcode(id: Int)
    {
        getDbInstance().use {
            delete(BarcodeToPrint.TABLE_NAME, BarcodeToPrint.COLUMN_ID + "=" + id)
        }
    }


}