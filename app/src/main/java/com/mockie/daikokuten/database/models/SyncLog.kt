package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class SyncLog(
        val _id: Int,
        val table_name:String,
        val last_sync: String?,
        val last_sync_temp: String?,
        val progress: String?,
        val to_download: Int?,
        val next_page: Int?,
        val until_time: String?
) {
    companion object {
        const val TABLE_NAME = "sync_log"
        const val COLUMN_ID = "_id"
        const val COLUMN_TABLE_NAME = "table_name"
        const val COLUMN_LAST_SYNC = "last_sync"
        const val COLUMN_LAST_SYNC_TEMP = "last_sync_temp"
        const val COLUMN_PROGRESS = "progress"
        const val COLUMN_TO_DOWNLOAD = "to_download"
        const val COLUMN_NEXT_PAGE = "next_page"
        const val COLUMN_UNTIL_TIME = "until_time"
    }
}