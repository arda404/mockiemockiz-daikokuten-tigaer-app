package com.mockie.daikokuten.database.models

import java.math.BigDecimal
import java.text.DecimalFormat

/**
 * Created by mockie on 18/12/17.
 */

data class InvoicesCustomerAddressRelationship(
        val _id: Int,
        val user_id:Int,
        val customer_id:Int,
        val customer_name:String?,
        val customer_phone:String?,
        val address_id:Int,
        val address_name:String?,
        val address_phone:String?,
        val address:String?,
        val shipping_charge:Double,
        val total_billing: Double,
        val status:Int,
        val note:String?,
        val created_at:String,
        val updated_at:String,
        val additionalPricePerItem:Int,
        val meta:String
) {
}