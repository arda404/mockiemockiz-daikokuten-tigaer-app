package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.FirebaseToken
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 22/02/18.
 */

class FirebaseToken(override val context: Context): BaseRepository(context) {

    fun insert(token: String): Int
    {
        return getDbInstance().use {

            insert(FirebaseToken.TABLE_NAME,
                    FirebaseToken.COLUMN_TOKEN to token).toInt()

        }
    }

    fun getToken(): FirebaseToken?
    {
        return getDbInstance().use {

            val columns = FirebaseToken.COLUMN_TOKEN

            val query = select(FirebaseToken.TABLE_NAME, columns)

            query.exec {
                val rowParser = classParser<FirebaseToken>()
                parseList(rowParser)
            }.firstOrNull()

        }
    }

    fun deleteToken(token: String?)
    {

        getDbInstance().use {

            delete(FirebaseToken.TABLE_NAME, FirebaseToken.COLUMN_TOKEN + "='" + token +"'")

        }

    }

}