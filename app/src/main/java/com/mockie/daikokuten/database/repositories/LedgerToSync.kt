package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.LedgerToSync
import com.mockie.daikokuten.database.models.RowCount
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.select


class LedgerToSync(override val context: Context):BaseRepository(context) {

    fun insert(
            userId:Int,
            customerId:Int,
            customerPhone:String,
            invoiceId: Int,
            tempInvoiceId: Int,
            store: String,
            value: Int,
            type:String
    ): Int
    {
        return getDbInstance().use {
            insert(LedgerToSync.TABLE_NAME,
                    LedgerToSync.COLUMN_USER_ID to userId,
                    LedgerToSync.COLUMN_CUSTOMER_ID to customerId,
                    LedgerToSync.COLUMN_CUSTOMER_PHONE to customerPhone,
                    LedgerToSync.COLUMN_INVOICE_ID to invoiceId,
                    LedgerToSync.COLUMN_TEMP_INVOICE_ID to tempInvoiceId,
                    LedgerToSync.COLUMN_STORE to store,
                    LedgerToSync.COLUMN_VALUE to value,
                    LedgerToSync.COLUMN_TYPE to type
            ).toInt()
        }
    }

    fun getTotalRow(): Int
    {
        return getDbInstance().use {
            val query = select(LedgerToSync.TABLE_NAME, "count(*) AS rowCount")

            query.exec {
                val rowParser = classParser<RowCount>()
                parseList(rowParser)
            }.firstOrNull()!!.rowCount
        }
    }

}