package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class Invoices(
        val _id: Int,
        val customer_id:Int,
        val address_id:Int,
        val shipping_charge:Double,
        val total_billing:Double,
        val status:Int,
        val store:String?,
        val note:String?,
        val user_id:Int,
        val created_at:String,
        val updated_at:String,
        val additionalPricePerItem:Int,
        val meta:String
) {
    companion object {
        const val TABLE_NAME = "invoices"
        const val COLUMN_ID = "_id"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_ADDRESS_ID = "address_id"
        const val COLUMN_SHIPPING_CHARGE = "shipping_charge"
        const val COLUMN_TOTAL_BILLING = "total_billing"
        const val COLUMN_STATUS = "status"
        const val COLUMN_STORE = "store"
        const val COLUMN_NOTE = "note"
        const val COLUMN_USER_ID = "user_id"
        const val COLUMN_CREATED_AT = "created_at"
        const val COLUMN_UPDATED_AT = "updated_at"
        const val COLUMN_ADDITIONAL_PRICE_PER_ITEM = "additional_price_per_item"
        const val COLUMN_META = "meta"
    }
}