package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class PaymentToSync(
        val _id:Int,
        val userId:Int,
        val store:String,
        val customerId:Int,
        val customerPhone:String,
        val amount:Int,
        val method:String
) {
    companion object {
        const val TABLE_NAME = "payment_to_sync"
        const val COLUMN_ID = "id"
        const val COLUMN_USER_ID = "user_id"
        const val COLUMN_STORE = "store"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_CUSTOMER_PHONE = "customer_phone"
        const val COLUMN_AMOUNT = "amount"
        const val COLUMN_METHOD = "method"
    }
}