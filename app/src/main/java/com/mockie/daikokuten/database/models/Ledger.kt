package com.mockie.daikokuten.database.models


/**
 * Created by mockie on 18/12/17.
 */

data class Ledger(
        val _id: Int,
        val userId:Int,
        val customerId:Int,
        val description: String,
        val value: Double,
        val invoiceId: Int,
        val debit:String,
        val credit:String,
        val dbBalance:Double,
        val crBalance:Double,
        val customerBalance:Double,
        val createdAt:String) {
    companion object {
        const val TABLE_NAME = "ledger"
        const val COLUMN_ID = "_id"
        const val COLUMN_USER_ID = "user_id"
        const val COLUMN_CUSTOMER_ID = "customer_id"
        const val COLUMN_DESCRIPTION = "description"
        const val COLUMN_VALUE = "value"
        const val COLUMN_INVOICE_ID = "invoice_id"
        const val COLUMN_DEBIT = "debit"
        const val COLUMN_CREDIT = "credit"
        const val COLUMN_DB_BALANCE = "db_balance"
        const val COLUMN_CR_BALANCE = "cr_balance"
        const val COLUMN_CUSTOMER_BALANCE = "customer_balance"
        const val COLUMN_CREATED_AT = "created_at"
    }
}