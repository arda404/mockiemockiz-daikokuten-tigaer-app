package com.mockie.daikokuten.database.repositories

import android.content.Context
import com.mockie.daikokuten.database.models.DraftBarcode
import org.jetbrains.anko.db.*

/**
 * Created by mockie on 18/12/17.
 */

class DraftBarcode(override val context: Context):BaseRepository(context) {

    fun insert(pageName: String, barcode: String, qty:Int, meta:String, price:Int): Int
    {
        return getDbInstance().use {

            insert(DraftBarcode.TABLE_NAME,
                    DraftBarcode.COLUMN_PAGE_NAME to pageName,
                    DraftBarcode.COLUMN_BARCODE to barcode,
                    DraftBarcode.COLUMN_QTY to qty,
                    DraftBarcode.COLUMN_META to meta,
                    DraftBarcode.COLUMN_PRICE to price
            ).toInt()

        }
    }

    fun getDraftBarcodeByPageName(pageName: String): List<DraftBarcode>
    {
        return getDbInstance().use {

            var columns = DraftBarcode.COLUMN_PAGE_NAME + "," + DraftBarcode.COLUMN_BARCODE + ","
            columns += DraftBarcode.COLUMN_QTY + "," + DraftBarcode.COLUMN_META + "," + DraftBarcode.COLUMN_PRICE

            val query = select(DraftBarcode.TABLE_NAME, columns)
                    .whereArgs("(" + DraftBarcode.COLUMN_PAGE_NAME  + "={page_name})",
                            "page_name" to pageName)

            query.exec {
                val rowParser = classParser<DraftBarcode>()
                parseList(rowParser)
            }

        }
    }

    fun removeDraftBarcode(pageName: String)
    {
        getDbInstance().use {
            delete(DraftBarcode.TABLE_NAME, DraftBarcode.COLUMN_PAGE_NAME + "='" + pageName + "'")
        }
    }

}