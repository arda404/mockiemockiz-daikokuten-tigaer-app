package com.mockie.daikokuten.database.upgrades

import android.database.sqlite.SQLiteDatabase
import com.mockie.daikokuten.database.models.*
import org.jetbrains.anko.db.*


class Version8(database: SQLiteDatabase) {

    private fun alterSyncLog(database: SQLiteDatabase)
    {
        database.execSQL("ALTER TABLE " + SyncLog.TABLE_NAME + " ADD COLUMN " + SyncLog.COLUMN_NEXT_PAGE + " INTEGER DEFAULT 0;")
        database.execSQL("ALTER TABLE " + SyncLog.TABLE_NAME + " ADD COLUMN " + SyncLog.COLUMN_LAST_SYNC_TEMP + " VARCHAR;")
        database.execSQL("ALTER TABLE " + SyncLog.TABLE_NAME + " ADD COLUMN " + SyncLog.COLUMN_PROGRESS + " VARCHAR;")
        database.execSQL("ALTER TABLE " + SyncLog.TABLE_NAME + " ADD COLUMN " + SyncLog.COLUMN_TO_DOWNLOAD + " INTEGER;")
        database.execSQL("ALTER TABLE " + SyncLog.TABLE_NAME + " ADD COLUMN " + SyncLog.COLUMN_UNTIL_TIME + " VARCHAR;")
    }

    private fun insertLastSyncPaymentMethodTable(database: SQLiteDatabase)
    {
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PaymentMethod.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun insertLastSyncInventoryTable(database: SQLiteDatabase)
    {
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to Inventory.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun insertLastSyncStockTaking(database: SQLiteDatabase)
    {
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to StockTaking.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun insertLastMoveInventory(database: SQLiteDatabase)
    {
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to MovedInventory.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun createInvoiceToSync(database: SQLiteDatabase)
    {
        database.dropTable(InvoiceToSync.TABLE_NAME, true)
        database.createTable(
                InvoiceToSync.TABLE_NAME,
                true,
                InvoiceToSync.COLUMN_INVOICE_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                InvoiceToSync.COLUMN_REAL_INVOICE_ID to INTEGER,
                InvoiceToSync.COLUMN_USER_ID to INTEGER,
                InvoiceToSync.COLUMN_STORE to TEXT,
                InvoiceToSync.COLUMN_CUSTOMER_ID to INTEGER,
                InvoiceToSync.COLUMN_CUSTOMER_NAME to TEXT,
                InvoiceToSync.COLUMN_CUSTOMER_PHONE to TEXT,
                InvoiceToSync.COLUMN_ADDRESS_ID to INTEGER,
                InvoiceToSync.COLUMN_ADDRESS_NAME to TEXT,
                InvoiceToSync.COLUMN_ADDRESS_PHONE to TEXT,
                InvoiceToSync.COLUMN_ADDRESS to TEXT,
                InvoiceToSync.COLUMN_NOTE to TEXT,
                InvoiceToSync.COLUMN_STATUS to INTEGER,
                InvoiceToSync.COLUMN_SHIPPING_FEE to REAL,
                InvoiceToSync.COLUMN_TOTAL_BILLING to REAL,
                InvoiceToSync.COLUMN_ADDITIONAL_PRICE_PER_ITEM to INTEGER,
                InvoiceToSync.COLUMN_LOCK_DATE to TEXT,
                InvoiceToSync.COLUMN_LOCAL_CREATED_AT to TEXT
        )

        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to InvoiceToSync.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun createInvoiceItemToSync(database: SQLiteDatabase)
    {
        database.dropTable(InvoiceItemToSync.TABLE_NAME, true)
        database.createTable(
                InvoiceItemToSync.TABLE_NAME,
                true,
                InvoiceItemToSync.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                InvoiceItemToSync.COLUMN_TEMP_INVOICE_ID to INTEGER,
                InvoiceItemToSync.COLUMN_REAL_INVOICE_ID to INTEGER,
                InvoiceItemToSync.COLUMN_ITEM_CODE to TEXT,
                InvoiceItemToSync.COLUMN_ITEM_QUANTITY to INTEGER,
                InvoiceItemToSync.COLUMN_ITEM_PRICE to INTEGER,
                InvoiceItemToSync.COLUMN_ITEM_RETUR to INTEGER)

        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to InvoiceItemToSync.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun createDraftBarcode(database: SQLiteDatabase)
    {
        database.dropTable(DraftBarcode.TABLE_NAME, true)
        database.createTable(
                DraftBarcode.TABLE_NAME,
                true,
                DraftBarcode.COLUMN_ID to INTEGER + PRIMARY_KEY,
                DraftBarcode.COLUMN_PAGE_NAME to TEXT,
                DraftBarcode.COLUMN_BARCODE to TEXT,
                DraftBarcode.COLUMN_QTY to INTEGER,
                DraftBarcode.COLUMN_META to TEXT,
                DraftBarcode.COLUMN_PRICE to INTEGER)
    }

    private fun createPaymentMethod(database: SQLiteDatabase)
    {
        database.dropTable(PaymentMethod.TABLE_NAME, true)
        database.createTable(
                PaymentMethod.TABLE_NAME,
                true,
                PaymentMethod.COLUMN_ID to INTEGER + PRIMARY_KEY,
                PaymentMethod.COLUMN_READABLE to TEXT)

        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PaymentMethod.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun createReturnToSync(database: SQLiteDatabase)
    {
        database.dropTable(ReturnToSync.TABLE_NAME, true)
        database.createTable(
                ReturnToSync.TABLE_NAME,
                true,
                ReturnToSync.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                ReturnToSync.COLUMN_USER_ID to INTEGER,
                ReturnToSync.COLUMN_STORE to TEXT,
                ReturnToSync.COLUMN_CUSTOMER_ID to INTEGER,
                ReturnToSync.COLUMN_CUSTOMER_PHONE to TEXT,
                ReturnToSync.COLUMN_TEMP_INVOICE_ID to INTEGER,
                ReturnToSync.COLUMN_REAL_INVOICE_ID to INTEGER,
                ReturnToSync.COLUMN_BARCODE to TEXT,
                ReturnToSync.COLUMN_QUANTITY to INTEGER,
                ReturnToSync.COLUMN_CALCULATED_PRICE to INTEGER,
                ReturnToSync.COLUMN_LOCAL_CREATED_AT to TEXT
                )

        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to ReturnToSync.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun createPaymentToSync(database: SQLiteDatabase)
    {
        database.dropTable(PaymentToSync.TABLE_NAME, true)
        database.createTable(
                PaymentToSync.TABLE_NAME,
                true,
                PaymentToSync.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                PaymentToSync.COLUMN_USER_ID to INTEGER,
                PaymentToSync.COLUMN_STORE to TEXT,
                PaymentToSync.COLUMN_CUSTOMER_ID to INTEGER,
                PaymentToSync.COLUMN_CUSTOMER_PHONE to TEXT,
                PaymentToSync.COLUMN_AMOUNT to INTEGER,
                PaymentToSync.COLUMN_METHOD to TEXT
        )

        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to PaymentToSync.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0")
    }

    private fun createCustomer(database: SQLiteDatabase)
    {
        database.dropTable(Customer.TABLE_NAME, true)
        database.createTable(
                Customer.TABLE_NAME,
                true,
                Customer.COLUMN_ID to INTEGER + PRIMARY_KEY,
                Customer.COLUMN_NAME to TEXT,
                Customer.COLUMN_PHONE to TEXT,
                Customer.COLUMN_BALANCE to REAL,
                Customer.COLUMN_CREATED_AT to TEXT,
                Customer.COLUMN_UPDATED_AT to TEXT)

        // insert
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to Customer.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
    }


    private fun createLedgerToSync(database: SQLiteDatabase)
    {
        database.dropTable(LedgerToSync.TABLE_NAME, true)
        database.createTable(
                LedgerToSync.TABLE_NAME,
                true,
                LedgerToSync.COLUMN_ID to INTEGER + PRIMARY_KEY,
                LedgerToSync.COLUMN_USER_ID to INTEGER,
                LedgerToSync.COLUMN_CUSTOMER_ID to INTEGER,
                LedgerToSync.COLUMN_CUSTOMER_PHONE to TEXT,
                LedgerToSync.COLUMN_INVOICE_ID to INTEGER,
                LedgerToSync.COLUMN_TEMP_INVOICE_ID to INTEGER,
                LedgerToSync.COLUMN_STORE to TEXT,
                LedgerToSync.COLUMN_TYPE to TEXT,
                LedgerToSync.COLUMN_VALUE to INTEGER)

        // insert
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to LedgerToSync.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
    }

    private fun createCustomerAddress(database: SQLiteDatabase)
    {
        database.dropTable(CustomerAddress.TABLE_NAME, true)
        database.createTable(
                CustomerAddress.TABLE_NAME,
                true,
                CustomerAddress.COLUMN_ID to INTEGER + PRIMARY_KEY,
                CustomerAddress.COLUMN_CUSTOMER_ID to INTEGER,
                CustomerAddress.COLUMN_NAME to TEXT,
                CustomerAddress.COLUMN_PHONE to TEXT,
                CustomerAddress.COLUMN_ADDRESS to TEXT,
                CustomerAddress.COLUMN_CREATED_AT to TEXT,
                CustomerAddress.COLUMN_UPDATED_AT to TEXT)

        // insert
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to CustomerAddress.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
    }

    private fun createInvoice(database: SQLiteDatabase)
    {
        database.dropTable(Invoices.TABLE_NAME, true)
        database.createTable(
                Invoices.TABLE_NAME,
                true,
                Invoices.COLUMN_ID to INTEGER + PRIMARY_KEY,
                Invoices.COLUMN_CUSTOMER_ID to INTEGER,
                Invoices.COLUMN_ADDRESS_ID to INTEGER,
                Invoices.COLUMN_SHIPPING_CHARGE to REAL,
                Invoices.COLUMN_TOTAL_BILLING to REAL,
                Invoices.COLUMN_STATUS to INTEGER,
                Invoices.COLUMN_STORE to TEXT,
                Invoices.COLUMN_NOTE to TEXT,
                Invoices.COLUMN_USER_ID to INTEGER,
                Invoices.COLUMN_CREATED_AT to TEXT,
                Invoices.COLUMN_UPDATED_AT to TEXT,
                Invoices.COLUMN_ADDITIONAL_PRICE_PER_ITEM to INTEGER,
                Invoices.COLUMN_META to TEXT)
        // insert
        database.insert(SyncLog.TABLE_NAME,
                SyncLog.COLUMN_TABLE_NAME to Invoices.TABLE_NAME,
                SyncLog.COLUMN_LAST_SYNC to "0"
        )
    }

    private fun createInvoiceItem(database: SQLiteDatabase)
    {
        database.dropTable(InvoiceItems.TABLE_NAME, true)
        database.createTable(
                InvoiceItems.TABLE_NAME,
                true,
                InvoiceItems.COLUMN_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                InvoiceItems.COLUMN_INVOICE_ID to INTEGER,
                InvoiceItems.COLUMN_CODE to TEXT,
                InvoiceItems.COLUMN_QUANTITY to INTEGER,
                InvoiceItems.COLUMN_PRICE to INTEGER,
                InvoiceItems.COLUMN_RETUR to INTEGER)
    }

    fun upgrade(database: SQLiteDatabase)
    {
        alterSyncLog(database)
        createDraftBarcode(database)
        createInvoiceToSync(database)
        createInvoiceItemToSync(database)
        createCustomer(database)
        createCustomerAddress(database)
        createInvoice(database)
        createInvoiceItem(database)
        createPaymentMethod(database)
        createReturnToSync(database)
        createPaymentToSync(database)
        createLedgerToSync(database)

        insertLastSyncInventoryTable(database)
        insertLastMoveInventory(database)
        insertLastSyncStockTaking(database)
        insertLastSyncPaymentMethodTable(database)
    }
}

