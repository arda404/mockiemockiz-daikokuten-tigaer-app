package com.mockie.daikokuten.database.models

/**
 * Created by mockie on 18/12/17.
 */

data class StockTaking(
        val localCreatedAt:String,
        val id:Int,
        val code: String,
        val stock_deviation: Int,
        val store: String
) {

    companion object {
        const val TABLE_NAME = "stock_taking"
        const val COLUMN_ID = "id"
        const val COLUMN_CODE = "code"
        const val COLUMN_STOCK_DEVIATION = "stock_deviation"
        const val COLUMN_STORE = "store"
        const val COLUMN_LOCAL_CREATED_AT = "local_created_at"
    }
}