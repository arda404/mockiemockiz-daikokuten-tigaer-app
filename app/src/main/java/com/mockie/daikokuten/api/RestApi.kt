package com.mockie.daikokuten.api

/**
 * Created by mockie on 06/12/17.
 */

import android.content.Context
import com.mockie.daikokuten.database.models.*
import com.mockie.daikokuten.database.models.Customer
import com.mockie.daikokuten.database.models.CustomerAddress
import com.mockie.daikokuten.database.repositories.FirebaseToken
import com.mockie.daikokuten.database.repositories.SyncLog
import com.mockie.daikokuten.database.repositories.User
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


class RestAPI(val context: Context) {

    private val tigaerApi: TigaerApi

    private val user = User(context).isLogin()

    private val email:String? = user?.email?.trim()

    private val store:String? = user?.store?.trim()

    private val firebaseToken = FirebaseToken(context).getToken()?.token

    init {
        var apiUrl = "app.tigaer.id"
        val userUrl = user?.api_base_url?.trim()


        if (userUrl != null )
        {
            apiUrl = user!!.api_base_url
        }

        val client = OkHttpClient()
        client.newBuilder().connectTimeout(5, TimeUnit.SECONDS)
        client.newBuilder().readTimeout(5, TimeUnit.SECONDS)

        val retrofit = Retrofit.Builder()
                .client(client)
                .baseUrl("http://$apiUrl/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        tigaerApi = retrofit.create(TigaerApi::class.java)
    }

    fun registerDevice(firebaseToken:String): Call<RegisterDeviceDataResponse> {
        return tigaerApi.registerDevice(user?.api_key, firebaseToken, email, store)
    }

    fun getUser(): Call<UserDataResponse> {
        return tigaerApi.getUser(user?.api_key)
    }

    fun getCustomer(): Call<CustomerDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(Customer.TABLE_NAME)
        return tigaerApi.getCustomer(user?.api_key, syncLog?.last_sync, syncLog?.next_page, syncLog?.until_time)
    }

    fun getInvoices(): Call<InvoiceSyncDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(Invoices.TABLE_NAME)
        return tigaerApi.getInvoices(user?.api_key, syncLog?.last_sync, syncLog?.next_page, syncLog?.until_time, store)
    }

    fun getCustomerAddress(): Call<CustomerAddressDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(CustomerAddress.TABLE_NAME)
        return tigaerApi.getCustomerAddress(user?.api_key, syncLog?.last_sync, syncLog?.next_page, syncLog?.until_time)
    }

    fun getCategory(): Call<ProductCategoryDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(PrdCategories.TABLE_NAME)
        return tigaerApi.getCategory(user?.api_key, syncLog?.last_sync, syncLog?.next_page, syncLog?.until_time)
    }

    fun getType(): Call<ProductTypeDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(PrdTypes.TABLE_NAME)
        return tigaerApi.getType(user?.api_key, syncLog?.last_sync, syncLog?.next_page, syncLog?.until_time)
    }

    fun getPriceRule(): Call<PriceRuleDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(PriceRules.TABLE_NAME)
        return tigaerApi.getPriceRule(user?.api_key, syncLog?.last_sync)
    }

    fun getPaymentMethod(): Call<GetPaymentMethodDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(PriceRules.TABLE_NAME)
        return tigaerApi.getPaymentMethod(user?.api_key, syncLog?.last_sync)
    }

    fun getSize(): Call<ProductSizeDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(PrdSizes.TABLE_NAME)
        return tigaerApi.getSize(user?.api_key, syncLog?.last_sync, syncLog?.next_page, syncLog?.until_time)
    }

    fun getPrice(): Call<ProductPriceDataResponse> {
        val syncLog = SyncLog(context).getTableInformation(PrdPrices.TABLE_NAME)
        return tigaerApi.getPrice(user?.api_key, syncLog?.last_sync, syncLog?.next_page, syncLog?.until_time)
    }

    fun addInventory(localCreatedAt:String, localId: Int, code: String, quantity: Int, store: String): Call<InventoryDataResponse> {
        return tigaerApi.addInventory(user?.api_key, firebaseToken, user!!.uid, localCreatedAt, localId, code, quantity, store)
    }

    fun moveInventory(localCreatedAt:String, localId:Int, code: String, quantity: Int, oldStore: String, newStore: String): Call<MovedInventoryDataResponse> {
        return tigaerApi.moveInventory(user?.api_key, firebaseToken, user!!.uid, localCreatedAt, localId, code, quantity, oldStore, newStore)
    }

    fun stockTaking(localCreatedAt:String, localId:Int, code: String, quantity: Int, store: String): Call<StockTakingDataResponse> {
        return tigaerApi.stockTaking(user?.api_key, firebaseToken, user!!.uid, localCreatedAt, localId, code, quantity, store)
    }

    fun addPayment(
            paymentId: ArrayList<Int>,
            paymentUserId: ArrayList<Int>,
            paymentStore: ArrayList<String>,
            paymentCustomerId: ArrayList<Int>,
            paymentCustomerPhone: ArrayList<String>,
            paymentAmount: ArrayList<Int>,
            paymentMethod:ArrayList<String>
    ): Call<AddPaymentDataResponse>
    {
        return tigaerApi.addPayment(user?.api_key,
                firebaseToken,
                paymentId,
                paymentUserId,
                paymentStore,
                paymentCustomerId,
                paymentCustomerPhone,
                paymentAmount,
                paymentMethod)
    }


    fun addReturn(
            returnLocalCreatedAt: ArrayList<String>,
            returnId: ArrayList<Int>,
            returnInvoiceId: ArrayList<Int>,
            returnUserId: ArrayList<Int>,
            returnStore: ArrayList<String>,
            returnCustomerId: ArrayList<Int>,
            returnCustomerPhone: ArrayList<String>,
            returnBarcode: ArrayList<String>,
            returnQty: ArrayList<Int>,
            returnAmount:ArrayList<Int>
    ): Call<AddReturnDataResponse>
    {
        return tigaerApi.addReturn(user?.api_key,
                firebaseToken,
                returnLocalCreatedAt,
                returnId,
                returnInvoiceId,
                returnUserId,
                returnStore,
                returnCustomerId,
                returnCustomerPhone,
                returnBarcode,
                returnQty,
                returnAmount)
    }

    fun addInvoice(
            localCreatedAt:String?,
            localId:Int,
            realInvoiceId:Int,
            customerId:Int,
            shippingCharge: Double,
            totalBilling:Double,
            status:Int,
            store:String?,
            addressId:Int,
            note:String?,
            userId:Int,
            customerName:String,
            customerPhone:String,
            customerAddressName:String?,
            customerAddressPhone:String?,
            customerAddress:String?,
            additionalPricePerItem:Int,
            itemCode:ArrayList<String>,
            itemQuantity: ArrayList<String>,
            itemPrice: ArrayList<Int>,
            paymentId: ArrayList<Int>,
            paymentUserId: ArrayList<Int>,
            paymentStore: ArrayList<String>,
            paymentCustomerId: ArrayList<Int>,
            paymentCustomerPhone: ArrayList<String>,
            paymentAmount: ArrayList<Int>,
            paymentMethod:ArrayList<String>,
            returnId:ArrayList<Int>,
            returnLocalCreatedAt: ArrayList<String>,
            returnInvoiceId: ArrayList<Int>,
            returnStore: ArrayList<String>,
            returnUserId: ArrayList<Int>,
            returnCustomerId: ArrayList<Int>,
            returnCustomerPhone: ArrayList<String>,
            returnBarcode:ArrayList<String>,
            returnQty:ArrayList<Int>,
            returnAmount:ArrayList<Int>
    ): Call<AddInvoiceDataResponse> {

        return tigaerApi.addInvoice(user?.api_key,
                firebaseToken,
                localCreatedAt,
                localId,
                realInvoiceId,
                customerId,
                shippingCharge,
                totalBilling,
                status,
                store,
                addressId,
                note,
                userId,
                customerName,
                customerPhone,
                customerAddressName,
                customerAddressPhone,
                customerAddress,
                additionalPricePerItem,
                itemCode,
                itemQuantity,
                itemPrice,
                paymentId,
                paymentUserId,
                paymentStore,
                paymentCustomerId,
                paymentCustomerPhone,
                paymentAmount,
                paymentMethod,
                returnId,
                returnLocalCreatedAt,
                returnInvoiceId,
                returnStore,
                returnUserId,
                returnCustomerId,
                returnCustomerPhone,
                returnBarcode,
                returnQty,
                returnAmount)
    }

}