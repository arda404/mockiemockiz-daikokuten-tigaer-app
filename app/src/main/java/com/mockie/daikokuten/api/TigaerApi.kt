package com.mockie.daikokuten.api

/**
 * Created by mockie on 06/12/17.
 */


import retrofit2.Call
import retrofit2.http.*

interface  TigaerApi {

    @POST("api/registerDevice")
    @FormUrlEncoded
    fun registerDevice(
            @Query("token") token: String?,
            @Field("firebase_token") firebaseToken: String?,
            @Field("email") email: String?,
            @Field("store") store: String?
    ): Call<RegisterDeviceDataResponse>

    @GET("api/user/get/token")
    fun getUser(
            @Query("token") token: String?
    ): Call<UserDataResponse>

    @GET("api/sync/prdtype?app=1")
    fun getType(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?,
            @Query("page") page: Int?,
            @Query("until_time") untilTime: String?
    ): Call<ProductTypeDataResponse>

    @GET("api/sync/categories?app=1")
    fun getCategory(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?,
            @Query("page") page: Int?,
            @Query("until_time") untilTime: String?
    ): Call<ProductCategoryDataResponse>

    @GET("api/sync/priceRule?app=1")
    fun getPriceRule(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?
    ): Call<PriceRuleDataResponse>

    @GET("api/payment/method/get?app=1")
    fun getPaymentMethod(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?
    ): Call<GetPaymentMethodDataResponse>


    @GET("api/sync/prdsize?app=1")
    fun getSize(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?,
            @Query("page") page: Int?,
            @Query("until_time") untilTime: String?
    ): Call<ProductSizeDataResponse>

    @GET("api/sync/customer?app=1")
    fun getCustomer(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?,
            @Query("page") page: Int?,
            @Query("until_time") untilTime: String?
    ): Call<CustomerDataResponse>

    @GET("api/sync/invoice?app=1")
    fun getInvoices(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?,
            @Query("page") page: Int?,
            @Query("until_time") untilTime: String?,
            @Query("store") store: String?
    ): Call<InvoiceSyncDataResponse>

    @GET("api/sync/customer-address?app=1")
    fun getCustomerAddress(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?,
            @Query("page") page: Int?,
            @Query("until_time") untilTime: String?
    ): Call<CustomerAddressDataResponse>

    @GET("api/sync/prdprice?app=1")
    fun getPrice(
            @Query("token") token: String?,
            @Query("last_sync") lastSync: String?,
            @Query("page") page: Int?,
            @Query("until_time") untilTime: String?
    ): Call<ProductPriceDataResponse>

    @POST("api/sync/inventory/add?app=1")
    @FormUrlEncoded
    fun addInventory(
            @Query("token") token: String?,
            @Field("firebase_token") firebaseToken: String?,
            @Field("user_id") userId: Int,
            @Field("local_created_at") localCreatedAt: String?,
            @Field("local_id") localId: Int,
            @Field("code") code: String?,
            @Field("quantity") quantity: Int?,
            @Field("store") store: String?
    ): Call<InventoryDataResponse>

    @POST("api/sync/inventory/move?app=1")
    @FormUrlEncoded
    fun moveInventory(
            @Query("token") token: String?,
            @Field("firebase_token") firebaseToken: String?,
            @Field("user_id") userId: Int,
            @Field("local_created_at") localCreatedAt: String?,
            @Field("local_id") localId: Int,
            @Field("code") code: String?,
            @Field("quantity") quantity: Int?,
            @Field("old_store") oldStore: String?,
            @Field("new_store") newStore: String?
    ): Call<MovedInventoryDataResponse>

    @POST("api/sync/inventory/stock-taking?app=1")
    @FormUrlEncoded
    fun stockTaking(
            @Query("token") token: String?,
            @Field("firebase_token") firebaseToken: String?,
            @Field("user_id") userId: Int,
            @Field("local_created_at") localCreatedAt: String?,
            @Field("local_id") localId: Int,
            @Field("code") code: String?,
            @Field("quantity") quantity: Int?,
            @Field("store") oldStore: String?
    ): Call<StockTakingDataResponse>

    @POST("api/sync/payment/add?app=1")
    @FormUrlEncoded
    fun addPayment(
            @Query("token") token: String?,
            @Field("firebase_token") firebaseToken: String?,
            @Field("payment[id][]") paymentId: ArrayList<Int>,
            @Field("payment[user_id][]") paymentUserId: ArrayList<Int>,
            @Field("payment[store][]") paymentStore: ArrayList<String>,
            @Field("payment[customer_id][]") paymentCustomerId: ArrayList<Int>,
            @Field("payment[customer_phone][]") paymentCustomerPhone: ArrayList<String>,
            @Field("payment[amount][]") paymentAmount: ArrayList<Int>,
            @Field("payment[method][]") paymentMethod: ArrayList<String>
    ): Call<AddPaymentDataResponse>


    @POST("api/sync/return/add?app=1")
    @FormUrlEncoded
    fun addReturn(
            @Query("token") token: String?,
            @Field("firebase_token") firebaseToken: String?,
            @Field("retur[local_created_at][]") returnLocalCreatedAt: ArrayList<String>,
            @Field("retur[id][]") returnId: ArrayList<Int>,
            @Field("retur[invoice_id][]") returnInvoiceId: ArrayList<Int>,
            @Field("retur[user_id][]") returnUserId: ArrayList<Int>,
            @Field("retur[store][]") returnStore: ArrayList<String>,
            @Field("retur[customer_id][]") returnCustomerId: ArrayList<Int>,
            @Field("retur[customer_phone][]") returnCustomerPhone: ArrayList<String>,
            @Field("retur[barcode][]") returnBarcode: ArrayList<String>,
            @Field("retur[qty][]") returnQty: ArrayList<Int>,
            @Field("retur[amount][]") returnAmount: ArrayList<Int>
    ): Call<AddReturnDataResponse>

    @POST("api/sync/invoice/add?app=1")
    @FormUrlEncoded
    fun addInvoice(
            @Query("token") token: String?,
            @Field("firebase_token") firebaseToken: String?,
            @Field("local_created_at") localCreatedAt: String?,
            @Field("local_id") localId: Int,
            @Field("invoice_id") invoiceId: Int, // REAL INVOICE ID NOT TEMPORARY
            @Field("customer_id") customerId: Int,
            @Field("shipping_charge") shippingCharge: Double,
            @Field("total_billing") totalBilling: Double,
            @Field("status") status: Int,
            @Field("store") store: String?,
            @Field("address_id") address_id: Int,
            @Field("note") note: String?,
            @Field("user_id") userId: Int,
            @Field("customer_name") customerName: String,
            @Field("customer_phone") customerPhone: String,
            @Field("customer_address_name") customerAddressName: String?,
            @Field("customer_address_phone") customerAddressPhone: String?,
            @Field("customer_address") customerAddress: String?,
            @Field("additional_price_per_item") additionalPricePerItem: Int,
            @Field("item[code][]") itemCode: ArrayList<String>,
            @Field("item[quantity][]") itemQuantity: ArrayList<String>,
            @Field("item[price][]") itemPrice: ArrayList<Int>,
            @Field("payment[id][]") paymentId: ArrayList<Int>,
            @Field("payment[user_id][]") paymentUserId: ArrayList<Int>,
            @Field("payment[store][]") paymentStore: ArrayList<String>,
            @Field("payment[customer_id][]") paymentCustomerId: ArrayList<Int>,
            @Field("payment[customer_phone][]") paymentCustomerPhone: ArrayList<String>,
            @Field("payment[amount][]") paymentAmount: ArrayList<Int>,
            @Field("payment[method][]") paymentMethod: ArrayList<String>,
            @Field("retur[id][]") returnId: ArrayList<Int>,
            @Field("retur[local_created_at][]") returLocalCreatedAt: ArrayList<String>,
            @Field("retur[invoice_id][]") returnInvoiceId: ArrayList<Int>,
            @Field("retur[store][]") returnStore: ArrayList<String>,
            @Field("retur[user_id][]") returnUserId: ArrayList<Int>,
            @Field("retur[customer_id][]") returnCustomerId: ArrayList<Int>,
            @Field("retur[customer_phone][]") returnCustomerPhone: ArrayList<String>,
            @Field("retur[barcode][]") returnBarcode: ArrayList<String>,
            @Field("retur[qty][]") returnQty: ArrayList<Int>,
            @Field("retur[amount][]") returnAmount: ArrayList<Int>
    ): Call<AddInvoiceDataResponse>

}