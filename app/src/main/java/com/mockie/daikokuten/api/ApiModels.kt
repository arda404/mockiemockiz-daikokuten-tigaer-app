package com.mockie.daikokuten.api



class RegisterDeviceDataResponse(
        val status: String,
        val message:String
)

// ============================= prdtype =========================
class ProductType(
        val productType: String,
        val appactive: Int,
        val readable: String,
        val patterned: Int,
        val sorter: Int,
        val categories: String,
        val updated_at: String
)


class ProductTypeDataResponse(
        val progress: String,
        val until_time: String,
        val status: String,
        val message: String,
        val current_page: Int,
        val first_page_url: String,
        val from: Int?,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String?,
        val path: String,
        val per_page: Int,
        val prev_page_url: String?,
        val to: Int?,
        val total: Int,
        val time: String,
        val data: List<ProductType>
)

// ============================= prdsize =========================

class ProductSize(
        val size: String,
        val readable: String,
        val sorter: Int,
        val updated_at: String
)

class ProductSizeDataResponse(
        val progress: String,
        val until_time: String,
        val status: String,
        val message: String,
        val current_page: Int,
        val first_page_url: String,
        val from: Int?,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String?,
        val path: String,
        val per_page: Int,
        val prev_page_url: String?,
        val to: Int?,
        val total: Int,
        val time: String,
        val data: List<ProductSize>
)

// ============================= prdprice =========================

class ProductPrice(
        val typesize: String,
        val active: Int,
        val price: String,
        val weight: Int,
        val sort: Int,
        val apptypesize: String,
        val appactive: Int,
        val appupdate: Int,
        val updated_at: String
)

class ProductPriceDataResponse(
        val progress: String,
        val until_time: String,
        val status: String,
        val message: String,
        val current_page: Int,
        val first_page_url: String,
        val from: Int?,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String?,
        val path: String,
        val per_page: Int,
        val prev_page_url: String?,
        val to: Int?,
        val total: Int,
        val time: String,
        val data: List<ProductPrice>
)

class InventoryDataResponse(
        val status: String,
        val status_code: String,
        val message: String,
        val time:String
)

class AddPaymentDataResponse(
        val status: String,
        val message: String,
        val time: String,
        val payment_ids: List<Int>
)

class AddReturnDataResponse(
        val status: String,
        val message: String,
        val time: String,
        val return_ids: List<Int>
)

class AddInvoiceDataResponse(
        val status: String,
        val message: String,
        val time: String,
        val payment_ids: List<Int>,
        val return_ids: List<Int>
)

class MovedInventoryDataResponse(
        val status: String,
        val time: String,
        val message: String
)

class StockTakingDataResponse(
        val status: String,
        val status_code: String,
        val time: String,
        val message: String
)

class UploadPaymentMethodDataResponse(
        val status: String,
        val status_code: String,
        val time: String,
        val message: String
)

class UserDataResponse(
        val status: String,
        val message: String,
        val data: MyUser
)

class PriceRuleDataResponse(
        val status: String,
        val message: String,
        val time: String,
        val total_data: Int,
        val data: List<PriceRule>
)

class PaymentMethod(
        val id:Int,
        val name:String
)

class GetPaymentMethodDataResponse(
        val status: String,
        val message: String,
        val time: String,
        val total_data: Int,
        val data: List<PaymentMethod>
)

class PriceRule(
        val store: String,
        val start: Int,
        val amount: Int
)

class MyUser(
        val user: Int,
        val email: String
)

// ============================= prdtype =========================
class ProductCategory(
        val category: String,
        val readable: String,
        val sort: Int,
        val updated_at: String
)

class ProductCategoryDataResponse(
        val progress: String,
        val until_time: String,
        val status: String,
        val message: String,
        val current_page: Int,
        val first_page_url: String,
        val from: Int?,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String?,
        val path: String,
        val per_page: Int,
        val prev_page_url: String?,
        val to: Int?,
        val total: Int,
        val time: String,
        val data: List<ProductCategory>
)

// ============================= prdsize =========================

class Customer(
        val id: Int,
        val name:String,
        val phone: String,
        val balance: Int,
        val created_at:String,
        val updated_at:String
)


class CustomerDataResponse(
        val progress: String,
        val until_time: String,
        val status: String,
        val message: String,
        val current_page: Int,
        val first_page_url: String,
        val from: Int?,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String?,
        val path: String,
        val per_page: Int,
        val prev_page_url: String?,
        val to: Int?,
        val total: Int,
        val time: String,
        val data: List<Customer>
)


class CustomerAddress(
        val id: Int,
        val customer_id:Int,
        val name:String,
        val phone: String,
        val address: String,
        val created_at:String,
        val updated_at:String
)


class CustomerAddressDataResponse(
        val progress: String,
        val until_time: String,
        val status: String,
        val message: String,
        val current_page: Int,
        val first_page_url: String,
        val from: Int?,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String?,
        val path: String,
        val per_page: Int,
        val prev_page_url: String?,
        val to: Int?,
        val total: Int,
        val time: String,
        val data: List<CustomerAddress>
)

class InvoiceSyncDataResponse(
        val progress: String,
        val until_time: String,
        val status: String,
        val message: String,
        val current_page: Int,
        val first_page_url: String,
        val from: Int?,
        val last_page: Int,
        val last_page_url: String,
        val next_page_url: String?,
        val path: String,
        val per_page: Int,
        val prev_page_url: String?,
        val to: Int?,
        val total: Int,
        val time: String,
        val data:List<InvoiceDataResponse2>
)

class InvoiceDataResponse2(
        val invoice: InvoiceTheData,
        val item: List<ItemTheData>,
        val address: AddressTheData?,
        val customer: Customer
)

class InvoiceTheData(
        val id:Int,
        val customer_id: Int,
        val shipping_charge:Int,
        val total_billing: Int,
        val status: Int,
        val store:String,
        val address_id:Int,
        val note:String,
        val user_id: Int,
        val created_at: String,
        val updated_at: String,
        val additional_charge_per_item: Int,
        val meta: String
)

class ItemTheData(
        val id:Int,
        val invoice_id:Int,
        val code: String,
        val quantity:Int,
        val price:Int,
        val retur:Int
)

class AddressTheData(
        val id:Int,
        val customer_id:Int,
        val name:String,
        val phone: String,
        val address:String,
        val created_at:String,
        val updated_at:String
)

