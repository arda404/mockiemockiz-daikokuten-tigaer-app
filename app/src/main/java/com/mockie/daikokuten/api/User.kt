package com.mockie.daikokuten.api

import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import retrofit2.Response
import java.lang.ref.WeakReference
import com.mockie.daikokuten.helpers.AlertHelper

/**
 * Created by mockie on 06/12/17.
 */

class User(val context: Context, val activity: Activity) : AsyncTask<Void, Void, Response<UserDataResponse>>() {

    var weakContext: WeakReference<Context> = WeakReference(context)

    override fun doInBackground(vararg params: Void): Response<UserDataResponse> {

        val typeAPI = RestAPI(weakContext.get()!!)
        val callResponse = typeAPI.getUser()

        return callResponse.execute()

    }

    override fun onPostExecute(response: Response<UserDataResponse>) {
        try {
            if (response.isSuccessful) {
                val myResponse: UserDataResponse = response.body()!!

                if (myResponse.status != "success")
                {
                    AlertHelper(weakContext.get()!!).wrongToken()
                }
            }
        } catch (e:Exception) {}
    }
}