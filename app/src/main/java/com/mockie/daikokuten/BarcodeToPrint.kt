package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.mockie.daikokuten.adapters.RVBarcodeToPrintAdapter
import com.mockie.daikokuten.database.repositories.BarcodeToPrintRepository
import com.mockie.daikokuten.models.BarcodeToPrintList
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import com.mockie.daikokuten.Printers.TscTtp244Printer
import com.mockie.daikokuten.listeners.RecyclerBarcodeToPrintTouchHelper
import java.lang.Math.ceil


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [BarcodeToPrint.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [BarcodeToPrint.newInstance] factory method to
 * create an instance of this fragment.
 */
class BarcodeToPrint : BarcodeRecyclerBaseFragment(), RecyclerBarcodeToPrintTouchHelper.RecyclerItemTouchHelperListener{

    lateinit var adapter: RVBarcodeToPrintAdapter
    lateinit var barcode: ArrayList<BarcodeToPrintList>
    lateinit var print:Button

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is RVBarcodeToPrintAdapter.ViewHolder) {
            adapter.removeItem(viewHolder.adapterPosition)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_barcode_to_print, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        print = view.findViewById(R.id.print)

        val data = BarcodeToPrintRepository(context).getAllBarcodes()
        barcode = BarcodeToPrintList.add(0, "", "", "",0, false)
        barcode.clear()

        for (i in 0 until data.size){
            barcode = BarcodeToPrintList.add(data[i].id, data[i].barcode, data[i].meta, data[i].size, data[i].qty, false)
        }

        adapter = RVBarcodeToPrintAdapter(context, recyclerView, barcode)
        recyclerView.adapter = adapter

        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = false
        recyclerView.layoutManager = layoutManager

        val itemTouchHelperCallback = RecyclerBarcodeToPrintTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        val fab = view.findViewById<View>(R.id.fab)
        fab.setOnClickListener {
            onClickAdd()
        }

        print.setOnClickListener {
            onClickPrint()
        }

        // Inflate the layout for this fragment
        return view
    }

    private fun onClickPrint()
    {
        val printer = TscTtp244Printer(context)
        val handler = Handler()
        print.isEnabled = false
        handler.postDelayed({
            for(i in 0 until barcode.size)
            {
                if (barcode[i].checked)
                {
                    val page: Int = ceil(barcode[i].qty.toString().toDouble() /3).toInt()
                    printer.print(barcode[i].barcode, barcode[i].meta, barcode[i].size, page)
                }
            }
            print.isEnabled = true
        }, 3000)
    }

    private fun onClickAdd()
    {
        val someFragment = PrintBarcodeFragment()
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.inventoryContainer, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BarcodeToPrint.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): BarcodeToPrint {
            val fragment = BarcodeToPrint()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
