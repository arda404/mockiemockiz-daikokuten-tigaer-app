package com.mockie.daikokuten

import android.app.Activity
import android.support.v4.app.Fragment
import android.widget.Button
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.helpers.UserHelper
import com.mockie.daikokuten.models.BarcodeList

/**
 * Created by mockie on 05/03/18.
 */

open class BarcodeRecyclerBaseFragment : Fragment() {

    lateinit var barcodeList: ArrayList<BarcodeList>

    var permissionRoles = ArrayList<String>()

    fun button()
    {
        if (this.context != null)
        {
            val dialog = AlertHelper(this.context).onBackPressedAlert()

            dialog.findViewById<Button>(R.id.editButton).setOnClickListener {

                try {
                    (this.context as Activity).moveTaskToBack(true)
                } catch (e:Exception) {}

                dialog.dismiss()
            }

            dialog.findViewById<Button>(R.id.noDialog).setOnClickListener {
                dialog.hide()
                dialog.dismiss()
            }
        }
    }

    fun setPermissionsRoles(role: String)
    {
        permissionRoles.add(role)
    }

    fun checkPermission()
    {
        if (!UserHelper(this.context).checkPermission(permissionRoles))
        {
            AlertHelper(this.context).permissionDenied()
        }
    }
}
