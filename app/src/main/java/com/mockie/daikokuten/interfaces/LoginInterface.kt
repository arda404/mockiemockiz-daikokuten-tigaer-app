package com.mockie.daikokuten.interfaces

import android.content.Context
import android.content.Intent
import android.util.Log
import android.webkit.JavascriptInterface
import com.mockie.daikokuten.AfterLoginActivity
import com.mockie.daikokuten.database.repositories.User
import com.mockie.daikokuten.helpers.DeviceID
import java.util.regex.Pattern
import org.json.JSONObject


/**
 * Created by mockie on 17/12/17.
 */

internal class LoginInterface(val context: Context) {

    @JavascriptInterface
    fun showHTML(html: String) {
        var json: JSONObject?
        val pattern = Pattern.compile("<body>(\\{.+\\})</body>")
        val matcher = pattern.matcher(html)

        if (matcher.find())
        {
            val jsonResponse = matcher.group(1)

            json = JSONObject(jsonResponse)

            val userRepo = User(context)
            userRepo.logout()

            val uid = json.get("uid")!!.toString().toInt()
            val name = json.get("name")!!.toString()
            val balance = json.get("balance")!!.toString().toInt()
            val store = json.get("store")!!.toString()
            val key = json.get("key")!!.toString()
            val adminCap = json.get("admincaps")!!.toString()
            val apiBaseUrl = json.get("apibase")!!.toString()
            val email = json.get("name")!!.toString()

            //val deviceId = DeviceID(context).getDeviceUniqueID()

            userRepo.add(uid, name, balance, adminCap, store, key, apiBaseUrl, email)

            val intent = Intent(context, AfterLoginActivity::class.java)
            context.startActivity(intent)
        }
    }

}