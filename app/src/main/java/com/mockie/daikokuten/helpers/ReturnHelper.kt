package com.mockie.daikokuten.helpers

import android.content.Context
import com.mockie.daikokuten.database.repositories.InvoiceItem
import com.mockie.daikokuten.database.repositories.InvoiceItemToSync
import com.mockie.daikokuten.database.repositories.ReturnToSync
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.models.ItemInBasket


class ReturnHelper(val context: Context)
{

    private fun getRetur(tempInvoiceId:Int, realInvoiceId:Int, barcode:String):Int
    {
        var retur = 0

        val returToSync = ReturnToSync(context).getReturnBy(tempInvoiceId, realInvoiceId, barcode)
        if (returToSync != null) {
            retur += returToSync.quantity
        }

        return retur
    }

    fun getItemInBasket(tempInvoiceId:Int, realInvoiceId:Int): ArrayList<ItemInBasket>
    {
        val itemInBasket = ArrayList<ItemInBasket>()

        if (tempInvoiceId != 0)
        {
            val invoiceItem = InvoiceItemToSync(context).getInvoiceItemToSyncByTempInvoiceId(tempInvoiceId)

            for (inv in invoiceItem)
            {
                val rt = inv.retur + getRetur(tempInvoiceId, realInvoiceId, inv.code)
                itemInBasket.add(ItemInBasket(inv.code, inv.quantity, inv.price, rt))

            }
        }
        else
        {
            val invoiceItem = InvoiceItem(context).getInvoiceItemsByInvoiceId(realInvoiceId)

            for (inv in invoiceItem)
            {
                val rt = inv.retur + getRetur(0, realInvoiceId, inv.code)
                itemInBasket.add(ItemInBasket(inv.code, inv.quantity, inv.price, rt))
            }
        }

        return itemInBasket
    }

}