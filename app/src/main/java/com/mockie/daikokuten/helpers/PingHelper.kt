package com.mockie.daikokuten.helpers

import android.os.AsyncTask
import java.net.InetAddress
import java.net.UnknownHostException


/**
 * Created by mockie on 04/04/18.
 */

class PingHelper : AsyncTask<String, Int, String>() {
    override fun doInBackground(vararg params: String): String {
        var addr: InetAddress? = null
        try {
            addr = InetAddress.getByName(params[0])
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        }

        return addr!!.getHostAddress()
    }
}