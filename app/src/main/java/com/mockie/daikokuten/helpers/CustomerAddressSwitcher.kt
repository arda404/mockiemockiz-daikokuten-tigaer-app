package com.mockie.daikokuten.helpers

/**
 * Created by mockie on 07/12/17.
 */

class CustomerAddressSwitcher(var string: String, var tag: Int, var phone:String, val customerAddress:String) {

    override fun toString(): String {
        return string
    }

    fun getId(): Any {
        return tag
    }

    fun getCustomerAddressPhone():String
    {
        return phone
    }

    fun getAddress(): String
    {
        return customerAddress
    }
}