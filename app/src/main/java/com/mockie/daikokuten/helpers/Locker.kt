package com.mockie.daikokuten.helpers

import android.content.Context
import com.mockie.daikokuten.database.repositories.InvoiceToSync


class Locker (val context: Context) {

    fun onMainActivity()
    {
        InvoiceToSync(context).unlockAllData()
    }

    fun onInvoiceToEdit(phone:String)
    {
        InvoiceToSync(context).lockData(phone)
    }

    fun onInvoiceToSave(phone:String)
    {
        InvoiceToSync(context).unlockData(phone)
    }

    fun onPaymentToSave(phone:String)
    {
        InvoiceToSync(context).unlockData(phone)
    }

    fun onReturnToSave(phone:String)
    {
        InvoiceToSync(context).unlockData(phone)
    }

}