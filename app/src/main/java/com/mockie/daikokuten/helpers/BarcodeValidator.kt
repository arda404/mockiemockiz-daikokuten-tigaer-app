package com.mockie.daikokuten.helpers

import android.app.Activity
import android.content.Context
import android.os.Build
import android.support.v4.content.res.ResourcesCompat
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.models.ItemInBasket
import java.util.regex.Pattern

/**
 * Created by mockie on 09/07/18.
 */


class BarcodeValidator (val context:Context) {

    private var messageBarcode = ArrayList<String>()
    private var messageQty = ArrayList<String>()
    var message = ArrayList<String>()

    init {
        messageBarcode.removeAll(messageBarcode)
        messageQty.removeAll(messageQty)
    }

    fun lengthValidator(barcode:String): Boolean
    {
        if (barcode.length != 12)
        {
            messageBarcode.add("jumlah karakter barcode salah")
            return false
        }

        return true
    }

    fun skuValidator(barcode:String): Boolean
    {
        val bar = barcode.toUpperCase()
        val barcodeHelper = BarcodeListHelper(context)

        if (barcodeHelper.getTypeReadable(bar) == "" ||
                barcodeHelper.getSizeReadable(bar) == "" || barcodeHelper.getPrice(bar) == 0)
        {
            messageBarcode.add("SKU salah")
            return false
        }

        return true
    }

    fun barcodePatternValidator(barcode: String):Boolean
    {
        val bar = barcode.toUpperCase()
        val pattern = Pattern.compile("([a-zA-Z]{3}[0-9]{9})")
        val matcher = pattern.matcher(bar)

        if (!matcher.find())
        {
            messageBarcode.add("format barcode salah")
        }

        return matcher.find()
    }

    fun validator(): Boolean
    {
        if ((messageBarcode.size > 0) || (messageQty.size > 0)) {
            return false
        } else {
            return true
        }
    }

    fun isInBasket(barcode:String, itemInBasket:ArrayList<ItemInBasket>): Boolean
    {
        Log.d("momo 1", barcode)

        for (it in itemInBasket)
        {
            Log.d("momo 2", it.barcode + " | " + it.price + " | " + it.quantity)
        }

        var found = false
        val bar = barcode.toUpperCase()
        for (iib in itemInBasket) {
            if (iib.barcode == bar) {
                Log.d("momo", "(" + bar +")" + iib.barcode + " | " + iib.quantity + " | " + iib.retur)
                found = true
                break
            }
        }

        if (!found)
        {
            messageBarcode.add("Barang tidak ada di keranjang")
        }

        return found
    }

    fun checkReturQuantity(barcode:String, qty:Int, itemInBasket:ArrayList<ItemInBasket>): Int
    {
        val bar = barcode.toUpperCase()
        var found = false
        for (iib in itemInBasket) {
            Log.d("dodol 3", iib.barcode + " | " + bar)
            if (iib.barcode == bar) {
                val returQty = (iib.quantity - iib.retur)

                if (qty <= returQty)
                {
                    return qty
                }
                else
                {
                    messageBarcode.add("Kesempatan retur habis")
                    return returQty

                }
            }
        }

        return 0
    }

    fun getErrorMessage(): ArrayList<String>
    {
        message.addAll(messageBarcode)
        message.addAll(messageQty)

        return message
    }

    fun showMessage(barcodeForm:EditText?, qtyForm:EditText?, descriptionForm:TextView)
    {
        if (barcodeForm != null)
        {
            if (messageBarcode.size > 0)
            {
                barcodeForm.postDelayed({

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        barcodeForm.background = ResourcesCompat.getDrawable((context as Activity).resources, R.drawable.error, null)
                    } else {
                        barcodeForm.setBackgroundResource(R.drawable.error)
                    }

                    barcodeForm.postDelayed({
                        barcodeForm.requestFocus()
                        barcodeForm.selectAll()
                    }, 100)
                    BeepHelper().beep(600)
                }, 70)
            }
            else
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    barcodeForm.background = ResourcesCompat.getDrawable((context as Activity).resources, R.drawable.border_list, null)
                } else {
                    barcodeForm.setBackgroundResource(R.drawable.border_list)
                }
            }
        }

        if (qtyForm != null)
        {
            if (messageQty.size > 0)
            {
                qtyForm.postDelayed({
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        qtyForm.background = ResourcesCompat.getDrawable((context as Activity).resources, R.drawable.error, null)
                    } else {
                        qtyForm.setBackgroundResource(R.drawable.error)
                    }

                    qtyForm.setText("0")
                    qtyForm.requestFocus()
                    BeepHelper().beep(600)
                }, 70)
            }
            else
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    qtyForm.background = ResourcesCompat.getDrawable((context as Activity).resources, R.drawable.border_list, null)
                } else {
                    qtyForm.setBackgroundResource(R.drawable.border_list)
                }
            }
        }

        var message = ""

        for (mes in getErrorMessage())
        {
            message += mes + " | "
        }
Log.d("momo shit", message)
        descriptionForm.text = message
    }

}