package com.mockie.daikokuten.helpers

import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Handler
import android.os.Looper
import android.content.Context
import android.media.MediaPlayer
import android.util.Log
import com.mockie.daikokuten.R


/**
 * Created by mockie on 27/03/18.
 */

class BeepHelper
{


    fun beep(duration: Int)
    {
        try {
            val toneG = ToneGenerator(AudioManager.STREAM_ALARM, 100)

            toneG.startTone(ToneGenerator.TONE_DTMF_S, duration)
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                toneG.release()
            }, (duration + 50).toLong())
        } catch (e:Exception) {}
    }

    fun syncNotif(context: Context)
    {
        val mPlayer = MediaPlayer.create(context, R.raw.hellyeah)
        mPlayer.setVolume(0.3.toFloat(), 0.3.toFloat())
        mPlayer.start()
    }
}