package com.mockie.daikokuten.helpers

import android.app.Service
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import java.io.IOException


/**
 * Created by mockie on 18/01/18.
 */

class ConnectionHelper (val context: Context) {

    fun isConnectingToInterne(): Boolean
    {
        var connected = false
        val connectivityManager = context.getSystemService(Service.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo

        if ((activeNetworkInfo != null && activeNetworkInfo.isConnected))
        {
            connected = true
        }

        return connected
    }

    fun ping(): Boolean {
        val runtime = Runtime.getRuntime()
        try {
            val ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
            val exitValue = ipProcess.waitFor()
            return exitValue == 0
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        return false
    }

}