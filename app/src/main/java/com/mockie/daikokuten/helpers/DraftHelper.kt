package com.mockie.daikokuten.helpers

import android.content.Context
import android.util.Log
import com.mockie.daikokuten.database.repositories.Draft
import com.mockie.daikokuten.database.repositories.DraftBarcode
import com.mockie.daikokuten.models.BarcodeList
import java.util.regex.Pattern

/**
 * Created by mockie on 04/04/18.
 */

class DraftHelper(val context: Context){

    fun insertDraft(pageName: String, keyName:String, content:String?)
    {
        Draft(context).insert(pageName, keyName, content)
    }

    fun getDraft(pageName: String, keyName:String): com.mockie.daikokuten.database.models.Draft?
    {
        return Draft(context).getDraftByPageNameAndKeyName(pageName, keyName)
    }


    fun removeAllDraft(pageName: String)
    {
        Draft(context).removeAllDraft(pageName)
    }

    fun getDraftBarcode(pageName: String): ArrayList<BarcodeList>
    {
        val draft = DraftBarcode(context).getDraftBarcodeByPageName(pageName)
        var barcodeList = BarcodeList.initializeBarcode()

        for (a in 0 until draft.count())
        {
            barcodeList.add(BarcodeList(draft[a].barcode, draft[a].qty, draft[a].meta, draft[a].price, null))
        }

        return barcodeList
    }

    fun draftBarcode(pageName: String, barcodeList: ArrayList<BarcodeList>)
    {
        DraftBarcode(context).removeDraftBarcode(pageName)

        for(i in 0 until barcodeList.size)
        {
            if(barcodeList[i].barcode.trim()!="")
            {
                DraftBarcode(context).insert(pageName,
                        barcodeList[i].barcode.toUpperCase(),
                        barcodeList[i].qty.toString().toInt(),
                        barcodeList[i].meta,
                        barcodeList[i].price
                )
            }
        }
    }

    fun removeDraftBarcode(pageName: String)
    {
        DraftBarcode(context).removeDraftBarcode(pageName)
    }

}