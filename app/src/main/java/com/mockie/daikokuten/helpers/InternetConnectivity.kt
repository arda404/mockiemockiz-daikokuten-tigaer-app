package com.mockie.daikokuten.helpers

import android.os.AsyncTask
import java.net.InetSocketAddress
import java.net.Socket


internal class InternetCheck(private val mConsumer: Consumer) : AsyncTask<Void, Void, Boolean>() {

    interface Consumer {
        fun accept(internet: Boolean?)
    }

    init {
        execute()
    }

    override fun doInBackground(vararg voids: Void): Boolean? {
        var connected = false

        try{
            val sock = Socket()
            sock.connect(InetSocketAddress("8.8.8.8", 53), 500)
//            Log.d("rocket", "conneected : " + sock.isConnected.toString() + " | stream : " + sock.getInputStream().read() + " | output :" + sock.getOutputStream().write(1))
            connected = sock.isConnected
            sock.close()
        } catch (e:Exception) { return false }

        return connected
    }

    override fun onPostExecute(internet: Boolean?) {
        mConsumer.accept(internet)
    }
}
