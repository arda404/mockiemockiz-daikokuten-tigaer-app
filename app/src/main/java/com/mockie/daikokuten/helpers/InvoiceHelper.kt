package com.mockie.daikokuten.helpers

import android.content.Context


class InvoiceHelper(context: Context) {

    fun statusReadable(status:Int): String
    {
        when (status) {
            0 -> return "Draft"
            1 -> return "Tersimpan"
            2 -> return "Selesai"
            1000 -> return "Dihapus"
            else -> return "error"
        }
    }

}