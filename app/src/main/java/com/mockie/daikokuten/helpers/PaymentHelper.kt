package com.mockie.daikokuten.helpers

import android.util.Log
import android.widget.TextView
import com.mockie.daikokuten.models.PaymentMethod
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class PaymentHelper {

    fun readableAmountToInt(readable:String):Int
    {
        var num = 0

        try {
            num = readable.replace("Rp", "").replace(".", "").toInt()
        } catch (e:Exception) {}

        return num
    }

    fun amountToReadable(amount:Int): String
    {
        val format = NumberFormat.getCurrencyInstance(Locale("id", "id"))

        var money = ""
        try{
            money = format.format(BigDecimal(amount))
        } catch (e:Exception){}

        return money
    }

    fun getChange(paymentMethod: ArrayList<PaymentMethod>, debt:Int):Int
    {
        var totalPayment = 0

        for (d in 0 until paymentMethod.size)
        {
            if (paymentMethod[d].method.tag!="0")
            {
                try {
                    totalPayment += paymentMethod[d].amount.toInt()
                }catch (e:Exception) {}
            }
        }

        val ch:Int = -(debt - totalPayment)

        return if (ch > 0) { ch } else { 0 }
    }

}

