package com.mockie.daikokuten.helpers

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.mockie.daikokuten.LoginActivity
import com.mockie.daikokuten.NewMainActivity
import com.mockie.daikokuten.database.repositories.User
import java.util.regex.Pattern
import com.mockie.daikokuten.database.models.User as UserModel


/**
 * Created by mockie on 06/01/18.
 */


class UserHelper (val context: Context) {

    fun isLogin()
    {
        val userRepo = User(context)
        val loggedInUser = userRepo.isLogin()

        if (loggedInUser == null)
        {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        } else {
            val intent = Intent(context, NewMainActivity::class.java)
            context.startActivity(intent)
        }
    }

    fun checkToken()
    {
//        val userRepo = User(context)
//        val loggedInUser = userRepo.isLogin()
//
//        if (loggedInUser != null) {
//            InternetCheck(object : InternetCheck.Consumer {
//                override fun accept(internet: Boolean?) {
//                    if (internet == true) {
//                        val userApi = com.mockie.daikokuten.api.User(context, (context as Activity))
//                        userApi.execute()
//                    }
//                }
//            })
//        }
    }

    fun checkPermission(roles: ArrayList<String>): Boolean
    {
        val userRepo = User(context)
        val loggedInUser = userRepo.isLogin()
        var permissions = ArrayList<String>()

        if (loggedInUser != null)
        {
            val pattern = Pattern.compile("\"([a-zA-Z]+)\"")
            val matcher = pattern.matcher(loggedInUser.adminCaps)

            while (matcher.find()) {
                val group = matcher.group(1)
                permissions.add(group)
            }

            roles.forEach {
                if(it in permissions)
                {
                   return true
                }
            }

        }

        return false
    }

}