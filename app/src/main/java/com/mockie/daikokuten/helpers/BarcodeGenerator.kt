package com.mockie.daikokuten.helpers

import android.content.Context
import com.mockie.daikokuten.database.repositories.PrdSizes
import com.mockie.daikokuten.database.repositories.PrdTypes

/**
 * Created by mockie on 11/05/18.
 */


class BarcodeGenerator(val context: Context, val type: String, val size: String, var code: String, val version: String) {

    fun getCode_(): String
    {
        return code.padStart(5, '0')
    }

    fun getVersion_(): String
    {
        return version.padStart(2, '0')
    }

    fun getTypeReadable_(): String?
    {
        val data = PrdTypes(context).getTypeById(type)
        return data?.readable
    }

    fun getSizeReadable_(): String?
    {
        val data = PrdSizes(context).getReadableById(size)
        return data?.readable
    }

    fun getBarcodeTitle(): String
    {
        var text = getTypeReadable_()?.toUpperCase()?.replace("/|A|U|E|I|O|/gi".toRegex(), "") + " "
        text += getCode_().toInt()
        text += "."
        text += version.toInt()

        return text
    }

    fun getBarcode(): String
    {
        return type + size + getCode_() + getVersion_()
    }

}