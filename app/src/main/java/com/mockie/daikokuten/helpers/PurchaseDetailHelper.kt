package com.mockie.daikokuten.helpers

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.database.repositories.PrdPrices
import com.mockie.daikokuten.database.repositories.PriceRules
import com.mockie.daikokuten.database.repositories.User
import com.mockie.daikokuten.models.BarcodeList

class PurchaseDetailHelper(val context: Context) {

    fun getTotalPriceItem(additionalFee:Int, barcodeList: ArrayList<BarcodeList>):Int
    {
        var totalPrice = 0
        for(a in 0 until barcodeList.size)
        {
            val priceCode = barcodeList[a].barcode.substring(0, 5)
            val price = PrdPrices(context).getPriceByCode(priceCode)
            totalPrice += ((price!!.price.toInt() + additionalFee) * barcodeList[a].qty.toString().toInt())
        }

        return totalPrice
    }

    fun getAdditonalFee(barcodeList: ArrayList<BarcodeList>):Int
    {
        var totalQty = 0
        for(a in 0 until barcodeList.size)
        {
            totalQty += barcodeList[a].qty.toString().toInt()
        }

        val priceRule = PriceRules(context).getPriceRules(totalQty, User(context).getLoggedInUserStore()!!)

        if (priceRule!=null)
        {
            return priceRule.amount
        }

        return 0
    }

    fun getTotalBilling(parentLayout: View)
    {
        val totalPriceItemForm = parentLayout.findViewById<TextView>(R.id.totalItem)
        val shippingFeeForm = parentLayout.findViewById<EditText>(R.id.shippingFee)
        val totalBillingForm = parentLayout.findViewById<TextView>(R.id.totalBilling)

        val totalPriceItem = PaymentHelper().readableAmountToInt(totalPriceItemForm.text.toString())

        var shippingFee = 0
        try{
            shippingFee = shippingFeeForm.text.toString().toInt()
        } catch (e:Exception) {}

        val total = totalPriceItem + shippingFee
Log.d("fuck2", total.toString() + "(" + totalPriceItemForm.text.toString() +" + "+ shippingFee.toString() +")")
        totalBillingForm.text = PaymentHelper().amountToReadable(total)
    }



}

