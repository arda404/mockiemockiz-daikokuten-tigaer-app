package com.mockie.daikokuten.helpers

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.watchers.QtyConfirmationTextWatcher

/**
 * Created by mockie on 11/01/18.
 */


class ConfirmationHelper(val context: Context) {

    var toSave = ArrayList<BarcodeList>()

    fun stockerConfirmation(data: ArrayList<BarcodeList>)
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(true)
        commentDialog.setContentView(R.layout.modal_stocking_confirmation)

        if (commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        val mainLayout = commentDialog.findViewById<LinearLayout>(R.id.parent_barcode_list_layout)
        val inflater = commentDialog.layoutInflater

        for (i in 0 until data.size) {

            val barcode = data[i].barcode
            val qty = data[i].qty
            val price = data[i].price

            if (toSave.size == 0)
            {
                toSave.add(BarcodeList(barcode, qty, "", price, null))
            } else {
                for (a in 0 until toSave.size) {
                    if (barcode == toSave[a].barcode)
                    {
                        toSave[a].qty = (toSave[a].qty.toString().toInt() + qty.toString().toInt())
                        toSave[a].price = (toSave[a].price + price)
                    }else{
                        toSave.add(BarcodeList(barcode, qty, "", price,null))
                    }
                    break
                }
            }
        }

        for (c in 0 until toSave.size) {
            val myLayout = inflater.inflate(R.layout.barcode_list, mainLayout, false)
            mainLayout.addView(myLayout, mainLayout.childCount)
            val barcodeEdit = myLayout.findViewById<TextView>(R.id.barcode)
            val qtyEdit = myLayout.findViewById<TextView>(R.id.qty)

            qtyEdit.addTextChangedListener(QtyConfirmationTextWatcher(toSave, c))

            myLayout.findViewById<TextView>(R.id.number).text = (c+1).toString()
            barcodeEdit.text = toSave[c].barcode
            qtyEdit.text = toSave[c].qty.toString()
        }

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Konfirmasi"
        val save: Button = commentDialog.findViewById(R.id.saveButton)
        val cancel: Button = commentDialog.findViewById(R.id.cancelButton)

        save.setOnClickListener {
            for (c in 0 until toSave.size) {
                Log.d("data ", c.toString() + " : " + toSave[c].barcode + " | " + toSave[c].qty)
            }
        }

        cancel.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }
    }
}