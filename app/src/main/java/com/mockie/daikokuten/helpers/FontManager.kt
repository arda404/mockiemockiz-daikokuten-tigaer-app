package com.mockie.daikokuten.helpers

import android.content.Context
import android.graphics.Typeface


class FontManager (val context: Context) {

    val ROOT = "fonts/"
    val FONTAWESOME = ROOT + "fa-solid-900.ttf"

    fun getTypeface(font: String): Typeface {
        return Typeface.createFromAsset(context.assets, font)
    }

//    fun markAsIconContainer(v: View, typeface: Typeface) {
//        if (v is ViewGroup) {
//            val vg = v
//            for (i in 0 until vg.childCount) {
//                val child = vg.getChildAt(i)
//                markAsIconContainer(child, typeface)
//            }
//        } else if (v is TextView) {
//            (v).typeface = typeface
//        }
//    }

}