package com.mockie.daikokuten.helpers

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.wifi.WifiManager
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.support.v4.content.PermissionChecker
import android.telephony.TelephonyManager
import java.util.*


/**
 * Created by mockie on 20/02/18.
 */

private var uniqueID: String? = null
private val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"

class DeviceID(val context: Context)
{

    @SuppressLint("HardwareIds")
    fun getAndroidID(): String
    {
        return Settings.Secure.getString(context.contentResolver,
                Settings.Secure.ANDROID_ID)
    }

    fun getMacAddress(): String
    {
        val manager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val info = manager.connectionInfo
        val address = info.macAddress

        return address
    }

    @SuppressLint("HardwareIds")
    fun getImei(): String
    {
        val telephonyManager: TelephonyManager = context.applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        val permissionCheck = ContextCompat.checkSelfPermission((context as Activity),
                Manifest.permission.READ_PHONE_STATE)

        if (permissionCheck != PermissionChecker.PERMISSION_DENIED){
            return telephonyManager.subscriberId
        }

        return ""
    }

//    @Synchronized
    fun id(): String? {
        if (uniqueID == null) {
            val sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE)
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString()
                val editor = sharedPrefs.edit()
                editor.putString(PREF_UNIQUE_ID, uniqueID)
                editor.apply()
            }
        }
        return uniqueID
    }

    fun getDeviceUniqueID(): String
    {
        val time = System.currentTimeMillis().toInt()

        if (id() != null)
        {
            return id() !!
        }

        if(getImei() != "")
        {
            return getImei()
        }

        if(getMacAddress()!="")
        {
            return getMacAddress()
        }

        if(getAndroidID()!="")
        {
            return getAndroidID()
        }

        return time.toString()
    }
}