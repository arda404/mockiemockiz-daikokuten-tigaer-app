package com.mockie.daikokuten.helpers

import android.content.Context
import com.mockie.daikokuten.database.repositories.PrdPrices
import com.mockie.daikokuten.database.repositories.PrdSizes
import com.mockie.daikokuten.database.repositories.PrdTypes
import com.mockie.daikokuten.models.BarcodeList

class BarcodeListHelper(val context: Context) {

    fun getTypeReadable(barcode:String):String?
    {
        try {
            val typeCode = barcode.substring(0, 3)
            val dataType = PrdTypes(context).getTypeById(typeCode)

            if (dataType != null)
            {
                return dataType.readable
            }
        } catch (e:Exception){}

        return ""
    }

    fun getSizeReadable(barcode:String):String?
    {
        try {
            val sizeCode = barcode.substring(3,5)
            val dataSize = PrdSizes(context).getReadableById(sizeCode)

            if (dataSize != null)
            {
                return dataSize.readable
            }
        } catch (e:Exception){}

        return ""
    }

    fun getPrice(barcode:String):Int
    {
        try {
            val priceCode = barcode.substring(0, 5)
            val dataPrice = PrdPrices(context).getPriceByCode(priceCode)

            if (dataPrice != null)
            {
                return dataPrice.price
            }
        } catch (e:Exception){}

        return 0
    }

}