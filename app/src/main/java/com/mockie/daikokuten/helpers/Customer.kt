package com.mockie.daikokuten.helpers

import android.content.Context
import com.mockie.daikokuten.database.repositories.*
import com.mockie.daikokuten.database.repositories.Customer

class Customer(val context:Context)
{
    fun getDebt(customerId:Int, phone:String, toShow:Boolean = false):Int
    {
        var customerBalance = 0
        var invoiceBill = 0

        if (customerId > 0)
        {
            val cust = Customer(context).getCustomerById(customerId)
            if (cust != null)
            {
                customerBalance = cust.balance.toInt()
            }
        }

        val invoices = InvoiceToSync(context).getInvoiceToSyncStatusReadyAndShippedByCustomerPhone(phone)
        for ( a in 0 until invoices.count())
        {
            val inv = Invoices(context).getInvoiceById(invoices[a].real_invoice_id)
            invoiceBill += invoices[a].total_billing.toInt()
            if (inv != null)
            {
                invoiceBill -= inv.total_billing.toInt()
            }
        }


        val theDebt = (customerBalance + invoiceBill) - (getPaymentUnsync(phone))

        if (!toShow)
        {
            return theDebt
        }
        else
        {
            return if (theDebt < 0) 0 else theDebt
        }
    }

    fun getPaymentUnsync(phone:String):Int
    {
        var paymentUnsync = 0
        var paymentChangeUnsync = 0


        val paymentToSync = PaymentToSync(context).getPaymentByPhone(phone)
        for (a in 0 until paymentToSync.size)
        {
            paymentUnsync += paymentToSync[a].amount
        }

        val paymentChangeToSync = PaymentToSync(context).getPaymentChangeByPhone(phone)
        for (a in 0 until paymentChangeToSync.size)
        {
            paymentChangeUnsync += paymentChangeToSync[a].amount
        }

        return paymentUnsync - paymentChangeUnsync
    }

    fun getCustomerSaldo(customerId:Int, phone: String):Int
    {
        val debt = getDebt(customerId, phone)

        return if (debt < 0) -debt else 0
    }

}

