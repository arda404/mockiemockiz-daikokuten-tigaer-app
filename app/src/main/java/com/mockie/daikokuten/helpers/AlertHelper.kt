package com.mockie.daikokuten.helpers

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentManager
import android.util.Log
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.models.BarcodeList
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.mockie.daikokuten.*
import com.mockie.daikokuten.LauncherActivity
import com.mockie.daikokuten.Printers.MPT2
import com.mockie.daikokuten.adapters.RVPaymentMethodAdapter
import com.mockie.daikokuten.database.models.TheInvoice
import com.mockie.daikokuten.database.repositories.*
import com.mockie.daikokuten.listeners.CustomerModalAutoComplete
import com.mockie.daikokuten.models.ItemInBasket
import com.mockie.daikokuten.models.PairedDevices
import com.mockie.daikokuten.models.PaymentMethod
import com.mockie.daikokuten.watchers.CustomerModalAutoCompleteWatcher


/**
 * Created by mockie on 11/01/18.
 */


class AlertHelper (val context: Context) {

    @SuppressLint("SetTextI18n")
    fun alertLocation()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_yes_no)

        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Apakah kamu di " + User(context).getLoggedInUserStore()  + "?"
        val yes: Button = commentDialog.findViewById(R.id.editButton)
        val no: Button = commentDialog.findViewById(R.id.noDialog)

        yes.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }

        no.setOnClickListener {
            User(context).logout()
            commentDialog.hide()
            commentDialog.dismiss()
            (context as Activity).finish()
            context.startActivity(Intent(context, LauncherActivity::class.java))
            context.finish()
        }
    }

    private fun barcodeScreening(barcodeList:ArrayList<BarcodeList>): ArrayList<BarcodeList>
    {
        val newBarcodeList = ArrayList<BarcodeList>()
        var match:Boolean
        var index = 0
        val matchedIndex = arrayListOf<Int>()

        for (a in 0 until barcodeList.size)
        {
            match = false

            val theBarcode = barcodeList[a].barcode.trim().toUpperCase()
            val validator = BarcodeValidator(context)
            validator.lengthValidator(theBarcode)
            validator.barcodePatternValidator(theBarcode)
            validator.skuValidator(theBarcode)

            val isValid = validator.validator()

            if (isValid)
            {

                if (newBarcodeList.count() > 0)
                {
                    for (i in 0 until newBarcodeList.count())
                    {
                        if (barcodeList[a].barcode == newBarcodeList[i].barcode)
                        {
                            match = true
                            index = i
                            matchedIndex.add(a)
                            break
                        }
                    }

                    if (match)
                    {
                        newBarcodeList[index].qty = (newBarcodeList[index].qty.toString().toInt() + barcodeList[a].qty.toString().toInt())
                    } else {
                        newBarcodeList.add(barcodeList[a])
                    }
                } else {
                    newBarcodeList.add(barcodeList[a])
                }
            }
        }

        return newBarcodeList
    }

    @SuppressLint("SetTextI18n")
    fun barcodeListConfirmation(adapter: RecyclerViewBarcodeAdapter, barcodeList:ArrayList<BarcodeList>, pageName: String):Dialog
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_barcode_confirmation)
        val linearLayout = commentDialog.findViewById<LinearLayout>(R.id.parentLinear)


        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Cek data"

        val newBarcodeList = barcodeScreening(barcodeList)
        barcodeList.removeAll(barcodeList)
        barcodeList.addAll(newBarcodeList)

        adapter.notifyDataSetChanged()

        for (a in 0 until newBarcodeList.count())
        {
            val view = (context as Activity).layoutInflater.inflate(R.layout.barcode_simple_list, linearLayout, false)
            linearLayout.addView(view, linearLayout.childCount - 1)
            view.findViewById<TextView>(R.id.barcode).text = barcodeList[a].barcode
            view.findViewById<TextView>(R.id.qty).text = barcodeList[a].qty.toString()
        }

        DraftHelper(context).draftBarcode(pageName, barcodeList)

        val no: Button = commentDialog.findViewById(R.id.noDialog)

        no.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }

        return commentDialog
    }

    @SuppressLint("SetTextI18n")
    fun barcodeReturnConfirmation(barcodeList:ArrayList<BarcodeList>, itemInBasket: ArrayList<ItemInBasket>):Dialog
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_barcode_confirmation)
        val linearLayout = commentDialog.findViewById<LinearLayout>(R.id.parentLinear)

        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Cek data"

        for (bbb in itemInBasket)
        {
            Log.d("dodol 2", bbb.barcode + " | " +bbb.quantity + " | " + bbb.retur)
        }

        for (a in 0 until barcodeList.size)
        {
            val itm = barcodeList[a]

            val validator = BarcodeValidator(context)

            validator.lengthValidator(itm.barcode)
            validator.skuValidator(itm.barcode)
            validator.barcodePatternValidator(itm.barcode)
            validator.checkReturQuantity(itm.barcode, itm.qty.toString().toInt(), itemInBasket)

            if (!validator.validator())
            {
                barcodeList.removeAt(a)
            }

        }

        for (a in 0 until barcodeList.size)
        {
            val itm = barcodeList[a]

            val view = (context as Activity).layoutInflater.inflate(R.layout.barcode_simple_list, linearLayout, false)
            linearLayout.addView(view, linearLayout.childCount - 1)
            view.findViewById<TextView>(R.id.barcode).text = itm.barcode
            view.findViewById<TextView>(R.id.qty).text = itm.qty.toString()
        }

        val no: Button = commentDialog.findViewById(R.id.noDialog)

        no.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }

        return commentDialog
    }

    @SuppressLint("SetTextI18n")
    fun paymentConfirmation(adapter: RVPaymentMethodAdapter, payment: ArrayList<PaymentMethod>):Pair<Dialog, Button>
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_payment_confirm)
        val linearLayout = commentDialog.findViewById<LinearLayout>(R.id.parentLinear)

        val no: Button = commentDialog.findViewById(R.id.noDialog)
        val yes: Button = commentDialog.findViewById(R.id.yesButton)

        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Konfirmasi Pembayaran"

        for (a in 0 until payment.size)
        {
            Log.d("ffff", payment[a].method.tag + " | " + payment[a].amount)
            if (payment[a].method.tag == "0")
            {
                payment.removeAt(a)
            }
        }

        if (payment.size == 0)
        {
            payment.add(PaymentMethod("0", StringWithTag("", "0"), ""))
            yes.isEnabled = false
        }

        for (a in 0 until payment.size)
        {
            val view = (context as Activity).layoutInflater.inflate(R.layout.payment_list, linearLayout, false)
            linearLayout.addView(view, linearLayout.childCount - 1)
            view.findViewById<TextView>(R.id.amount).text = PaymentHelper().amountToReadable(payment[a].amount.toInt())
            view.findViewById<TextView>(R.id.method).text = payment[a].method.toString()
        }

        adapter.notifyDataSetChanged()


        no.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }

        return Pair(commentDialog, yes)
    }

    fun wrongToken()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_wrong_token)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        val login: Button = commentDialog.findViewById(R.id.loginButtonDialog)

        login.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
            User(context).logout()
            context.startActivity(Intent(context, LauncherActivity::class.java))
            (context as Activity).finish()
        }
    }

    @SuppressLint("SetTextI18n")
    fun onBackPressedAlert(): Dialog {

        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_yes_no)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Ada input yang belum di simpan. Yakin mau keluar?"

        return commentDialog
    }

    @SuppressLint("SetTextI18n")
    fun printerNotFound()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Printer tidak di temukan. Mohon sambungkan printer"
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        ok.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
            //context.startActivity(Intent(context, InventoryActivity::class.java))
        }
    }

    @SuppressLint("SetTextI18n")
    fun stockTakingWarning()
    {
        val newStock =  Inventory(context).getTotalRow()
        val moveStock = MovedInventory(context).getCount()

        if (newStock > 0 || moveStock > 0)
        {
            val commentDialog = Dialog(context)
            commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
            commentDialog.setCancelable(false)
            commentDialog.setContentView(R.layout.modal_ok)
            if(commentDialog.isShowing)
            {
                commentDialog.dismiss()
            }

            commentDialog.show()

            commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
            commentDialog.findViewById<TextView>(R.id.message).text = "Mohon tunggu sampai semua data terupload"
            val ok: Button = commentDialog.findViewById(R.id.okDialog)

            ok.setOnClickListener {
                commentDialog.hide()
                commentDialog.dismiss()
                context.startActivity(Intent(context, NewMainActivity::class.java))
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun printerFailedToConnect()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Printer gagal terhubung"
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        ok.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
            //context.startActivity(Intent(context, InventoryActivity::class.java))
        }
    }

    @SuppressLint("SetTextI18n")
    fun setCustomerPayment(context:Context, view:View, fragment: FragmentManager)
    {
        val item = ArrayList<CustomerSwitcher>()

        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_set_customer)

        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Atur pelanggan"
        val ok: Button = commentDialog.findViewById(R.id.saveButton)

        val handler = Handler()

        handler.postDelayed({

            val actv = commentDialog.findViewById(R.id.autoCompleteTextView) as AutoCompleteTextView

            // set our adapter
            val myAdapter = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, item)
            actv.setAdapter(myAdapter)

            actv.threshold = 1
            actv.addTextChangedListener(CustomerModalAutoCompleteWatcher(context, commentDialog, view, myAdapter, item))
            actv.onItemClickListener = CustomerModalAutoComplete(context, commentDialog, view)

            val customerNameModal = commentDialog.findViewById<TextView>(R.id.autoCompleteTextView)
            val customerPhoneModal = commentDialog.findViewById<TextView>(R.id.phone)
            val customerIdModal = commentDialog.findViewById<TextView>(R.id.customerId)

            ok.setOnClickListener {

                commentDialog.hide()
                commentDialog.dismiss()

                try{

                    val bdl = Bundle(3)
                    bdl.putInt("customerIdGet", customerIdModal.text.toString().toInt()) // invoice id from invoice_to_sync table or from invoice table
                    bdl.putString("customerNameGet", customerNameModal.text.toString())
                    bdl.putString("customerPhoneGet", customerPhoneModal.text.toString())

                    val fragmentTransaction = fragment.beginTransaction()

                    val fragment2 = PaymentFragment()
                    fragment2.arguments = bdl
                    fragmentTransaction.replace(R.id.container, fragment2, "cashier_fragment")
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()

                } catch (e:Exception) {}

            }

        },100)


    }

    @SuppressLint("SetTextI18n")
    fun permissionDenied()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Anda tidak mempunyai akses"
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        ok.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
            context.startActivity(Intent(context, NewMainActivity::class.java))
            (context as Activity).finish()
        }
    }


    @SuppressLint("SetTextI18n")
    fun noOrderFound()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Tidak ada barang di pesan"
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        ok.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }
    }

    @SuppressLint("SetTextI18n")
    fun noInvoiceFound()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Invoice tidak ditemukan"
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        ok.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }
    }

    @SuppressLint("SetTextI18n")
    fun noItemFound()
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)
        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Tidak ada barang untuk di proses"
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        ok.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }
    }


    @SuppressLint("SetTextI18n")
    fun itemHasChanged(): Pair<Button, Dialog>
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)

        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = "Item ini telah di edit, tekan ok untuk me-refresh data"
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        return Pair(ok, commentDialog)
    }

    @SuppressLint("SetTextI18n")
    fun PurchaseDetailDataChecker(errorMessage:String)
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(false)
        commentDialog.setContentView(R.layout.modal_ok)

        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.show()

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Peringatan"
        commentDialog.findViewById<TextView>(R.id.message).text = errorMessage
        val ok: Button = commentDialog.findViewById(R.id.okDialog)

        ok.setOnClickListener {
            commentDialog.hide()
            commentDialog.dismiss()
        }
    }

    @SuppressLint("SetTextI18n")
    fun printerList(context: Context): Pair<Dialog, Spinner>
    {
        val commentDialog = Dialog(context)

        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(true)
        commentDialog.setContentView(R.layout.modal_printer_list)

        val printers = commentDialog.findViewById<Spinner>(R.id.printers)
        val printerList = MPT2(context).getPairedBluetoothDevices()


        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        commentDialog.findViewById<TextView>(R.id.titleMessage).text = "Printer"

        val adapter = ArrayAdapter(context,
                R.layout.spinner_item, printerList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        printers.adapter = adapter

        commentDialog.show()

        return Pair(commentDialog, printers)
    }

    fun printInvoice(context: Context, theInvoice:TheInvoice)
    {
        val cm = printerList(context)
        val okDialog = cm.first.findViewById<Button>(R.id.okDialog)

        okDialog.setOnClickListener {

            okDialog.isEnabled = false

            val handler = Handler()
            handler.postDelayed({
                okDialog.isEnabled = true
            }, 7000)

            val typePrinter = cm.second.selectedItem as PairedDevices
            MPT2(context).printInvoice(typePrinter.deviceAddress, theInvoice)
        }
    }

    fun printPayment(context: Context, payment: ArrayList<PaymentMethod>, customerName:String, customerPhone:String, dialog: Dialog)
    {
        val cm = printerList(context)
        val okDialog = cm.first.findViewById<Button>(R.id.okDialog)

        okDialog.setOnClickListener {

            dialog.hide()
            dialog.dismiss()

            val typePrinter = cm.second.selectedItem as PairedDevices
            MPT2(context).printPayment(typePrinter.deviceAddress, payment, customerName, customerPhone)

        }
    }

    fun invoiceListOption(context: Context, fragmentManager: FragmentManager, tempInvoiceId:Int, realInvoiceId:Int)
    {
        val commentDialog = Dialog(context)
        commentDialog.window.attributes.windowAnimations = R.style.PauseDialogAnimation
        commentDialog.setCancelable(true)
        commentDialog.setContentView(R.layout.modal_invoice_list_option)

        val editButton = commentDialog.findViewById<Button>(R.id.editButton)
        val payButton = commentDialog.findViewById<Button>(R.id.payButton)
        val printButton = commentDialog.findViewById<Button>(R.id.printButton)
        val returnButton = commentDialog.findViewById<Button>(R.id.returnButton)

        val realInvoiceIdForm = commentDialog.findViewById<TextView>(R.id.realInvoiceId)
        val dateForm = commentDialog.findViewById<TextView>(R.id.date)

        val customerNameForm = commentDialog.findViewById<TextView>(R.id.customerName)
        val customerPhoneForm = commentDialog.findViewById<TextView>(R.id.customerPhone)
        val noteForm = commentDialog.findViewById<TextView>(R.id.note)

        val addressNameForm = commentDialog.findViewById<TextView>(R.id.addressName)
        val addressPhoneForm = commentDialog.findViewById<TextView>(R.id.addressPhone)
        val addressForm = commentDialog.findViewById<TextView>(R.id.address)

        val shippingChargeForm = commentDialog.findViewById<TextView>(R.id.shippingCharge)
        val totalForm = commentDialog.findViewById<TextView>(R.id.totalPayment)
        val statusForm = commentDialog.findViewById<TextView>(R.id.status)

        val cashierName:TextView = commentDialog.findViewById(R.id.cashierName)

        if(commentDialog.isShowing)
        {
            commentDialog.dismiss()
        }

        val linearParent = commentDialog.findViewById<LinearLayout>(R.id.invoiceItemParentLayout)

        val theInvoice = TheInvoice(context, tempInvoiceId, realInvoiceId)

        cashierName.text = theInvoice.meta

        realInvoiceIdForm.text = realInvoiceId.toString()
        dateForm.text = theInvoice.createdAt

        customerNameForm.text = theInvoice.customerName
        customerPhoneForm.text = theInvoice.customerPhone
        noteForm.text = theInvoice.note

        addressNameForm.text = theInvoice.addressName
        addressPhoneForm.text = theInvoice.addressPhone
        addressForm.text = theInvoice.address

        statusForm.text = InvoiceHelper(context).statusReadable(theInvoice.status)

        shippingChargeForm.text = PaymentHelper().amountToReadable(theInvoice.shippingFee)
        totalForm.text = PaymentHelper().amountToReadable(theInvoice.totalBilling)

        var aa = 0
        for (invoiceItem in theInvoice._getItems())
        {
            val view = LayoutInflater.from(context).inflate(R.layout.list_item_child, null)
            linearParent.addView(view)

            var desc = ""
            desc += BarcodeListHelper(context).getTypeReadable(invoiceItem.code)
            desc += " " + BarcodeListHelper(context).getSizeReadable(invoiceItem.code)
            view.findViewById<TextView>(R.id.descriptionText0).text = desc

            aa += 1

            view.findViewById<TextView>(R.id.number).text = aa.toString()
            view.findViewById<TextView>(R.id.barcodeText0).text = invoiceItem.code
            view.findViewById<TextView>(R.id.qtyText0).text = invoiceItem.quantity.toString()
        }

        if (theInvoice.isInvoiceExisted)
        {
            commentDialog.show()

            if (theInvoice.status == 1000)
            {
                printButton.isEnabled = false
                editButton.isEnabled = false
                payButton.isEnabled = false
                returnButton.isEnabled = false
            }
            else if (theInvoice.status == 1)
            {
                returnButton.isEnabled = false
            }
            else if (theInvoice.status == 2)
            {
                editButton.isEnabled = false
            }
            else if (theInvoice.status == 0)
            {
                printButton.isEnabled = false
                returnButton.isEnabled = false
            }

            printButton.setOnClickListener {
                printInvoice(context, theInvoice)
            }

            editButton.setOnClickListener {

                Locker(context).onInvoiceToEdit(customerPhoneForm.text.toString())

                commentDialog.hide()
                commentDialog.dismiss()

                try{

                    val bdl = Bundle(2)
                    bdl.putInt("tempInvoiceId", tempInvoiceId) // invoice id from invoice_to_sync table or from invoice table
                    bdl.putInt("realInvoiceId", realInvoiceId)

                    val fragmentTransaction = fragmentManager.beginTransaction()

                    val fragment = CashierFragment()
                    fragment.arguments = bdl
                    fragmentTransaction.replace(R.id.container, fragment, "cashier_fragment")
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()

                } catch (e:Exception) {}
            }

            payButton.setOnClickListener {

                Locker(context).onInvoiceToEdit(customerPhoneForm.text.toString())

                commentDialog.hide()
                commentDialog.dismiss()

                val bdl = Bundle(3)
                bdl.putInt("customerIdGet", theInvoice.customerId) // invoice id from invoice_to_sync table or from invoice table
                bdl.putString("customerNameGet", theInvoice.customerName) // invoice id from invoice_to_sync table or from invoice table
                bdl.putString("customerPhoneGet", theInvoice.customerPhone)

                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment2 = PaymentFragment()
                fragment2.arguments = bdl
                fragmentTransaction.replace(R.id.container, fragment2, "cashier_fragment")
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

            returnButton.setOnClickListener {

                Locker(context).onInvoiceToEdit(customerPhoneForm.text.toString())

                commentDialog.hide()
                commentDialog.dismiss()

                val bdl = Bundle(2)
                bdl.putInt("tempInvoiceIdGet", tempInvoiceId) // invoice id from invoice_to_sync table or from invoice table
                bdl.putInt("realInvoiceIdGet", realInvoiceId) // invoice id from invoice_to_sync table or from invoice table

                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment2 = Return()
                fragment2.arguments = bdl
                fragmentTransaction.replace(R.id.container, fragment2, "return_fragment")
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

        } else {

            noInvoiceFound()

        }

    }


}
