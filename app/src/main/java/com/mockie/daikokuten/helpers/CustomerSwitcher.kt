package com.mockie.daikokuten.helpers

import java.math.BigDecimal

/**
 * Created by mockie on 07/12/17.
 */

class CustomerSwitcher(var name: String, var customerId: Int, var phone:String,var tempInvoiceId: Int) {

    override fun toString(): String {
        return name
    }

    fun toCustomerId(): Int {
        return customerId
    }

    fun toCustomerPhone():String
    {
        return phone
    }

    fun toTempInvoiceId():Int
    {
        return tempInvoiceId
    }
}