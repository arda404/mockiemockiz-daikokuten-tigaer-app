package com.mockie.daikokuten

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mockie.daikokuten.helpers.CustomerSwitcher
import com.mockie.daikokuten.listeners.CustomerAutoComplete
import com.mockie.daikokuten.listeners.SearchCustomerAutoComplete
import com.mockie.daikokuten.watchers.CustomerAutoCompleteWatcher
import com.mockie.daikokuten.watchers.SearchCustomerAutoCompleteWatcher


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SearchInvoiceFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SearchInvoiceFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SearchInvoiceFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null


    var item = ArrayList<CustomerSwitcher>()
    lateinit var actv:AutoCompleteTextView
    lateinit var invoiceId:EditText
    lateinit var customerPhone:EditText
    lateinit var customerId:TextView

    lateinit var viewD:View
    lateinit var status:Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    fun setSpinner(selectedItem:Int)
    {
        status = viewD.findViewById(R.id.status)

        // Initializing a String Array

        val statusList = ArrayList<String>()

        // Set the disable item text color

        statusList.add("Draft")
        statusList.add("Simpan")
        statusList.add("Kirim")

        val arrAdapter = object: ArrayAdapter<String>(context, R.layout.spinner_item, statusList){

            override fun isEnabled(position: Int): Boolean {
                return selectedItem != 2
            }

            override fun getDropDownView(position: Int, convertView: View?,
                                         parent: ViewGroup): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                if (!isEnabled(position)) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(Color.BLACK)
                }
                return view
            }

        }

        arrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        status.adapter = arrAdapter
        status.setSelection(selectedItem)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewD = inflater.inflate(R.layout.fragment_search_invoice, container, false)
        val searchButton = viewD.findViewById<Button>(R.id.searchButton)

        actv = viewD.findViewById(R.id.customerName) as AutoCompleteTextView
        invoiceId = viewD.findViewById(R.id.invoiceId)
        customerId = viewD.findViewById(R.id.customerId)
        customerPhone = viewD.findViewById(R.id.customerPhone)

        // set our adapter
        val myAdapter = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, item)
        actv.setAdapter(myAdapter)

        actv.threshold = 1
        actv.addTextChangedListener(SearchCustomerAutoCompleteWatcher(context, viewD, myAdapter, item))
        actv.onItemClickListener = SearchCustomerAutoComplete(context, viewD)

        setSpinner(0)

        searchButton.setOnClickListener {
            onClickSearch()
        }

        return viewD
    }

    fun onClickSearch()
    {
        val bdl = Bundle(4)
        try {
            bdl.putInt("searchInvoiceId", invoiceId.text.toString().toInt())
            bdl.putInt("searchCustomerId", customerId.text.toString().toInt())
            bdl.putString("searchCustomerName", actv.text.toString())
            bdl.putString("searchCustomerPhone", customerPhone.text.toString())
        } catch (e:Exception) {}


        val selectedStatus = status.selectedItem.toString()

        if (selectedStatus == "Draft")
        {
            bdl.putInt("searchStatus", 0)
        }
        else if (selectedStatus == "Simpan")
        {
            bdl.putInt("searchStatus", 1)
        }
        else if (selectedStatus == "Kirim")
        {
            bdl.putInt("searchStatus", 2)
        }

        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragment2 = InvoiceList()
        fragment2.arguments = bdl
        fragmentTransaction.replace(R.id.container, fragment2, "move_stock_fragment")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchInvoiceFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                SearchInvoiceFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
