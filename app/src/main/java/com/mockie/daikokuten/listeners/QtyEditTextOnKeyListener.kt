package com.mockie.daikokuten.listeners

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.View
import android.widget.*
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.helpers.DraftHelper
import java.util.regex.Pattern

/**
 * Created by mockie on 12/12/17.
 */

class QtyEditTextOnKeyListener(
        val mContext: Context,
        val adapter: RecyclerViewBarcodeAdapter,
        val mContacts: MutableList<BarcodeList>,
        val recyclerView: RecyclerView,
        val currentQty: EditText,
        val pageName: String,
        val keyName: String
) : View.OnKeyListener {

//    lateinit var barcodeList: ArrayList<BarcodeList>
    val pattern = Pattern.compile("([0-9]+)?([a-zA-Z]{3}[0-9]+)")
    var barcode: String = ""
    var focus: String = "barcode"
    lateinit var oldData: Any
    var qty: Int = 0

    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
        try{
            oldData = currentQty.text.toString().toInt()
        }catch (e: Exception){}

        if (event.action == KeyEvent.ACTION_UP) {

            return if (keyCode == 66) {

                currentQty.postDelayed({
                    val matcher = pattern.matcher(currentQty.text.toString())
                    if (matcher.find())
                    {
                        qty = if (matcher.group(1)!= null)  matcher.group(1).toInt() else oldData.toString().toInt()

                        if (matcher.group(2)!= null)
                        {
                            barcode = matcher.group(2)
                            focus = "barcode"
                        } else {
                            barcode = ""
                        }
                    } else {
                        barcode = ""
                        qty = oldData.toString().toInt()
                        focus = "barcode"
                    }

                    currentQty.setText(qty.toString())

                    var defaultQty = 1

                    if (pageName=="stocker"){
                        defaultQty = 10
                    }

                    if ((mContacts.size - 1) >= 0)
                    {
                        if (mContacts[mContacts.size-1].barcode == "")
                        {
                            mContacts[mContacts.size-1].focus = "barcode"
                        }
                        else
                        {
                            mContacts.add(BarcodeList(barcode, defaultQty, "", 0, focus))
                        }
                    }

                    adapter.notifyDataSetChanged()


                    // scroll the recycler view to the last position
                    recyclerView.postDelayed({
                        recyclerView.layoutManager.scrollToPosition(adapter.itemCount -1)
                    }, 200)
                }, 50)

                true
            } else false

        }

        return false
    }
}