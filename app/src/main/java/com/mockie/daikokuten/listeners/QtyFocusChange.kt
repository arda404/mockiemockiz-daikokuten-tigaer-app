package com.mockie.daikokuten.listeners

import android.content.Context
import android.view.View
import android.widget.EditText
import android.widget.TextView

open class QtyFocusChange(
        open val context:Context
): View.OnFocusChangeListener {

    lateinit var barcode:EditText
    lateinit var qty:EditText
    lateinit var description:TextView

    fun setBarcodeEditText(barcodeVal: EditText)
    {
        barcode = barcodeVal
    }

    fun _getBarcode(): EditText
    {
        return barcode
    }

    fun setDescriptionEditText(descriptionVal: TextView)
    {
        description = descriptionVal
    }

    fun _getDescription(): TextView
    {
        return description
    }

    fun setQtyEditText(qtyVal: EditText)
    {
        qty = qtyVal
    }

    fun _getQty(): EditText
    {
        return qty
    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {

    }

}