package com.mockie.daikokuten.listeners

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.mockie.daikokuten.R
import kotlinx.android.synthetic.main.fragment_stock_taking.view.*
import android.os.Build
import android.graphics.drawable.GradientDrawable
import android.view.ViewGroup
import android.animation.ValueAnimator





class MyOnTouchListener(val ctx: Context, val cl: ConstraintLayout, val constraintLayout: View) : OnTouchListener {

    private val gestureDetector: GestureDetector

    init {
        gestureDetector = GestureDetector(ctx, GestureListener())
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        return gestureDetector.onTouchEvent(event)
    }

    private inner class GestureListener : SimpleOnGestureListener() {

        private val SWIPE_THRESHOLD = 100
        private val SWIPE_VELOCITY_THRESHOLD = 100

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            var result = false
            try {
                val diffY = e2.y - e1.y
                val diffX = e2.x - e1.x
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight()
                        } else {
                            onSwipeLeft()
                        }
                        result = true
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom()
                    } else {
                        onSwipeTop()
                    }
                    result = true
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

            return result
        }
    }

    fun onSwipeRight() {}

    fun onSwipeLeft() {}

    fun onSwipeTop() {
        cl.layoutParams = cl.layoutParams.apply {
            height = 120
            cl.findViewById<TextView>(R.id.textView9).visibility = View.GONE
            cl.findViewById<TextView>(R.id.textView11).visibility = View.GONE
            cl.findViewById<TextView>(R.id.textView15).visibility = View.GONE
            cl.findViewById<TextView>(R.id.textView17).visibility = View.GONE

            cl.findViewById<Spinner>(R.id.typeSpinner).visibility = View.GONE
            cl.findViewById<Spinner>(R.id.sizeSpinner).visibility = View.GONE

            cl.findViewById<EditText>(R.id.selectedColor).visibility = View.GONE
        }

        val anim1 = ValueAnimator.ofInt(cl.measuredHeight, 150)
        anim1.addUpdateListener { valueAnimator ->
            val animValue = valueAnimator.animatedValue as Int
            val layoutParams = cl.layoutParams
            layoutParams.height = animValue
            cl.layoutParams = layoutParams
        }
        anim1.duration = 700
        anim1.start()

        val anim = ValueAnimator.ofInt(constraintLayout.measuredHeight, 750)
        anim.addUpdateListener { valueAnimator ->
            val animValue = valueAnimator.animatedValue as Int
            val layoutParams = constraintLayout.layoutParams
            layoutParams.height = animValue
            constraintLayout.layoutParams = layoutParams
        }
        anim.duration = 500
        anim.start()
    }

    fun onSwipeBottom() {
        cl.layoutParams = cl.layoutParams.apply {
            cl.findViewById<TextView>(R.id.textView9).visibility = View.VISIBLE
            cl.findViewById<TextView>(R.id.textView11).visibility = View.VISIBLE
            cl.findViewById<TextView>(R.id.textView15).visibility = View.VISIBLE
            cl.findViewById<TextView>(R.id.textView17).visibility = View.VISIBLE

            cl.findViewById<Spinner>(R.id.typeSpinner).visibility = View.VISIBLE
            cl.findViewById<Spinner>(R.id.sizeSpinner).visibility = View.VISIBLE

            cl.findViewById<EditText>(R.id.selectedColor).visibility = View.VISIBLE

            height = 615
        }

        val anim1 = ValueAnimator.ofInt(cl.measuredHeight, 615)
        anim1.addUpdateListener { valueAnimator ->
            val animValue = valueAnimator.animatedValue as Int
            val layoutParams = cl.layoutParams
            layoutParams.height = animValue
            cl.layoutParams = layoutParams
        }
        anim1.duration = 700
        anim1.start()

        val anim = ValueAnimator.ofInt(constraintLayout.measuredHeight, 800)
        anim.addUpdateListener { valueAnimator ->
            val animValue = valueAnimator.animatedValue as Int
            val layoutParams = constraintLayout.getLayoutParams()
            layoutParams.height = animValue
            constraintLayout.layoutParams = layoutParams
        }
        anim.duration = 500
        anim.start()

    }
}