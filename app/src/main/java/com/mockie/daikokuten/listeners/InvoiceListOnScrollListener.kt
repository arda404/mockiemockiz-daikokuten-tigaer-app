package com.mockie.daikokuten.listeners

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.mockie.daikokuten.adapters.RVInvoiceListAdapter
import com.mockie.daikokuten.database.repositories.Invoices
import com.mockie.daikokuten.models.InvoiceListData
import com.mockie.daikokuten.models.SearchKeyword

class InvoiceListOnScrollListener(
        val context:Context,
        val layoutManager: LinearLayoutManager,
        val adapter: RVInvoiceListAdapter,
        val dataList: ArrayList<InvoiceListData>,
        val keyword:SearchKeyword
): RecyclerView.OnScrollListener() {

    var loading = false
    var offset = 0

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val totalItem = layoutManager.itemCount
        val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

        if (!loading && lastVisibleItem == totalItem - 1) {
            loading = true

            offset += 10
            val initialSize = dataList.size
            val invoiceData = Invoices(context).getInvoiceListData(offset, 10, keyword)

            for (a in 0 until invoiceData.size)
            {
                dataList.add(InvoiceListData(0,
                        invoiceData[a]._id,
                        invoiceData[a].customer_name,
                        invoiceData[a].address_name,
                        invoiceData[a].address,
                        invoiceData[a].total_billing,
                        invoiceData[a].status,
                        invoiceData[a].created_at
                ))
            }

            val updatedSize = dataList.size
            Log.d(":dump3 total", "updt size " + updatedSize)
            recyclerView?.post { adapter.notifyItemRangeInserted(initialSize, updatedSize) }

            loading = false
        }
    }

}