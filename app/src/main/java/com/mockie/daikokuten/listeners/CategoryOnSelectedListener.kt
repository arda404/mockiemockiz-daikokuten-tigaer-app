package com.mockie.daikokuten.listeners

import android.R
import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.mockie.daikokuten.database.repositories.PrdSizes
import com.mockie.daikokuten.database.repositories.PrdTypes
import com.mockie.daikokuten.helpers.StringWithTag

/**
 * Created by mockie on 07/12/17.
 */


class CategoryOnSelectedListener(val context: Context, var style: Any?) : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(
            parent: AdapterView<*>,
            view: View?,
            pos: Int,
            id: Long
    ) {

        val s = parent.getItemAtPosition(pos) as StringWithTag
        val tag = s.tag

        populateTypeSpinner(tag.toString())
    }

    private fun populateTypeSpinner(category: String)
    {
        val sizes = PrdTypes(context).getTypeByCategory(category)
        val listSpinner = ArrayList<StringWithTag>()

        (0 until sizes.count()).mapTo(listSpinner) { StringWithTag(sizes[it].readable, sizes[it].type) }

        var myStyle = com.mockie.daikokuten.R.layout.spinner_style
        if (style != null) {
            myStyle = style.toString().toInt()
        }

        val adapter = ArrayAdapter(context,
                myStyle, listSpinner)
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)

        val spin = (context as Activity).findViewById<Spinner>(com.mockie.daikokuten.R.id.typeSpinner)
        spin.adapter = adapter
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }

}