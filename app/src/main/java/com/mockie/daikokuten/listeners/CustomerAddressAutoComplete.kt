package com.mockie.daikokuten.listeners

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.helpers.CustomerAddressSwitcher
import com.mockie.daikokuten.helpers.CustomerSwitcher

class CustomerAddressAutoComplete(val context:Context, val view:View): AdapterView.OnItemClickListener {

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        val s = parent.getItemAtPosition(0) as CustomerAddressSwitcher
        val phone = (context as Activity).findViewById<EditText>(R.id.customerAddressPhone)
        val addressId = (context).findViewById<EditText>(R.id.addressId)
        val address = (context as Activity).findViewById<EditText>(R.id.address)
        val oldAdressPhone = (context as Activity).findViewById<TextView>(R.id.oldCustomerAddressPhone)

        oldAdressPhone.setText(s.getCustomerAddressPhone())
        phone.setText(s.getCustomerAddressPhone())
        addressId.setText(s.tag.toString())
        address.setText(s.getAddress())
    }
}