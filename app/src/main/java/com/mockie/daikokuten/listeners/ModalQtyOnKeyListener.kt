package com.mockie.daikokuten.listeners

import android.app.Dialog
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import com.mockie.daikokuten.models.BarcodeList
import java.util.regex.Pattern


/**
 * Created by mockie on 12/12/17.
 */


class ModalQtyOnKeyListener(
        private val dialog: Dialog,
        private val QtyTextToEdit: EditText,
        private val QtyModal: EditText,
        private var oldQty: Any,
        private val currentBarcodeText: EditText,
        val recyclerView: RecyclerView,
        private val mContacts:  MutableList<BarcodeList>) : View.OnKeyListener {

    var qty: String = ""
    var barcode: String = ""

    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {

        if (event.action == KeyEvent.ACTION_UP) {

            return if (keyCode == 66) {

                val pattern = Pattern.compile("([0-9]+)?([a-zA-Z]{3}[0-9]+)?")
                val matcher = pattern.matcher(QtyModal.text.toString())

                if (matcher.find())
                {
                    qty = if (matcher.group(1)!= null)  matcher.group(1) else "1"
                    barcode = if (matcher.group(2)!= null) matcher.group(2) else ""
                    val newQty = QtyTextToEdit.text.toString().toInt() + qty.toInt()
                    QtyTextToEdit.setText(newQty.toString())
                }

                recyclerView.layoutManager.scrollToPosition(mContacts.size - 1)

                currentBarcodeText.requestFocus()
                currentBarcodeText.setText("")

                if(barcode.length==12)
                {
                    currentBarcodeText.setText(barcode)
                    currentBarcodeText.setSelection(barcode.length)

                    currentBarcodeText.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER))
                    currentBarcodeText.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER))
                }

                dialog.hide()
                dialog.dismiss()
                true
            } else false
        }

        return false
    }
}