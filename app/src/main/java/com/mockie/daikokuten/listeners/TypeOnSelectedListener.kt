package com.mockie.daikokuten.listeners

import android.R
import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.mockie.daikokuten.PrintBarcodeFragment
import com.mockie.daikokuten.database.repositories.PrdSizes
import com.mockie.daikokuten.helpers.StringWithTag

/**
 * Created by mockie on 07/12/17.
 */


class TypeOnSelectedListener (val context: Context, var style: Any?, val appActive: Int) : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(
            parent: AdapterView<*>,
            view: View?,
            pos: Int,
            id: Long
    ) {

        val s = parent.getItemAtPosition(pos) as StringWithTag
        val tag = s.tag

        populateSizeSpinner(tag)
    }

    fun populateSizeSpinner(type: String)
    {
        val sizes = PrdSizes(context).getSizeByType(type, appActive)
        val listSpinner = ArrayList<StringWithTag>()

        (0 until sizes.count()).mapTo(listSpinner) { StringWithTag(sizes[it].readable, sizes[it].size) }

        var myStyle = com.mockie.daikokuten.R.layout.spinner_style
        if (style != null) {
            myStyle = style.toString().toInt()
        }

        val adapter = ArrayAdapter(context,
                myStyle, listSpinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        val spin = (context as Activity).findViewById<Spinner>(com.mockie.daikokuten.R.id.sizeSpinner)
        spin.adapter = adapter
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }

}