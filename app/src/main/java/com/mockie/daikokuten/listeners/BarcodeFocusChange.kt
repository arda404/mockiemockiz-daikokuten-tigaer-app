package com.mockie.daikokuten.listeners

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.helpers.BarcodeListHelper
import com.mockie.daikokuten.helpers.BarcodeValidator

/**
 * Created by mockie on 09/07/18.
 */


open class BarcodeFocusChange(
        open val context: Context
) : View.OnFocusChangeListener {

    lateinit var barcode:EditText
    lateinit var qty:EditText
    lateinit var description:TextView

    var passed = false

    protected fun fillTheDescription(barcode:String)
    {
        try{
            _getDescription().postDelayed({
                val barcodeHelper = BarcodeListHelper(context)
                val type = barcodeHelper.getTypeReadable(barcode.toUpperCase())
                val size = barcodeHelper.getSizeReadable(barcode.toUpperCase())
                val price = barcodeHelper.getPrice(barcode.toUpperCase())

                _getDescription().text = "$type, $size, $price"
            }, 100)
        } catch (e:Exception) {}
    }

    fun setBarcodeEditText(barcodeVal:EditText)
    {
        barcode = barcodeVal
    }

    fun _getBarcode():EditText
    {
        return barcode
    }

    fun setDescriptionEditText(descriptionVal:TextView)
    {
        description = descriptionVal
    }

    fun _getDescription():TextView
    {
        return description
    }

    fun setQtyEditText(qtyVal:EditText)
    {
        qty = qtyVal
    }

    fun _getQty():EditText
    {
        return qty
    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {

        if (!hasFocus) {

            try {
                val theBarcode = _getBarcode().text.toString()
                val validator = BarcodeValidator(context)
                validator.lengthValidator(theBarcode)
                validator.barcodePatternValidator(theBarcode)
                validator.skuValidator(theBarcode)

                val isValid = validator.validator()

                validator.showMessage(_getBarcode(), _getQty(), _getDescription())

                if (isValid) {
                    passed = true
                    fillTheDescription(barcode.text.toString().toUpperCase())
                }

            } catch (e:Exception) {}

        }

    }

}