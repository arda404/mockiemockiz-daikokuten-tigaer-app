package com.mockie.daikokuten.listeners


import android.app.Activity
import android.content.Context
import android.os.Build
import android.support.v4.content.res.ResourcesCompat
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.adapters.RecyclerViewReturnAdapter
import com.mockie.daikokuten.helpers.BarcodeValidator
import com.mockie.daikokuten.helpers.BeepHelper
import com.mockie.daikokuten.models.ItemInBasket

/**
 * Created by mockie on 09/07/18.
 */


class ReturnBarcodeFocusChange(
        override val context: Context,
        private val itemInBasket: ArrayList<ItemInBasket>)
    : BarcodeFocusChange(context) {


    override fun onFocusChange(v: View, hasFocus: Boolean) {
        super.onFocusChange(v, hasFocus)
        if (!hasFocus)
        {
            if (passed)
            {
                val theBarcode = _getBarcode().text.toString()
                val validator = BarcodeValidator(context)
                validator.isInBasket(theBarcode, itemInBasket)
                val isValid = validator.validator()

                _getBarcode().postDelayed({
                    validator.showMessage(_getBarcode(), _getQty(), _getDescription())
                }, 70)

                Log.d("momo dodol", isValid.toString())
                if (isValid)
                {
                    fillTheDescription(_getBarcode().text.toString())
                }
            }
        }

    }

}