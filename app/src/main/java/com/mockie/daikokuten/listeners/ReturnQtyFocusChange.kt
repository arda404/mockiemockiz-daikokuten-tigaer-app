package com.mockie.daikokuten.listeners

import android.content.Context
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.helpers.BarcodeValidator
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.models.ItemInBasket

open class ReturnQtyFocusChange(
        override val context:Context,
        val itemInBasket: ArrayList<ItemInBasket>
): QtyFocusChange(context) {

    override fun onFocusChange(v: View, hasFocus: Boolean) {

        if (!hasFocus)
        {
            val theBarcode = _getBarcode().text.toString()
            val theQty = _getQty().text.toString().toInt()

            val validator = BarcodeValidator(context)
            validator.checkReturQuantity(theBarcode, theQty, itemInBasket)

            validator.validator()

            validator.showMessage( null, _getQty(), _getDescription())
        }

    }

}