package com.mockie.daikokuten.listeners

import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.CheckBox
import android.widget.Spinner
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.adapters.RVPaymentMethodAdapter
import com.mockie.daikokuten.helpers.PaymentHelper
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.models.PaymentMethod
import kotlinx.android.synthetic.main.payment_method_list_child.view.*

class PaymentMethodOnClickListener(
        val adapter:RVPaymentMethodAdapter,
        mSpinner:Spinner,
        val paymentMethod: ArrayList<PaymentMethod>,
        val paymentMethodDesc:TextView,
        val debt:TextView,
        val kurang:TextView,
        val change:CheckBox
): AdapterView.OnItemSelectedListener{

    var selectedItem = mSpinner.selectedItemPosition

    var dataPosition = 0

    fun updatePosition(pos:Int)
    {
        dataPosition = pos
    }

    override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {

        //PaymentHelper().getKurang(paymentMethod, total, kurang)

        val itemD = paymentMethod.get(dataPosition)
        val readableSelected =  parentView.getItemAtPosition(position) as StringWithTag

        itemD.method = readableSelected

        paymentMethodDesc.text = readableSelected.toString()

        paymentMethod.add(PaymentMethod("0", StringWithTag("", "0"), "amount"))
        adapter.notifyItemInserted(paymentMethod.size)

        selectedItem = position

        val customerDebt = PaymentHelper().readableAmountToInt(debt.text.toString())
        val theChange = PaymentHelper().getChange(paymentMethod, customerDebt)

        if (theChange > 0)
        {
            change.isEnabled = true
        }

        kurang.text = PaymentHelper().amountToReadable(theChange)
    }

    override fun onNothingSelected(parentView: AdapterView<*>) {
        // Your code here
    }

}
