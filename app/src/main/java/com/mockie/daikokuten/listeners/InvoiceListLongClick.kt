package com.mockie.daikokuten.listeners


import android.support.v4.app.FragmentManager
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.helpers.AlertHelper


class InvoiceListLongClick(val context: Context, val fragmentManager:FragmentManager, val itemView:View): View.OnLongClickListener{

    override fun onLongClick(arg0: View): Boolean {
        val realInvoiceId = itemView.findViewById<TextView>(R.id.realInvoiceId)
        val tempInvoiceId = itemView.findViewById<TextView>(R.id.tempInvoiceId)

        if (tempInvoiceId.text.toString().toInt() > 0)
        {
            AlertHelper(context).invoiceListOption(context, fragmentManager, tempInvoiceId.text.toString().toInt(), realInvoiceId.text.toString().toInt())
        } else {

            val checkUnsync = InvoiceToSync(context).checkIfUnSyncDataExisted(realInvoiceId.text.toString().toInt())

            if (checkUnsync != null)
            {
                AlertHelper(context).invoiceListOption(context, fragmentManager,checkUnsync.invoice_id, checkUnsync.real_invoice_id)
            } else {
                AlertHelper(context).invoiceListOption(context, fragmentManager,0, realInvoiceId.text.toString().toInt())
            }
        }

        return false

    }

}

