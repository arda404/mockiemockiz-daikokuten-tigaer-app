package com.mockie.daikokuten.listeners

import android.app.Dialog
import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.helpers.Customer
import com.mockie.daikokuten.helpers.CustomerSwitcher
import com.mockie.daikokuten.helpers.PaymentHelper

class CustomerModalAutoComplete(val context: Context, val commentDialog:Dialog, val view:View): AdapterView.OnItemClickListener {

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        val customerPhone = commentDialog.findViewById<TextView>(R.id.phone)
        val customerId = commentDialog.findViewById<TextView>(R.id.customerId)

        val s = parent.getItemAtPosition(position) as CustomerSwitcher

        customerId.text = s.toCustomerId().toString()
        customerPhone.text = s.toCustomerPhone()
    }
}