package com.mockie.daikokuten.listeners

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.support.constraint.ConstraintLayout
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.database.repositories.PrdPrices
import com.mockie.daikokuten.database.repositories.PrdSizes
import com.mockie.daikokuten.database.repositories.PrdTypes
import com.mockie.daikokuten.helpers.BarcodeListHelper
import com.mockie.daikokuten.helpers.BarcodeValidator
import com.mockie.daikokuten.models.BarcodeList
import java.util.regex.Pattern
import com.mockie.daikokuten.helpers.BeepHelper


/**
 * Created by mockie on 12/12/17.
 */
class BarcodeEditTextOnKeyListener(
        private val mContext: Context,
        private val adapter: RecyclerViewBarcodeAdapter,
        private val recyclerView: RecyclerView,
        private val currentBarcode: EditText,
        private val currentQty: EditText,
        private val description: TextView,
        private var mContacts: MutableList<BarcodeList>,
        val pageName: String,
        val keyName: String) : View.OnKeyListener {

    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {

        if (event.action == KeyEvent.ACTION_UP) {

            return if (keyCode == 66 || keyCode == 77) {

                process()
                true

            } else false
        }
        return false
    }

    fun process()
    {
        currentBarcode.postDelayed({

            if (!isBarcodeExisted())
            {
                // request focus to qty edittext
                currentQty.postDelayed({
                    currentQty.requestFocus()
                    currentQty.setOnKeyListener(QtyEditTextOnKeyListener(mContext, adapter, mContacts, recyclerView, currentQty, pageName, keyName))
                    if (currentQty.isFocused)
                    {
                        val imm = (mContext as Activity).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.showSoftInput(currentQty, 0)

                        BeepHelper().beep(100)
                    }
                }, 70)
            }

        }, 50)
    }

    private fun isBarcodeExisted(): Boolean
    {
        var existed = false
        val theBar =currentBarcode.text.toString().toUpperCase()
        currentBarcode.postDelayed({

            val theBarcodeShit = currentBarcode.tag.toString().toInt()

            for (i in 0 until mContacts.size-1)
            {
                if (theBar == mContacts[i].barcode.toUpperCase())
                {
                    if (theBarcodeShit != i)
                    {
                        existed = true

                        val commentDialog = Dialog(mContext)

                        if (commentDialog.isShowing)
                        {
                            commentDialog.dismiss()
                        }

                        commentDialog.setContentView(R.layout.edit_qty)
                        commentDialog.show()

                        val modalLayout = commentDialog.findViewById<ConstraintLayout>(R.id.modal_layout)
                        val modalQty = modalLayout.findViewWithTag<EditText>("editQty")
                        val oldQty = modalQty.text.toString()

                        recyclerView.layoutManager.scrollToPosition(i)

                        recyclerView.postDelayed({
                            // Call smooth scroll

                            val qtyText = recyclerView.layoutManager.findViewByPosition(i).findViewById<EditText>(R.id.qtyText0)

                            modalQty.setOnKeyListener(ModalQtyOnKeyListener(
                                    commentDialog,
                                    qtyText,
                                    modalQty,
                                    oldQty,
                                    currentBarcode,
                                    recyclerView,
                                    mContacts))

                            val imm = (mContext as Activity).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.showSoftInput(modalQty, 0)

                        }, 50)


                    }
                }
            }

        }, 80)

        return existed
    }
}