package com.mockie.daikokuten.listeners

import android.app.Activity
import android.content.Context
import android.os.Build
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.helpers.BeepHelper
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.models.BarcodeList

/**
 * Created by mockie on 19/01/18.
 */


class StockTakingBarcodeEditOnKeyListener(
        private val mContext: Context,
        private val adapter: RecyclerViewBarcodeAdapter,
        private val recyclerView: RecyclerView,
        private val currentBarcode: EditText,
        private val currentQty: EditText,
        private val description: TextView,
        private var mContacts: MutableList<BarcodeList>,
        private val pageName: String,
        private val keyName: String,
        private val scope: List<String>): View.OnKeyListener
{
    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {

        var isPassed = false
        var errorMessage: String

        if (event.action == KeyEvent.ACTION_UP) {

            if (keyCode == 66) {

                val scope = scope[0] + scope[1] + scope[2]

                currentBarcode.postDelayed({
                    var passed = true
                    val barcode = currentBarcode.text
                    if(barcode.toString().trim().count() >= scope.count())
                    {
                        if (barcode.toString().trim().substring(0, scope.count()) != scope)
                        {
                            passed = false
                        }
                    }

                    if (!passed)
                    {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            currentBarcode.background = ResourcesCompat.getDrawable((mContext).resources, R.drawable.error, null)
                        } else {
                            currentBarcode.setBackgroundResource(R.drawable.error)
                        }

                        currentBarcode.requestFocus()
                        currentBarcode.selectAll()
                        errorMessage = "kode item ini tidak termasuk scope"
                        description.text = errorMessage
                        BeepHelper().beep(600)
                    }
                    else
                    {
                        BarcodeEditTextOnKeyListener(
                                mContext,
                                adapter,
                                recyclerView,
                                currentBarcode,
                                currentQty,
                                description,
                                mContacts,
                                pageName,
                                keyName).process()

                        isPassed = true
                    }

                }, 100)
            }
        }

        return isPassed

    }
}