package com.mockie.daikokuten.listeners

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.helpers.CustomerSwitcher

class SearchCustomerAutoComplete(val context:Context, val view:View): AdapterView.OnItemClickListener {

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        val phone = (context as Activity).findViewById<EditText>(R.id.customerPhone)
        val customerId = (context).findViewById<TextView>(R.id.customerId)

        val s = parent.getItemAtPosition(position) as CustomerSwitcher
        phone.setText(s.toCustomerPhone())
        customerId.setText(s.toCustomerId().toString())
    }
}