package com.mockie.daikokuten.watchers

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.mockie.daikokuten.models.BarcodeList


/**
 * Created by mockie on 09/02/18.
 */

class MetaPositionWatcher : TextWatcher {
    private var position: Int = 0

    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        try {
            val todo = BarcodeList.getItem(position)

            if (todo != null && charSequence.toString() != "")
            {
                todo.meta = charSequence.toString()
            }
        } catch (e:Exception) {}
    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}