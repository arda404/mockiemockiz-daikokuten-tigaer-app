package com.mockie.daikokuten.watchers

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.mockie.daikokuten.models.BarcodeList

/**
 * Created by mockie on 09/02/18.
 */

class QtyConfirmationTextWatcher(val data: ArrayList<BarcodeList>, val position: Int) : TextWatcher {

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        val todo = BarcodeList.getItem(position)

        if (charSequence.toString() != "")
        {
            try{
                if (todo != null)
                {
                    todo.qty = charSequence.toString().toInt()
                }
            }catch (e: Exception){}
        }
    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}