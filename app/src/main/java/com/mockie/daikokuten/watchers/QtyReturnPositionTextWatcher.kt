package com.mockie.daikokuten.watchers

import android.content.Context
import android.text.Editable
import android.util.Log
import android.widget.TextView
import com.mockie.daikokuten.adapters.RecyclerViewReturnAdapter
import com.mockie.daikokuten.helpers.BarcodeValidator
import com.mockie.daikokuten.helpers.PaymentHelper
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.models.ItemInBasket


/**
 * Created by mockie on 09/02/18.
 */

class QtyReturnPositionTextWatcher(
        override val context:Context,
        val itemInBasket: ArrayList<ItemInBasket>,
        val additionalPrice:Int,
        val totalReturForm:TextView,
        val adapter:RecyclerViewReturnAdapter
) : QtyPositionTextWatcher(context) {

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {

        val todo = BarcodeList.getItem(position)
        var qty = 0
        var totalRetur = 0

        if (charSequence.toString() != "")
        {
            try{

                if (todo != null)
                {
                    qty = charSequence.toString().toInt()

                    val validator = BarcodeValidator(context)
                    val returQty = validator.checkReturQuantity(_getBarcode().text.toString(), qty, itemInBasket)
                    Log.d("hooh", qty.toString() + " = " + returQty.toString())

                    todo.qty = returQty
                }

                totalReturForm.postDelayed({

                    val items = BarcodeList.getAllBarcode()

                    for (it in items)
                    {
                        for (iib in itemInBasket)
                        {
                            if (it.barcode == iib.barcode)
                            {
                                totalRetur += (iib.price + additionalPrice) * it.qty.toString().toInt()
                            }
                        }
                    }

                    totalReturForm.text = PaymentHelper().amountToReadable(totalRetur)

                }, 50)

            }catch (e: Exception){}

        }
    }

    override fun afterTextChanged(editable: Editable) {

    }

}