package com.mockie.daikokuten.watchers

import android.content.Context
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.models.ItemInBasket


/**
 * Created by mockie on 09/02/18.
 */

class ReturnBarcodeTextWatcher(override val context: Context, val itemInBasket:ArrayList<ItemInBasket>) : PositionTextWatcher(context) {

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {

        val todo = BarcodeList.getItem(position)

        if (todo != null)
        {
            todo.barcode = charSequence.toString().toUpperCase()

            for (iib in itemInBasket)
            {
                if (todo.barcode == iib.barcode)
                {
                    todo.price = iib.price
                }
            }
        }
    }

}