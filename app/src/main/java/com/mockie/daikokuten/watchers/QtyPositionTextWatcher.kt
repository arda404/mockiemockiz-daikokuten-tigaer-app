package com.mockie.daikokuten.watchers

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.helpers.DraftHelper
import com.mockie.daikokuten.models.BarcodeList


/**
 * Created by mockie on 09/02/18.
 */

open class QtyPositionTextWatcher(open val context:Context) : TextWatcher {

    open var position: Int = 0

    lateinit var barcode: EditText
    lateinit var qty: EditText
    lateinit var description: TextView

    fun setBarcodeEditText(barcodeVal: EditText)
    {
        barcodeVal.postDelayed({
            barcode = barcodeVal
        }, 80)
    }

    fun _getBarcode(): EditText
    {
        return barcode
    }

    fun setDescriptionEditText(descriptionVal: TextView)
    {
        descriptionVal.postDelayed({
            description = descriptionVal
        }, 70)
    }

    fun _getDescription(): TextView
    {
        return description
    }

    fun setQtyEditText(qtyVal: EditText)
    {
       qtyVal.postDelayed({
           qty = qtyVal
       }, 70)
    }

    fun _getQty(): EditText
    {
        return qty
    }

    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        val todo = BarcodeList.getItem(position)

        if (charSequence.toString() != "")
        {
            try{
                if (todo != null)
                {
                    todo.qty = charSequence.toString().toInt()
                }
            }catch (e: Exception){}
        }
    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}