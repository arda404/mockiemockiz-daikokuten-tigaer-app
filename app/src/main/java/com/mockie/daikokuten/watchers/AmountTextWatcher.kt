package com.mockie.daikokuten.watchers

import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Adapter
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.adapters.RVPaymentMethodAdapter
import com.mockie.daikokuten.helpers.PaymentHelper
import com.mockie.daikokuten.models.PaymentMethod
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by mockie on 09/02/18.
 */

class AmountTextWatcher(
        val payment:ArrayList<PaymentMethod>,
        val amount:EditText,
        val debt:TextView,
        val kurang:TextView,
        val readableAmout:TextView
) : TextWatcher {

    private var position: Int = 0

    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {

        val dodol = PaymentMethod.getItem(position)

        if (charSequence.isNotEmpty())
        {
            readableAmout.text = PaymentHelper().amountToReadable(charSequence.toString().toInt())
        }

        if (dodol != null)
        {
            dodol.amount = charSequence.toString()
        }

    }

    override fun afterTextChanged(editable: Editable) {

        val customerDebt = PaymentHelper().readableAmountToInt(debt.text.toString())
        val theChange = PaymentHelper().getChange(payment, customerDebt)
        kurang.text = PaymentHelper().amountToReadable(theChange)
    }
}