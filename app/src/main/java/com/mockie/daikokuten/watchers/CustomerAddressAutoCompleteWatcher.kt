package com.mockie.daikokuten.watchers

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.database.repositories.Customer
import com.mockie.daikokuten.database.repositories.CustomerAddress
import com.mockie.daikokuten.helpers.CustomerAddressSwitcher
import com.mockie.daikokuten.helpers.CustomerSwitcher
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.models.BarcodeList


/**
 * Created by mockie on 09/02/18.
 */

class CustomerAddressAutoCompleteWatcher(val context:Context, val view: View) : TextWatcher {
    private var position: Int = 0

    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op
    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {

        view.findViewById<EditText>(R.id.addressId).setText("0")
        view.findViewById<EditText>(R.id.customerAddressPhone).setText("")
        view.findViewById<TextView>(R.id.oldCustomerAddressPhone).setText("")
        view.findViewById<EditText>(R.id.address).setText("")

        val customerIdText = view.findViewById<EditText>(R.id.customerId)
        var customerId = 0

        try {
            customerId = customerIdText.text.toString().toInt()
        } catch (e:Exception){}

        if (customerId != 0)
        {
            val customerAddress = CustomerAddress(context).findCustomerAddressByNameAndCustomerId(
                    charSequence.toString(),
                    customerId
            )
            val listSpinner = ArrayList<CustomerAddressSwitcher>()
            val actv = view.findViewById(R.id.customerAddressName) as AutoCompleteTextView

            try{
                (0 until customerAddress.count()).mapTo(listSpinner) {
                    CustomerAddressSwitcher(customerAddress[it].name, customerAddress[it]._id, customerAddress[i].phone, customerAddress[i].address)
                }

                val adapter = ArrayAdapter(context,
                        R.layout.spinner_item, listSpinner)

                actv.setAdapter(adapter)
            } catch (e:Exception) {}
        }
    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}