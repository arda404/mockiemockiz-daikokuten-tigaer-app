package com.mockie.daikokuten.watchers

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.database.repositories.Customer
import com.mockie.daikokuten.helpers.CustomerSwitcher
import com.mockie.daikokuten.helpers.PurchaseDetailHelper
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.models.BarcodeList


/**
 * Created by mockie on 09/02/18.
 */

class ShippingChargeWatcher(val context:Context, val view: View) : TextWatcher {
    private var position: Int = 0

    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        PurchaseDetailHelper(context).getTotalBilling(view)
    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}