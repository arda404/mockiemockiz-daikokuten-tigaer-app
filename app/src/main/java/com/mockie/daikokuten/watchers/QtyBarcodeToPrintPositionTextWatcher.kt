package com.mockie.daikokuten.watchers

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.mockie.daikokuten.database.repositories.BarcodeToPrintRepository
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.models.BarcodeToPrintList

/**
 * Created by mockie on 09/02/18.
 */

class QtyBarcodeToPrintPositionTextWatcher(val context: Context) : TextWatcher {
    private var position: Int = 0

    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op
    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {

        val todo = BarcodeToPrintList.getItem(position)
        try{
            todo.qty = charSequence.toString().toInt()
            BarcodeToPrintRepository(context).update(todo.id, todo.barcode, todo.meta, todo.size, charSequence.toString().toInt())
        }catch (e: Exception){}

    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}