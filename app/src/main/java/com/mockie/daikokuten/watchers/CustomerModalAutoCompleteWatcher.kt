package com.mockie.daikokuten.watchers

import android.app.Dialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.database.repositories.Customer
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.helpers.CustomerSwitcher


/**
 * Created by mockie on 09/02/18.
 */

class CustomerModalAutoCompleteWatcher(
        val context:Context,
        val dialog:Dialog,
        val view: View,
        val myAdapter:ArrayAdapter<CustomerSwitcher>,
        val item:ArrayList<CustomerSwitcher>) : TextWatcher {

    private var position: Int = 0

    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {

        view.findViewById<TextView>(R.id.customerId).text = "0"
        view.findViewById<TextView>(R.id.customerName).text = ""
        view.findViewById<TextView>(R.id.customerPhone).text = ""
        view.findViewById<TextView>(R.id.customerBalance).text = "0"
        view.findViewById<TextView>(R.id.customerDebt).text = "0"

        val actv = dialog.findViewById(R.id.autoCompleteTextView) as AutoCompleteTextView

        try{

            val customer = Customer(context).findCustomerByName(charSequence.toString())
            val customerINV = InvoiceToSync(context).findCustomerByName(charSequence.toString())


            for (a in 0 until customerINV.count())
            {
                var found2 = false
                for (b in 0 until item.count())
                {
                    if (
                            (item[b].name == customerINV[a].name && item[b].phone == customerINV[a].phone) ||
                            customerINV[a].customerId == item[b].customerId)
                    {
                        found2 = true
                    }
                }

                if (!found2)
                {
                    item.add(CustomerSwitcher(customerINV[a].name, customerINV[a].customerId, customerINV[a].phone, customerINV[a].tempInvoiceId))
                }
            }

            for (a in 0 until customer.count())
            {
                var found2 = false
                for (b in 0 until item.count())
                {
                    if (
                            (item[b].name == customer[a].name && item[b].phone == customer[a].phone) ||
                            customer[a]._id == item[b].customerId)
                    {
                        found2 = true
                    }
                }

                if (!found2)
                {
                    item.add(CustomerSwitcher(customer[a].name, customer[a]._id, customer[a].phone, 0))
                }
            }

            myAdapter.notifyDataSetChanged()

            val adapter = ArrayAdapter(context,
                    R.layout.spinner_item, item)

            actv.setAdapter(adapter)

        } catch (e:Exception){}
    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}