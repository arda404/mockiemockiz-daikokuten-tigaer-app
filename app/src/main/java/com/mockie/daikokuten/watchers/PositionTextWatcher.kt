package com.mockie.daikokuten.watchers

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.helpers.BarcodeListHelper
import com.mockie.daikokuten.models.BarcodeList


/**
 * Created by mockie on 09/02/18.
 */

open class PositionTextWatcher(open val context: Context) : TextWatcher {
    protected var position: Int = 0

    lateinit var barcode:EditText
    lateinit var qty:EditText
    lateinit var description:TextView

    fun setBarcodeEditText(barcodeVal: EditText)
    {
        barcode = barcodeVal
    }

    fun _getBarcode(): EditText
    {
        return barcode
    }

    fun setDescriptionEditText(descriptionVal: TextView)
    {
        description = descriptionVal
    }

    fun _getDescription(): TextView
    {
        return description
    }

    fun setQtyEditText(qtyVal: EditText)
    {
        qty = qtyVal
    }

    fun _getQty(): EditText
    {
        return qty
    }


    fun updatePosition(pos: Int) {
        position = pos
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        // no op

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        val todo = BarcodeList.getItem(position)
        if(todo != null)
        {
            todo.barcode = charSequence.toString().toUpperCase()

            try {
                todo.price = BarcodeListHelper(context).getPrice(todo.barcode)
            } catch (e:Exception) {}
        }
    }

    override fun afterTextChanged(editable: Editable) {
        // no op
    }
}