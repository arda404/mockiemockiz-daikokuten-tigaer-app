package com.mockie.daikokuten.permissions

/**
 * Created by mockie on 28/02/18.
 */

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Parcelable

/**
 * Created by mockie on 11/12/17.
 */


class UsbReceiver : BroadcastReceiver() {

    private var device: UsbDevice? = null

    val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        if (ACTION_USB_PERMISSION.equals(action)) {
            synchronized(this) {
                intent.getParcelableExtra<Parcelable>(UsbManager.EXTRA_DEVICE) as UsbDevice
            }
        }
    }

    fun getDevice() : UsbDevice?
    {
        return this.device
    }
}