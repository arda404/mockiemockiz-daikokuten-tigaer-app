package com.mockie.daikokuten.permissions

/**
 * Created by mockie on 28/02/18.
 */


import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.util.Log
import android.widget.Toast

/**
 * Created by mockie on 11/12/17.
 */

class UsbPermission (mContext: Context) {

    private val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"

    private val mUsbReceiver = UsbReceiver()

    private val context: Context = mContext

    private var mUsbManager: UsbManager

    private var mPermissionIntent: PendingIntent? = null

    private var deviceList= HashMap<String, UsbDevice>()

    private var device: UsbDevice

    init {
        mUsbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
        mPermissionIntent = PendingIntent.getBroadcast(context, 0, Intent(mUsbReceiver.ACTION_USB_PERMISSION), 0)
        val filter = IntentFilter(mUsbReceiver.ACTION_USB_PERMISSION)

        deviceList = mUsbManager.deviceList

        device = setDevice()

        context.registerReceiver(mUsbReceiver, filter)
    }

    fun getUsb(): UsbReceiver
    {
        return mUsbReceiver
    }

    fun getTotalDevice(): Int
    {
        return deviceList.size
    }

    private fun getDeviceIterator(): MutableIterator<UsbDevice>
    {
        return deviceList.values.iterator()
    }

    private fun setDevice(): UsbDevice
    {
        while(getDeviceIterator().hasNext())
        {
            device = getDeviceIterator().next()
            if(getDeviceIterator().next().vendorId == 4611)
            {
                break
            }
        }

        return device
    }

    fun getDevice(): UsbDevice
    {
        return device
    }

    fun showPermissionDialog()
    {
        val mPermissionIntent: PendingIntent = PendingIntent.getBroadcast(context, 0,
                Intent(ACTION_USB_PERMISSION), PendingIntent.FLAG_ONE_SHOT)
        mUsbManager.requestPermission(device, mPermissionIntent)
    }

    fun hasPermission(): Boolean
    {
        return mUsbManager.hasPermission(device)
    }

    fun getUsbManager():UsbManager
    {
        return mUsbManager
    }

}