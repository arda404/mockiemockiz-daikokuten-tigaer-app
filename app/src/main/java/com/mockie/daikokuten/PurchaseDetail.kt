package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mockie.daikokuten.listeners.CustomerAddressAutoComplete
import com.mockie.daikokuten.listeners.CustomerAutoComplete
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.sync.helpers.UploadInvoiceSynchronizer
import com.mockie.daikokuten.watchers.CustomerAddressAutoCompleteWatcher
import com.mockie.daikokuten.watchers.CustomerAutoCompleteWatcher
import com.mockie.daikokuten.watchers.ShippingChargeWatcher
import android.graphics.Color
import android.util.Log
import com.mockie.daikokuten.database.repositories.*
import android.widget.TextView
import android.widget.ArrayAdapter
import kotlin.collections.ArrayList
import com.mockie.daikokuten.database.models.TheInvoice
import com.mockie.daikokuten.helpers.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PurchaseDetail.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PurchaseDetail.newInstance] factory method to
 * create an instance of this fragment.
 */
class PurchaseDetail  : BarcodeRecyclerBaseFragment() {

    val pageName: String = "purchase_detail"

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    lateinit var viewD:View
//    lateinit var barcodeList: ArrayList<BarcodeList>

    lateinit var customerId:TextView
    lateinit var customerName:AutoCompleteTextView
    lateinit var customerPhone:EditText
    lateinit var note:EditText

    lateinit var oldCustomerPhone:TextView
    lateinit var oldAddressPhone:TextView

    lateinit var addressId:TextView
    lateinit var addressName:AutoCompleteTextView
    lateinit var addressPhone:EditText
    lateinit var address:EditText
    lateinit var warning:TextView
    lateinit var option:Spinner


    var pageNameCashier = ""

    lateinit var totalBilling:TextView

    lateinit var ok:Button

    var item = ArrayList<CustomerSwitcher>()

    var isFragmentActive = false

    var tempInvoiceId = 0
    var realInvoiceId = 0

    lateinit var shippingCharge:EditText

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onStart()
    {
        super.onStart()
    }

    fun edit(tempInvoiceId:Int, realInvoiceId:Int)
    {
        DraftHelper(context).removeAllDraft(pageName)

        val theInvoice = TheInvoice(context, tempInvoiceId, realInvoiceId)

        DraftHelper(context).insertDraft(pageName, "customerId", theInvoice.customerId.toString())
        DraftHelper(context).insertDraft(pageName, "customerName", theInvoice.customerName)
        DraftHelper(context).insertDraft(pageName, "customerPhone", theInvoice.customerPhone)

        DraftHelper(context).insertDraft(pageName, "oldCustomerPhone", theInvoice.customerPhone)
        DraftHelper(context).insertDraft(pageName, "oldAddressPhone", theInvoice.addressPhone)

        DraftHelper(context).insertDraft(pageName, "note", theInvoice.note)

        DraftHelper(context).insertDraft(pageName, "customerAddressId", theInvoice.addressId.toString())
        DraftHelper(context).insertDraft(pageName, "customerAddressName", theInvoice.addressName)
        DraftHelper(context).insertDraft(pageName, "customerAddressPhone", theInvoice.addressPhone)
        DraftHelper(context).insertDraft(pageName, "customerAddress", theInvoice.address)

        DraftHelper(context).insertDraft(pageName, "shippingCharge", theInvoice.shippingFee.toString())
        DraftHelper(context).insertDraft(pageName, "status", theInvoice.status.toString())

        DraftHelper(context).insertDraft(pageName, "totalBilling", theInvoice.totalBilling.toString())
    }

    fun populateDraft() {

        customerId.text = DraftHelper(context).getDraft(pageName, "customerId")?.content
        customerName.setText(DraftHelper(context).getDraft(pageName, "customerName")?.content)
        customerPhone.setText(DraftHelper(context).getDraft(pageName, "customerPhone")?.content)
        note.setText(DraftHelper(context).getDraft(pageName, "note")?.content)

        addressId.text = DraftHelper(context).getDraft(pageName, "customerAddressId")?.content
        addressName.setText(DraftHelper(context).getDraft(pageName, "customerAddressName")?.content)
        addressPhone.setText(DraftHelper(context).getDraft(pageName, "customerAddressPhone")?.content)
        address.setText(DraftHelper(context).getDraft(pageName, "customerAddress")?.content)

        oldCustomerPhone.text = DraftHelper(context).getDraft(pageName, "oldCustomerPhone")?.content
        oldAddressPhone.text = DraftHelper(context).getDraft(pageName, "oldAddressPhone")?.content

        val shipCha = DraftHelper(context).getDraft(pageName, "shippingCharge")?.content
        if (shipCha != null)
        {
            shippingCharge.setText(Math.round(shipCha.toDouble()).toString())
        }

        val stat1 = DraftHelper(context).getDraft(pageName, "status")
        val stat = if (stat1 != null) stat1.content!!.toInt() else 0

        setSpinner(stat)

    }

    fun setSpinner(selectedItem:Int)
    {
        option = viewD.findViewById(R.id.option)

        // Initializing a String Array

        val statusList = ArrayList<String>()

        // Set the disable item text color

        statusList.add("Draft")
        statusList.add("Simpan")
        statusList.add("Kirim")

        if (realInvoiceId > 0)
        {
            statusList.add("Hapus")
        }

        val arrAdapter = object: ArrayAdapter<String>(context, R.layout.spinner_item, statusList){

            override fun isEnabled(position: Int): Boolean {
                return selectedItem != 2
            }

            override fun getDropDownView(position: Int, convertView: View?,
                                         parent: ViewGroup): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                if (!isEnabled(position)) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(Color.BLACK)
                }
                return view
            }

        }

        arrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        option.adapter = arrAdapter
        option.setSelection(selectedItem)

    }

    fun checkChanges()
    {
        if (realInvoiceId > 0)
        {
            var updatedAt1: String
            val checkData = Invoices(context).getInvoiceById(realInvoiceId)
            updatedAt1 = checkData?.updated_at ?: ""

            isFragmentActive = true
            val handler = Handler()
            val runnableCode = object : Runnable {

                override fun run() {

                    if (context != null) {

                        val checkData2 = Invoices(context).getInvoiceById(realInvoiceId)
                        val updatedAt2 = checkData2?.updated_at ?: ""

                        if (updatedAt1 != updatedAt2)
                        {
                            updatedAt1 = updatedAt2
                            val alert =  AlertHelper(context).itemHasChanged()

                            alert.first.setOnClickListener {

                                alert.second.hide()
                                alert.second.dismiss()

                                val bdl = Bundle(2)
                                bdl.putInt("tempInvoiceId", tempInvoiceId)
                                bdl.putInt("realInvoiceId", realInvoiceId)

                                val someFragment = CashierFragment()
                                someFragment.arguments = bdl

                                val transaction = fragmentManager.beginTransaction()
                                transaction.replace(R.id.container, someFragment) // give your fragment container id in first parameter
                                transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
                                transaction.commit()
                            }
                        }

                    }

                    if (isFragmentActive) {
                        handler.postDelayed(this, 4000)
                    }
                }

            }

            handler.post(runnableCode)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        try{

            val args = arguments
            pageNameCashier = args.getString("pageNameCashier", "")
            realInvoiceId = args.getInt("realInvoiceId", 0)
            tempInvoiceId = args.getInt("tempInvoiceId", 0)

        } catch (e:Exception){
            DraftHelper(context).removeAllDraft(pageName)
        }

        Log.d("check_bug", "realInvoiceId: " + realInvoiceId)
        Log.d("check_bug", "tempInvoiceId: " + tempInvoiceId)

        // Inflate the layout for this fragment
        viewD  =  inflater!!.inflate(R.layout.fragment_purchase_detail, container, false)
        shippingCharge = viewD.findViewById(R.id.shippingFee) as EditText
        val additionalFee = viewD.findViewById<TextView>(R.id.additionalFee)
        val totalPriceItem = viewD.findViewById<TextView>(R.id.totalItem)
        totalBilling = viewD.findViewById<TextView>(R.id.totalBilling)

        setPermissionsRoles("purchaser")
        setPermissionsRoles("developer")
        setPermissionsRoles("purchasehelper")
        checkPermission()

        customerId = viewD.findViewById(R.id.customerId)
        customerName = viewD.findViewById(R.id.customerName)
        customerPhone = viewD.findViewById(R.id.customerPhone)

        oldCustomerPhone = viewD.findViewById(R.id.oldCustomerPhone)
        oldAddressPhone = viewD.findViewById(R.id.oldCustomerAddressPhone)

        note = viewD.findViewById(R.id.note)

        ok = viewD.findViewById(R.id.ok)


        addressId = viewD.findViewById(R.id.addressId)
        addressName = viewD.findViewById(R.id.customerAddressName)
        addressPhone = viewD.findViewById(R.id.customerAddressPhone)
        address = viewD.findViewById(R.id.address)

        warning = viewD.findViewById(R.id.warning)

        checkChanges()

        setSpinner(0)

        edit(tempInvoiceId, realInvoiceId)
        populateDraft()

        ok.setOnClickListener {

            ok.isEnabled = false
            val handler = Handler()
            handler.postDelayed({
                ok.isEnabled = true
            },1000)

            val selectedStatus = option.selectedItem.toString()

            if (selectedStatus == "Draft")
            {
                saveInvoice(0)
            }
            else if (selectedStatus == "Simpan")
            {
                saveInvoice(1)
            }
            else if (selectedStatus == "Kirim")
            {
                saveInvoice(2)
            }
            else
            {
                saveInvoice(1000)
            }
        }

        barcodeList = DraftHelper(context).getDraftBarcode(pageNameCashier)
        val additionFeeAmount = PurchaseDetailHelper(context).getAdditonalFee(barcodeList)
        val totalPrice  = PurchaseDetailHelper(context).getTotalPriceItem(additionFeeAmount, barcodeList)

        additionalFee.text = PaymentHelper().amountToReadable(additionFeeAmount)
        totalPriceItem.text = PaymentHelper().amountToReadable(totalPrice)

        PurchaseDetailHelper(context).getTotalBilling(viewD)
        shippingCharge.addTextChangedListener(ShippingChargeWatcher(context, viewD))

        val actv = viewD.findViewById(R.id.customerName) as AutoCompleteTextView

        // set our adapter
        val myAdapter = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, item)
        actv.setAdapter(myAdapter)

        actv.threshold = 1
        actv.addTextChangedListener(CustomerAutoCompleteWatcher(context, viewD, myAdapter, item))
        actv.onItemClickListener = CustomerAutoComplete(context, viewD)

        val customerAddress = viewD.findViewById(R.id.customerAddressName) as AutoCompleteTextView
        customerAddress.threshold = 1
        customerAddress.addTextChangedListener(CustomerAddressAutoCompleteWatcher(context, viewD))
        customerAddress.onItemClickListener = CustomerAddressAutoComplete(context, viewD)

        return viewD
    }

    fun saveInvoice(status: Int)
    {
        val additionalFeeForm = viewD.findViewById<TextView>(R.id.additionalFee)
        val totalPriceItemForm = viewD.findViewById<TextView>(R.id.totalItem)
        val shippingFeeForm = viewD.findViewById<EditText>(R.id.shippingFee)
        val totalBillingForm = viewD.findViewById<TextView>(R.id.totalBilling)

        val customerId2 = customerId.text.toString().toInt()
        var customerAddressId2 = addressId.text.toString().toInt()
        val customerName2 = customerName.text.toString().trim()
        val customerPhone2 = customerPhone.text.toString().trim()
        val note2 = note.text.toString().trim()

        val customerAddressName2 = addressName.text.toString().trim()
        val customerAddressPhone2 = addressPhone.text.toString().trim()
        val customerAddress2 = address.text.toString().trim()

        var shippingFee = PaymentHelper().readableAmountToInt(shippingFeeForm.text.toString())
        val additionalFee = PaymentHelper().readableAmountToInt(additionalFeeForm.text.toString())
        var totalBilling = PaymentHelper().readableAmountToInt(totalBillingForm.text.toString())

        var checkCustomer = true
        var errorMessage = ""

        if (customerName2 == "" || customerPhone2 == "")
        {
            checkCustomer = false
            errorMessage = "nama dan telp customer wajib"
        }

        var checkAddress = true
        if (customerAddressName2 != "" || customerAddressPhone2 != "" || customerAddress2 != "")
        {
            if (customerAddressName2 == "" || customerAddressPhone2 == "" || customerAddress2 == "")
            {
                checkAddress = false
                errorMessage = "Nama alamat, no.telp alamat dan alamat wajib di isi"
            }
        }

        if (!checkCustomer || !checkAddress) {

            AlertHelper(context).PurchaseDetailDataChecker(errorMessage)

        } else {

            Log.d("check_bug", "saved real inv id: " + realInvoiceId.toString())

            val saveData = InvoiceToSync(context).insert(
                    tempInvoiceId,
                    realInvoiceId,
                    User(context).getLoggedInUserId(),
                    User(context).getLoggedInUserStore()!!,
                    customerId2,
                    customerName2,
                    customerPhone2,
                    customerAddressId2,
                    customerAddressName2,
                    customerAddressPhone2,
                    customerAddress2,
                    note2,
                    status,
                    shippingFee.toDouble(),
                    totalBilling.toDouble(),
                    additionalFee,
                    barcodeList)

            if (saveData)
            {
                InvoiceToSync(context).updateCustomerByPhone(
                        oldCustomerPhone.text.toString(),
                        customerName2,
                        customerPhone2
                )

                InvoiceToSync(context).updateCustomerAddressByPhone(
                        oldAddressPhone.text.toString(),
                        customerAddressName2,
                        customerAddressPhone2,
                        customerAddress2
                )

                PaymentToSync(context).updateCustomerByPhone(
                        oldCustomerPhone.text.toString(),
                        customerPhone2
                )


                if (tempInvoiceId > 0)
                {
                    InvoiceToSync(context).removeInvoiceToSync(tempInvoiceId)
                }

                UploadInvoiceSynchronizer(this.context).syncNow()

                customerId.text = "0"
                customerName.setText("")
                customerPhone.setText("")
                oldCustomerPhone.setText("")
                note.setText("")

                addressId.text = "0"
                addressName.setText("")
                addressPhone.setText("")
                oldAddressPhone.setText("")
                address.setText("")

                additionalFeeForm.text = "0"
                totalPriceItemForm.text = "0"
                shippingFeeForm.setText("0")
                totalBillingForm.text = "0"

                barcodeList.removeAll(barcodeList)

                DraftHelper(context).removeDraftBarcode(pageNameCashier)

                Locker(context).onInvoiceToSave(oldCustomerPhone.text.toString())

                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment2 = InvoiceList()
                fragmentTransaction.replace(R.id.container, fragment2, "invoice_list_fragment")
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

        }
    }


    override fun onPause() {
        super.onPause()
        isFragmentActive = false
    }

    override fun onResume() {
        super.onResume()
        isFragmentActive = true
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PurchaseDetail.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): PurchaseDetail {
            val fragment = PurchaseDetail()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
