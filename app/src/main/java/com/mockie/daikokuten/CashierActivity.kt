package com.mockie.daikokuten

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.helpers.AlertHelper
import kotlinx.android.synthetic.main.activity_cashier.*
import kotlinx.android.synthetic.main.app_bar_new_main.*

class CashierActivity :
        BaseNavigationDrawer(),
        CashierFragment.OnFragmentInteractionListener,
        InvoiceList.OnFragmentInteractionListener,
        PurchaseDetail.OnFragmentInteractionListener,
        BarcodeForm.OnFragmentInteractionListener,
        PaymentFragment.OnFragmentInteractionListener,
        Return.OnFragmentInteractionListener,
        SearchInvoiceFragment.OnFragmentInteractionListener
{

    override var contentV = R.layout.activity_new_cashier

    override fun onFragmentInteraction(uri: Uri) {

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.inventory_navigation_home -> {
                startActivity(Intent(this, NewMainActivity::class.java))
                finish()
                return@OnNavigationItemSelectedListener true
            }
            R.id.add_invoice -> {

                val bdl = Bundle(2)
                bdl.putInt("invoiceId", 0)
                bdl.putBoolean("unsync", true)

                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment = CashierFragment()
                fragment.arguments = bdl
                fragmentTransaction.replace(R.id.container, fragment, "cashier_fragment")
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.invoice_list -> {
                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()

                val fragment = InvoiceList()
                fragmentTransaction.replace(R.id.container, fragment, "invoice_list_fragment")
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.payment_page -> {
                val fragmentManager = supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()

                val bdl = Bundle(3)
                bdl.putInt("customerIdGet", 0) // invoice id from invoice_to_sync table or from invoice table
                bdl.putString("customerNameGet", "")
                bdl.putString("customerPhoneGet", "")

                val fragment = PaymentFragment()
                fragment.arguments = bdl
                fragmentTransaction.replace(R.id.container, fragment, "payment_fragment")
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        InvoiceToSync(applicationContext).unlockAllData()

        AlertHelper(this).alertLocation()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (intent.extras != null)
        {
            val intentFragment = intent.extras!!.getString("go_to_fragment")

            when (intentFragment) {

                "add_invoice" -> {
                    navigation.selectedItemId = R.id.add_invoice
                }

                "invoice_list" -> {
                    navigation.selectedItemId = R.id.invoice_list
                }

                "cashier" -> {
                    navigation.selectedItemId = R.id.payment_page
                }

            }
        }
    }
}
