package com.mockie.daikokuten.models

import java.math.BigDecimal
import java.text.DecimalFormat

class InvoiceListData(
        val tempInvoiceId: Int,
        val realInvoiceId: Int,
        val customerName:String?,
        val customerAddressName:String?,
        val customerAddress:String?,
        val total:Double,
        val status:Int,
        val createdDate: String) {

    companion object {

        private val invoiceList = ArrayList<InvoiceListData>()

        fun initializeBarcode():ArrayList<InvoiceListData>
        {
            invoiceList.removeAll(invoiceList)
            return invoiceList
        }

        fun getItem(position: Int): InvoiceListData
        {
            return invoiceList[position]
        }

        fun add(tempInvoiceId: Int, realInvoiceId: Int, customerName:String, customerAddressName: String, customerAddress:String, total:Double, status:Int, createdDate: String): ArrayList<InvoiceListData>
        {
            val contactModel = InvoiceListData(tempInvoiceId, realInvoiceId,  customerName, customerAddressName, customerAddress,  total, status, createdDate)

            invoiceList.add(contactModel)

            return invoiceList
        }
    }
    
}