package com.mockie.daikokuten.models

/**
 * Created by mockie on 30/01/18.
 */

class BarcodeListInvoice(var barcode: String, var qty: Any, var meta:String, var price:Int, val retur:Int, var focus: String?) {

    companion object {

        private val barcodeList = ArrayList<BarcodeListInvoice>()

        fun initializeBarcode():ArrayList<BarcodeListInvoice>
        {
            barcodeList.removeAll(barcodeList)
            return barcodeList
        }

        fun getAllBarcode():ArrayList<BarcodeListInvoice>
        {
            return barcodeList
        }

        fun getItem(position: Int): BarcodeListInvoice?
        {
            try {
                return barcodeList[position]
            } catch (e:Exception) {}

            return null
        }

        fun add(barcode: String, qty: Int, meta: String, price:Int, retur:Int, focus: String?): ArrayList<BarcodeListInvoice>
        {
            val contactModel = BarcodeListInvoice(barcode, qty, meta, price, retur, focus)

            barcodeList.add(contactModel)

            return barcodeList
        }
    }
}