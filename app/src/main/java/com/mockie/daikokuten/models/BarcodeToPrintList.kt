package com.mockie.daikokuten.models

/**
 * Created by mockie on 30/01/18.
 */

class BarcodeToPrintList(var id: Int, var barcode: String, var meta:String, var size:String, var qty: Int, var checked: Boolean) {

    companion object {

        private val barcodeList = ArrayList<BarcodeToPrintList>()

        fun getItem(position: Int): BarcodeToPrintList
        {
            return barcodeList[position]
        }

        fun add(id: Int, barcode: String, meta: String, size:String, qty: Int, checked: Boolean): ArrayList<BarcodeToPrintList>
        {
            val contactModel = BarcodeToPrintList(id, barcode, meta, size, qty, checked)

            barcodeList.add(contactModel)

            return barcodeList
        }
    }
}