package com.mockie.daikokuten.models

import com.mockie.daikokuten.helpers.StringWithTag

/**
 * Created by mockie on 30/01/18.
 */

class PaymentMethod(var amount:String, var method: StringWithTag, var focus: String?) {

     companion object {

        private val paymentMethod = ArrayList<PaymentMethod>()

        fun initializePaymentMethod():ArrayList<PaymentMethod>
        {
            paymentMethod.removeAll(paymentMethod)
            return paymentMethod
        }

        fun getAllPaymentMethods():ArrayList<PaymentMethod>
        {
            return paymentMethod
        }

        fun getItem(position: Int): PaymentMethod?
        {
            try {
                return paymentMethod[position]
            } catch (e:Exception) {}

            return null
        }

        fun add(amount:String, method:StringWithTag, focus: String?): ArrayList<PaymentMethod>
        {
            paymentMethod.add(PaymentMethod(amount, method, focus))

            return paymentMethod
        }

         fun update(position: Int, amount: String, method: StringWithTag, focus: String?)
         {
             paymentMethod[position] = PaymentMethod(amount, method, focus)
         }

         fun remove(position: Int)
         {
             paymentMethod.removeAt(position)
         }
    }
}