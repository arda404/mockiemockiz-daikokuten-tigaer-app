package com.mockie.daikokuten.models

/**
 * Created by mockie on 30/01/18.
 */

class BarcodeList(var barcode: String, var qty: Any, var meta:String, var price:Int, var focus: String?) {

    var isBarcodeFocus:Boolean = false
    var isQtyFocus:Boolean = false

    companion object {

        private val barcodeList = ArrayList<BarcodeList>()

        fun initializeBarcode():ArrayList<BarcodeList>
        {
            barcodeList.removeAll(barcodeList)
            return barcodeList
        }

        fun getAllBarcode():ArrayList<BarcodeList>
        {
            return barcodeList
        }

        fun removeBarcode(position: Int)
        {
            barcodeList.removeAt(position)
        }

        fun getItem(position: Int): BarcodeList?
        {
            try {
                return barcodeList[position]
            } catch (e:Exception) {}

            return null
        }

        fun add(barcode: String, qty: Int, meta: String, price:Int, focus: String?): ArrayList<BarcodeList>
        {
//            for (i in 0 until barcodeList.count())
//            {
//                barcodeList[i].isBarcodeFocus = false
//                barcodeList[i].isQtyFocus = false
//            }

            val contactModel = BarcodeList(barcode, qty, meta, price, focus)

            barcodeList.add(contactModel)

            return barcodeList
        }
    }
}