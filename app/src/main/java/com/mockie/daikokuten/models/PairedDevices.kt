package com.mockie.daikokuten.models


class PairedDevices (val deviceName:String, val deviceAddress:String)
{
    override fun toString(): String {
        return deviceName
    }

    fun getId(): Any {
        return deviceAddress
    }
}