package com.mockie.daikokuten

import android.arch.lifecycle.Observer
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.mockie.daikokuten.adapters.RVInvoiceListAdapter
import com.mockie.daikokuten.database.repositories.BaseRepository
import com.mockie.daikokuten.database.repositories.InvoiceToSync
import com.mockie.daikokuten.database.repositories.Invoices
import com.mockie.daikokuten.database.repositories.SyncLog
import com.mockie.daikokuten.listeners.InvoiceListOnScrollListener
import com.mockie.daikokuten.models.BarcodeToPrintList
import com.mockie.daikokuten.models.InvoiceListData
import com.mockie.daikokuten.models.SearchKeyword
import com.mockie.daikokuten.sync.models.GetInvoiceModel


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [InvoiceList.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [InvoiceList.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class InvoiceList : Fragment() {

    lateinit var adapter: RVInvoiceListAdapter
    lateinit var barcode: ArrayList<BarcodeToPrintList>

    lateinit var invoiceList: ArrayList<InvoiceListData>


    var isFragmentActive = false

    var lastSyncInvoice:String? = null

    lateinit var keyword:SearchKeyword

    var checkRow = 0

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_invoice_list, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewInvoiceList)

        var searchInvoiceId = 0
        var searchCustomerId = 0
        var searchCustomerName = ""
        var searchCustomerPhone = ""
        var searchStatus = 2000

        try {

            val args = arguments
            searchInvoiceId = args.getInt("searchInvoiceId", 0)
            searchCustomerId = args.getInt("searchCustomerId", 0)
            searchCustomerName = args.getString("searchCustomerName", "")
            searchCustomerPhone = args.getString("searchCustomerPhone", "")
            searchStatus = args.getInt("searchStatus", 0)

        } catch (e:Exception) {}

        keyword = SearchKeyword(searchInvoiceId, searchCustomerId, searchCustomerName, searchCustomerPhone, searchStatus)

        invoiceList = InvoiceListData.initializeBarcode()

        val fab = view.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            onClickSearch()
        }


        adapter = RVInvoiceListAdapter(context, fragmentManager, recyclerView, invoiceList)
        recyclerView.adapter = adapter

        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = false
        recyclerView.layoutManager = layoutManager

        refreshData()

        WorkManager.getInstance().getWorkInfosByTagLiveData(GetInvoiceModel.PERIODIC_TAG)
                .observe(this, Observer { workStatus ->

                    if (workStatus != null) {
                        for (works in workStatus) {
                            if (works.state == WorkInfo.State.SUCCEEDED) {
                                refreshData()
                            }
                        }
                    }

                })


        val syncLog = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME)
        if (syncLog != null)
        {
            lastSyncInvoice = syncLog.last_sync
        }

        checkRow = InvoiceToSync(context).getTotalRow()

        isFragmentActive = true
        val handler = Handler()
        val runnableCode = object : Runnable {

            override fun run() {

                if (context != null)
                {
                    try {
                        val syncLog2 = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.Invoices.TABLE_NAME)
                        val checkRow2 = InvoiceToSync(context).getTotalRow()

                        if ((syncLog2 != null && syncLog2.last_sync != lastSyncInvoice) || (checkRow != checkRow2)) {
                            lastSyncInvoice = if (syncLog2!=null) syncLog2.last_sync else ""
                            checkRow = checkRow2
                            refreshData()
                        }
                    } catch (e:Exception) {}
                }

                if (isFragmentActive) {
                    handler.postDelayed(this, 4000)
                }
            }

        }

        handler.post(runnableCode)

        recyclerView.addOnScrollListener(InvoiceListOnScrollListener(context, layoutManager, adapter, invoiceList, keyword))

        return view
    }

    fun onClickSearch()
    {
        val someFragment = SearchInvoiceFragment()

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.container, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }

    fun refreshData()
    {
        invoiceList.removeAll(invoiceList)
        adapter.notifyDataSetChanged()

        val unsyncInvoices2 = InvoiceToSync(context).getInvoiceListData(keyword)

        for (a in 0 until unsyncInvoices2.size)
        {
            invoiceList.add(InvoiceListData(
                    unsyncInvoices2[a].invoice_id,
                    unsyncInvoices2[a].real_invoice_id,
                    unsyncInvoices2[a].customer_name,
                    unsyncInvoices2[a].address_name,
                    unsyncInvoices2[a].address,
                    unsyncInvoices2[a].total_billing,
                    unsyncInvoices2[a].status,
                    "UNSYNC"
            ))
        }

        val invoiceDataR = Invoices(context).getInvoiceListData(0, 10, keyword)

        for (a in 0 until invoiceDataR.size)
        {
            invoiceList.add(InvoiceListData(0,
                    invoiceDataR[a]._id,
                    invoiceDataR[a].customer_name,
                    invoiceDataR[a].address_name,
                    invoiceDataR[a].address,
                    invoiceDataR[a].total_billing,
                    invoiceDataR[a].status,
                    invoiceDataR[a].created_at
            ))
        }

        adapter.notifyDataSetChanged()
    }

    override fun onPause() {
        super.onPause()
        isFragmentActive = false
    }

    override fun onResume() {
        super.onResume()
        isFragmentActive = true
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InvoiceList.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                InvoiceList().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
