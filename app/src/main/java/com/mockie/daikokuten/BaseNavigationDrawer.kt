package com.mockie.daikokuten

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.mockie.daikokuten.database.repositories.User
import com.mockie.daikokuten.helpers.InternetCheck
import kotlinx.android.synthetic.main.activity_new_main.*
import kotlinx.android.synthetic.main.app_bar_new_main.*

open class BaseNavigationDrawer:AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {

    lateinit protected var drawerLayout:DrawerLayout

    var isFragmentActive = false

    open val contentV = R.layout.activity_new_main
    open val drawerL = R.id.drawer_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(contentV)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(drawerL)

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        val headerView = navigationView.getHeaderView(0)
        val userNameForm = headerView.findViewById<TextView>(R.id.userName)
        val userEmailForm = headerView.findViewById<TextView>(R.id.userStore)
        val userEnvForm = headerView.findViewById<TextView>(R.id.userEnvironment)

        val user = User(this).isLogin()

        if (user != null)
        {
            if(user.name!="")
            {
                userNameForm.text = user.name
            }

            userEmailForm.text = "Toko: "+ user.store
            userEnvForm.text = "Env: " + user.api_base_url
        }

        userNameForm.setTextColor(Color.RED)

        isFragmentActive = true
        val handler = Handler()
        val runnableCode = object : Runnable {

            override fun run() {

                try {
                    InternetCheck(object : InternetCheck.Consumer {
                        override fun accept(internet: Boolean?) {

                            if (internet == true) {
                                userNameForm.setTextColor(Color.GREEN)
                            } else {
                                userNameForm.setTextColor(Color.RED)
                            }

                        }
                    })

                    if (isFragmentActive)
                    {
                        handler.postDelayed(this, 3000)
                    }
                } catch (e:Exception) {}

            }

        }

        handler.post(runnableCode)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onPause() {
        super.onPause()

        isFragmentActive = false
    }

    override fun onResume() {
        super.onResume()

        isFragmentActive = true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.new_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_inventory -> {
                // Handle the camera action
                startActivity(Intent(this, InventoryActivity::class.java))
            }
            R.id.nav_cashier -> {
                startActivity(Intent(this, CashierActivity::class.java))
            }
//            R.id.nav_report -> {
//
//            }
            R.id.nav_setting -> {
                startActivity(Intent(this, SettingActivity::class.java))
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

}

