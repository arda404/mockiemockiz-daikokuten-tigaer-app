package com.mockie.daikokuten

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.work.WorkManager
import com.mockie.daikokuten.helpers.Locker
import com.mockie.daikokuten.sync.helpers.*
import com.mockie.daikokuten.sync.models.UploadInventoryModel

class NewMainActivity : BaseNavigationDrawer() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val addStockButton = findViewById<ImageView>(R.id.add_stock_icon)
        val stockTakingButton = findViewById<ImageView>(R.id.stock_taking_icon)
        val moveStockButton = findViewById<ImageView>(R.id.move_stock)
        val printBarcodeButton = findViewById<ImageView>(R.id.print_barcode_icon)

        val inventoryIntent = Intent(applicationContext, InventoryActivity::class.java)

        addStockButton.setOnClickListener {
            inventoryIntent.putExtra("go_to_fragment", "add_stock")

            startActivity(inventoryIntent)
            finish()
        }

        stockTakingButton.setOnClickListener {
            inventoryIntent.putExtra("go_to_fragment", "stock_taking")

            startActivity(inventoryIntent)
            finish()
        }

        moveStockButton.setOnClickListener {
            inventoryIntent.putExtra("go_to_fragment", "move_stock")

            startActivity(inventoryIntent)
            finish()
        }

        printBarcodeButton.setOnClickListener {
            inventoryIntent.putExtra("go_to_fragment", "print_barcode")

            startActivity(inventoryIntent)
            finish()
        }

        val addInvoiceButton = findViewById<ImageView>(R.id.add_invoice_icon)
        val invoiceListButton = findViewById<ImageView>(R.id.invoice_list_icon)
        val cashierButton = findViewById<ImageView>(R.id.cashier_icon)

        val cashierIntent = Intent(applicationContext, CashierActivity::class.java)

        addInvoiceButton.setOnClickListener {
            cashierIntent.putExtra("go_to_fragment", "add_invoice")

            startActivity(cashierIntent)
            finish()
        }

        invoiceListButton.setOnClickListener {
            cashierIntent.putExtra("go_to_fragment", "invoice_list")

            startActivity(cashierIntent)
            finish()
        }

        cashierButton.setOnClickListener {
            cashierIntent.putExtra("go_to_fragment", "cashier")

            startActivity(cashierIntent)
            finish()
        }

        Locker(this).onMainActivity()

        //["packer","moneymanager","purchaser","purchasehelper","stocktaker","developer","customersupport","usermanager","returnedrecorder","spectator","producteditor"]

        /**
         * Check User Token
         */
         CheckUserTokenSyncronizer(this).syncWithAsyncTask()

        /**
         *  Register Firebase token right away and set periodic sync
         */

        FirebaseTokenSyncronizer(this).setPeriodicSync((this as Activity))

        UploadInvoiceSynchronizer(this).setPeriodicSyncToUnlock()

        WorkManager.getInstance().pruneWork()
        WorkManager.getInstance().getWorkInfosByTagLiveData(UploadInventoryModel.ONE_TIME_TAG)
            .observe(this, Observer { workStatus ->

                if (workStatus != null) {
                    for (works in workStatus) {
                        Log.d(":sync_observer", "upload inventory : " + works.id.toString() + " | " +works.outputData + "|" + works.state.toString() + " | " +works.tags)
                    }
                }

            })


    }
}
