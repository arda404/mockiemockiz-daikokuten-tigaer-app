package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.view.inputmethod.BaseInputConnection
import android.widget.*
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.adapters.RecyclerViewStockTakingAdapter
import com.mockie.daikokuten.database.models.StockTaking as StockTakingModel
import com.mockie.daikokuten.database.repositories.StockTaking
import com.mockie.daikokuten.database.repositories.User
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.helpers.DraftHelper
import com.mockie.daikokuten.listeners.RecyclerItemTouchHelper
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.sync.helpers.StockTakingSynchronizer


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [StockTakingFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [StockTakingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StockTakingFragment : BarcodeRecyclerBaseFragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    val pageName:String = "stock_taking"

    val keyName:String = "barcode"

    var type = ""
    var size = ""
    var color = ""


    private lateinit var scope:List<String>

    lateinit var adapter: RecyclerViewStockTakingAdapter
    var currentStore: String? = null

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }


    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {

        if (viewHolder is RecyclerViewBarcodeAdapter.ViewHolder) {
            adapter.removeItem(viewHolder.adapterPosition)
            DraftHelper(context).draftBarcode(pageName, barcodeList)
            if (barcodeList.size < 1)
            {
                BarcodeList.add("", 10, "", 0,null)
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val inflaterView = inflater!!.inflate(R.layout.fragment_stock_taking, container, false)
        val recyclerView = inflaterView.findViewById<RecyclerView>(R.id.recyclerView)
        val saveButton = inflaterView.findViewById<Button>(R.id.saveStock)
        val scopeButton = inflaterView.findViewById<Button>(R.id.fab)
        val barcodeButton = inflaterView.findViewById<Button>(R.id.createBarcode)

        setPermissionsRoles("developer")
        setPermissionsRoles("stocktaker")
        setPermissionsRoles("producteditor")
        checkPermission()

        barcodeList = DraftHelper(context).getDraftBarcode(pageName)
        barcodeList.add(BarcodeList("", 1, "", 0,"barcode"))

        val args = arguments
        type = args.getString("type", "")
        size = args.getString("size", "")
        color = args.getString("color", "")
        scope = arrayListOf(type, size, color)

        adapter = RecyclerViewStockTakingAdapter(this.context, recyclerView, barcodeList, pageName, keyName, scope)
        recyclerView.adapter = adapter

        scopeButton.setOnClickListener {
            onClickSetting()
        }

        barcodeButton.setOnClickListener {

            val lastItem = barcodeList.get(barcodeList.lastIndex)
            var defaultVal = type + size

            if (color!="")
            {
                defaultVal += color.padStart(5, '0')
            }

            if (lastItem.barcode == "")
            {
                lastItem.barcode = defaultVal
                lastItem.focus = "barcode"
                adapter.notifyItemChanged(barcodeList.lastIndex)
            }
            else
            {
                barcodeList.add(BarcodeList(defaultVal, 1, "", 0, "barcode"))
                adapter.notifyDataSetChanged()
            }

        }

        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = false
        recyclerView.layoutManager = layoutManager

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        recyclerView.postDelayed({
            recyclerView.layoutManager.scrollToPosition(adapter.itemCount -1)
        }, 200)

        saveButton.setOnClickListener {
            saveStockDeviation()
        }

        return inflaterView
    }

    private fun onClickSetting()
    {
        // save draft
        DraftHelper(context).draftBarcode(pageName, barcodeList)

        val someFragment = StockTakingScopeFragment()

        val bdl = Bundle(2)
        someFragment.arguments = bdl

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.inventoryContainer, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }

    private fun saveStockDeviation()
    {
        currentStore = User(this.context).getLoggedInUserStore()

        var emptyList = false
        if(barcodeList.size == 1 && barcodeList[0].barcode == "" && barcodeList[0].qty.toString().toInt() < 1)
        {
            emptyList = true
        }

        if (!emptyList)
        {
            val dialog = AlertHelper(context).barcodeListConfirmation(adapter, barcodeList, pageName)
            val yes = dialog.findViewById<Button>(R.id.editButton)

            yes.setOnClickListener {

                dialog.hide()
                dialog.dismiss()

                for (data in barcodeList) {
                    StockTaking(context).save(data.barcode, currentStore!!, data.qty.toString().toInt())
                }

                for(a in 0 until barcodeList.size) {
                    adapter.removeItem(0)
                }

                DraftHelper(context).removeDraftBarcode(pageName)
                BarcodeList.add("", 10, "", 0,null)

                StockTakingSynchronizer(this.context).syncNow()
            }

        } else {
            AlertHelper(context).noItemFound()
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment StockTakingFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): StockTakingFragment {
            val fragment = StockTakingFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
