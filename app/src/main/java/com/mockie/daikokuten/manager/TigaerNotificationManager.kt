package com.mockie.daikokuten.manager

/**
 * Created by mockie on 20/02/18.
 */


import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import android.text.Html
import com.mockie.daikokuten.R
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import android.app.NotificationChannel
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import com.mockie.daikokuten.LoginActivity
import com.mockie.daikokuten.NewMainActivity
import com.mockie.daikokuten.helpers.BeepHelper
import java.util.*


class TigaerNotificationManager(val mCtx: Context) {

    val ID_BIG_NOTIFICATION: Int = 234

    val ID_SMALL_NOTIFICATION: Int = 235

    val ID_SYNC_NOTIFICATION: Int = 236

    val channelId:String = "my_channel_tigaer_01"

    fun channel()
    {
        val mNotificationManager = mCtx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

// The user-visible name of the channel.
        val name = "The user-visible name of the channel"

// The user-visible description of the channel.
        val description = "he user-visible description of the channel."

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

             val importance = NotificationManager.IMPORTANCE_LOW

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                val mChannel =NotificationChannel(channelId, name, importance)

                mChannel.description = description

                mChannel.enableLights(true)

                mChannel.lightColor = Color.RED

                mChannel.enableVibration(true)
                mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)

                mNotificationManager.createNotificationChannel(mChannel)
            }
        }
    }

    fun showBigNotification(title: String, message: String, url: String, intent: Intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel()
        }
        val resultPendingIntent: PendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_BIG_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                )

        //NotificationCompat.BigPictureStyle
        val bigPictureStyle: NotificationCompat.BigPictureStyle = NotificationCompat.BigPictureStyle()
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(getBitmapFromURL(url))

        val mBuilder:NotificationCompat.Builder = NotificationCompat.Builder(mCtx, channelId);
        val notification: Notification

        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setStyle(bigPictureStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mCtx.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .build();

        notification.flags = Notification.FLAG_AUTO_CANCEL

        val notificationManager:NotificationManager = mCtx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;
        notificationManager.notify(ID_BIG_NOTIFICATION, notification)
    }

    fun showSyncNotification(title: String)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel()
        }

        val mBuilder:NotificationCompat.Builder = NotificationCompat.Builder(mCtx, channelId)
        val  notification: Notification

        notification = mBuilder.setSmallIcon(R.drawable.tigaer).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.tigaer)
                .build()

        notification.flags = Notification.FLAG_AUTO_CANCEL

        BeepHelper().syncNotif(mCtx)

        val notificationManager:NotificationManager = mCtx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(ID_SYNC_NOTIFICATION, notification)

        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            notificationManager.cancel(ID_SYNC_NOTIFICATION)
        }, 10000)
    }

    //the method will show a small notification
    //parameters are title for message title, message for message text and an intent that will open
    //when you will tap on the notification

    fun showSmallNotification(title: String, message: String, intent:Intent) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel()
        }

        val resultPendingIntent:PendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                )


        val mBuilder:NotificationCompat.Builder = NotificationCompat.Builder(mCtx, channelId)
        val  notification: Notification

        notification = mBuilder.setSmallIcon(R.drawable.tigaer).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.tigaer)
                .setLargeIcon(BitmapFactory.decodeResource(mCtx.resources, R.drawable.tigaer))
                .setContentText(message)
                .build()

        notification.flags = Notification.FLAG_AUTO_CANCEL

        BeepHelper().syncNotif(mCtx)

        with(NotificationManagerCompat.from(mCtx)) {
            // notificationId is a unique int for each notification that you must define
            notify(ID_SMALL_NOTIFICATION, mBuilder.build())
        }
    }

    //The method will return Bitmap from an image URL
    private fun getBitmapFromURL(strURL: String):Bitmap? {
        try {
            val url = URL(strURL)
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input: InputStream = connection.inputStream
            val myBitmap: Bitmap = BitmapFactory.decodeStream(input)

            return myBitmap
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }
}