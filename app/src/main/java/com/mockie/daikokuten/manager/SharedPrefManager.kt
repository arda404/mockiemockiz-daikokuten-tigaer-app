package com.mockie.daikokuten.manager

/**
 * Created by mockie on 20/02/18.
 */

import android.content.Context


/**
 * Created by mockie on 21/12/17.
 */

class SharedPrefManager constructor(context: Context) {

    //this method will fetch the device token from shared preferences
    val deviceToken: String?
        get() {
            val sharedPreferences = mCtx?.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences?.getString(TAG_TOKEN, null)
        }

    init {
        mCtx = context
    }

    //this method will save the device token to shared preferences
    fun saveDeviceToken(token: String): Boolean {
        val sharedPreferences = mCtx?.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.putString(TAG_TOKEN, token)
        editor?.apply()
        return true
    }

    companion object {
        private val SHARED_PREF_NAME = "FCMSharedPref"
        private val TAG_TOKEN = "tagtoken"

        private var mInstance: SharedPrefManager? = null
        private var mCtx: Context? = null

        @Synchronized
        fun getInstance(context: Context): SharedPrefManager {
            if (mInstance == null) {
                mInstance = SharedPrefManager(context)
            }
            return mInstance as SharedPrefManager
        }
    }

}