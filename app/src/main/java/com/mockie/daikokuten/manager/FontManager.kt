package com.mockie.daikokuten.manager

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import android.view.ViewGroup





/**
 * Created by mockie on 25/02/18.
 */

object FontManager {

    val ROOT = "fonts/"
    val FONTAWESOME = ROOT + "font-awesome-free-solid-900.otf"

    fun getTypeface(context: Context, font: String): Typeface {
        return Typeface.createFromAsset(context.assets, font)
    }

    fun markAsIconContainer(v: View, typeface: Typeface) {
        if (v is ViewGroup) {
            val vg = v
            for (i in 0 until vg.childCount) {
                val child = vg.getChildAt(i)
                markAsIconContainer(child, typeface)
            }
        } else if (v is TextView) {
            (v).typeface = typeface
        }
    }

}