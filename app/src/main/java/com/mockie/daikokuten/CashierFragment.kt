package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.database.models.TheInvoice
import com.mockie.daikokuten.database.repositories.InvoiceItem
import com.mockie.daikokuten.database.repositories.InvoiceItemToSync
import com.mockie.daikokuten.database.repositories.Invoices
import com.mockie.daikokuten.helpers.AlertHelper
import com.mockie.daikokuten.helpers.BarcodeListHelper
import com.mockie.daikokuten.helpers.DraftHelper
import com.mockie.daikokuten.listeners.RecyclerItemTouchHelper
import com.mockie.daikokuten.models.BarcodeList


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CashierFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CashierFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CashierFragment : BarcodeRecyclerBaseFragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    lateinit var adapter: RecyclerViewBarcodeAdapter

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    lateinit var recyclerView: RecyclerView

    val pageName: String = "cashier"
    val keyName:String = "barcode"

    var realInvoiceId:Int = 0
    var tempInvoiceId:Int = 0

    private var mListener: OnFragmentInteractionListener? = null


    var isFragmentActive = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is RecyclerViewBarcodeAdapter.ViewHolder) {
            adapter.removeItem(viewHolder.adapterPosition)

            DraftHelper(context).draftBarcode(pageName, barcodeList)

            if (barcodeList.size < 1)
            {
                BarcodeList.add("", 10, "", 0, "barcode")
            }
        }
    }

    override fun setMenuVisibility(visible: Boolean) {
        Log.d("visibility", visible.toString())
        if (!visible) {
            AlertHelper(context).noOrderFound()
        }

        super.setMenuVisibility(visible)
    }

    fun edit(tempInvoiceId:Int, realInvoiceId:Int)
    {
        if (tempInvoiceId > 0 || realInvoiceId > 0)
        {
            barcodeList = BarcodeList.initializeBarcode()
            DraftHelper(context).removeDraftBarcode(pageName)

            val theInvoice = TheInvoice(context, tempInvoiceId, realInvoiceId)

            if (theInvoice.status == 2)
            {
                AlertHelper(context).permissionDenied()
            }

            for (inv in theInvoice._getItems())
            {
                val barcodeHelper = BarcodeListHelper(context)
                val type = barcodeHelper.getTypeReadable(inv.code)
                val size = barcodeHelper.getSizeReadable(inv.code)
                val price = barcodeHelper.getPrice(inv.code)

                barcodeList.add(BarcodeList(inv.code, inv.quantity, "$type, $size, $price", price, ""))
            }

            DraftHelper(context).draftBarcode(pageName, barcodeList)
        }
    }

    fun checkChanges()
    {
        var updatedAt: String
        if (realInvoiceId > 0){

            val checkData = Invoices(context).getInvoiceById(realInvoiceId)
            updatedAt = checkData?.updated_at ?: ""

            isFragmentActive = true
            val handler = Handler()

            val runnableCode = object : Runnable {

                override fun run() {

                    if (context != null) {

                        val checkData2 = Invoices(context).getInvoiceById(realInvoiceId)
                        val updatedAt2 = checkData2?.updated_at ?: ""

                        if (updatedAt != updatedAt2)
                        {
                            updatedAt = updatedAt2
                            val alert =  AlertHelper(context).itemHasChanged()

                            alert.first.setOnClickListener {

                                alert.second.hide()
                                alert.second.dismiss()

                                try{

                                    val bdl = Bundle(2)
                                    bdl.putInt("realInvoiceId", realInvoiceId)
                                    bdl.putInt("tempInvoiceId", tempInvoiceId)

                                    val fragmentTransaction = fragmentManager.beginTransaction()

                                    val fragment = CashierFragment()
                                    fragment.arguments = bdl
                                    fragmentTransaction.replace(R.id.container, fragment, "cashier_fragment")
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                } catch (e:Exception) {}

                            }
                        }

                    }

                    if (isFragmentActive) {
                        handler.postDelayed(this, 4000)
                    }
                }

            }

            handler.post(runnableCode)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val inflaterView = inflater!!.inflate(R.layout.fragment_cashier, container, false)
        recyclerView = inflaterView.findViewById(R.id.recyclerView)

        val checkout = inflaterView.findViewById<Button>(R.id.checkout)

        setPermissionsRoles("purchaser")
        setPermissionsRoles("developer")
        setPermissionsRoles("purchasehelper")
        checkPermission()

        checkout.setOnClickListener {
            onCheckout()
        }

        try {

            val args = arguments
            realInvoiceId = args.getInt("realInvoiceId", 0)
            tempInvoiceId = args.getInt("tempInvoiceId", 0)

        } catch (e:Exception) { }
        Log.d("check_bug cash", pageName)
        Log.d("check_bug", "realInvoiceId cashier" + realInvoiceId)
        Log.d("check_bug", "tempInvoiceId cashier" + tempInvoiceId)

        edit(tempInvoiceId, realInvoiceId)
        checkChanges()

        barcodeList = DraftHelper(context).getDraftBarcode(pageName)
        barcodeList.add(BarcodeList("", 1, "",  0,"barcode"))

        adapter = RecyclerViewBarcodeAdapter(this.context, recyclerView, barcodeList, pageName, keyName)
        recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = false
        recyclerView.layoutManager = layoutManager

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        recyclerView.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom < oldBottom) {
                recyclerView.layoutManager.scrollToPosition(
                        barcodeList.size -1)
            }
        }

        val fab = inflaterView.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            onClickAdd()
        }

        return inflaterView
    }

    private fun onCheckout()
    {
        var emptyList = true
        var qty = 0

        for (a in 0 until barcodeList.size)
        {
            if (barcodeList[a].barcode != "")
            {
                emptyList = false
            }
            qty += barcodeList[a].qty.toString().toInt()
        }

        if (emptyList || qty == 0)
        {
            AlertHelper(context).noOrderFound()
        } else {
            val dialog = AlertHelper(context).barcodeListConfirmation(adapter, barcodeList, pageName)
            val yes = dialog.findViewById<Button>(R.id.editButton)

            yes.setOnClickListener {

                dialog.hide()
                dialog.dismiss()

                val bdl = Bundle(5)
                bdl.putString("pageNameCashier", pageName)
                bdl.putInt("realInvoiceId", realInvoiceId)
                bdl.putInt("tempInvoiceId", tempInvoiceId)

                val someFragment = PurchaseDetail()
                someFragment.arguments = bdl

                val transaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.container, someFragment) // give your fragment container id in first parameter
                transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
                transaction.commit()
            }
        }
    }

    private fun onClickAdd()
    {
        // save draft
        DraftHelper(context).draftBarcode(pageName, barcodeList)

        val someFragment = BarcodeForm()
        val bdl = Bundle(3)
        bdl.putString("pageName", pageName)
        bdl.putInt("realInvoiceId", realInvoiceId)
        bdl.putInt("tempInvoiceId", tempInvoiceId)

        someFragment.arguments = bdl

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.container, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }


    override fun onPause() {
        super.onPause()
        isFragmentActive = false
    }

    override fun onResume() {
        super.onResume()
        isFragmentActive = true
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CashierFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): CashierFragment {
            val fragment = CashierFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}
