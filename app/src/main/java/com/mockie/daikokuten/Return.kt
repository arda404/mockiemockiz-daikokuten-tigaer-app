package com.mockie.daikokuten

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.mockie.daikokuten.adapters.RecyclerViewBarcodeAdapter
import com.mockie.daikokuten.adapters.RecyclerViewReturnAdapter
import com.mockie.daikokuten.database.models.TheInvoice
import com.mockie.daikokuten.database.repositories.ReturnToSync
import com.mockie.daikokuten.database.repositories.User
import com.mockie.daikokuten.helpers.*
import com.mockie.daikokuten.listeners.RecyclerItemTouchHelper
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.models.ItemInBasket
import com.mockie.daikokuten.sync.helpers.UploadReturnSynchronizer

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Return.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Return.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Return : BarcodeRecyclerBaseFragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    val pageName: String = "return"
    val keyName:String = "barcode"

    private var tempInvoiceGet = 0
    private var realInvoiceIdGET = 0

    lateinit var customerName: TextView
    lateinit var invoiceId: TextView
    lateinit var invoiceDate:TextView
    lateinit var totalRetur:TextView

    lateinit var itemInBasket:ArrayList<ItemInBasket>

    lateinit var save: Button

    private lateinit var theInvoice:TheInvoice

    lateinit var adapter: RecyclerViewReturnAdapter
    lateinit var recyclerView: RecyclerView

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is RecyclerViewBarcodeAdapter.ViewHolder) {
            adapter.removeItem(viewHolder.adapterPosition)

            DraftHelper(context).draftBarcode(pageName, barcodeList)

            if (barcodeList.size < 1)
            {
                BarcodeList.add("", 10, "", 0, "barcode")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val inflaterView = inflater.inflate(R.layout.fragment_return, container, false)
        recyclerView = inflaterView.findViewById(R.id.recyclerView)
        customerName = inflaterView.findViewById(R.id.customerName)
        invoiceId = inflaterView.findViewById(R.id.invoiceId)
        invoiceDate = inflaterView.findViewById(R.id.invoiceDate)
        totalRetur = inflaterView.findViewById(R.id.totalRetur)

        save = inflaterView.findViewById(R.id.save)

        try{

            val args = arguments
            tempInvoiceGet = args.getInt("tempInvoiceIdGet", 0)
            realInvoiceIdGET = args.getInt("realInvoiceIdGet", 0)

        } catch (e:Exception) {}

        theInvoice = TheInvoice(context, tempInvoiceGet, realInvoiceIdGET)

        val invt = realInvoiceIdGET.toString() + " (" + PaymentHelper().amountToReadable(theInvoice.totalBilling)  +")"
        invoiceId.text = invt
        invoiceDate.text = theInvoice.createdAt
        customerName.text = theInvoice.customerName + " (" + theInvoice.customerPhone + ")"

        barcodeList = BarcodeList.initializeBarcode()
        barcodeList.add(BarcodeList("", 1, "",  0,"barcode"))

        itemInBasket = ReturnHelper(context).getItemInBasket(tempInvoiceGet, realInvoiceIdGET)

        adapter = RecyclerViewReturnAdapter(this.context, recyclerView, barcodeList, pageName, keyName, itemInBasket, theInvoice.additionalPrice, totalRetur)
        recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = false
        recyclerView.layoutManager = layoutManager

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        val fab = inflaterView.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            onClickAdd()
        }

        save.setOnClickListener {
            onSave()
        }

        return inflaterView
    }

    private fun onSave()
    {
        val dialog = AlertHelper(context).barcodeReturnConfirmation(barcodeList, itemInBasket)

        val yesBut = dialog.findViewById<Button>(R.id.editButton) // ini yes button salah nama

        yesBut.setOnClickListener {

            for (a in 0 until barcodeList.size)
            {
                ReturnToSync(context).insert(
                            User(context).getLoggedInUserId(),
                            User(context).getLoggedInUserStore()!!,
                            theInvoice.customerId,
                            theInvoice.customerPhone!!,
                            tempInvoiceGet,
                            realInvoiceIdGET,
                            barcodeList[a].barcode,
                            barcodeList[a].qty.toString().toInt(),
                            ((barcodeList[a].price + theInvoice.additionalPrice) * barcodeList[a].qty.toString().toInt())
                        )
            }

            barcodeList.removeAll(barcodeList)

            adapter.notifyDataSetChanged()

            UploadReturnSynchronizer(context).syncNow()

            Locker(context).onReturnToSave(theInvoice.customerPhone!!)

            dialog.hide()
            dialog.dismiss()

            onClickInvoiceList()
        }
    }

    private fun onClickInvoiceList()
    {
        val someFragment = InvoiceList()

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.container, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }

    private fun onClickAdd()
    {
        // save draft
        DraftHelper(context).draftBarcode(pageName, barcodeList)

        val someFragment = BarcodeForm()
        val bdl = Bundle(7)
        bdl.putString("pageName", pageName)
        bdl.putInt("invoiceId", tempInvoiceGet) // invoice id from invoice_to_sync table or from invoice table
        bdl.putInt("realInvoiceId", realInvoiceIdGET) // invoice id from invoice_to_sync table or from invoice table

        someFragment.arguments = bdl

        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.container, someFragment) // give your fragment container id in first parameter
        transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
        transaction.commit()
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Return.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                Return().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
