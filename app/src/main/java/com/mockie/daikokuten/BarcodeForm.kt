package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mockie.daikokuten.database.repositories.PrdCategories
import com.mockie.daikokuten.database.repositories.PrdPrices
import com.mockie.daikokuten.helpers.BarcodeGenerator
import com.mockie.daikokuten.helpers.DraftHelper
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.listeners.CategoryOnSelectedListener
import com.mockie.daikokuten.listeners.TypeOnSelectedListener
import com.mockie.daikokuten.models.BarcodeList
import kotlinx.android.synthetic.main.fragment_purchase_detail.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [BarcodeForm.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [BarcodeForm.newInstance] factory method to
 * create an instance of this fragment.
 */
class BarcodeForm : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    lateinit var categorySpinner: Spinner
    lateinit var typeSpinner: Spinner
    lateinit var sizeSpinner: Spinner
    lateinit var codeType: EditText
    lateinit var versionType: EditText
    lateinit var qtyText: EditText
    lateinit var typeRaw: StringWithTag
    lateinit var sizeRaw: StringWithTag
    lateinit var type: Any
    lateinit var size: Any
    lateinit var code: String
    lateinit var version: String
    lateinit var save: Button

    lateinit var pageName: String

    var realInvoiceId:Int = 0
    var tempInvoiceId:Int = 0

    var customerId = 0
    var customerName = ""
    var customerPhone = ""

    var qty: Int = 0

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view:View = inflater!!.inflate(R.layout.fragment_barcode_form, container, false)
        categorySpinner = view.findViewById(R.id.categorySpinner)
        sizeSpinner = view.findViewById(R.id.sizeSpinner)
        typeSpinner = view.findViewById(R.id.typeSpinner)
        save = view.findViewById(R.id.save)
        qtyText = view.findViewById(R.id.quantityText)
        codeType = view.findViewById(R.id.codeText)
        versionType = view.findViewById(R.id.versiText)

        val args = arguments
        pageName = args.getString("pageName", "")
        realInvoiceId = args.getInt("realInvoiceId", 0)
        tempInvoiceId = args.getInt("tempInvoiceId", 0)

        Log.d("check_bug", "pageName barcode form: " + pageName)
        Log.d("check_bug", "realInvoiceId checkout: " + realInvoiceId)
        Log.d("check_bug", "tempInvoiceId checkout: " + tempInvoiceId)

        typeSpinner.onItemSelectedListener = TypeOnSelectedListener(activity, R.layout.spinner_item, -1)

        populateSpinnerCategory()

        val fab = view.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { _ ->
            goToList()
        }

        save.setOnClickListener {
            save.isEnabled = false
            onCLickSave()
            save.postDelayed({
                save.isEnabled = true
            }, 500)
        }

        return view
    }

    fun goToCashier()
    {
        val bdl = Bundle(2)
        bdl.putInt("tempInvoiceId", tempInvoiceId) // invoice id from invoice_to_sync table or from invoice table
        bdl.putInt("realInvoiceId", realInvoiceId)

        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragment = CashierFragment()
        fragment.arguments = bdl
        fragmentTransaction.replace(R.id.container, fragment, "cashier_fragment")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun goToReturn()
    {
        val bdl = Bundle(2)
        bdl.putInt("tempInvoiceId", tempInvoiceId)
        bdl.putInt("realInvoiceIdGet", realInvoiceId)

        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragment2 = Return()
        fragment2.arguments = bdl
        fragmentTransaction.replace(R.id.container, fragment2, "return_fragment")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun goToAddStock()
    {
        val bdl = Bundle(2)
        bdl.putString("pageName", pageName)

        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragment2 = StockerFragment()
        fragment2.arguments = bdl
        fragmentTransaction.replace(R.id.inventoryContainer, fragment2, "stocker_fragment")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun goToMoveStock()
    {
        val bdl = Bundle(2)
        bdl.putString("pageName", pageName)

        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragment2 = MovingStock()
        fragment2.arguments = bdl
        fragmentTransaction.replace(R.id.inventoryContainer, fragment2, "move_stock_fragment")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun goToList()
    {
        if (pageName == "cashier")
        {
            goToCashier()
        }
        else if (pageName == "stocker")
        {
            goToAddStock()
        }
        else if (pageName == "return")
        {
            goToReturn()
        }
        else if (pageName == "move_stock")
        {
            goToMoveStock()
        }
    }

    private fun onCLickSave()
    {
        typeRaw = typeSpinner.getItemAtPosition(typeSpinner.selectedItemPosition) as StringWithTag
        sizeRaw = sizeSpinner.getItemAtPosition(sizeSpinner.selectedItemPosition) as StringWithTag
        code = codeType.text.toString().trim()
        version = versionType.text.toString().trim()
        qty = qtyText.text.toString().toInt()
        type = typeRaw.tag
        size = sizeRaw.tag

        if(code == "" || version == "" || qty < 1) {
            Toast.makeText(context, "Kode, Versi, qty wajib di isi ", Toast.LENGTH_LONG).show()
        } else {

            val draft = DraftHelper(context).getDraftBarcode(pageName)

            val barcodeGenerator = BarcodeGenerator(context,type.toString(),size.toString(),code,version)
            val priceCode = barcodeGenerator.getBarcode().substring(0, 5)
            val price = PrdPrices(context).getPriceByCode(priceCode)
            val meta = typeRaw.toString() + ", " + sizeRaw.toString() + ", " + price?.price

            var matches = false
            for (a in 0 until draft.size)
            {
                if (draft[a].barcode==barcodeGenerator.getBarcode())
                {
                    matches = true
                    draft[a].qty = draft[a].qty.toString().toInt() + qty
                    break
                }
            }

            if (!matches)
            {
                draft.add(BarcodeList(barcodeGenerator.getBarcode(), qty, meta, price!!.price,null))
            }

            DraftHelper(context).draftBarcode(pageName, draft)
        }

        val handler = Handler()
        handler.postDelayed({
            save.isEnabled = true
        }, 500)

        goToList()
    }

    private fun populateSpinnerCategory()
    {
        val types = PrdCategories(activity).getAllCategories()
        val listSpinner = ArrayList<StringWithTag>()

        (0 until types.count()).mapTo(listSpinner) {
            StringWithTag(types[it].readable, types[it].category)
        }

        val adapter = ArrayAdapter(activity,
                R.layout.spinner_item, listSpinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        categorySpinner.adapter = adapter
        categorySpinner.onItemSelectedListener = CategoryOnSelectedListener(activity, R.layout.spinner_item)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BarcodeForm.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): BarcodeForm {
            val fragment = BarcodeForm()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
