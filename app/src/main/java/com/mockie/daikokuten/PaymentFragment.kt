package com.mockie.daikokuten

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import com.mockie.daikokuten.adapters.RVPaymentMethodAdapter
import com.mockie.daikokuten.database.repositories.*
import com.mockie.daikokuten.helpers.*
import com.mockie.daikokuten.helpers.Customer
import com.mockie.daikokuten.listeners.PaymentRecyclerItemTouchHelper
import com.mockie.daikokuten.models.PaymentMethod
import com.mockie.daikokuten.sync.helpers.UploadPaymentSynchronizer
import kotlin.collections.ArrayList


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PaymentFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PaymentFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PaymentFragment : Fragment(), PaymentRecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    var isFragmentActive = false

    private lateinit var mView: View
    lateinit var recyclerView: RecyclerView
    lateinit var adapter: RVPaymentMethodAdapter
    lateinit var paymentMethod:ArrayList<PaymentMethod>

    lateinit var customerName:TextView
    lateinit var customerPhone:TextView
    lateinit var customerId:TextView
    lateinit var change:CheckBox

    lateinit var balance:TextView
    lateinit var total:TextView
    lateinit var kurang:TextView
    lateinit var debt:TextView
    lateinit var save: Button

    var customerIdGET = 0
    var customerNameGET = ""
    var customerPhoneGET = ""

    var triggerUpdate = false

    var invoiceId = 0

    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is RVPaymentMethodAdapter.ViewHolder) {
            adapter.removeItem(viewHolder.adapterPosition)
//            DraftHelper(context).draftBarcode(pageName, barcodeList)
            val theDebt = PaymentHelper().readableAmountToInt(debt.text.toString())
            PaymentHelper().getChange(paymentMethod, theDebt)

            if (paymentMethod.size < 1)
            {
                paymentMethod.add(PaymentMethod("0", StringWithTag("", "0"), ""))
            }
        }
    }

    private fun checkChange()
    {
        if (triggerUpdate && context!= null)
        {
            isFragmentActive = true
            val handler = Handler()

            val runnableCode = object : Runnable {

                override fun run() {

                    if (context!=null)
                    {
                        Log.d("triggerup", triggerUpdate.toString())

                        val syncInfo = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.Customer.TABLE_NAME)
                        val oldLastSync = if (syncInfo != null) syncInfo.last_sync else ""

                        if (balance.text.toString() != "" && debt.text.toString() != "")
                        {
                            if (context != null) {

                                val syncInfo2 = SyncLog(context).getTableInformation(com.mockie.daikokuten.database.models.Customer.TABLE_NAME)
                                val newLastSync = if (syncInfo2 != null) syncInfo2.last_sync else ""

                                if (oldLastSync != newLastSync)
                                {
                                    triggerUpdate = false
                                    isFragmentActive = false

                                    val alert =  AlertHelper(context).itemHasChanged()

                                    alert.first.setOnClickListener {

                                        alert.second.hide()
                                        alert.second.dismiss()

                                        try{

                                            val bdl = Bundle(2)
                                            bdl.putInt("customerIdGet", customerIdGET) // invoice id from invoice_to_sync table or from invoice table
                                            bdl.putString("customerNameGet", customerNameGET) // invoice id from invoice_to_sync table or from invoice table
                                            bdl.putString("customerPhoneGet", customerPhoneGET)

                                            val fragmentTransaction = fragmentManager.beginTransaction()

                                            val fragment2 = PaymentFragment()
                                            fragment2.arguments = bdl
                                            fragmentTransaction.replace(R.id.container, fragment2, "payment_fragment")
                                            fragmentTransaction.addToBackStack(null)
                                            fragmentTransaction.commit()

                                        } catch (e:Exception) {}

                                    }

                                }

                            }
                        }

                        if (isFragmentActive) {
                            handler.postDelayed(this, 3000)
                        }
                    }

                }
            }
            handler.post(runnableCode)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_payment, container, false)
        recyclerView = mView.findViewById(R.id.recyclerViewPayment)
        total = mView.findViewById(R.id.customerBalance)
        kurang = mView.findViewById(R.id.kurang)
        debt = mView.findViewById(R.id.customerDebt)
        change = mView.findViewById(R.id.change)
        balance = mView.findViewById(R.id.customerBalance)

        customerName = mView.findViewById(R.id.customerName)
        customerPhone = mView.findViewById(R.id.customerPhone)
        customerId = mView.findViewById(R.id.customerId)

        change.isEnabled = false

        try{

            val args = arguments
            customerIdGET = args.getInt("customerIdGet", 0)
            customerNameGET = args.getString("customerNameGet", "")
            customerPhoneGET = args.getString("customerPhoneGet", "")

//            Log.d("check_bug", "customerId payment" + customerIdGET)
//            Log.d("check_bug", "customerName payment" + customerNameGET)
//            Log.d("check_bug", "customerphone payment" + customerPhoneGET)

            val customerDebt = Customer(context).getDebt(customerIdGET, customerPhoneGET, true)
            val customerSaldo = Customer(context).getCustomerSaldo(customerIdGET, customerPhoneGET)

            if (customerIdGET == 0 && customerPhoneGET == "")
            {
                AlertHelper(context).setCustomerPayment(context, mView, fragmentManager)
            }
            else
            {
                customerId.text = customerIdGET.toString()
                customerName.text = customerNameGET
                customerPhone.text = customerPhoneGET
                debt.text = PaymentHelper().amountToReadable(customerDebt)
                balance.text = PaymentHelper().amountToReadable(customerSaldo)

                triggerUpdate = true
            }

            checkChange()

        } catch (e:Exception){ }

        save = mView.findViewById(R.id.save)

        val setCustomer = mView.findViewById<Button>(R.id.setCustomer)

        setCustomer.setOnClickListener {
            AlertHelper(context).setCustomerPayment(context, mView, fragmentManager)
        }

        save.setOnClickListener {
            save()
        }

        paymentMethod = PaymentMethod.initializePaymentMethod()
        paymentMethod.add(PaymentMethod("0", StringWithTag("", "0"), ""))

        adapter = RVPaymentMethodAdapter(context, fragmentManager, recyclerView, paymentMethod, total, kurang, debt, change)
        recyclerView.adapter = adapter

        val layoutManager = LinearLayoutManager(this.context)
        layoutManager.stackFromEnd = false
        recyclerView.layoutManager = layoutManager

        val itemTouchHelperCallback = PaymentRecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        return mView
    }

    fun save()
    {
        val dialog = AlertHelper(context).paymentConfirmation(adapter, paymentMethod)

        dialog.second.setOnClickListener {

            for (item in paymentMethod)
            {
                if (item.amount != "0" || item.amount != "")
                {
                    PaymentToSync(context).insert(
                            User(context).getLoggedInUserId(),
                            User(context).getLoggedInUserStore()!!,
                            customerId.text.toString().toInt(),
                            customerPhone.text.toString(),
                            item.amount.toInt(),
                            item.method.toString()
                    )
                }
            }

            val changeAmount = PaymentHelper().readableAmountToInt(kurang.text.toString())

            val dodol = ArrayList<PaymentMethod>()
            dodol.addAll(paymentMethod)

            if (change.isChecked)
            {
                if (changeAmount > 0)
                {
                    PaymentToSync(context).insert(
                            User(context).getLoggedInUserId(),
                            User(context).getLoggedInUserStore()!!,
                            customerId.text.toString().toInt(),
                            customerPhone.text.toString(),
                            changeAmount,
                            "CHANGE"
                    )

                    dodol.add(PaymentMethod(changeAmount.toString(), StringWithTag("CHANGE", ""), ""))
                }
            }

            AlertHelper(context).printPayment(
                    context,
                    dodol,
                    customerName.text.toString(),
                    customerPhone.text.toString(),
                    dialog.first
            )

            paymentMethod.removeAll(paymentMethod)
            paymentMethod.add(PaymentMethod("0", StringWithTag("", "0"), ""))

            UploadPaymentSynchronizer(context).syncNow()

            dialog.first.hide()
            dialog.first.dismiss()

            val bdl = Bundle(3)
            bdl.putInt("customerIdGet", customerIdGET) // invoice id from invoice_to_sync table or from invoice table
            bdl.putString("customerNameGet", customerNameGET) // invoice id from invoice_to_sync table or from invoice table
            bdl.putString("customerPhoneGet", customerPhoneGET)

            val fragmentTransaction = fragmentManager.beginTransaction()

            val fragment2 = PaymentFragment()
            fragment2.arguments = bdl
            fragmentTransaction.replace(R.id.container, fragment2, "cashier_fragment")
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

            Locker(context).onPaymentToSave(customerPhone.text.toString())

        }

    }


    override fun onPause() {
        super.onPause()
        isFragmentActive = false
    }

    override fun onResume() {
        super.onResume()
        isFragmentActive = true
    }


    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PaymentFragment.
         */

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                PaymentFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
