package com.mockie.daikokuten.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.mockie.daikokuten.models.BarcodeList
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.listeners.QtyEditTextOnKeyListener
import com.mockie.daikokuten.listeners.StockTakingBarcodeEditOnKeyListener


/**
 * Created by mockie on 30/01/18.
 */

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
class RecyclerViewStockTakingAdapter(
        override val mContext: Context,
        override val recyclerView: RecyclerView,
        override var mContacts: MutableList<BarcodeList>,
        override val pageName: String,
        override val keyName: String,
        private val scope: List<String>)
    : RecyclerViewBarcodeAdapter(mContext, recyclerView, mContacts, pageName, keyName) {

    override fun setBarcodeKeyListener(barcodeEditText: EditText, qtyEditText: EditText, descriptionEdittext: TextView)
    {
        barcodeEditText.setOnKeyListener(StockTakingBarcodeEditOnKeyListener(
                mContext,
                this,
                recyclerView,
                barcodeEditText,
                qtyEditText,
                descriptionEdittext,
                mContacts,
                pageName,
                keyName,
                scope))

        qtyEditText.setOnKeyListener(QtyEditTextOnKeyListener(mContext, this, mContacts, recyclerView, qtyEditText, pageName, keyName))
    }

}