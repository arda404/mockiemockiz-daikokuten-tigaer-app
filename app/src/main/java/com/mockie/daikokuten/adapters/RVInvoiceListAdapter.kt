package com.mockie.daikokuten.adapters

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.app.FragmentManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.helpers.InvoiceHelper
import com.mockie.daikokuten.helpers.PaymentHelper
import com.mockie.daikokuten.listeners.InvoiceListLongClick
import com.mockie.daikokuten.models.InvoiceListData


/**
 * Created by mockie on 30/01/18.
 */

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
open class RVInvoiceListAdapter(
        open val mContext: Context,
        val fragmentManager: FragmentManager,
        open val recyclerView: RecyclerView,
        val mContacts: ArrayList<InvoiceListData>) : RecyclerView.Adapter<RVInvoiceListAdapter.ViewHolder>() {

    var lastPosition = -1

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    // Easy access to the context object in the recyclerview
    private fun getContext(): Context {
        return mContext
    }

    open fun setLongClickListener(itemView:View)
    {
        itemView.postDelayed({
            val containerList = itemView.findViewById<ConstraintLayout>(R.id.barcodeToPrintChildParent)
            containerList.setOnLongClickListener(InvoiceListLongClick(mContext, fragmentManager, itemView))
        }, 500)

    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    open inner class ViewHolder// We also create a constructor that accepts the entire item row
    // and does the view lookups to find each subview
    (
            itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        var tempInvoiceId: TextView = itemView.findViewById(R.id.tempInvoiceId)
        var realInvoiceId: TextView = itemView.findViewById(R.id.realInvoiceId)
        var status: TextView = itemView.findViewById(R.id.status)
        var customerName: TextView = itemView.findViewById(R.id.customerName)
        var dateText: TextView = itemView.findViewById(R.id.date)
        var address: TextView = itemView.findViewById(R.id.address)
        var total: TextView = itemView.findViewById(R.id.totalPayment)


        init{
            setLongClickListener(itemView)
        }

        fun clearAnimation() {
            itemView.clearAnimation()
        }

        // to access the context from any ViewHolder instance.
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVInvoiceListAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.invoice_list_child, parent, false)

        // Return a new holder instance
        return ViewHolder(contactView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: RVInvoiceListAdapter.ViewHolder, position: Int) {

        // Get the data model based on position
        val contact = mContacts[position]

//        viewHolder.qtyPositionTextWatcher.updatePosition(position)
        viewHolder.tempInvoiceId.text = contact.tempInvoiceId.toString()
        viewHolder.realInvoiceId.text = contact.realInvoiceId.toString()
        viewHolder.customerName.text = contact.customerName.toString()
        viewHolder.dateText.text = contact.createdDate
        viewHolder.total.text = PaymentHelper().amountToReadable(contact.total.toInt())
        viewHolder.address.text = contact.customerAddressName.toString()
        viewHolder.status.text = InvoiceHelper(getContext()).statusReadable(contact.status)

        setAnimation(viewHolder.itemView, position)
    }

    override fun onViewAttachedToWindow(holder: RVInvoiceListAdapter.ViewHolder) {
        super.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: RVInvoiceListAdapter.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.clearAnimation()
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mContacts.size
    }

    fun removeItem(position: Int) {
//        val id = mContacts[position].id
        //BarcodeToPrintRepository(mContext).removeBarcode(id)

        mContacts.removeAt(position)
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position)
    }

}