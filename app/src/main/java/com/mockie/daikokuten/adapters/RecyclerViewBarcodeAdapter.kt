package com.mockie.daikokuten.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.R
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import com.mockie.daikokuten.watchers.PositionTextWatcher
import android.support.constraint.ConstraintLayout
import android.view.KeyEvent
import android.widget.TextView
import com.mockie.daikokuten.listeners.BarcodeEditTextOnKeyListener
import com.mockie.daikokuten.watchers.MetaPositionWatcher
import com.mockie.daikokuten.watchers.QtyPositionTextWatcher
import android.view.animation.AnimationUtils
import android.view.inputmethod.BaseInputConnection
import com.mockie.daikokuten.listeners.BarcodeFocusChange
import com.mockie.daikokuten.listeners.QtyEditTextOnKeyListener
import com.mockie.daikokuten.listeners.QtyFocusChange

/**
 * Created by mockie on 30/01/18.
 */

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
open class RecyclerViewBarcodeAdapter(
        open val mContext: Context,
        open val recyclerView: RecyclerView,
        open var mContacts: MutableList<BarcodeList>,
        open val pageName: String,
        open val keyName: String
) : RecyclerView.Adapter<RecyclerViewBarcodeAdapter.ViewHolder>() {

    private var lastPosition = -1

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    open fun setBarcodeKeyListener(
            barcodeEditText: EditText, qtyEditText: EditText, descriptionEdittext: TextView
    )
    {
        barcodeEditText.setOnKeyListener(BarcodeEditTextOnKeyListener(
                mContext,
                this,
                recyclerView,
                barcodeEditText,
                qtyEditText,
                descriptionEdittext,
                mContacts,
                pageName,
                keyName))

        qtyEditText.setOnKeyListener(QtyEditTextOnKeyListener(mContext, this, mContacts, recyclerView, qtyEditText, pageName, keyName))
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    open inner class ViewHolder// We also create a constructor that accepts the entire item row
    // and does the view lookups to find each subview
    (
            itemView: View,
            val positionTextWatcher: PositionTextWatcher,
            val qtyPositionTextWatcher: QtyPositionTextWatcher,
            val metaPositionWatcher: MetaPositionWatcher,
            barcodeOnFocusChange: BarcodeFocusChange,
            qtyFocusChange:QtyFocusChange
    ) : RecyclerView.ViewHolder(itemView) {

        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        var barcodeEditText: EditText = itemView.findViewById(R.id.barcodeText0)
        var qtyEditText: EditText = itemView.findViewById(R.id.qtyText0)
        var descriptionEdittext: TextView = itemView.findViewById(R.id.descriptionText0)
        var numberText: TextView = itemView.findViewById(R.id.number)
        var viewForeground: ConstraintLayout = itemView.findViewById(R.id.stockerChildParent)

        init{
            barcodeEditText.addTextChangedListener(positionTextWatcher)
            descriptionEdittext.addTextChangedListener(metaPositionWatcher)

            qtyPositionTextWatcher.setBarcodeEditText(barcodeEditText)
            qtyPositionTextWatcher.setQtyEditText(qtyEditText)
            qtyPositionTextWatcher.setDescriptionEditText(descriptionEdittext)
            qtyEditText.addTextChangedListener(qtyPositionTextWatcher)

            barcodeOnFocusChange.setBarcodeEditText(barcodeEditText)
            barcodeOnFocusChange.setQtyEditText(qtyEditText)
            barcodeOnFocusChange.setDescriptionEditText(descriptionEdittext)

            barcodeEditText.onFocusChangeListener = barcodeOnFocusChange

            qtyFocusChange.setBarcodeEditText(barcodeEditText)
            qtyFocusChange.setQtyEditText(qtyEditText)
            qtyFocusChange.setDescriptionEditText(descriptionEdittext)
            qtyEditText.onFocusChangeListener = qtyFocusChange

            setBarcodeKeyListener(barcodeEditText, qtyEditText, descriptionEdittext)
        }

        fun clearAnimation() {
            itemView.clearAnimation()
        }

        // to access the context from any ViewHolder instance.
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewBarcodeAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.stocker_child, parent, false)

        // Return a new holder instance
        return ViewHolder(
                contactView,
                PositionTextWatcher(context),
                QtyPositionTextWatcher(context),
                MetaPositionWatcher(),
                BarcodeFocusChange(mContext),
                QtyFocusChange(mContext)
        )
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: RecyclerViewBarcodeAdapter.ViewHolder, position: Int) {

        // Get the data model based on position
        val contact = mContacts[position]

        viewHolder.positionTextWatcher.updatePosition(position)
        viewHolder.qtyPositionTextWatcher.updatePosition(position)
        viewHolder.metaPositionWatcher.updatePosition(position)

        viewHolder.barcodeEditText.setText(contact.barcode)
        viewHolder.qtyEditText.setText(contact.qty.toString())
        viewHolder.descriptionEdittext.text = contact.meta
        viewHolder.numberText.text = (position+1).toString()

        setAnimation(viewHolder.itemView, position)

        viewHolder.barcodeEditText.postDelayed({
            viewHolder.barcodeEditText.tag = position
        },50)

        if (contact.focus == "qty")
        {
            viewHolder.qtyEditText.postDelayed({

                viewHolder.qtyEditText.requestFocus()

                val inputConnection = BaseInputConnection(viewHolder.qtyEditText, true)
                inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER))
                inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER))

                contact.focus = null
            }, 100)
        }

        if (contact.focus == "barcode")
        {
            viewHolder.barcodeEditText.postDelayed({

                viewHolder.barcodeEditText.requestFocus()

                val inputConnection = BaseInputConnection(viewHolder.barcodeEditText, true)
                inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER))
                inputConnection.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER))
                contact.focus = null

            }, 100)


        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerViewBarcodeAdapter.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.clearAnimation()
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mContacts.size
    }

    fun removeItem(position: Int) {
       try {
           mContacts.removeAt(position)
           // notify the item removed by position
           // to perform recycler view delete animations
           // NOTE: don't call notifyDataSetChanged()
           notifyItemRemoved(position)
       } catch (e:Exception) {}
    }

}