package com.mockie.daikokuten.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.mockie.daikokuten.models.BarcodeList
import com.mockie.daikokuten.R
import com.mockie.daikokuten.listeners.ReturnBarcodeFocusChange
import com.mockie.daikokuten.listeners.ReturnQtyFocusChange
import com.mockie.daikokuten.models.ItemInBasket
import com.mockie.daikokuten.watchers.*


/**
 * Created by mockie on 30/01/18.
 */

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
class RecyclerViewReturnAdapter(
        override val mContext: Context,
        override val recyclerView: RecyclerView,
        override var mContacts: MutableList<BarcodeList>,
        override val pageName: String,
        override val keyName: String,
        private val itemInBasket:ArrayList<ItemInBasket>,
        private val additionalPrice:Int,
        private val totalRetur:TextView
) : RecyclerViewBarcodeAdapter(mContext, recyclerView, mContacts, pageName, keyName) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewBarcodeAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.stocker_child, parent, false)
        // Return a new holder instance
        return ViewHolder(
                contactView,
                ReturnBarcodeTextWatcher(mContext, itemInBasket),
                QtyReturnPositionTextWatcher(mContext, itemInBasket, additionalPrice, totalRetur, this),
                MetaPositionWatcher(),
                ReturnBarcodeFocusChange(context, itemInBasket),
                ReturnQtyFocusChange(mContext, itemInBasket)
        )
    }
}