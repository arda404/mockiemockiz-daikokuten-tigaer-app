package com.mockie.daikokuten.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.TextView
import com.mockie.daikokuten.R
import com.mockie.daikokuten.database.repositories.BarcodeToPrintRepository
import com.mockie.daikokuten.models.BarcodeToPrintList
import com.mockie.daikokuten.watchers.QtyBarcodeToPrintPositionTextWatcher
import android.widget.CheckBox



/**
 * Created by mockie on 30/01/18.
 */

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
open class RVBarcodeToPrintAdapter(
        open val mContext: Context,
        open val recyclerView: RecyclerView,
        val mContacts: ArrayList<BarcodeToPrintList>) : RecyclerView.Adapter<RVBarcodeToPrintAdapter.ViewHolder>() {

    var lastPosition = -1

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    open inner class ViewHolder// We also create a constructor that accepts the entire item row
    // and does the view lookups to find each subview
    (
            itemView: View,
            val qtyPositionTextWatcher: QtyBarcodeToPrintPositionTextWatcher
    ) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        var barcodeEditText: TextView = itemView.findViewById(R.id.barcodeText0)
        var qtyEditText: EditText = itemView.findViewById(R.id.qtyTextView)
        var descriptionEdittext: TextView = itemView.findViewById(R.id.descriptionText0)
        var numberText: TextView = itemView.findViewById(R.id.number)
        var checkbox: CheckBox = itemView.findViewById(R.id.checkBox)
        var viewForeground: ConstraintLayout = itemView.findViewById(R.id.barcodeToPrintChildParent)

        init{
            qtyEditText.addTextChangedListener(qtyPositionTextWatcher)
        }

        fun clearAnimation() {
            itemView.clearAnimation()
        }

        // to access the context from any ViewHolder instance.
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVBarcodeToPrintAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.barcode_to_print_child, parent, false)

        // Return a new holder instance
        return ViewHolder(contactView, QtyBarcodeToPrintPositionTextWatcher(mContext))
    }

    // Involves populating data into the item through holder
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: RVBarcodeToPrintAdapter.ViewHolder, position: Int) {

        // Get the data model based on position
        val contact = mContacts[position]

        viewHolder.qtyPositionTextWatcher.updatePosition(position)

        viewHolder.barcodeEditText.text = contact.barcode
        viewHolder.qtyEditText.setText(contact.qty.toString())
        viewHolder.descriptionEdittext.text = contact.meta + " | " + contact.size
        viewHolder.numberText.text = (position+1).toString()
        val currentItem = mContacts[position]

        viewHolder.checkbox.setOnClickListener {
            currentItem.checked = viewHolder.checkbox.isChecked
        }

        if (currentItem.checked)
        {
            viewHolder.checkbox.isChecked = true
        }

        setAnimation(viewHolder.itemView, position)
    }

    override fun onViewDetachedFromWindow(holder: RVBarcodeToPrintAdapter.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.clearAnimation()
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mContacts.size
    }

    fun removeItem(position: Int) {
        val id = mContacts[position].id
        BarcodeToPrintRepository(mContext).removeBarcode(id)

        mContacts.removeAt(position)
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position)
    }

}