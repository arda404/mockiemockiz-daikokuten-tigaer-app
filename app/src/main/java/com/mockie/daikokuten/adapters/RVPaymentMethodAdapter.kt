package com.mockie.daikokuten.adapters

import android.content.Context
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.v4.app.FragmentManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import com.mockie.daikokuten.R
import com.mockie.daikokuten.helpers.StringWithTag
import com.mockie.daikokuten.listeners.PaymentMethodOnClickListener
import com.mockie.daikokuten.models.PaymentMethod
import com.mockie.daikokuten.watchers.AmountTextWatcher
import java.util.ArrayList
import com.mockie.daikokuten.R.id.recyclerView
import android.widget.TextView
import kotlinx.android.synthetic.main.payment_method_list_child.view.*


/**
 * Created by mockie on 30/01/18.
 */

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
open class RVPaymentMethodAdapter(
        open val mContext: Context,
        val fragmentManager: FragmentManager,
        open val recyclerView: RecyclerView,
        val mContacts: ArrayList<PaymentMethod>,
        val total:TextView,
        val kurang:TextView,
        val debt:TextView,
        val change:CheckBox) : RecyclerView.Adapter<RVPaymentMethodAdapter.ViewHolder>() {

    var lastPosition = -1

    lateinit var dodol:AmountTextWatcher
    lateinit var dodol2:PaymentMethodOnClickListener

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    // Easy access to the context object in the recyclerview
    private fun getContext(): Context {
        return mContext
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    open inner class ViewHolder// We also create a constructor that accepts the entire item row
    // and does the view lookups to find each subview
    (
            itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        var amount: EditText = itemView.findViewById(R.id.amount)
        var method: Spinner = itemView.findViewById(R.id.method)
        var paymentMethodDesc: TextView = itemView.findViewById(R.id.paymentMethodDesc)
        var readableAmount: TextView = itemView.findViewById(R.id.readableAmount)
        var viewForeground: ConstraintLayout = itemView.findViewById(R.id.paymentMethodParent)
    //   val customerDebt : TextView = itemView.findViewById(R.id.customerDebt)
   //     var cutomerDebt = (recyclerView.findViewHolderForAdapterPosition(R.id.amount).itemView.findViewById(R.id.customerDebt) as TextView).text.toString()
        init {
            amount.setText(debt.text) // get value from customer debt ??
            populateSpinnerPaymentMethod(method)
            dodol = AmountTextWatcher(mContacts, amount, debt, kurang, readableAmount)
            dodol2 = PaymentMethodOnClickListener(this@RVPaymentMethodAdapter, method, mContacts, paymentMethodDesc, debt, kurang, change)
            amount.postDelayed({
                amount.addTextChangedListener(dodol)
            }, 50)

            method.postDelayed({
                method.onItemSelectedListener = dodol2
            }, 50)
        }

        fun clearAnimation() {
            itemView.clearAnimation()
        }

        // to access the context from any ViewHolder instance.
    }

    private fun populateSpinnerPaymentMethod(method: Spinner)
    {
        val types = com.mockie.daikokuten.database.repositories.PaymentMethod(mContext).getAllPaymentMethod()
        val listSpinner = ArrayList<StringWithTag>()

        (0 until types.count()).mapTo(listSpinner) {
            StringWithTag(types[it].name, types[it]._id.toString())
        }

        val adapter = ArrayAdapter(mContext,
                R.layout.spinner_item, listSpinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        method.adapter = adapter
    }

    private fun spinnerMethod(method: Spinner)
    {
        val res = mContext.resources
        val storesAsArray = res.getStringArray(R.array.payment_method)
        val stores = storesAsArray.toMutableList()

        val arrAdapter = ArrayAdapter(mContext, R.layout.spinner_item, stores)
        arrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        method.adapter = arrAdapter
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVPaymentMethodAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.payment_method_list_child, parent, false)
        val method = contactView.findViewById<Spinner>(R.id.method)

        spinnerMethod(method)

        // Return a new holder instance
        return ViewHolder(contactView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: RVPaymentMethodAdapter.ViewHolder, position: Int) {

        // Get the data model based on position
        val contact = PaymentMethod.getItem(position)

        if (contact != null)
        {
            viewHolder.amount.setText(contact.amount)
            viewHolder.amount.postDelayed({
                if (contact.focus == "amount")
                {
                    viewHolder.amount.requestFocus()
                }
            }, 100)
        }

        val handler = Handler()

        handler.postDelayed({
            dodol.updatePosition(viewHolder.adapterPosition)
        }, 100)

        handler.postDelayed({
            dodol2.updatePosition(viewHolder.adapterPosition)
        }, 100)

        setAnimation(viewHolder.itemView, position)
    }

    override fun onViewAttachedToWindow(holder: RVPaymentMethodAdapter.ViewHolder) {
        super.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: RVPaymentMethodAdapter.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.clearAnimation()
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mContacts.size
    }

    fun removeItem(position: Int) {
//        val id = mContacts[position].id
        //BarcodeToPrintRepository(mContext).removeBarcode(id)

        mContacts.removeAt(position)
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position)
    }

}